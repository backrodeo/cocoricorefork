# Exemple #

Construire, lancer et rentrer dans un docker

```console
docker build --rm -t sudouheste .
docker run --rm -ti sudouheste
docker exec -ti CONTAINER_ID(3) /bin/bash
```

Liste des containers lancés (+ supprimer)

```console
docker ps
docker stop CONTAINER_ID(3)
docker rm CONTAINER_ID(3)
```

Lancer un docker branché sur les sources et build

```console
docker run -ti --rm -v /home/luc/__WEB__/sudouheste/docker/dev:/app test
docker ps (pour chopper id)
docker exec -ti 33d /app/script.sh
```

Liste des images

```console
docker images
```

# Docker #

[Docker store](https://store.docker.com/)

[Docker doc](https://docs.docker.com/)

## Docker main command ##

### Run a docker ###

```console
docker run [OPTIONS] IMAGE
```

Some [OPTIONS] :

|                       | ASCII                                             |
----------------------  | --------------------------------------------------- 
| `--name NAME`         | Assign a name to the container id                 |
| `-d`                  | Run container in background                       |
| `-i`                  | Keep STDIN open even                              |
| `-t`                  | Allocate a pseudo-TTY                             |
| `-p HOST:CONTAINER`   | Publish container port to the host                |
| `-v HOST:CONTAINER`   | Bind container directory to the host              |
| `--rm`                | Remove the container when it exits                |
| `-e VAR=VALUE`        | Set environment variables                         |

### Exec command in a docker ###

```console
docker exec [OPTIONS] CONTAINER [COMMAND]
```

For example, to get bash command inside container :

```console
docker exec -ti CONTAINER /bin/bash
```

## Save docker image in file ##

1 - Create new docker image

```console
docker commit <container_id> <image_name>
```

2 - Save image as file

```console
docker save <image_name> > <directory_target>/<filename>.tar
```

## Docker compose ##

Docker compose is a tool for defining and running multi-container Docker applications.

[Video example](https://youtu.be/F9R1EOaA7EA?t=23m43s)

1 - Install :

```console
sudo curl -L https://github.com/docker/compose/releases/download/1.16.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

2 - Create a docker-compose.yml containing configuration.

3 - Call docker compose

```console
docker-compose up
```
