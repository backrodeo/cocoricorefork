var fs = require("fs");
var path = require("path");
const { exec, execSync } = require("child_process")

if (process.argv.length <= 2) {
    console.error("Missing directory");
    return -1;
}

let dir = process.argv[2];
let basename = path.basename(dir);
let vscode = path.join(dir, '.vscode');

if (process.argv.length >= 4) {
    if (process.argv[3] == "-u") {
        copySnippets();
        copyGenerator();
        return 0;
    }
    else {
        console.error("Bad argument : " + process.argv[3]);
        return -1;
    }

}

cleanDirs();
copySnippets();
copyGenerator();
openVsCode(dir, 'README.md');


function cleanDirs() {
    // Directory structure
    execSync(`rm -rf ${dir}`); //deleteFolderRecursive
    fs.mkdirSync(dir);
    fs.mkdirSync(vscode);

    // BOOT.sh
    fs.writeFileSync(
        path.join(dir, 'README.md'),
        "# Ctrl + A \n# F1 terminalrunselected => terminal: run Selected Text in Active Terminal \nnode gen.js boot"
    );
}

function copySnippets() {
    // generate snippets from files
    let snippets = generateSnippets();
    fs.writeFileSync(path.join(vscode, 'cocoriboot.code-snippets'), JSON.stringify(snippets, null, 4));

    // copy *.code-snippets
    copyFiles(__dirname, vscode, 'code-snippets');

    // replace the word MyNamespace
    replaceInFiles(vscode, 'MyNamespace', basename);
}

function copyGenerator() {
    var file = 'gen.js';
    console.log(path.join(dir, file));
    var content = fs.readFileSync(path.join(__dirname, file), 'utf8');
    content = content.replace(new RegExp('MyNamespace', 'g'), basename);
    fs.writeFileSync(path.join(dir, file), content, 'utf8');
}

function openVsCode(dir, file) {
    let openCode = 'code -g "' + dir + '" ' + path.join(dir, file) + ': 1';
    console.log(openCode);
    exec(openCode).unref();
}
/*
copyFolderRecursiveSync(
    path.join(__dirname, '..', '..', 'CocoriCore.Page.GenericFront', 'wwwRoot'),
    path.join(dir, 'CocoriCore.Page.GenericFront', 'wwwRoot'));
*/

function generateSnippets() {
    var foundSnippets = {};
    let files = fs.readdirSync('snippets').filter(x => x.match(/.*\.(cs)/ig));
    files.forEach(file => {
        var content = fs.readFileSync(path.join('snippets', file), 'utf8');
        content = content.replace(new RegExp('_1', 'g'), '$1');
        let snippet = {
            prefix: path.parse(file).name,
            body: content.split('\n')
        };
        console.log('found snippet : ' + snippet.prefix);
        foundSnippets[snippet.prefix] = snippet;
    });
    return foundSnippets;
}


function replaceInFiles(dir, search, replace) {
    let files = fs.readdirSync(dir);
    files.forEach(file => {
        if (fs.lstatSync(path.join(dir, file)).isFile()) {
            var content = fs.readFileSync(path.join(dir, file), 'utf8');
            content = content.replace(new RegExp(search, 'g'), replace);
            fs.writeFileSync(path.join(dir, file), content, 'utf8');
        }
    });
}

function copyFiles(from, to, extension) {
    fs.readdirSync(from)
        .filter(x => x.match(new RegExp(`.*\.(${extension})`, 'ig')))
        .forEach(file => {
            console.log(path.join(to, file));
            fs.copyFileSync(file, path.join(to, file));
        });
}
/*
function copyFolderRecursiveSync(source, target) {
    var files = [];

    //check if folder needs to be created or integrated
    var targetFolder = path.join(target, path.basename(source));
    if (!fs.existsSync(targetFolder)) {
        fs.mkdirSync(targetFolder);
    }

    //copy
    if (fs.lstatSync(source).isDirectory()) {
        files = fs.readdirSync(source);
        files.forEach(function (file) {
            var curSource = path.join(source, file);
            if (fs.lstatSync(curSource).isDirectory()) {
                copyFolderRecursiveSync(curSource, targetFolder);
            } else {
                fs.copyFileSync(curSource, targetFolder);
            }
        });
    }
}

function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path.join(path, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};*/