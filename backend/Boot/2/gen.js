var fs = require("fs");
var path = require("path");
const { exec, execSync } = require("child_process")

let snippets = loadSnippets();

let a0 = process.argv[2];
let a1 = process.argv[3];
if (a0 == 'entite_creer_lister_lire') {
    insertSnippet(snippets, 'entite', `MyNamespace.Application/Entite/${a1}.cs`, [a1]);
    insertSnippet(snippets, 'command_creer', `MyNamespace.Application/Message/Creer${a1}Command.cs`, [a1]);
    insertSnippet(snippets, 'query_lire', `MyNamespace.Application/Message/Lire${a1}Query.cs`, [a1]);
    insertSnippet(snippets, 'query_lister', `MyNamespace.Application/Message/Lister${a1}Query.cs`, [a1]);
    insertSnippet(snippets, 'page_creer', `MyNamespace.Application/Page/Creer${a1}Page.cs`, [a1]);
    insertSnippet(snippets, 'page_lister', `MyNamespace.Application/Page/Lister${a1}Page.cs`, [a1]);
    insertSnippet(snippets, 'page_lire', `MyNamespace.Application/Page/Lire${a1}Page.cs`, [a1]);
    insertSnippet(snippets, 'test_page_creer_lister_lire', `MyNamespace.Application/Test/Test${a1}Page.cs`, [a1]);
}
else if (a0 == 'boot') {
    boot();
}
else if (a0 == 'application') {
    insertSnippet(snippets, 'testbase', `MyNamespace.Application/Test/TestBase.cs`, []);
    insertSnippet(snippets, 'messagebusconfiguration', `MyNamespace.Application/MessageBusConfiguration.cs`, []);
}
else if (a0 == 'webapi') {
    insertSnippet(snippets, 'startup', `MyNamespace.WebApi/Startup.cs`, []);
    insertSnippet(snippets, 'middleware', `MyNamespace.WebApi/ApplicationMiddleware.cs`, []);
    insertSnippet(snippets, 'routerconfiguration', `MyNamespace.WebApi/RouterConfiguration.cs`, []);
}
else {
    console.error('bad generator name');
    return -1;
}


function boot() {
    fs.writeFileSync('.gitignore', '**/bin\n**/obj');
    fs.writeFileSync('.vscode/settings.json', JSON.stringify({ "files.exclude": { "**/bin": true, "**/obj": true } }, null, 4));
    execSyncLog('dotnet new sln');
    execSyncLog('dotnet new web -o MyNamespace.WebApi');
    execSyncLog('dotnet new xunit -o MyNamespace.Application -f netcoreapp3.0');
    execSyncLog('dotnet sln add MyNamespace.WebApi');
    execSyncLog('dotnet sln add MyNamespace.Application');

    execSyncLog('dotnet add package CocoriCoreForkAll', { cwd: 'MyNamespace.Application' });
    execSyncLog('dotnet add package Autofac', { cwd: 'MyNamespace.Application' });
    execSyncLog('dotnet add package FluentAssertions', { cwd: 'MyNamespace.Application' });


    execSyncLog('dotnet add reference ../MyNamespace.Application', { cwd: 'MyNamespace.WebApi' });
    execSyncLog('dotnet add package CocoriCoreForkAll', { cwd: 'MyNamespace.WebApi' });
    execSyncLog('dotnet add package Autofac', { cwd: 'MyNamespace.WebApi' });
    execSyncLog('dotnet add package Automapper', { cwd: 'MyNamespace.WebApi' });

    execSyncLog('dotnet restore');

    fs.unlinkSync(path.join('MyNamespace.Application', 'UnitTest1.cs'));

    fs.writeFileSync(
        path.join('MyNamespace.Application', 'Class1.cs'),
        'using System.Reflection;namespace MyNamespace.Application{public static class AssemblyInfo { public static Assembly Assembly => typeof(AssemblyInfo).Assembly; } }'
    );

    fs.mkdirSync(path.join('MyNamespace.Application', 'Entite'));
    fs.mkdirSync(path.join('MyNamespace.Application', 'Message'));
    fs.mkdirSync(path.join('MyNamespace.Application', 'Test'));
    fs.mkdirSync(path.join('MyNamespace.Application', 'Page'));

    fs.writeFileSync(path.join('MyNamespace.Application', 'Entite', '_.cs'), '// TODO : entite');
    fs.writeFileSync(path.join('MyNamespace.Application', 'Message', '_.cs'), '// TODO : command/query');
    fs.writeFileSync(path.join('MyNamespace.Application', 'Page', '_.cs'), '// TODO : page');
    fs.writeFileSync(path.join('MyNamespace.Application', 'Test', '_.cs'), '// TODO : test');
    fs.writeFileSync(path.join('MyNamespace.Application', 'Test', 'TestBase.cs'), '// TODO : testbase');


    fs.writeFileSync(path.join('MyNamespace.WebApi', 'ApplicationMiddleware.cs'), '// TODO : middleware');
    fs.writeFileSync(path.join('MyNamespace.WebApi', 'RouterConfiguration.cs'), '// TODO : routerconfiguration');
    fs.writeFileSync(path.join('MyNamespace.WebApi', 'Startup.cs'), '// TODO : startup\nclass Startup { }');

    console.log('DONE');
}

/*
 * Functions
 */

function execSyncLog(cmd, options) {
    console.log(cmd);
    execSync(cmd, options);
}

function loadSnippets() {
    var foundSnippets = [];
    let files = fs.readdirSync('.vscode').filter(x => x.match(/.*\.(code-snippets)/ig));
    files.forEach(function (file) {
        var content = fs.readFileSync('.vscode/' + file, 'utf8');
        let obj = JSON.parse(content);//RJSON.transform(content));
        for (let key in obj) {
            //console.log('snippet :' + key);
            foundSnippets[key] = obj[key];
        }
    });
    return foundSnippets;
}

function insertSnippet(snippets, snippetName, file, args) {
    console.log('insertSnippet ' + snippetName + " in file " + file);
    let s = snippets[snippetName];
    if (s == undefined) {
        throw Error('snippet not found : ' + snippetName);
    }
    let body = snippets[snippetName].body.join('\n');
    for (let i = 0; i < args.length; i++)
        body = body.replace(new RegExp('\\$' + (i + 1), 'g'), args[i]);
    fs.writeFileSync(file, body);
}