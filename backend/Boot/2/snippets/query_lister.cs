using System;
using System.Threading.Tasks;
using AutoMapper;
using CocoriCore;
using CocoriCore.Linq.Async;

namespace MyNamespace.Application
{
    public class Lister_1Query : IMessage<Lister_1ResponseItem[]>
    {
    }

    public class Lister_1ResponseItem
    {
        public Guid Id;
    }

    public class Lister_1Handler : MessageHandler<Lister_1Query, Lister_1ResponseItem[]>
    {
        private readonly IRepository repository;
        private readonly IMapper mapper;

        public Lister_1Handler(IRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public override async Task<Lister_1ResponseItem[]> ExecuteAsync(Lister_1Query message)
        {
            var liste_1 = await repository.Query<_1>().ToArrayAsync();
            var reponse = mapper.Map<Lister_1ResponseItem[]>(liste_1);
            return reponse;
        }
    }

    public class Lister_1QueryProfile : Profile
    {
        public Lister_1QueryProfile()
        {
            CreateMap<_1, Lire_1Query>();
            CreateMap<Lister_1ResponseItem, _1>();
        }
    }
}