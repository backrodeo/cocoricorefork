using CocoriCore;
using CocoriCore.Router;
using CocoriCore.Page;
using MyNamespace.Application;

namespace MyNamespace.WebApi
{
    public class RouterConfiguration
    {
        public static void Configure(RouterOptionsBuilder builder)
        {
            builder.Post<GenericMessage>().SetPath("pages/call").UseBody();
            //builder.Get<AccueilPageQuery>().SetPath("pages/accueil");
            //builder.Get<MyMessage>().SetPath(x => $"api/mymessage/{x.Id}");
        }
    }
}