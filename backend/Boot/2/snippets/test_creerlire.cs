using System;
using System.Threading.Tasks;
using CocoriCore;
using Xunit;
using FluentAssertions;

namespace MyNamespace.Application
{
    public class CreerLire_1Test : TestBase
    {
        [Fact]
        public async Task Test()
        {
            var command = new Creer_1Command();
            var id = await this.ExecuteAsync(command);
            var query = new Lire_1Query() { Id = id };
            var response = await this.ExecuteAsync(query);
            response.Should().NotBeNull();
        }
    }
}