using System;
using System.Threading.Tasks;
using CocoriCore;
using CocoriCore.Page;

namespace MyNamespace.Application
{
    public class Creer_1PageQuery : IPageQuery<Creer_1Page>
    {

    }

    public class Creer_1Page : PageBase<Creer_1PageQuery>
    {
        public Form<Creer_1Command, Lire_1PageQuery> Creer_1;
    }

    public class Creer_1PageModule : PageModule
    {
        public Creer_1PageModule()
        {
            HandlePage<Creer_1PageQuery, Creer_1Page>();
            ForMessage<Creer_1Command>()
                .WithResponse<Guid>()
                .BuildModel<Lire_1PageQuery>((c, r, m) => { m.Id = r; });
        }

    }
}