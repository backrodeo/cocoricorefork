using System;
using System.Threading.Tasks;
using CocoriCore;
using CocoriCore.Page;
using System.Linq;

namespace MyNamespace.Application
{
    public class Lister_1PageQuery : IPageQuery<Lister_1Page>
    {
    }

    public class Lister_1Page : PageBase<Lister_1PageQuery>
    {
        public OnInitCall<Lister_1Query, Lister_1PageItem[]> Lister_1;
    }


    public class Lister_1PageItem
    {
        public Lire_1PageQuery LienLire;
    }

    public class Lister_1PageModule : PageModule
    {
        public Lister_1PageModule()
        {
            HandlePage<Lister_1PageQuery, Lister_1Page>();
            ForMessage<Lister_1Query>()
                .WithResponse<Lister_1ResponseItem[]>()
                .BuildModel<Lister_1PageItem[]>((c, r) =>
                    r.Select(x => new Lister_1PageItem() { LienLire = { Id = x.Id } }).ToArray());
        }
    }
}