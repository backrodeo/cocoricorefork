using System.Reflection;
using CocoriCore;

namespace MyNamespace.Application
{
    public static class MessageBusConfiguration
    {
        private static Assembly applicationAssembly = MyNamespace.Application.AssemblyInfo.Assembly;

        public static void Configure(MessageBusOptionsBuilder builder)
        {
            builder
                .ForAll<IMessage>(applicationAssembly)
                //.CallValidators(applicationAssembly)
                //.CallHandlerValidators(cocoriCoreApplicationAssembly, applicationAssembly)
                .CallMessageHandlers(applicationAssembly)
                //.Call<IDerivationBus>((b, c) => b.ApplyRulesAsync())
                //.Call<ITransactionHolder>((t, c) => t.CommitAsync())
                ;
        }
    }
}