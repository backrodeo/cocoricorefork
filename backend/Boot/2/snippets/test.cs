using System;
using System.Threading.Tasks;
using CocoriCore;
using Xunit;
using FluentAssertions;

namespace MyNamespace.Application
{
    public class _1Test : TestBase
    {
        [Fact]
        public async Task Test()
        {
            var message = new _1();
            var response = await this.ExecuteAsync(message);
            response.Should().NotBeNull();
        }
    }
}