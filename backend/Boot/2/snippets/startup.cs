using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using CocoriCore;
using CocoriCore.Router;
using CocoriCore.Autofac;
using CocoriCore.Autofac.All;
using Autofac;
using Autofac.Features.ResolveAnything;
using MyNamespace.Application;

namespace MyNamespace.WebApi
{
    public class Startup
    {
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var container = CreateContainer();
            var unitOfWorkFactory = container.Resolve<IUnitOfWorkFactory>();

            app.Use(async (ctx, next) =>
            {
                if (ctx.Request.Path.Value.StartsWith("/api"))
                {
                    using (var unitOfWork = unitOfWorkFactory.NewUnitOfWork())
                    {
                        await unitOfWork.Resolve<ApplicationMiddleware>().InvokeAsync(ctx, next);
                    }
                }
            });
        }
        public static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());
            builder.RegisterModule(new CocoricoreAutofacModule());
            builder.RegisterModule(new CocoriCoreAutofacApiModule()
            {
                RouterConfiguration = RouterConfiguration.Configure,
                HttpResponseWriterConfiguration = HttpResponseWriterConfiguration.Configure,
                HttpErrorWriterConfiguration = HttpErrorWriterConfiguration.Configure,
                ErrorBusConfiguration = ErrorBusConfiguration.Configure
            });
            builder.RegisterModule(new CocoriCoreAutofacApplicationModule()
            {
                MessageBusConfiguration = MessageBusConfiguration.Configure
            });
            builder.RegisterModule(new CocoriCoreAutofacRepositoryModule());
            builder.RegisterModule(new CocoriCoreAutoMapperModule(MyNamespace.Application.AssemblyInfo.Assembly));

            return builder.Build();
        }
    }

    public class HttpResponseWriterConfiguration
    {
        public static void Configure(HttpResponseWriterOptionsBuilder builder)
        {
            builder.For<FileResponse>().Call<HttpResponseWriterFileHandler>();
            builder.For<object>().Call<HttpResponseWriterDefaultHandler>();
        }
    }

    public class HttpErrorWriterConfiguration
    {

        public static void Configure(HttpErrorWriterOptionsBuilder builder)
        {
            builder.SetDebugMode(() => true);
            //builder.For<DeserializeMessageException>().Call<DeserializationExceptionWriter>();
            builder.For<RouteNotFoundException>().Call<RouteNotFoundExceptionWriter>();
            builder.For<AuthenticationException>().Call<AuthenticationExceptionWriter>();
            builder.For<InvalidTokenException>().Call<InvalidTokenExceptionWriter>();
            builder.For<Exception>().Call<DefaultExceptionWriter>();
        }
    }

    public class ErrorBusConfiguration
    {
        public static void Configure(ErrorBusOptionsBuilder builder)
        {
            builder.For<Exception>().Call<ConsoleLogger>((l, e) => l.LogAsync(e));

            builder.SetUnexpectedExceptionHandler(async (ue, e) =>
            {
                var logger = new ConsoleLogger();
                await logger.LogAsync(ue);
                await logger.LogAsync(e);
            });
        }
    }

}