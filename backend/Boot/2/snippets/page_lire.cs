using System;
using System.Threading.Tasks;
using CocoriCore;
using CocoriCore.Page;

namespace MyNamespace.Application
{
    public class Lire_1PageQuery : IPageQuery<Lire_1Page>
    {
        public Guid Id;
    }

    public class Lire_1Page : PageBase<Lire_1PageQuery>
    {
        public OnInitCall<Lire_1Query, Lire_1Response> _1;
    }

    public class Lire_1PageModule : PageModule
    {
        public Lire_1PageModule()
        {
            HandlePage<Lire_1PageQuery, Lire_1Page>();
        }
    }
}