using System;
using System.Threading.Tasks;
using AutoMapper;
using CocoriCore;

namespace MyNamespace.Application
{
    public class Lire_1Query : IMessage<Lire_1Response>
    {
        public Guid Id;
    }

    public class Lire_1Response
    {
    }

    public class Lire_1Handler : MessageHandler<Lire_1Query, Lire_1Response>
    {
        private readonly IRepository repository;
        private readonly IMapper mapper;

        public Lire_1Handler(IRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        public override async Task<Lire_1Response> ExecuteAsync(Lire_1Query message)
        {
            var entite = await repository.LoadAsync<_1>(message.Id);
            var reponse = mapper.Map<Lire_1Response>(entite);
            return reponse;
        }
    }

    public class Lire_1QueryProfile : Profile
    {
        public Lire_1QueryProfile()
        {
            CreateMap<_1, Lire_1Query>();
            CreateMap<Lire_1Response, _1>();
        }
    }
}