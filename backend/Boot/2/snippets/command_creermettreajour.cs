using System;
using System.Threading.Tasks;
using AutoMapper;
using CocoriCore;

namespace MyNamespace.Application
{
    public class CreerMettreAJour_1Command : IMessage<Guid>
    {
        public Guid Id;
    }

    public class Creer_1Handler : MessageHandler<CreerMettreAJour_1Command, Guid>
    {
        private readonly IRepository repository;
        private readonly IMapper mapper;

        public _1Handler(IRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public override async Task<Guid> ExecuteAsync(CreerMettreAJour_1Command command)
        {
            _1 entite;
            if (command.Id != Guid.Empty)
            {
                entite = mapper.Map<_1>(command);
                await repository.InsertAsync(entite);
            }
            else
            {
                entite = await repository.LoadAsync(command.Id);
                entite = mapper.Map<_1>(command);
                await repository.UpdateAsync(entite);
            }
            return entite.Id;
        }
    }
}