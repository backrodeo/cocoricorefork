using System;
using System.Threading.Tasks;
using AutoMapper;
using CocoriCore;

namespace MyNamespace.Application
{
    public class Creer_1Command : IMessage<Guid>
    {
    }

    public class Creer_1Handler : MessageHandler<Creer_1Command, Guid>
    {
        private readonly IRepository repository;
        private readonly IMapper mapper;

        public Creer_1Handler(IRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        public override async Task<Guid> ExecuteAsync(Creer_1Command command)
        {
            var entite = new _1();
            entite = mapper.Map<_1>(command);
            await repository.InsertAsync(entite);
            return entite.Id;
        }
    }

    public class Creer_1CommandProfile : Profile
    {
        public Creer_1CommandProfile()
        {
            CreateMap<Creer_1Command, _1>();
        }
    }
}