using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Features.ResolveAnything;
using CocoriCore;
using CocoriCore.Autofac;
using CocoriCore.Autofac.All;
using CocoriCore.Linq.Async;

namespace MyNamespace.Application
{
    public class TestBase
    {
        private IContainer container;

        public TestBase()
        {
            container = CreateContainer();
        }

        public IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());
            builder.RegisterModule(new CocoricoreAutofacModule());
            builder.RegisterModule(new CocoriCoreAutofacApplicationModule()
            {
                MessageBusConfiguration = MessageBusConfiguration.Configure
            });
            builder.RegisterModule(new CocoriCoreAutofacRepositoryModule());
            builder.RegisterModule(new CocoriCoreAutoMapperModule(
                MyNamespace.Application.AssemblyInfo.Assembly
            ));
            builder.RegisterModule(new CocoriCore.Page.Autofac.CocoricorePageAutofacModule(
                new CocoriCore.Page.PageMapperOptions(MyNamespace.Application.AssemblyInfo.Assembly),
                new CocoriCore.Page.EmailMapperOptions(MyNamespace.Application.AssemblyInfo.Assembly)
            ));

            // Add registrations here

            return builder.Build();
        }

        protected async Task<T> ExecuteAsync<T>(IMessage<T> message)
        {
            var unitOfWorkFactory = container.Resolve<IUnitOfWorkFactory>();
            using (var unitOfWork = unitOfWorkFactory.NewUnitOfWork())
            {
                var messagebus = unitOfWork.Resolve<IMessageBus>();
                var response = await messagebus.ExecuteAsync(message);
                return (T)response;
            }
        }

        protected async Task<List<T>> RepoQueryToListAsync<T>(Func<IQueryable<T>, IQueryable<T>> funcQueryable) where T : class, IEntity
        {
            var unitOfWorkFactory = container.Resolve<IUnitOfWorkFactory>();
            using (var unitOfWork = unitOfWorkFactory.NewUnitOfWork())
            {
                var repository = unitOfWork.Resolve<IRepository>();
                var queryable = funcQueryable(repository.Query<T>());
                return await queryable.ToListAsync();
            }
        }

    }
}