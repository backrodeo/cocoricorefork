#!/bin/sh
if [ $# -eq 0 ] ; then
    echo 'missing directory'
    exit 0
fi

base=$(basename $1)
cp *.code-snippets $1/.vscode
sed -i "s/MyNamespace/$base/g" $1/.vscode/*.code-snippets