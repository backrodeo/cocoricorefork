#!/bin/sh
if [ $# -eq 0 ] ; then
    echo 'missing directory'
    exit 0
fi

base=$(basename $1)

rm -rf $1
mkdir $1
mkdir $1/.vscode
cp *.code-snippets $1/.vscode
echo \# Boot your project with the snippet : boot > $1/BOOT.sh
code $1 -g $1/BOOT.sh:3
sed -i "s/MyNamespace/$base/g" $1/.vscode/*.code-snippets