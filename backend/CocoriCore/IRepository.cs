using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRepository : IDisposable
    {
        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task<bool> ExistsAsync(Type entityType, Guid id);

        Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value);

        Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection);

        Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value) where TEntity : class, IEntity;

        Task<object> LoadAsync(Type type, Guid id);//TODO retourner une IEntity ici

        Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids);

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;

        IQueryable<IEntity> Query(Type type);
    }
}