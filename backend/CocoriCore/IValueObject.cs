namespace CocoriCore
{
    public interface IValueObject<T> : IValueObject
    {
        new T Value { get; }
    }

    public interface IValueObject
    {
        object Value { get; }
        string ValidationRegex { get; }//TODO séparer la regex de l'object valeur plutôt ?
        bool HasValue { get; }
    }
}