using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class TransactionDecorator : IRepository, ITransactionHolder, IBufferedRepository
    {
        private IRepository _repository;
        private IStateStore _stateStore;
        private List<(CUDOperation operation, IEntity entity)> _transactionOperations;
        private List<(CUDOperation operation, IEntity entity)> _flushOperations;
        private Dictionary<object, object> _cache;
        private bool _buffered;

        public TransactionDecorator(IRepository repository, IStateStore stateStore, bool buffered = false)
        {
            _repository = repository;
            _stateStore = stateStore;
            _transactionOperations = new List<(CUDOperation, IEntity)>();
            _flushOperations = new List<(CUDOperation, IEntity)>();
            _cache = new Dictionary<object, object>();
            _buffered = buffered;
        }

        public async Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_buffered)
            {
                _flushOperations.Add((CUDOperation.CREATE, entity));
            }
            else
            {
                await _repository.InsertAsync(entity);
            }
            _transactionOperations.Add((CUDOperation.CREATE, entity));
        }

        public async Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_buffered)
            {
                _flushOperations.Add((CUDOperation.UPDATE, entity));
            }
            else
            {
                await _repository.UpdateAsync(entity);
            }
            _transactionOperations.Add((CUDOperation.UPDATE, entity));
        }

        public async Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (_buffered)
            {
                _flushOperations.Add((CUDOperation.DELETE, entity));
            }
            else
            {
                await _repository.DeleteAsync(entity);
            }
            _transactionOperations.Add((CUDOperation.DELETE, entity));
        }

        public Task<bool> ExistsAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return _repository.ExistsAsync<TEntity>(id);
        }

        public Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return _repository.ExistsAsync(entityType, id);
        }

        public Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            return _repository.ExistsAsync(entityType, member, value);
        }

        public Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            return _repository.ExistsAsync(entityType, collection);
        }

        public async Task<TEntity> LoadAsync<TEntity>(Guid id)
           where TEntity : class, IEntity
        {
            return (TEntity)await LoadAsync(typeof(TEntity), id);
        }

        public async Task<object> LoadAsync(Type type, Guid id)
        {
            var entity = await _repository.LoadAsync(type, id);
            return _stateStore.SaveStateAndCreateCopy(entity);
        }

        public async Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
            where TEntity : class, IEntity
        {
            var entity = await _repository.LoadAsync(uniqueMember, value);
            return (TEntity)_stateStore.SaveStateAndCreateCopy(entity);
        }

        public async Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var results = await _repository.LoadAsync(type, ids);
            return results.Select(x => _stateStore.SaveStateAndCreateCopy(x)).ToArray();
        }

        public IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            var queryable = _repository.Query<TEntity>();
            return new ExtendedEnumerable<TEntity>(queryable, e =>
            {
                return (TEntity)_stateStore.SaveStateAndCreateCopy(e);
            });
        }

        public IQueryable<IEntity> Query(Type type)
        {
            var queryable = _repository.Query(type);
            return new ExtendedEnumerable<IEntity>(queryable, e =>
            {
                return (IEntity)_stateStore.SaveStateAndCreateCopy(e);
            });
        }

        public async Task CommitAsync()
        {
            await FlushAsync();
            _stateStore.Clear();
            _transactionOperations.Clear();
        }

        public async Task RollbackAsync()
        {
            await RollbackDeletesAsync();
            await RollbackUpdatesAsync();
            await RollbackInsertsAsync();
            _stateStore.Clear();
            _transactionOperations.Clear();
        }

        private async Task RollbackDeletesAsync()
        {
            var operations = _transactionOperations.Where(x => x.Item1 == CUDOperation.DELETE);
            foreach (var operation in operations)
            {
                var entity = operation.Item2;
                if (!await _repository.ExistsAsync(entity.GetType(), entity.Id))
                {
                    await _repository.InsertAsync(entity);
                }
            }
        }

        private async Task RollbackUpdatesAsync()
        {
            foreach (var state in _stateStore)
            {
                await _repository.UpdateAsync((IEntity)state.PreviousEntity);
            }
        }

        private async Task RollbackInsertsAsync()
        {
            var operations = _transactionOperations.Where(x => x.Item1 == CUDOperation.CREATE);
            foreach (var operation in operations)
            {
                var entity = operation.Item2;
                var entityState = _stateStore.GetState(entity);
                if (await _repository.ExistsAsync(entity.GetType(), entity.Id))
                {
                    await _repository.DeleteAsync(entity);
                }
            }
        }

        public async Task FlushAsync()
        {
            foreach (var element in _flushOperations)
            {
                if (element.operation == CUDOperation.CREATE)
                {
                    await _repository.InsertAsync(element.entity);
                }
                else if (element.operation == CUDOperation.UPDATE)
                {
                    await _repository.UpdateAsync(element.entity);
                }
                else if (element.operation == CUDOperation.DELETE)
                {
                    await _repository.DeleteAsync(element.entity);
                }
                else
                {
                    throw new NotSupportedException($"Operation not supported : {element.operation}.");
                }
            }
            _flushOperations.Clear();
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}