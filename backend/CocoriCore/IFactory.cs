﻿using System;

namespace CocoriCore
{
    public interface IFactory
    {
        T Create<T>();
        object Create(Type type);
    }
}