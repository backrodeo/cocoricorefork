using System;

namespace CocoriCore
{
    public class ResetPasswordClaims : IClaims
    {
        public Guid Id;
        public DateTime ExpireAt { get; set; }
    }
}