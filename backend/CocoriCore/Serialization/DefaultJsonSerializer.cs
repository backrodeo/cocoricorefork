using Newtonsoft.Json;
using System;

namespace CocoriCore
{
    public class DefaultJsonSerializer : JsonSerializer
    {
        private IFormats _formats;

        public DefaultJsonSerializer(IFormats formats)
        {
            _formats = formats;
            DateFormatString = _formats.Get<DateTime>();
            DateTimeZoneHandling = _formats.DateTimeInUTC ? DateTimeZoneHandling.Utc : DateTimeZoneHandling.Local;
            Converters.Add(new ValueObjectConverter());
            //Converters.Add(new DynamicObjectConverter());
            Converters.Add(new TimeSpanConverter(_formats));
            Converters.Add(new DateTimeConverter(_formats));
            ContractResolver = new NoNullEnumerableResolver();
            DateParseHandling = DateParseHandling.None;
        }
    }
}