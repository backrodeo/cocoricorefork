﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace CocoriCore
{
    public class TimeSpanConverter : JsonConverter
    {
        private IFormats _formats;

        public TimeSpanConverter(IFormats formats)
        {
            _formats = formats;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(TimeSpan);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String)
            {
                var value = reader.Value;
                try
                {
                    return TimeSpan.ParseExact((string)value, _formats.Get<TimeSpan>(), CultureInfo.InvariantCulture);
                }
                catch (Exception e)
                {
                    var lineInfo = reader as IJsonLineInfo;
                    throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                        + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
                }
            }
            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((TimeSpan)value).ToString(_formats.Get<TimeSpan>()));
        }
    }
}
