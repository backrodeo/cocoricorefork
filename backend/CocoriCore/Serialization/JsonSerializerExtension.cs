﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace CocoriCore
{
    public static class JsonSerializerExtension
    {
        public static JToken Parse(this JsonSerializer serializer, string value)
        {
            return serializer.Parse<JToken>(value);
        }

        public static T Parse<T>(this JsonSerializer serializer, string value)
        {
            if (!value.StartsWith("[") && !value.StartsWith("\"") && !value.StartsWith("{"))
                value = $"\"{value}\"";
            return serializer.Deserialize<T>(new JsonTextReader(new StringReader(value)));
        }

        public static string Serialize(this JsonSerializer serializer, object obj)
        {
            using (var sw = new StringWriter())
            {
                serializer.Serialize(sw, obj);
                return sw.ToString();
            }
        }

        public static T Deserialize<T>(this JsonSerializer serializer, string json)
        {
            using (var sr = new StringReader(json))
            using (var jtr = new JsonTextReader(sr))
            {
                return serializer.Deserialize<T>(jtr);
            }
        }
    }
}
