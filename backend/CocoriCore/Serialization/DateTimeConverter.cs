﻿using Newtonsoft.Json;
using System;
using System.Globalization;

namespace CocoriCore
{
    public class DateTimeConverter : JsonConverter//TODO factoriser code avec TimeSpanConverter
    {
        private IFormats _formats;

        public DateTimeConverter(IFormats formats)
        {
            _formats = formats;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.String/* || reader.TokenType == JsonToken.Date*/)
            {
                var value = reader.Value;
                try
                {
                    var dateTime = DateTime.ParseExact((string)value, _formats.Get<DateTime>(), CultureInfo.InvariantCulture);
                    if (_formats.DateTimeInUTC)
                        dateTime = dateTime.ToUniversalTime();
                    return dateTime;
                }
                catch (Exception e)
                {
                    var lineInfo = reader as IJsonLineInfo;
                    throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                        + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
                }
            }
            return existingValue;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((DateTime)value).ToString(_formats.Get<DateTime>()));
        }
    }
}
