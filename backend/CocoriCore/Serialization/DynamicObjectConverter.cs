using System;
using Newtonsoft.Json.Converters;

namespace CocoriCore
{
    public class DynamicObjectConverter : ExpandoObjectConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(object);
        }
    }
}