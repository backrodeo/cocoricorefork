using System;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class ValueObjectConverter : JsonConverter
    {
        public ValueObjectConverter()
        {
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.GetInterface(typeof(IValueObject).Name) != null;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            return Activator.CreateInstance(objectType, new object[] {reader.Value});
        }
        
        public override void WriteJson(JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
            IValueObject objetValeur = (IValueObject)value;
            serializer.Serialize(writer, objetValeur?.Value);
        }
    }
    
}