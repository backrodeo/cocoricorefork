﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore
{
    //TODO au propre et découpler camelCase
    public class NoNullEnumerableResolver : CamelCasePropertyNamesContractResolver
    {
        protected override JsonContract CreateContract(Type objectType)//TODO ne faire que pour les objets d'un namespace pour restreindre
        {
            var contractResolver = base.CreateContract(objectType);
            contractResolver.OnDeserializedCallbacks.Add((o, c) =>
            {
                var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
                foreach (var fieldInfo in o.GetType().GetFields(bindingFlags))//TODO optimiser on passe partout y compris dans les champs des collections
                {
                    var fieldType = fieldInfo.FieldType;
                    if (typeof(IEnumerable).IsAssignableFrom(fieldType))
                    {
                        var fieldValue = fieldInfo.GetValue(o);
                        fieldValue = GetEmptyCollectionIfNull(fieldType, fieldValue);
                        fieldInfo.SetValue(o, fieldValue);
                    }
                }

                foreach (var propertyInfo in o.GetType().GetProperties(bindingFlags))
                {
                    var propertyType = propertyInfo.PropertyType;
                    if (typeof(IEnumerable).IsAssignableFrom(propertyType) && !propertyInfo.GetGetMethod().IsSpecialName)
                    {
                        var propertyValue = propertyInfo.GetValue(o);
                        propertyValue = GetEmptyCollectionIfNull(propertyType, propertyValue);
                        propertyInfo.SetValue(o, propertyValue);
                    }
                }
            });
            return contractResolver;
        }

        public object GetEmptyCollectionIfNull(Type objectType, object existingValue)
        {
            if (existingValue == null)
            {
                if (objectType.IsArray)
                    return Array.CreateInstance(objectType.GetElementType(), 0);
                if (objectType.IsConcrete())
                {
                    if (objectType.GetConstructors().Any(x => x.GetParameters().Length == 0))
                        return Activator.CreateInstance(objectType);
                    else
                        return existingValue;
                }
                if (objectType.GetGenericTypeDefinition().GetTypeHierarchy().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IList<>)))
                    return Activator.CreateInstance(typeof(List<>).MakeGenericType(objectType.GetGenericArguments()[0]));
                if (objectType.GetGenericTypeDefinition().GetTypeHierarchy().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))//TODO factoriser ce code là et rechercher les autres appels à GetGenericTypeDefinition()
                    return Array.CreateInstance(objectType.GetGenericArguments()[0], 0);
            }
            return existingValue;
        }
    }
}
