using System;

namespace CocoriCore
{
    //TODO sert à quoi ?
    public abstract class ValueObjectNullable<T> : IValueObject<T>
          where T : struct, IComparable
    {
        protected T? _value;

        protected ValueObjectNullable()
        {
        }

        public ValueObjectNullable(T? value)
        {
            _value = value;
        }

        public T Value => _value.Value;

        public virtual string ValidationRegex => ".*";

        object IValueObject.Value => _value.Value;

        public bool HasValue => _value != null;

        public static bool operator ==(ValueObjectNullable<T> objet1, ValueObjectNullable<T> objet2)
        {
            if (objet1 as object == null && objet2 as object == null)
                return true;
            else if (objet1 as object != null && objet2 as object == null)
                return false;
            else if (objet1 as object == null && objet2 as object != null)
                return false;
            return Equals(objet1._value, objet2._value);
        }

        public static bool operator !=(ValueObjectNullable<T> objet1, ValueObjectNullable<T> objet2)
        {
            return !(objet1 == objet2);
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override bool Equals(object objet)
        {
            return objet is ValueObjectNullable<T> && Equals(((ValueObjectNullable<T>)objet)._value, _value);
        }

        public override int GetHashCode()
        {
            return 13 ^ _value.GetHashCode();
        }

        public virtual int CompareTo(object obj)
        {
            if (obj == null)
                return 1;
            if (obj is ValueObjectNullable<T>)
                return _value.Value.CompareTo(((ValueObjectNullable<T>)obj)._value);
            else
                throw new ArgumentException($"Object in parameter must be of type {typeof(ValueObjectNullable<T>)}.");
        }
    }
}
