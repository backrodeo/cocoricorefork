using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ryadel.Components.Security;

namespace CocoriCore
{
    public class PasswordService : IPasswordService
    {
        private IHashService _hashService;
        private Regex _lowercase;
        private Regex _uppercase;
        private Regex _special;
        private Regex _number;
        private int _minimumLength;

        public PasswordService(IHashService hashService)
        {
            _hashService = hashService;
            _lowercase = new Regex("[a-z]");
            _uppercase = new Regex("[A-Z]");
            _special = new Regex(@"[ !""#$%&'()*+,\-.\/\\:;<=>?@\[\]^_`{|}~]");
            _number = new Regex("\\d");
            _minimumLength = 12;
        }

        public virtual Task<string> GeneratePasswordAsync()
        {
            return Task.FromResult(PasswordGenerator.Generate(12, 4, true, true, true, true));
        }

        public virtual async Task<bool> CheckPasswordAsync(string password, string correctHash)
        {
            return await _hashService.PasswordMatchHashAsync(password, correctHash);
        }

        public virtual Task<string> HashPasswordAsync(string password)
        {
            return _hashService.HashAsync(password);
        }

        public Task<bool> PasswordIsWeakAsync(string password)
        {
            var isWeak = string.IsNullOrEmpty(password) ||
                password.Length < _minimumLength ||
                !_lowercase.IsMatch(password) ||
                !_uppercase.IsMatch(password) ||
                !_special.IsMatch(password) ||
                !_number.IsMatch(password);
            return Task.FromResult(isWeak);
        }
    }
}