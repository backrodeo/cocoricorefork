using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IHashService
    {
        Task<string> HashAsync(string password);
        Task<bool> PasswordMatchHashAsync(string password, string correctHash);
    }
}