using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HashService : IHashService
    {
        public Task<string> HashAsync(string password)
        {
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }
            var hash = PasswordHash.PasswordHash.CreateHash(password);
            return Task.FromResult(hash);
        }

        public Task<bool> PasswordMatchHashAsync(string password, string correctHash)
        {
            if (password == null)
            {
                throw new ArgumentNullException(nameof(password));
            }
            if (correctHash == null)
            {
                throw new ArgumentNullException(nameof(correctHash));
            }
            var match = PasswordHash.PasswordHash.ValidatePassword(password, correctHash);
            return Task.FromResult(match);
        }
    }
}