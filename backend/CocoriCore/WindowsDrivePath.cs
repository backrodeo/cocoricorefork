using System;
using System.Text.RegularExpressions;

namespace CocoriCore
{
    public class WindowsDrivePath : Path
    {
        private static Regex _driveRegex = new Regex("^([a-zA-Z]):");
        private string _driveLetter;

        public WindowsDrivePath(string path)
        {
            path = System.IO.Path.GetFullPath(path);
            var match = _driveRegex.Match(path);
            if (!match.Success)
            {
                throw new ArgumentException($"Path '{path}' must be a windows absolute path with drive letter.");
            }
            _driveLetter = match.Groups[1].Value;
            Segments = GetSegments(path.Substring(3));
        }

        protected WindowsDrivePath(string driveLetter, params object[] paths)
        {
            _driveLetter = driveLetter;
            Segments = GetSegments(paths);
        }

        public override string ToString()
        {
            return $@"{_driveLetter}:\{base.ToString()}";
        }

        public override Path Append(params object[] paths)
        {
            return new WindowsDrivePath(_driveLetter, Segments, paths);
        }

        public static bool IsWindowsDrivePath(string path)
        {
            return _driveRegex.IsMatch(path);
        }

        public static implicit operator WindowsDrivePath(string value)
        {
            return new WindowsDrivePath(value);
        }

        public static implicit operator string(WindowsDrivePath path)
        {
            return path?.ToString();
        }
    }
}
