﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public class DistinctVisitor : ExpressionVisitor
    {
        private MethodInfo _distinctNoArguments;
        private MethodInfo _distinctWithEqualityComparer;

        public DistinctVisitor()
        {
            var distinctMethods = typeof(Queryable).GetMethods().Where(x => x.Name == nameof(Queryable.Distinct));
            _distinctNoArguments = distinctMethods.Where(x => x.GetParameters().Length == 1).Single();
            _distinctWithEqualityComparer = distinctMethods.Where(x => x.GetParameters().Length == 2).Single();
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.IsGenericMethod && node.Method.GetGenericMethodDefinition() == _distinctNoArguments)
            {
                var genericArgument = node.Method.GetGenericArguments().Single();
                var comparerType = typeof(PropertyOfFieldComparer<>).MakeGenericType(genericArgument);
                var newComparerExpression = Expression.New(comparerType);
                var distinctWithEqualityComparerType = _distinctWithEqualityComparer.MakeGenericMethod(genericArgument);
                return Expression.Call(node.Object, distinctWithEqualityComparerType, node.Arguments[0], newComparerExpression);
            }
            return base.VisitMethodCall(node);
        }
    }
}
