using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore.Linq.Async
{
    //TODO a tester et mettre au propre
    public static class AsyncQueryableExtensions
    {
        private static MethodInfo _queryableAny;
        private static MethodInfo _queryableSingle;
        private static MethodInfo _queryableSingleOrDefault;
        private static MethodInfo _queryableFirst;
        private static MethodInfo _queryableFirstOrDefault;
        private static MethodInfo _queryableCount;
        private static MethodInfo _queryableToList;

        private static Assembly _EFCoreAssembly;
        private static readonly string _EFCoreQueryableExtensions = "Microsoft.EntityFrameworkCore.EntityFrameworkQueryableExtensions";
        private static readonly string _EFCoreAsyncProvider = "Microsoft.EntityFrameworkCore.Query.Internal.IAsyncQueryProvider";
        private static MethodInfo _EFCoreExecuteAsync;
        private static MethodInfo _EFCoreExecuteAsyncEnumerable;
        private static MethodInfo _EFCoreAnyAsync;
        private static MethodInfo _EFCoreCountAsync;
        private static MethodInfo _EFCoreFirstAsync;
        private static MethodInfo _EFCoreFirstOrDefaultAsync;
        private static MethodInfo _EFCoreSingleAsync;
        private static MethodInfo _EFCoreSingleOrDefaultAsync;
        private static MethodInfo _EFCoreToArrayAsync;
        private static MethodInfo _EFCoreToDictionaryAsync;
        private static MethodInfo _EFCoreToListAsync;

        private static Assembly _mongoDriverAssembly;
        private static readonly string _mongoDriverProvider = "MongoDB.Driver.Linq.IMongoQueryProvider";
        private static readonly string _mongoDriverQueryableExtensions = "MongoDB.Driver.Linq.MongoQueryable";
        private static readonly string _mongoDriverCursorExtensions = "MongoDB.Driver.IAsyncCursorExtensions";
        private static MethodInfo _mongoDriverExecuteAsync;
        private static MethodInfo _mongoDriverFirstOrDefaultAsync;
        private static MethodInfo _mongoDriverAnyAsync;
        private static MethodInfo _mongoDriverSingleAsync;
        private static MethodInfo _mongoDriverSingleOrDefaultAsync;
        private static MethodInfo _mongoDriverCountAsync;
        private static MethodInfo _mongoDriverToListAsync;
        private static MethodInfo _mongoDriverToDictionaryAsync;

        private static MethodInfo QueryableAny
        {
            get
            {
                if (_queryableAny == null)
                {
                    _queryableAny = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "Any" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableAny;
            }
        }

        private static MethodInfo QueryableSingle
        {
            get
            {
                if (_queryableSingle == null)
                {
                    _queryableSingle = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "Single" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableSingle;
            }
        }

        private static MethodInfo QueryableSingleOrDefault
        {
            get
            {
                if (_queryableSingleOrDefault == null)
                {
                    _queryableSingleOrDefault = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "SingleOrDefault" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableSingleOrDefault;
            }
        }

        private static MethodInfo QueryableFirst
        {
            get
            {
                if (_queryableFirst == null)
                {
                    _queryableFirst = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "First" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableFirst;
            }
        }

        private static MethodInfo QueryableFirstOrDefault
        {
            get
            {
                if (_queryableFirstOrDefault == null)
                {
                    _queryableFirstOrDefault = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "FirstOrDefault" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableFirstOrDefault;
            }
        }

        private static MethodInfo QueryableCount
        {
            get
            {
                if (_queryableCount == null)
                {
                    _queryableCount = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "Count" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableCount;
            }
        }

        private static MethodInfo QueryableToList
        {
            get
            {
                if (_queryableToList == null)
                {
                    _queryableToList = typeof(Queryable)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ToList" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)))
                        .Single();
                }
                return _queryableToList;
            }
        }

        //TODO gestion d'erreur si assembly non dispo
        private static Assembly EFCoreAssembly
        {
            get
            {
                if (_EFCoreAssembly == null)
                {
                    _EFCoreAssembly = Assembly.Load("Microsoft.EntityFrameworkCore");
                }
                return _EFCoreAssembly;
            }
        }

        private static MethodInfo EFCoreExecuteAsync
        {
            get
            {
                if (_EFCoreExecuteAsync == null)
                {
                    _EFCoreExecuteAsync = EFCoreAssembly
                        .GetType(_EFCoreAsyncProvider)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ExecuteAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType == typeof(Expression) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreExecuteAsync;
            }
        }

        private static MethodInfo EFCoreExecuteAsyncEnumerable
        {
            get
            {
                if (_EFCoreExecuteAsyncEnumerable == null)
                {
                    _EFCoreExecuteAsyncEnumerable = EFCoreAssembly
                        .GetType(_EFCoreAsyncProvider)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ExecuteAsync" &&
                            x.GetParameters().Count() == 1 &&
                            x.GetParameters()[0].ParameterType == typeof(Expression))
                        .Single();
                }
                return _EFCoreExecuteAsync;
            }
        }

        private static MethodInfo EFCoreToArrayAsync
        {
            get
            {
                if (_EFCoreToArrayAsync == null)
                {
                    _EFCoreToArrayAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ToArrayAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreToArrayAsync;
            }
        }

        private static MethodInfo EFCoreToListAsync
        {
            get
            {
                if (_EFCoreToListAsync == null)
                {
                    _EFCoreToListAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ToListAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreToListAsync;
            }
        }

        private static MethodInfo EFCoreSingleAsync
        {
            get
            {
                if (_EFCoreSingleAsync == null)
                {
                    _EFCoreSingleAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "SingleAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreSingleAsync;
            }
        }

        private static MethodInfo EFCoreSingleOrDefaultAsync
        {
            get
            {
                if (_EFCoreSingleOrDefaultAsync == null)
                {
                    _EFCoreSingleOrDefaultAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "SingleOrDefaultAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreSingleOrDefaultAsync;
            }
        }

        private static MethodInfo EFCoreFirstAsync
        {
            get
            {
                if (_EFCoreFirstAsync == null)
                {
                    _EFCoreFirstAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "FirstAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreFirstAsync;
            }
        }

        private static MethodInfo EFCoreFirstOrDefaultAsync
        {
            get
            {
                if (_EFCoreFirstOrDefaultAsync == null)
                {
                    _EFCoreFirstOrDefaultAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "FirstOrDefaultAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreFirstOrDefaultAsync;
            }
        }

        private static MethodInfo EFCoreCountAsync
        {
            get
            {
                if (_EFCoreCountAsync == null)
                {
                    _EFCoreCountAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "CountAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreCountAsync;
            }
        }

        private static MethodInfo EFCoreAnyAsync
        {
            get
            {
                if (_EFCoreAnyAsync == null)
                {
                    _EFCoreAnyAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "AnyAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreAnyAsync;
            }
        }


        private static MethodInfo EFCoreToDictionaryAsync
        {
            get
            {
                if (_EFCoreToDictionaryAsync == null)
                {
                    _EFCoreToDictionaryAsync = EFCoreAssembly
                        .GetType(_EFCoreQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ToDictionaryAsync" &&
                            x.GetParameters().Count() == 4 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType.IsAssignableToGeneric(typeof(Func<,>)) &&
                            x.GetParameters()[2].ParameterType.IsAssignableToGeneric(typeof(Func<,>)) &&
                            x.GetParameters()[3].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _EFCoreToDictionaryAsync;
            }
        }

        private static Assembly MongoDriverAssembly
        {
            get
            {
                if (_mongoDriverAssembly == null)
                {
                    _mongoDriverAssembly = Assembly.Load("MongoDB.Driver");
                }
                return _mongoDriverAssembly;
            }
        }

        private static MethodInfo MongoDriverExecuteAsync
        {
            get
            {
                if (_mongoDriverExecuteAsync == null)
                {
                    _mongoDriverExecuteAsync = MongoDriverAssembly
                        .GetType(_mongoDriverProvider)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "ExecuteAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType == typeof(Expression) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverExecuteAsync;
            }
        }

        private static MethodInfo MongoDriverFirstOrDefaultAsync
        {
            get
            {
                if (_mongoDriverFirstOrDefaultAsync == null)
                {
                    _mongoDriverFirstOrDefaultAsync = MongoDriverAssembly
                        .GetType(_mongoDriverQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "FirstOrDefaultAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverFirstOrDefaultAsync;
            }
        }

        private static MethodInfo MongoDriverAnyAsync
        {
            get
            {
                if (_mongoDriverAnyAsync == null)
                {
                    _mongoDriverAnyAsync = MongoDriverAssembly
                        .GetType(_mongoDriverQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "AnyAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverAnyAsync;
            }
        }

        private static MethodInfo MongoDriverSingleAsync
        {
            get
            {
                if (_mongoDriverSingleAsync == null)
                {
                    _mongoDriverSingleAsync = MongoDriverAssembly
                        .GetType(_mongoDriverQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "SingleAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverSingleAsync;
            }
        }

        private static MethodInfo MongoDriverSingleOrDefaultAsync
        {
            get
            {
                if (_mongoDriverSingleOrDefaultAsync == null)
                {
                    _mongoDriverSingleOrDefaultAsync = MongoDriverAssembly
                        .GetType(_mongoDriverQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "SingleOrDefaultAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverSingleOrDefaultAsync;
            }
        }

        private static MethodInfo MongoDriverCountAsync
        {
            get
            {
                if (_mongoDriverCountAsync == null)
                {
                    _mongoDriverCountAsync = MongoDriverAssembly
                        .GetType(_mongoDriverQueryableExtensions)
                        .GetMethods()
                        .Where(x =>
                            x.Name == "CountAsync" &&
                            x.GetParameters().Count() == 2 &&
                            x.GetParameters()[0].ParameterType.IsAssignableToGeneric(typeof(IQueryable<>)) &&
                            x.GetParameters()[1].ParameterType == typeof(CancellationToken))
                        .Single();
                }
                return _mongoDriverCountAsync;
            }
        }

        private static MethodInfo MongoDriverToListAsync
        {
            get
            {
                if (_mongoDriverToListAsync == null)
                {
                    _mongoDriverToListAsync = Assembly.Load("CocoriCore.MongoDb")
                        .GetType("CocoriCore.MongoDb.MongoAsyncCusorExtensions")
                        .GetMethod("ToListAsync");
                }
                return _mongoDriverToListAsync;
            }
        }

        public static async Task<T> SingleAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreSingleAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else if (queryable.UseCocoriCore())
            {
                var singleMethod = QueryableSingle.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, singleMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(expression, cancellationToken);
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverSingleAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<T> SingleOrDefaultAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreSingleOrDefaultAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else if (queryable.UseCocoriCore())
            {
                var singleOrDefaultMethod = QueryableSingleOrDefault.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, singleOrDefaultMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(expression, cancellationToken);
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverSingleOrDefaultAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<T> FirstAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreFirstAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else if (queryable.UseCocoriCore())
            {
                var firstMethod = QueryableFirst.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, firstMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(expression, cancellationToken);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<T> FirstOrDefaultAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreFirstOrDefaultAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else if (queryable.UseCocoriCore())
            {
                var firstOrDefaultMethod = QueryableFirstOrDefault.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, firstOrDefaultMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(expression, cancellationToken);
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverFirstOrDefaultAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<int> CountAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreCountAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<int>();
            }
            else if (queryable.UseCocoriCore())
            {
                var countMethod = QueryableCount.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, countMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<int>(expression, cancellationToken);
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverCountAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<int>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<bool> AnyAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreAnyAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<bool>();
            }
            else if (queryable.UseCocoriCore())
            {
                var anyMethod = QueryableAny.MakeGenericMethod(typeof(T));
                var expression = Expression.Call(null, anyMethod, new[] { queryable.Expression });
                return await ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<bool>(expression, cancellationToken);
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverAnyAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<bool>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }


        public static async Task<List<T>> ToListAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreToListAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<List<T>>();
            }
            else if (queryable.UseCocoriCore())
            {
                var enumerable = ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(queryable.Expression);
                var enumerator = enumerable.GetAsyncEnumerator();
                var result = new List<T>();
                while (await enumerator.MoveNextAsync())
                {
                    result.Add(enumerator.Current);
                }
                if (enumerator is IAsyncDisposable asyncDisposable)
                {
                    await asyncDisposable.DisposeAsync();
                }
                return result;
            }
            else if (queryable.UseMongoDriver())
            {
                var task = (Task)MongoDriverToListAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<List<T>>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<T[]> ToArrayAsync<T>(this IQueryable<T> queryable, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreToArrayAsync
                   .MakeGenericMethod(typeof(T))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                return await task.GetResultAsync<T[]>();
            }
            else if (queryable.UseCocoriCore())
            {
                var enumerable = ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<T>(queryable.Expression);
                var enumerator = enumerable.GetAsyncEnumerator();
                var result = new List<T>();
                while (await enumerator.MoveNextAsync())
                {
                    result.Add(enumerator.Current);
                }
                if (enumerator is IAsyncDisposable asyncDisposable)
                {
                    await asyncDisposable.DisposeAsync();
                }
                return result.ToArray();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<Dictionary<TKey, TElement>> ToDictionaryAsync<TSource, TKey, TElement>(this IQueryable<TSource> queryable,
            Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (queryable.UseEntityFramework())
            {
                var task = (Task)EFCoreToDictionaryAsync
                   .MakeGenericMethod(typeof(TSource), typeof(TKey), typeof(TElement))
                   .Invoke(queryable, new object[] { queryable, keySelector, elementSelector, cancellationToken });
                return await task.GetResultAsync<Dictionary<TKey, TElement>>();
            }
            else if (queryable.UseCocoriCore())
            {
                var enumerable = ((IAsyncQueryProvider)queryable.Provider).ExecuteAsync<TSource>(queryable.Expression);
                var enumerator = enumerable.GetAsyncEnumerator();
                var result = new List<TSource>();
                while (await enumerator.MoveNextAsync())
                {
                    result.Add(enumerator.Current);
                }
                if (enumerator is IAsyncDisposable asyncDisposable)
                {
                    await asyncDisposable.DisposeAsync();
                }
                return result.ToDictionary(keySelector, elementSelector);
            }
            else if (queryable.UseMongoDriver())
            {
                //ca n'existe pas ToDictionaryAsync en mongo
                var task = (Task)MongoDriverToListAsync
                   .MakeGenericMethod(typeof(TSource))
                   .Invoke(queryable, new object[] { queryable, cancellationToken });
                var list = await task.GetResultAsync<List<TSource>>();
                return list.ToDictionary(keySelector, elementSelector);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static async Task<TResult> ExecuteAsync_<TResult>(this IQueryProvider provider, Expression expression, CancellationToken cancellationToken)
        {
            if (provider.UseEntityFramework())
            {
                var task = (Task)EFCoreExecuteAsync
                   .MakeGenericMethod(typeof(Task<>).MakeGenericType(typeof(TResult)))
                   .Invoke(provider, new object[] { expression, cancellationToken });
                return await task.GetResultAsync<TResult>();
            }
            else if (provider.UseCocoriCore())
            {
                return await ((IAsyncQueryProvider)provider).ExecuteAsync<TResult>(expression, cancellationToken);
            }
            else if (provider.UseMongoDriver())
            {
                var task = (Task)MongoDriverExecuteAsync
                   .MakeGenericMethod(typeof(TResult))
                   .Invoke(provider, new object[] { expression, cancellationToken });
                return await task.GetResultAsync<TResult>();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static IAsyncEnumerable<TResult> ExecuteAsync_<TResult>(this IQueryProvider provider, Expression expression)
        {
            if (provider.UseEntityFramework())
            {
                return (IAsyncEnumerable<TResult>)EFCoreExecuteAsyncEnumerable
                   .MakeGenericMethod(typeof(Task<>).MakeGenericType(typeof(TResult)))
                   .Invoke(provider, new object[] { expression });
            }
            else if (provider.UseCocoriCore())
            {
                return ((IAsyncQueryProvider)provider).ExecuteAsync<TResult>(expression);
            }
            else if (provider.UseMongoDriver())
            {
                throw new NotImplementedException();
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private static bool UseEntityFramework<T>(this IQueryable<T> queryable)
        {
            return queryable.Provider.UseEntityFramework();
        }

        private static bool UseEntityFramework(this IQueryProvider provider)
        {
            return provider.GetType().GetInterface(_EFCoreAsyncProvider) != null;
        }

        private static bool UseCocoriCore<T>(this IQueryable<T> queryable)
        {
            return queryable.Provider.UseCocoriCore();
        }

        private static bool UseCocoriCore(this IQueryProvider provider)
        {
            return provider is IAsyncQueryProvider;
        }

        private static bool UseMongoDriver<T>(this IQueryable<T> queryable)
        {
            return queryable.Provider.UseMongoDriver();
        }

        private static bool UseMongoDriver(this IQueryProvider provider)
        {
            return provider.GetType().GetInterface(_mongoDriverProvider) != null;
        }
    }
}