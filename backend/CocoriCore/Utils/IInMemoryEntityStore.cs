using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CocoriCore
{
    public interface IInMemoryEntityStore
    {
        void AddUniqueConstraint<TEntity>(Expression<Func<TEntity, dynamic>> member);
        void Add(IEntity entity);

        void Set(IEntity entity);

        bool Contains(Type entityType, Guid entityId);

        void Remove(Type entityType, Guid entityId);

        IEnumerable<IEntity> Get(Type entityType);

        IEnumerable<IEntity> Get(Type entityType, Guid entityId);

        void Clear();

        void CheckUniqueConstraints(Type entityType);
    }
}