using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CocoriCore
{
    public class IndexedLists<TKey, TValue> : IEnumerable<KeyValuePair<TKey, List<TValue>>>
    {
        private static List<TValue> _emptyCollection = new List<TValue>();
        private Dictionary<TKey, List<TValue>> _dictionary;

        public IndexedLists()
        {
            _dictionary = new Dictionary<TKey, List<TValue>>();
        }

        public IndexedLists<TKey, TValue> Add(IndexedLists<TKey, TValue> index)
        {   
            foreach(var kvp in index)
            {
                Add(kvp.Key, kvp.Value);
            }
            return this;
        }

        public IndexedLists<TKey, TValue> Add(TKey key, TValue value)
        {
            if (!_dictionary.ContainsKey(key))
                AddKey(key);
            _dictionary[key].Add(value);
            return this;
        }

        public IndexedLists<TKey, TValue> Add(TKey key, IEnumerable<TValue> values)
        {
            if (!_dictionary.ContainsKey(key))
                AddKey(key);
            foreach (var value in values)
            {
                _dictionary[key].Add(value);
            }
            return this;
        }

        public List<TValue> Get(TKey key)
        {
            if (!_dictionary.ContainsKey(key))
                return _emptyCollection;
            else
                return _dictionary[key];
        }

        public void AddKey(TKey key)
        {
            _dictionary[key] = new List<TValue>();
        }

        public void RemoveKey(TKey key)
        {
            _dictionary.Remove(key);
        }

        public List<TValue> this[TKey key] => Get(key);

        public bool ContainsKey(TKey key)
        {
            return _dictionary.ContainsKey(key);
        }

        public IEnumerable<TKey> Keys => _dictionary.Keys;

        public void Clear(TKey key)
        {
            _dictionary[key].Clear();
        }

        IEnumerator<KeyValuePair<TKey, List<TValue>>> IEnumerable<KeyValuePair<TKey, List<TValue>>>.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }
    }
}