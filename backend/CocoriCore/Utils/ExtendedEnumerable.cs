﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore
{
    public class ExtendedEnumerable<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, IAsyncQueryProvider
    {
        private IQueryable<T> _queryable;
        private Expression _expression;
        private Func<object, object> _action;
        private Type _actionType;
        private string _actionName;

        public ExtendedEnumerable(IQueryable<T> queryable, Func<T, T> action, string actionName = null)
        {
            _actionName = actionName;
            _actionType = typeof(T);
            _action = x => action((T)x);
            _expression = queryable.Expression;
            _queryable = queryable;
        }

        private ExtendedEnumerable(IQueryable<T> queryable, Expression expression,
            Func<object, object> action, Type actionParameterType, string actionName = null)
        {
            _actionName = actionName;
            _action = action;
            _actionType = actionParameterType;
            _expression = expression;
            _queryable = queryable;
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            var queryable = _queryable.Provider.CreateQuery<T>(_expression);
            return new ExtendedEnumerator<T>(queryable.GetEnumerator(), _action, _actionType, _actionName);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var queryable = _queryable.Provider.CreateQuery<T>(_expression);
            return new ExtendedEnumerator<T>(queryable.GetEnumerator(), _action, _actionType, _actionName);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            var queryable = _queryable.Provider.CreateQuery(_expression);
            return new ExtendedEnumerator<T>((IEnumerator<T>)queryable.GetEnumerator(), _action, _actionType, _actionName);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery(_expression);//TODO ici pourquoi on fait pas CreateQuery avec celle apssé en paramètre ?
            return new ExtendedEnumerable<T>(_queryable, expression, _action, _actionType, _actionName);
        }

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery<TResult>(_expression);//TODO ici pourquoi on fait pas CreateQuery avec celle apssé en paramètre ?
            return new ExtendedEnumerable<TResult>(queryable, expression, _action, _actionType, _actionName);
        }

        public object Execute(Expression expression)
        {
            return CompileExpressionItem<object>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return CompileExpressionItem<TResult>(expression);
        }

        public IAsyncEnumerable<TResult> ExecuteAsync<TResult>(Expression expression)
        {
            var queryable = _queryable.Provider.CreateQuery<TResult>(_expression);//TODO ici pourquoi on fait pas CreateQuery avec celle apssé en paramètre ?
            return new ExtendedEnumerable<TResult>(queryable, expression, _action, _actionType, _actionName);
        }

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            var result = await _queryable.Provider.ExecuteAsync_<TResult>(expression, cancellationToken);
            //used whith SingleOrdefaultAsync for example
            TryRunAction(result);
            return result;
        }

        private void TryRunAction<TResult>(TResult result)
        {
            if (result == null)
            {
                return;
            }
            if (result.GetType().IsAssignableTo(_actionType))
            {
                _action(result);
            }
            else if (result is IDictionary dictionary)
            {
                var dictionaryType = dictionary.GetType();
                if (dictionaryType.IsGenericType && dictionaryType.GenericTypeArguments[1].IsAssignableTo(_actionType))
                {
                    foreach (var element in dictionary.Values)
                    {
                        _action(result);
                    }
                }
            }
            else if (result is IEnumerable enumerable)
            {
                var enumerableType = enumerable.GetType();
                if (enumerableType.IsGenericType && enumerableType.GenericTypeArguments[0].IsAssignableTo(_actionType))
                {
                    foreach (var element in enumerable)
                    {
                        _action(result);
                    }
                }
            }
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get => _expression; }

        public IQueryProvider Provider => this;

        private static TResult CompileExpressionItem<TResult>(Expression expression)
        {
            return Expression
                .Lambda<Func<TResult>>(expression, (IEnumerable<ParameterExpression>)null)
                .Compile()();
        }
    }
}
