﻿using CocoriCore.Linq.Async;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class MultiSourceQueryProvider<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, IAsyncQueryProvider
    {
        private Expression _expression;
        private IQueryable[] _queryables;

        protected MultiSourceQueryProvider(Expression expression, params IQueryable[] queryables)
        {
            _queryables = queryables;
            _expression = expression;
        }

        public MultiSourceQueryProvider(params IQueryable<T>[] queryables)
        {
            _queryables = queryables;
            Expression<Func<IQueryable<T>>> thisExp = () => this;
            _expression = thisExp.Body;
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return (IAsyncEnumerator<T>)this.AsEnumerable().GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return ConstructMultiSourceEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ConstructMultiSourceEnumerator();
        }

        private MultiSourceEnumerator<T> ConstructMultiSourceEnumerator()
        {
            var enumerators = ConstructQueryables(_expression)
                .Select(q => q.GetEnumerator())
                .ToArray();
            return new MultiSourceEnumerator<T>(enumerators);
        }

        private List<IQueryable<T>> ConstructQueryables(Expression expression)
        {
            List<IQueryable<T>> queryables = new List<IQueryable<T>>();
            foreach (var queryable in _queryables)
            {
                var visitor = new InterfaceVisitor(queryable);
                var specializedExpression = visitor.Visit(expression);
                queryables.Add(queryable.Provider.CreateQuery<T>(specializedExpression));
            }
            return queryables;
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new MultiSourceQueryProvider<T>(expression, _queryables);
        }

        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            return new MultiSourceQueryProvider<TResult>(expression, _queryables);
        }

        public object Execute(Expression expression)
        {
            return CompileExpressionItem<object>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return CompileExpressionItem<TResult>(expression);
        }

        public IAsyncEnumerable<TResult> ExecuteAsync<TResult>(Expression expression)
        {
            return new MultiSourceQueryProvider<TResult>(expression, _queryables);
        }

        public async Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            List<TResult> results = new List<TResult>();
            foreach (var queryable in _queryables)
            {
                var visitor = new InterfaceVisitor(queryable);
                var specializedExpression = visitor.Visit(expression);
                var task = queryable.Provider.ExecuteAsync_<TResult>(specializedExpression, cancellationToken);
                var result = await task.GetResultAsync<TResult>();
                if (result != null)
                    results.Add(result);
            }
            //TODO ici peut etre que single or default n'ira pas dans tous les cas
            return results.SingleOrDefault();
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get => _expression; }

        public IQueryProvider Provider => this;

        private static TResult CompileExpressionItem<TResult>(Expression expression)
        {
            return Expression
                .Lambda<Func<TResult>>(expression, (IEnumerable<ParameterExpression>)null)
                .Compile()();
        }
    }

    public class InterfaceVisitor : ExpressionVisitor
    {
        private IQueryable _queryable;

        public InterfaceVisitor(IQueryable queryable)
        {
            _queryable = queryable;
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            var constantType = node?.Value?.GetType();
            if (constantType != null && constantType.IsAssignableTo<IAsyncQueryProvider>())
            {
                return _queryable.Expression;
            }
            else
            {
                return base.VisitConstant(node);
            }
        }
    }

    public class MultiSourceEnumerator<T> : IEnumerator<T>, IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T>[] _enumerators;
        private int _index;

        public MultiSourceEnumerator(IEnumerator<T>[] enumerators)
        {
            _enumerators = enumerators ?? throw new ArgumentNullException();
        }

        public T Current => _enumerators[_index].Current;

        object IEnumerator.Current => Current;



        public bool MoveNext()
        {
            var enumerator = _enumerators[_index];
            bool success = enumerator.MoveNext();
            if (!success)
            {
                if (_index == _enumerators.Length - 1)
                {
                    return false;
                }
                else
                {
                    enumerator.Dispose();
                    _index++;
                    return MoveNext();
                }
            }
            return success;
        }

        public async ValueTask<bool> MoveNextAsync()
        {
            bool success = false;
            var enumerator = _enumerators[_index];
            if (enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                success = await asyncEnumerator.MoveNextAsync();
            }
            else
            {
                success = enumerator.MoveNext();
            }
            if (!success)
            {
                if (_index == _enumerators.Length - 1)
                {
                    return false;
                }
                else
                {
                    await DisposeAsync(enumerator);
                    _index++;
                    return await MoveNextAsync();
                }
            }
            return success;
        }

        public void Reset()
        {
            _index = 0;
            for (var i = 0; i < _enumerators.Length; i++)
            {
                _enumerators[i].Reset();
            }
        }

        public void Dispose()
        {
            _enumerators.ForEach(x => x.Dispose());
        }

        public async ValueTask DisposeAsync()
        {
            foreach (var enumerator in _enumerators)
            {
                await DisposeAsync(enumerator);
            }
        }

        private async Task DisposeAsync(IEnumerator<T> enumerator)
        {
            if (enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                await asyncEnumerator.DisposeAsync();
            }
            else
            {
                enumerator.Dispose();
            }
        }
    }
}
