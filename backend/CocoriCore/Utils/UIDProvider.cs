using System;
using Castle.DynamicProxy;

namespace CocoriCore
{
    public class UIDProvider : IUIDProvider
    {
        public object GetUID(object obj)
        {
            if (obj == null)
                throw new ArgumentNullException(nameof(obj));
            if (obj is IEntity entity)
            {
                var type = GetType(entity);
                return new EntityUID(type, entity.Id);
            }
            else
                throw new InvalidOperationException($"This UIDProvider only works with object inhertied from {typeof(IEntity)}");
        }

        public object GetUID(Type type, object id)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));
            if (id == null)
                throw new ArgumentNullException(nameof(id));
            if (id is Guid guid)
            {
                return new EntityUID(type, guid);
            }
            else
                throw new InvalidOperationException("This UIDProvider only works with id of type Guid.");
        }

        private Type GetType(object obj)
        {
            var type = obj.GetType();
            while (type.IsAssignableTo<IProxyTargetAccessor>())
                type = type.BaseType;
            return type;
        }
    }
}
