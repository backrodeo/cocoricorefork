﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ExtendedEnumerator<T> : IEnumerator<T>, IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T> _enumerator;
        private Func<object, object> _action;
        private Type _actionType;
        private string _actionName;

        public ExtendedEnumerator(IEnumerator<T> enumerator, Func<object, object> action, Type actionType, string actionName = null)
        {
            _actionName = actionName;
            _action = action;
            _actionType = actionType;
            _enumerator = enumerator ?? throw new ArgumentNullException();
        }

        public T Current
        {
            get
            {
                T result = _enumerator.Current;
                if(_actionType.IsAssignableTo<T>())
                {
                    result = (T)_action(result);
                }
                return result;
            }
        }

        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            return _enumerator.MoveNext();
        }

        public async ValueTask<bool> MoveNextAsync()
        {
            if (_enumerator is IAsyncEnumerator<T> asyncEnumerator)
            {
                return await asyncEnumerator.MoveNextAsync();
            }
            else
            {
                return _enumerator.MoveNext();
            }
        }

        public void Reset()
        {
            _enumerator.Reset();
        }

        public void Dispose()
        {
            _enumerator.Dispose();
        }

        public ValueTask DisposeAsync()
        {
            _enumerator.Dispose();
            return new ValueTask(Task.CompletedTask);
        }
    }
}
