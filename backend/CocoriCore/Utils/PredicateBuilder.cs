﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore
{
    public static class PredicateBuilder
    {
        public static Expression<Func<T, bool>> True<T>() { return x => true; }
        public static Expression<Func<T, bool>> False<T>() { return x => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
            Expression<Func<T, bool>> expr2)
        {
            var parameter = Expression.Parameter(typeof(T), "x");
            var visitor = new ParameterVisitor(parameter);
            var leftExpression = visitor.Visit(expr1.Body);
            var rightExpression = visitor.Visit(expr2.Body);
            var orExpression = Expression.OrElse(leftExpression, rightExpression);
            return Expression.Lambda<Func<T, bool>>(orExpression, parameter);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var parameter = Expression.Parameter(typeof(T), "x");
            var visitor = new ParameterVisitor(parameter);
            var leftExpression = visitor.Visit(expr1.Body);
            var rightExpression = visitor.Visit(expr2.Body);
            var andExpression = Expression.AndAlso(leftExpression, rightExpression);
            return Expression.Lambda<Func<T, bool>>(andExpression, parameter);
        }
    }

    class ParameterVisitor : ExpressionVisitor
    {
        private ParameterExpression _paramater;

        public ParameterVisitor(ParameterExpression paramater)
        {
            _paramater = paramater;
        }

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return _paramater;
        }
    }
}
