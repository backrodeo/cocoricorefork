using System;

namespace CocoriCore
{
    public class EntityUID
    {
        public Type Type { get; }
        public Guid Id { get; }

        public EntityUID(Type type, Guid id)
        {
            Type = type;
            Id = id;
        }

        public override int GetHashCode()
        {
            return 3 ^ Type.GetHashCode() ^ Id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is EntityUID other && obj != null)
            {
                return Type == other.Type &&
                    object.Equals(Id, other.Id);
            }
            return false;
        }

        public override string ToString()
        {
            return $"{Type}-{Id}";
        }
    }
}
