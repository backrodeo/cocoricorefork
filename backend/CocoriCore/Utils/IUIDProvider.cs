using System;

namespace CocoriCore
{
    public interface IUIDProvider
    {
        object GetUID(object obj);
        object GetUID(Type type, object id);
    }
}
