using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    //TODO déplacer dans CocoriCore.Persistence.InMemory
    public class InMemoryEntityStore : IInMemoryEntityStore, IPersistentStore
    {
        private ConcurrentDictionary<Type, List<EntityReference>> _typeHierarchyDictionary;
        private ConcurrentDictionary<object, EntityReference> _UIDDictionary;
        private ConcurrentDictionary<Type, MemberInfo> _uniqueConstraints;
        private IUIDProvider _uidProvider;

        public InMemoryEntityStore(IUIDProvider uidProvider)
        {
            _uidProvider = uidProvider;
            _typeHierarchyDictionary = new ConcurrentDictionary<Type, List<EntityReference>>();
            _UIDDictionary = new ConcurrentDictionary<object, EntityReference>();
            _uniqueConstraints = new ConcurrentDictionary<Type, MemberInfo>();
        }

        public virtual void AddUniqueConstraint<TEntity>(Expression<Func<TEntity, dynamic>> member)
        {
            if (!_uniqueConstraints.ContainsKey(typeof(TEntity)))
            {
                _uniqueConstraints[typeof(TEntity)] = member.GetMemberInfo();
            }
        }

        public virtual void Add(IEntity entity)
        {
            var uid = _uidProvider.GetUID(entity);
            if (!_UIDDictionary.ContainsKey(uid))
            {
                var entityReference = new EntityReference(entity, uid);
                _UIDDictionary[uid] = entityReference;

                var entityTypeHierarchy = entity.GetTypeHierarchy();
                foreach (Type currentType in entityTypeHierarchy)
                {
                    lock (_typeHierarchyDictionary)
                    {
                        if (!_typeHierarchyDictionary.ContainsKey(currentType))
                        {
                            _typeHierarchyDictionary[currentType] = new List<EntityReference>();
                        }
                    }
                    _typeHierarchyDictionary[currentType].Add(entityReference);
                }
            }
            else
            {
                throw new InvalidOperationException($"There is already an entity of type {entity.GetType()} with id {entity.Id}");
            }
        }

        public virtual void Set(IEntity entity)
        {
            var uid = _uidProvider.GetUID(entity);
            if (_UIDDictionary.ContainsKey(uid))
            {
                _UIDDictionary[uid].Entity = entity;
            }
            else
            {
                throw new InvalidOperationException($"There is no entity of type {entity.GetType()} with id {entity.Id}");
            }
        }

        public virtual bool Contains(Type entityType, Guid entityId)
        {
            var uid = _uidProvider.GetUID(entityType, entityId);
            if (_UIDDictionary.ContainsKey(uid))
            {
                return true;
            }
            else
            {
                return _typeHierarchyDictionary.ContainsKey(entityType) &&
                    _typeHierarchyDictionary[entityType]
                    .Any(x => object.Equals(x.Entity.Id, entityId));
            }
        }

        public virtual void Remove(Type entityType, Guid entityId)
        {
            var uid = _uidProvider.GetUID(entityType, entityId);
            if (_UIDDictionary.ContainsKey(uid))
            {
                EntityReference entityReference = null;
                _UIDDictionary.TryRemove(uid, out entityReference);
                var entityTypeHierarchy = entityType.GetTypeHierarchy();
                foreach (Type currentType in entityTypeHierarchy)
                {
                    if (_typeHierarchyDictionary.ContainsKey(currentType))
                    {
                        var keysToRemove = _typeHierarchyDictionary[currentType].RemoveAll(x => object.Equals(x.Uid, uid));
                    }
                }
            }
            else
            {
                throw new InvalidOperationException($"There is no entity of type {entityType} with id {entityId}");
            }
        }

        public virtual IEnumerable<IEntity> Get(Type entityType)
        {
            if (_typeHierarchyDictionary.ContainsKey(entityType))
            {
                return _typeHierarchyDictionary[entityType].Select(x => x.Entity);
            }
            else
            {
                return new IEntity[0];
            }
        }

        public virtual IEnumerable<IEntity> Get(Type entityType, Guid entityId)
        {
            var uid = _uidProvider.GetUID(entityType, entityId);
            if (_UIDDictionary.ContainsKey(uid))
            {
                return new IEntity[] { _UIDDictionary[uid].Entity };
            }
            else if (_typeHierarchyDictionary.ContainsKey(entityType))
            {
                return _typeHierarchyDictionary[entityType]
                    .Where(x => object.Equals(x.Entity.Id, entityId))
                    .Select(x => x.Entity);
            }
            else
            {
                return new IEntity[0];
            }
        }

        public virtual void Clear()
        {
            _typeHierarchyDictionary.Clear();
            _UIDDictionary.Clear();
        }

        public virtual void CheckUniqueConstraints(Type entityType)
        {
            var entityTypeHierarchy = entityType.GetTypeHierarchy();
            foreach (Type currentType in entityTypeHierarchy)
            {
                if (_uniqueConstraints.ContainsKey(currentType))
                {
                    var memberInfo = _uniqueConstraints[currentType];
                    if (!Get(entityType).IsUnique(memberInfo))
                        throw new UniqueConstraintException(memberInfo);
                }
            }
        }

        class EntityReference
        {
            public EntityReference(IEntity entity, object uid)
            {
                Entity = entity;
                Uid = uid;
            }

            public IEntity Entity { get; set; }

            public object Uid { get; }
        }
    }
}