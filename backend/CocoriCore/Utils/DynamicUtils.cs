using System;

namespace CocoriCore
{
    public static class DynamicUtils
    {
        public static bool IsStringMatchingGuid(dynamic value)
        {
            Guid guid;
            return value is string && Guid.TryParse(value, out guid);
        }
        
        public static bool IsInteger(dynamic value)
        {
            return value is short || value is ushort || value is int || value is uint || value is long || value is ulong;
        }

        public static bool IsNumber(dynamic value)
        {
            return IsInteger(value) || value is decimal;
        }
    }
}