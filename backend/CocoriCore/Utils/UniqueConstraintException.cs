﻿using System;
using System.Reflection;

namespace CocoriCore
{
    public class UniqueConstraintException : Exception
    {
        public MemberInfo Member { get; }

        public UniqueConstraintException(MemberInfo member)
            : base($"The unique constraint for {member.ReflectedType.FullName} over member {member.Name} is not respected.")
        {
            Member = member;
        }

        public UniqueConstraintException(string entityName, string memberName)
            : base($"The unique constraint for {entityName} over member {memberName} is not respected.")
        {
        }

        public UniqueConstraintException(MemberInfo member, Exception inner)
            : base($"The unique constraint for {member.ReflectedType.FullName} over member {member.Name} is not respected.", inner)
        {
            Member = member;
        }

        public UniqueConstraintException(string entityName, string memberName, Exception inner)
            : base($"The unique constraint for {entityName} over member {memberName} is not respected.", inner)
        {
        }

        public UniqueConstraintException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
