﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore
{
    public class FakeAsyncEnumerable<T> : IAsyncEnumerable<T>, IOrderedQueryable<T>, IAsyncQueryProvider
    {
        private IQueryable<T> _queryable;
        private DistinctVisitor _visitor;//TODO rendre configurable le(s) visitors pour retransformer l'expression ?
        //TODO utiliser ExtendedEnumerable ici ?

        public FakeAsyncEnumerable(Expression expression)
        {
            Expression = expression;
            _visitor = new DistinctVisitor();
        }

        public FakeAsyncEnumerable(IQueryable<T> queryable)
             : this(queryable.Expression)
        {
        }

        public Type ElementType => typeof(T);

        public Expression Expression { get; }

        public IQueryProvider Provider => this;

        private IQueryable<T> Queryable
        {
            get
            {
                if (_queryable == null)
                {
                    _queryable = CompileExpressionItem<IQueryable<T>>(Expression);
                }
                return _queryable;
            }
        }

        IAsyncEnumerator<T> IAsyncEnumerable<T>.GetAsyncEnumerator(CancellationToken cancellationToken)
        {
            return (IAsyncEnumerator<T>)this.AsEnumerable().GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new FakeEnumerator<T>(Queryable.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new FakeEnumerator<T>(Queryable.GetEnumerator());
        }

        public IQueryable CreateQuery(Expression expression)
        {
            return new FakeAsyncEnumerable<T>(expression);
        }

        public IQueryable<TEntity> CreateQuery<TEntity>(Expression expression)
        {
            return new FakeAsyncEnumerable<TEntity>(expression);
        }

        public object Execute(Expression expression)
        {
            return CompileExpressionItem<object>(expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            return CompileExpressionItem<TResult>(expression);
        }

        public IAsyncEnumerable<TResult> ExecuteAsync<TResult>(Expression expression)
        {
            return new FakeAsyncEnumerable<TResult>(Expression);
        }

        public Task<TResult> ExecuteAsync<TResult>(Expression expression, CancellationToken cancellationToken)
        {
            return Task.FromResult(CompileExpressionItem<TResult>(expression));
        }

        private TResult CompileExpressionItem<TResult>(Expression expression)
        {
            expression = _visitor.Visit(expression);
            return Expression
                .Lambda<Func<TResult>>(expression, (IEnumerable<ParameterExpression>)null)
                .Compile()();
        }
    }

    public class FakeEnumerator<T> : IEnumerator<T>, IAsyncEnumerator<T>
    {
        private readonly IEnumerator<T> _enumerator;

        public FakeEnumerator(IEnumerator<T> enumerator)
        {
            _enumerator = enumerator ?? throw new ArgumentNullException();
        }

        public T Current => _enumerator.Current;

        object IEnumerator.Current => _enumerator.Current;

        public bool MoveNext()
        {
            return _enumerator.MoveNext();
        }

        public ValueTask<bool> MoveNextAsync()
        {
            return new ValueTask<bool>(Task.FromResult(_enumerator.MoveNext()));
        }

        public void Reset()
        {
            _enumerator.Reset();
        }

        public void Dispose()
        {
            _enumerator.Dispose();
        }

        public ValueTask DisposeAsync()
        {
            _enumerator.Dispose();
            return new ValueTask(Task.CompletedTask);
        }
    }
}
