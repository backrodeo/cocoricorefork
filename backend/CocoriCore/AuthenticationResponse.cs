namespace CocoriCore
{
    public class AuthenticationResponse
    {
        public string AccessToken;
        public string RefreshToken;
    }
}