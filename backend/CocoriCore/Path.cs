﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore
{
    public class Path
    {
        public virtual string[] Segments { get; protected set; }

        public Path()
        {
            Segments = new string[0];
        }

        public Path(string path)
        {
            Segments = GetSegments(path);
        }

        public Path(params object[] paths)
        {
            Segments = GetSegments(paths);
        }

        public Path(string[] segments)
        {
            Segments = segments.ToArray();
        }

        protected virtual string[] GetSegments(params object[] paths)
        {
            return paths.SelectMany(x => GetSegments(x)).ToArray();
        }

        protected virtual string[] GetSegments(object obj)
        {
            if (obj == null)
            {
                return new string[0];
            }
            else if (obj is Path path)
            {
                return path.Segments;
            }
            else if (obj is IEnumerable enumerable && !(obj is string))
            {
                return enumerable
                    .Cast<object>()
                    .SelectMany(x => GetSegments(x))
                    .ToArray();
            }
            else
            {
                return obj
                    .ToString()
                    .Replace(@"\", "/")
                    .Split("/")
                    .Where(x => !string.IsNullOrWhiteSpace(x))//TODO tester que résiste au // et \\ dans les chemins
                    .ToArray();
            }
        }

        public virtual Path Append(params object[] paths)
        {
            return new Path(Segments, paths);
        }

        public string GetExtension()
        {
            return System.IO.Path.GetExtension(Segments.Last());
        }

        public Path RemoveExtension()
        {
            return new Path(System.IO.Path.GetFileNameWithoutExtension(this));
        }

        public Path RelativeTo(Path path)
        {
            var newSegments = Segments.ToList();
            foreach (var segment in Segments)
            {
                if (newSegments.First() == segment)
                {
                    newSegments.RemoveAt(0);
                }
                else
                {
                    throw new InvalidOperationException($"Path '{path}' must be a parent of '{this}'.");
                }
            }
            return new Path(newSegments);
        }

        public virtual Path GetParent()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException("Can't get parent of an empty path.");
            }
            var newSegments = new string[Segments.Length - 1];
            Array.Copy(Segments, newSegments, Segments.Length - 1);
            return new Path(newSegments);
        }

        public virtual Path RemoveFirstSegment()
        {
            if (IsEmpty())
            {
                throw new InvalidOperationException("Can't remove segment on an empty path.");
            }
            var newSegments = new string[Segments.Length - 1];
            Array.Copy(Segments, 1, newSegments, 0, Segments.Length - 1);
            return new Path(newSegments);
        }

        public virtual bool IsEmpty()
        {
            return Segments.Length == 0;
        }

        public override string ToString()
        {
            return string.Join("/", Segments);
        }

        public override bool Equals(object objet)
        {
            return objet is Path path && path == this;
        }

        public override int GetHashCode()
        {
            return 13 ^ ToString().GetHashCode();
        }

        public static implicit operator Path(string value)
        {
            return new Path(value);
        }
        public static implicit operator string(Path path)
        {
            return path?.ToString();
        }

        public static bool operator ==(Path path1, Path path2)
        {
            return Equals(path1?.ToString(), path2?.ToString());
        }

        public static bool operator !=(Path path1, Path path2)
        {
            return !(path1 == path2);
        }
    }
}
