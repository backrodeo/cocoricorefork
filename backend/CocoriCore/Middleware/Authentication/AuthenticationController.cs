﻿using System;
using System.Linq;

namespace CocoriCore
{
    public class AuthenticationController : IAuthenticationController
    {
        private AuthenticationOptions _options;
        private IUnitOfWork _unitOfWork;

        public AuthenticationController(AuthenticationOptions options, IUnitOfWork unitOfWork)
        {
            _options = options;
            _unitOfWork = unitOfWork;
        }

        public bool RequireAuthentication(Type messageType)
        {
            return _options
                .Rules
                .First(r => r.Match(_unitOfWork, messageType))
                .IsPrivate;
        }
    }
}
