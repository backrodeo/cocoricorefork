using System;

namespace CocoriCore
{
    public class AuthenticationException : Exception
    {
        public AuthenticationException(string message, Exception inner) 
            : base(message, inner)
        {
        }

        public AuthenticationException(string message)
            : base(message)
        {
        }
    }
}