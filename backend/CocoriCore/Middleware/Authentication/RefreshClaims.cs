using System;

namespace CocoriCore
{
    public class RefreshClaims : IClaims
    {
        public Guid UserId;
        public DateTime ExpireAt { get; set; }
    }
}