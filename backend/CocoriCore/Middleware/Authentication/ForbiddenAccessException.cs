﻿using System;

namespace CocoriCore.Middleware.Authentication
{
    public class ForbiddenAccessException : Exception
    {
        public ForbiddenAccessException(string message) : base(message)
        {
        }
    }
}
