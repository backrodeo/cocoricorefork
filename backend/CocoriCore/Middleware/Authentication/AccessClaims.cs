using System;

namespace CocoriCore
{
    public class AccessClaims : IClaims
    {
        public Guid UserId { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}