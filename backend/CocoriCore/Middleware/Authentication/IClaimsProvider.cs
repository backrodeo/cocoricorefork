﻿namespace CocoriCore
{
    public interface IClaimsProvider
    {
        bool HasClaims { get; }
        TClaims GetClaims<TClaims>();
    }
}
