using System;
using System.Collections.Generic;

namespace CocoriCore
{
    public class AuthenticationOptionsBuilder
    {
        private List<AuthenticationRule> _rules;
        private bool _defaultPrivate;

        public AuthenticationOptionsBuilder()
        {
            _rules = new List<AuthenticationRule>();
            _defaultPrivate = true;
        }

        public AuthenticationOptions Options
        {
            get
            {
                var optionRules = new List<AuthenticationRule>(_rules);
                optionRules.Add(new AuthenticationRule((u, t) => true, _defaultPrivate));
                return new AuthenticationOptions(optionRules);
            }
        }
        public AuthenticationOptionsBuilder DefaultPrivate()
        {
            _defaultPrivate = true;
            return this;
        }

        public AuthenticationOptionsBuilder Public<T>()
        {
            var rule = new AuthenticationRule((u, t) => t.IsAssignableTo<T>(), false);
            _rules.Add(rule);
            return this;
        }

        public AuthenticationOptionsBuilder DefaultPublic()
        {
            _defaultPrivate = false;
            return this;
        }

        public AuthenticationOptionsBuilder Private<T>()
        {
            var rule = new AuthenticationRule((u, t) => t.IsAssignableTo<T>(), true);
            _rules.Add(rule);
            return this;
        }

        public AuthenticationOptionsBuilder Public(Func<Type, bool> predicate)
        {
            var rule = new AuthenticationRule((u, t) => predicate(t), false);
            _rules.Add(rule);
            return this;
        }

        public AuthenticationOptionsBuilder Private(Func<Type, bool> predicate)
        {
            var rule = new AuthenticationRule((u, t) => predicate(t), true);
            _rules.Add(rule);
            return this;
        }

        public AuthenticationOptionsBuilder Public<TService>(Func<TService, Type, bool> predicate)
        {
            var rule = new AuthenticationRule((u, t) => predicate(u.Resolve<TService>(), t), false);
            _rules.Add(rule);
            return this;
        }

        public AuthenticationOptionsBuilder Private<TService>(Func<TService, Type, bool> predicate)
        {
            var rule = new AuthenticationRule((u, t) => predicate(u.Resolve<TService>(), t), true);
            _rules.Add(rule);
            return this;
        }
    }
}