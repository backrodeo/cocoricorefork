﻿using System;

namespace CocoriCore
{
    public interface IAuthenticationController
    {
        bool RequireAuthentication(Type messageType);
    }
}
