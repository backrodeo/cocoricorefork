using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IHttpAuthenticator
    {
        Task AuthenticateAsync(HttpContext context);
    }
}
