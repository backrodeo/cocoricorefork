using System;
using System.Collections.Generic;

namespace CocoriCore
{
    public class AuthenticationOptions
    {
        public List<AuthenticationRule> Rules { get; }

        public AuthenticationOptions(List<AuthenticationRule> rules)
        {
            Rules = rules;
        }
    }

    public class AuthenticationRule
    {
        public AuthenticationRule(Func<IUnitOfWork, Type, bool> match, bool auhtenticationRequired)
        {
            Match = match;
            IsPrivate = auhtenticationRequired;
        }

        public Func<IUnitOfWork, Type, bool> Match { get; }
        public bool IsPrivate { get; }
    }
}