﻿using Microsoft.AspNetCore.Http;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class JwtAuthenticator : IHttpAuthenticator, IClaimsProvider
    {
        private const string _bearer = "Bearer ";
        private string _token;
        private ITokenService _tokenService;

        public JwtAuthenticator(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }

        public bool IsAuthenticated => HasClaims;
        public bool HasClaims => _token != null;
        public TClaims GetClaims<TClaims>()
        {
            if (_token == null)
            {
                throw new InvalidOperationException($"Method {nameof(AuthenticateAsync)} must be called "
                    + $"with a valid token before.");//TODO ou message pour dire que ca peut venir d'un problème de capture de dépendance.
            }
            return _tokenService.GetPayload<TClaims>(_token);//TODO mettre en cache le claim une fois deserialize
        }

        public virtual async Task AuthenticateAsync(HttpContext context)
        {
            var authorization = context.Request.Headers["Authorization"].FirstOrDefault();
            if (string.IsNullOrWhiteSpace(authorization))
            {
                throw new AuthenticationException("Authorization header is empty.");
            }
            if (!authorization.StartsWith(_bearer))
            {
                throw new AuthenticationException($"Authorization schema must be {_bearer}.");
            }
            var jwt = authorization.Substring(_bearer.Length);
            if(string.IsNullOrWhiteSpace(jwt))
            {
                throw new AuthenticationException($"JWT is empty.");
            }
            _token = await _tokenService.CheckIntegrityAndExpirationAsync(jwt);
        }
    }
}
