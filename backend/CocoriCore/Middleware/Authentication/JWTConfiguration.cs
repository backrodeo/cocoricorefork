using System.Text;
using Jose;

namespace CocoriCore
{
    public class JWTConfiguration
    {
        public JWTConfiguration(string secret, JwsAlgorithm algorithm)
        {
            SecretBytes = Encoding.UTF8.GetBytes(secret);
            Algorithm = algorithm;
        }

        protected JWTConfiguration()
        {
        }

        public string Secret
        {
            get => Encoding.UTF8.GetString(SecretBytes);
            set => SecretBytes = Encoding.UTF8.GetBytes(value);
        }
        public byte[] SecretBytes { get; set; }
        public JwsAlgorithm Algorithm { get; set; }
    }
}