using System;

namespace CocoriCore
{
    public class TokenConfiguration : JWTConfiguration
    {
        public TokenConfiguration()
        {
            AccessTokenDuration = TimeSpan.FromMinutes(10);
            RefreshTokenDuration = TimeSpan.FromDays(1);
            CreatePasswordTokenDuration = TimeSpan.FromDays(7);
            ResetPasswordTokenDuration = TimeSpan.FromMinutes(15);
        }

        public TimeSpan AccessTokenDuration { get; set; }
        public TimeSpan RefreshTokenDuration { get; set; }
        public TimeSpan CreatePasswordTokenDuration { get; set; }
        public TimeSpan ResetPasswordTokenDuration { get; set; }
    }
}