using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    //TODO permettre désactivation CORS ? -> oui en mettant ou pas ce middleware, pouvoir configurer la liste des middleware facilement ?
    public class CorsMiddleware : IMiddleware
    {
        protected bool _enabled;
        protected List<string> _allowedOriginsString;
        protected List<Regex> _allowedOriginsRegex;
        protected List<string> _allowedHeaders;
        protected List<string> _allowedMethods;
        protected bool _allowCredentials;

        public CorsMiddleware()
        {
            _enabled = true;
            _allowCredentials = false;
            _allowedOriginsString = new List<string>();
            _allowedOriginsRegex = new List<Regex>();
            _allowedHeaders = new List<string> { "Content-Type", "Authorization" };
            _allowedMethods = new List<string> { "POST", "PUT", "DELETE" };
        }

        public virtual async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var httpRequest = context.Request;
            var httpResponse = context.Response;
            string origin = GetOrigin(httpRequest);
            var originIsAllowed = OriginIsAllowed(origin);

            if (IsPreflightRequest(httpRequest))
            {
                if (originIsAllowed)
                    AddCorsResponseHeadersPreflight(httpResponse, origin);
                return;
            }
            else
            {
                if (originIsAllowed)
                    AddCorsResponseHeaders(httpResponse, origin);
                await next(context);
            }
        }

        protected virtual string GetOrigin(HttpRequest httpRequest)
        {
            var origin = httpRequest.Headers["Origin"].FirstOrDefault();
            if (origin == null)
                origin = string.Empty;
            return origin;
        }

        protected virtual bool IsPreflightRequest(HttpRequest httpRequest)
        {
            return httpRequest.Method == "OPTIONS" &&
                httpRequest.Headers.Keys.Any(x => x.StartsWith("Access-Control-"));
        }

        protected virtual void AddCorsResponseHeadersPreflight(HttpResponse httpResponse, string origin)
        {
            AddAllowHeadersHeader(httpResponse);
            AddAllowMethodsHeader(httpResponse);
            AddAllowOriginHeader(httpResponse, origin);
            AddAllowCredentialsHeader(httpResponse, origin);
        }

        private void AddAllowHeadersHeader(HttpResponse httpResponse)
        {
            httpResponse.Headers["Access-Control-Allow-Headers"] = string.Join(", ", _allowedHeaders);
        }

        private void AddAllowMethodsHeader(HttpResponse httpResponse)
        {
            httpResponse.Headers["Access-Control-Allow-Methods"] = string.Join(", ", _allowedMethods);
        }

        private void AddAllowOriginHeader(HttpResponse httpResponse, string origin)
        {
            httpResponse.Headers["Access-Control-Allow-Origin"] = origin;
        }

        private void AddAllowCredentialsHeader(HttpResponse httpResponse, string origin)
        {
            if(_allowCredentials)
                httpResponse.Headers["Access-Control-Allow-Credentials"] = "true";
        }

        protected virtual void AddCorsResponseHeaders(HttpResponse httpResponse, string origin)
        {
            AddAllowOriginHeader(httpResponse, origin);
            AddAllowCredentialsHeader(httpResponse, origin);
        }

        public CorsMiddleware AllowOrigins(params string[] origins)
        {
            origins = origins.Select(x => x.Trim()).ToArray();
            foreach (var origin in origins)
            {
                if (origin.Contains("*"))
                {
                    var originRegex = origin.Replace(".", "\\.").Replace("*", ".*");
                    _allowedOriginsRegex.Add(new Regex(originRegex, RegexOptions.IgnoreCase));
                }
                else
                    _allowedOriginsString.Add(origin);
            }
            return this;
        }

        public CorsMiddleware AllowCredentials() 
        {
            _allowCredentials = true;
            return this;
        }

        private bool OriginIsAllowed(string origin)
        {
            return _allowedOriginsString.Contains(origin) || _allowedOriginsRegex.Any(x => x.IsMatch(origin));
        }
    }
}