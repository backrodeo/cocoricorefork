using System.Threading.Tasks;

namespace CocoriCore
{
    //TODO est-ce que ici avoir le param�tre g�n�rique TContexte apporte vraiement qq chose ?
    public interface IHandlerDefinition<TContext> //: IHandlerDefinition
    {
        Task ProcessAsync(TContext context, IUnitOfWork unitOfWork);
    }
}