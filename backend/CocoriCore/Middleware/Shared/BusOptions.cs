using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class BusOptions<TContext, TRule> : Dictionary<string, object>
        where TRule : BusRule<TContext>, new()
    {
        protected List<KeyValuePair<Type, List<TRule>>> _typeRuleEntries;
        public FireAndForgetExceptionHandler FireAndForgetExceptionHandler { get; set; }
        public bool AllowMultipleRuleForType { get; set; }
        public bool ExceptionIfNoRuleDefined { get; set; }

        public BusOptions()
        {
            _typeRuleEntries = new List<KeyValuePair<Type, List<TRule>>>();
            AllowMultipleRuleForType = true;
            ExceptionIfNoRuleDefined = true;
        }

        public virtual Type GetTargetType(TContext context)
        {
            return context.GetType();
        }

        public TRule AddRule(Type targetType)
        {
            KeyValuePair<Type, List<TRule>> entry;
            var entryExists = TryGetEntry(targetType, out entry);
            if (entryExists && !AllowMultipleRuleForType)
            {
                throw new ConfigurationException($"Can't add a rule for type {targetType.FullName}"
                    + $" because a previous one is already defined and this rule doesn't allow multiple handlers.");
            }
            if (!entryExists)
            {
                entry = new KeyValuePair<Type, List<TRule>>(targetType, new List<TRule>());
                _typeRuleEntries.Add(entry);
            }
            var rule = new TRule();
            rule.TargetType = targetType;
            entry.Value.Add(rule);
            return rule;
        }

        private bool TryGetEntry(Type targetType, out KeyValuePair<Type, List<TRule>> existingEntry)
        {
            foreach (var entry in _typeRuleEntries)
            {
                if (entry.Key == targetType)
                {
                    existingEntry = entry;
                    return true;
                }
            }
            existingEntry = default(KeyValuePair<Type, List<TRule>>);
            return false;
        }

        public virtual void ValidateConfiguration()
        {
            var atLeastOneRuleInFireAndForget = _typeRuleEntries
                .SelectMany(x => x.Value)
                .Any(x => x.FireAndForget);
            if (FireAndForgetExceptionHandler == null && atLeastOneRuleInFireAndForget)
            {
                throw new ConfigurationException($"No fire and forget exception handler defined and at least one rule"
                    + $" is configured with .FireAndForget()."
                    + $" Please use SetFireAndForgetExceptionHandler() to define one.");
            }

            var rulesTypes = _typeRuleEntries.Select(x => x.Key).ToArray();
            for (var i = 0; i < rulesTypes.Length; i++)
            {
                var cuurentType = rulesTypes[i];
                for (var j = i + 1; j < rulesTypes.Length; j++)
                {
                    var type = rulesTypes[j];
                    if (cuurentType.IsAssignableFrom(type))
                    {
                        throw new ConfigurationException($"Rule for {cuurentType.FullName} is less "
                                 + $"specific than rule for {type.FullName} and should be defined after.");
                    }
                }
            }
        }

        public IEnumerable<TRule> GetMatchingRules(Type type, TContext context)
        {
            return _typeRuleEntries
                .Where(x => type.IsAssignableTo(x.Key) || type.IsAssignableToGeneric(x.Key))
                .SelectMany(x => x.Value)
                .Where(x => x.MatchesCondition(context));
        }
    }

    public class FireAndForgetExceptionHandler
    {
        public Type Type;
        public Func<object, Exception, Task> HandleAsync;

        public FireAndForgetExceptionHandler(Type type, Func<object, Exception, Task> handleFunc)
        {
            Type = type;
            HandleAsync = handleFunc;
        }
    }
}