using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore
{
    // TODO : renommer Bus Base/Options/OptionsBuilder/Rule/Context => Hub Base/Options/OptionsBuilder/Rule/Context
    // pour tout les Bus (error, httpresponse, httperror, repository, ... ?)
    public abstract class BusBase<TContext, TRule, TOptions>
        where TOptions : BusOptions<TContext, TRule>
        where TRule : BusRule<TContext>, new()
    {
        protected TOptions Options { get; private set; }
        protected IUnitOfWork _unitOfWork;
        protected IUnitOfWorkFactory _unitOfWorkFactory;
        protected List<Task> _fireAndForgetTasks;

        public BusBase(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, TOptions options)
        {
            Options = options;
            _unitOfWorkFactory = unitOfWorkFactory;
            _unitOfWork = unitOfWork;
            _fireAndForgetTasks = new List<Task>();
        }

        public IEnumerable<Task> FireAndForgetTasks { get => _fireAndForgetTasks; }

        public virtual async Task HandleAsync(TContext context)
        {
            await HandleInternalAsync(context);
        }

        protected virtual async Task HandleInternalAsync(TContext context)
        {
            var targetType = Options.GetTargetType(context);
            var mathcingRules = Options.GetMatchingRules(targetType, context);
            var lastRule = await ExecuteRules(mathcingRules, context);
            if (lastRule == null && Options.ExceptionIfNoRuleDefined)
            {
                throw new ConfigurationException($"No rule found for {targetType.FullName}");
            }
        }

        private async Task<TRule> ExecuteRules(IEnumerable<TRule> rules, TContext context)
        {
            TRule lastRule = null;
            foreach (var rule in rules)
            {
                lastRule = rule;

                if (rule.FireAndForget)
                {
                    _fireAndForgetTasks.Add(ProcessRuleFireAndForget(context, rule));
                }
                else
                {
                    await rule.ProcessAsync(context, _unitOfWork);
                }
                if (rule.StopPropagation) break;
            }
            return lastRule;
        }

        private async Task ProcessRuleFireAndForget(TContext context, TRule rule)
        {
            try
            {
                using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
                {
                    try
                    {
                        await rule.ProcessAsync(context, unitOfWork);
                        await unitOfWork.FinishAsync();
                    }
                    catch (Exception exception)
                    {
                        //TODO ajouter des infos sur la rule dans l'exception, pour le context on ne peut pas le mettre directement car �a peut etre la requ�te http.
                        //au final faire en sorte qu'on sache les param�tres entr�e, l'heure etc
                        //Faire de m�me pour la version synchrone
                        var errorOption = Options.FireAndForgetExceptionHandler;
                        var errorHandlerInstance = _unitOfWork.Resolve(errorOption.Type);
                        await errorOption.HandleAsync(errorHandlerInstance, exception);
                    }
                }
            }
            catch (Exception e)//TODO tester manuellement
            {
                await Console.Error.WriteLineAsync(e.ToString());
            }
        }
    }
}