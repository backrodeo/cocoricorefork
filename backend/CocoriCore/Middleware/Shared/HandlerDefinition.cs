using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HandlerDefinition<TContext> : IHandlerDefinition<TContext>
    {
        public Type Type { get; }
        public Func<object, TContext, IUnitOfWork, Task> Action { get; }

        public HandlerDefinition(Type type, Func<object, TContext, IUnitOfWork, Task> action)
        {
            Type = type;
            Action = action;
        }

        public async Task ProcessAsync(TContext context, IUnitOfWork unitOfWork)
        {
            var handlerType = Type;
            if (Type.IsGenericTypeDefinition && context is IGenericContext genericContext)
                handlerType = Type.MakeGenericType(genericContext.GetGenericParameters(handlerType));
            var handler = unitOfWork.Resolve(handlerType);
            await Action(handler, context, unitOfWork);
        }

        public override string ToString()
        {
            return $"HandlerDefinition<{Type.FullName}>";
        }
    }
}