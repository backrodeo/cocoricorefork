using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public interface IRouter
    {
        IRoute TryFindRoute(HttpRequest request);
        Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request, IRoute route);
    }
}