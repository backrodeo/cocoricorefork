﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRepositoryBus
    {
        Task ApplyRulesAsync();
    }
}