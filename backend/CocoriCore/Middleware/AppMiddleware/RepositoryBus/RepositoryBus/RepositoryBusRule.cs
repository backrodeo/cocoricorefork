using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class RepositoryBusRule : BusRule<RepositoryBusContext>
    {
        public RepositoryBusRule()
        {
            AllowMultipleHandler = true;
        }

        public RepositoryBusRule Call<THandler>(CUDOperation operation)
            where THandler : class, IRepositoryBusHandler
        {
            return Call<THandler>((h, c) => h.ExecuteAsync(c), operation);
        }

        public RepositoryBusRule Call<T>(Func<T, Func<RepositoryBusContext, Task>> action, CUDOperation operation)
            where T : class
        {
            return Call<T>((t, e) => action(t)(e), operation);
        }

        public RepositoryBusRule Call<T>(Func<T, RepositoryBusContext, Task> action, CUDOperation operation)
            where T : class
        {
            AddHandler<T>(action);
            SetCondition(c =>
            {
                return c.Operation == operation;
            });
            return this;
        }

        public override async Task ProcessHandlerAsync(IHandlerDefinition<RepositoryBusContext> handler,
            RepositoryBusContext context, IUnitOfWork unitOfWork)
        {
            try
            {
                await base.ProcessHandlerAsync(handler, context, unitOfWork);
            }
            catch (Exception exception)
            {
                var errorMessage = $"Exception occured while applying rule for {context?.Operation}"
                        + $" entity of type {context?.Entity?.GetType()?.FullName}.";
                var repositoryBusException = new RepositoryBusException(errorMessage, exception);
                repositoryBusException.Entity = context.Entity;
                repositoryBusException.EntityId = context.Entity.Id;
                repositoryBusException.Operation = context.Operation;
                repositoryBusException.State = context.State;
                repositoryBusException.Handler = handler.ToString();
                throw repositoryBusException;
            }
        }
    }
}