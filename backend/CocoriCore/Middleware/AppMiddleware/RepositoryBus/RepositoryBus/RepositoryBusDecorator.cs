using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class RepositoryBusDecorator : IRepository
    {
        private IRepository _repository;
        private IRepositoryBusContextStore _contextStore;
        private IUIDProvider _UIDProvider;

        public RepositoryBusDecorator(IRepository repository, IRepositoryBusContextStore contextStore, IUIDProvider UIDProvider)
        {
            _repository = repository;
            _contextStore = contextStore;
            _UIDProvider = UIDProvider;
        }

        public async Task InsertAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            await _repository.InsertAsync(entity);
            _contextStore.NotifyInsert(entity);
        }

        public async Task UpdateAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            await _repository.UpdateAsync(entity);
            _contextStore.NotifyUpdate(entity);
        }

        public async Task DeleteAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            await _repository.DeleteAsync(entity);
            _contextStore.NotifyDelete(entity);
        }

        public async Task<TEntity> LoadAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            var entity = await _repository.LoadAsync<TEntity>(id);
            return _contextStore.NotifyLoad(entity);
        }

        public async Task<object> LoadAsync(Type type, Guid id)
        {
            var entity = (IEntity)await _repository.LoadAsync(type, id);
            return _contextStore.NotifyLoad(entity);
        }

        async Task<TEntity> IRepository.LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
        {
            var entity = await _repository.LoadAsync(uniqueMember, value);
            return _contextStore.NotifyLoad(entity);
        }

        public async Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var entities = await _repository.LoadAsync(type, ids);
            return entities
                .Select(x => _contextStore.NotifyLoad(x))
                .ToArray();
        }

        public Task<bool> ExistsAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return _repository.ExistsAsync<TEntity>(id);
        }

        public Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return _repository.ExistsAsync(entityType, id);
        }

        public Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            return _repository.ExistsAsync(entityType, member, value);
        }

        public Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            return _repository.ExistsAsync(entityType, collection);
        }

        public IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            return _contextStore.NotifyQuery(_repository.Query<TEntity>());
        }

        public IQueryable<IEntity> Query(Type type)
        {
            return _contextStore.NotifyQuery(_repository.Query(type));
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}