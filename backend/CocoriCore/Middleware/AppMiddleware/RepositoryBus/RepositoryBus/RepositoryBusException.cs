using System;

namespace CocoriCore
{
    public class RepositoryBusException : Exception
    {
        public object Entity { get; set; }
        public object EntityId { get; set; }
        public CUDOperation Operation { get; set; }
        public IState State { get; set; }
        public string Handler { get; set; }

        public RepositoryBusException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}