using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class DeleteRuleBuidler<TEntity>
        where TEntity : class, IEntity
    {
        private RepositoryBusRule _rule;
        public DeleteRuleBuidler(RepositoryBusRule rule)
        {
            _rule = rule;
        }

        public DeleteRuleBuidler<TEntity> Call<THandler>()
            where THandler : class, IRepositoryBusHandler
        {
            _rule.Call<THandler>(CUDOperation.DELETE);
            return this;
        }

        public DeleteRuleBuidler<TEntity> Call<T>(Func<T, Func<RepositoryBusContext, Task>> action)
            where T : class
        {
            _rule.Call<T>(action, CUDOperation.DELETE);
            return this;
        }

        public DeleteRuleBuidler<TEntity> Call<T>(Func<T, RepositoryBusContext, Task> action)
            where T : class
        {
            _rule.Call<T>(action, CUDOperation.DELETE);
            return this;
        }

        public DeleteRuleBuidler<TEntity> FireAndForget()
        {
            _rule.FireAndForget = true;
            return this;
        }
    }
}