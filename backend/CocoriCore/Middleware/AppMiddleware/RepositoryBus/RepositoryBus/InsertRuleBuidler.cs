using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class InsertRuleBuidler<TEntity>
        where TEntity : class, IEntity
    {
        public RepositoryBusRule Rule { get; }
        public InsertRuleBuidler(RepositoryBusRule rule)
        {
            Rule = rule;
        }

        public InsertRuleBuidler<TEntity> Call<THandler>()
            where THandler : class, IRepositoryBusHandler
        {
            Rule.Call<THandler>(CUDOperation.CREATE);
            return this;
        }

        public InsertRuleBuidler<TEntity> Call<T>(Func<T, Func<RepositoryBusContext, Task>> action)
            where T : class
        {
            Rule.Call<T>(action, CUDOperation.CREATE);
            return this;
        }

        public InsertRuleBuidler<TEntity> Call<T>(Func<T, RepositoryBusContext, Task> action)
            where T : class
        {
            Rule.Call<T>(action, CUDOperation.CREATE);
            return this;
        }

        public InsertRuleBuidler<TEntity> FireAndForget()
        {
            Rule.FireAndForget = true;
            return this;
        }
    }
}