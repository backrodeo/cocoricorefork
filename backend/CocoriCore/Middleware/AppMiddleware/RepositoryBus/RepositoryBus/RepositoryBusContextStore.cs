using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class RepositoryBusContextStore : IRepositoryBusContextStore
    {
        private List<RepositoryBusContextContainer> _containers;
        private IUIDProvider _UIDProvider;
        private IStateStore _stateStore;

        public RepositoryBusContextStore(IUIDProvider UIDProvider, IStateStore stateStore)
        {
            _UIDProvider = UIDProvider;
            _stateStore = stateStore;
            _containers = new List<RepositoryBusContextContainer>();
            CreateNewContainer();
        }

        public virtual IEnumerable<RepositoryBusContext> PendingContexts => CurrentContainer.PendingContexts.Values;

        protected virtual RepositoryBusContextContainer CurrentContainer => _containers.Last();

        public virtual void CreateNewContainer()
        {
            _containers.Add(new RepositoryBusContextContainer(_UIDProvider));
        }

        public virtual void Clear()
        {
            CurrentContainer.Clear();
        }

        public virtual Task Notify(RepositoryBusContext context)
        {
            if (context.Entity.Id == default)
            {
                throw new ArgumentException($"The provider entity must have an id assigned before calling {nameof(Notify)}().");
            }
            if (context.Operation == CUDOperation.UPDATE && !context.State.HasChanged(context.Entity))
            {
                return Task.CompletedTask;
            }
            var uid = new EntityUID(context.Entity.GetType(), context.Entity.Id);
            var existingContext = CurrentContainer.PendingContexts.TryGetValue(uid, null);
            if (existingContext != null && existingContext.Operation == CUDOperation.CREATE)
            {
                context.Operation = CUDOperation.CREATE;
            }
            //TODO si on fait une insert et ensuite un delete alors enlever le context ?
            CurrentContainer.PendingContexts[uid] = context;
            return Task.CompletedTask;
        }

        public virtual void NotifyInsert(IEntity entity)
        {
            CreateAndAddContext(CUDOperation.CREATE, entity);
            _stateStore.SaveState(entity);
        }

        public virtual void NotifyUpdate(IEntity entity)
        {
            CreateAndAddContext(CUDOperation.UPDATE, entity);
        }

        public virtual void NotifyDelete(IEntity entity)
        {
            CreateAndAddContext(CUDOperation.DELETE, entity);
        }

        public virtual TEntity NotifyLoad<TEntity>(TEntity entity) where TEntity : IEntity
        {
            _stateStore.SaveState(entity);
            return entity;
        }

        public virtual IQueryable<T> NotifyQuery<T>(IQueryable<T> queryable)
        {
            return new ExtendedEnumerable<T>(queryable, e => _stateStore.SaveState(e), "save_state");
        }

        protected virtual void CreateAndAddContext(CUDOperation operation, IEntity entity)
        {
            var context = new RepositoryBusContext();
            context.Operation = operation;
            context.Entity = entity;
            var entityHasState = _stateStore.HasState(entity);
            if (!entityHasState && operation != CUDOperation.CREATE)
                throw new InvalidOperationException("No state associated with this entity, ensure you have loaded it before any update or delete.");
            //TODO ne plus mettre le state dans le context mais utiliser le stateStore directement ?
            context.State = entityHasState ? _stateStore.GetState(entity) : null;
            Notify(context);
        }
    }

    public class RepositoryBusContextContainer
    {
        public Dictionary<EntityUID, RepositoryBusContext> PendingContexts { get; }

        public RepositoryBusContextContainer(IUIDProvider UIDProvider)
        {
            PendingContexts = new Dictionary<EntityUID, RepositoryBusContext>();
        }

        public void Clear()
        {
            PendingContexts.Clear();
        }
    }
}