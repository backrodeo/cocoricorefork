using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class RepositoryBusOptionsBuilder : BusOptionsBuilder<RepositoryBusContext, RepositoryBusRule, RepositoryBusOptions>
    {
        public RepositoryBusOptionsBuilder()
        {
            _options.ExceptionIfNoRuleDefined = false;
        }

        public new RepositoryBusOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler<THandler>(unexpectedExceptionHandler);
            return this;
        }

        protected override RepositoryBusRule AddRule(Type targetType)
        {
            VirtualPropertyChecker.CheckAllVirtualSetter(targetType);
            return base.AddRule(targetType);
        }

        public InsertRuleBuidler<TEntity> WhenInsert<TEntity>()
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new InsertRuleBuidler<TEntity>(rule);
        }

        public UpdateRuleBuidler<TEntity> WhenUpdate<TEntity>()
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new UpdateRuleBuidler<TEntity>(rule);
        }

        public DeleteRuleBuidler<TEntity> WhenDelete<TEntity>()
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new DeleteRuleBuidler<TEntity>(rule);
        }

        public ModifyRuleBuidler<TEntity> WhenModify<TEntity>()
            where TEntity : class, IEntity
        {
            var insertRule = AddRule(typeof(TEntity));
            var updateRule = AddRule(typeof(TEntity));
            var deleteRule = AddRule(typeof(TEntity));
            return new ModifyRuleBuidler<TEntity>(insertRule, updateRule, deleteRule);
        }
    }
}