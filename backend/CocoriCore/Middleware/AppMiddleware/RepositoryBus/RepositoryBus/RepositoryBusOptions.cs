using System;

namespace CocoriCore
{
    public class RepositoryBusOptions : BusOptions<RepositoryBusContext, RepositoryBusRule>
    {
        public override Type GetTargetType(RepositoryBusContext context)
        {
            return context.Entity.GetType();
        }
    }
}