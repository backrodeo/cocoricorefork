using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ModifyRuleBuidler<TEntity>
        where TEntity : class, IEntity
    {
        private RepositoryBusRule _insertRule;
        private RepositoryBusRule _updateRule;
        private RepositoryBusRule _deleteRule;
        public ModifyRuleBuidler(RepositoryBusRule insertRule, RepositoryBusRule updateRule, RepositoryBusRule deleteRule)
        {
            _insertRule = insertRule;
            _updateRule = updateRule;
            _deleteRule = deleteRule;
        }

        public ModifyRuleBuidler<TEntity> Call<THandler>()
            where THandler : class, IRepositoryBusHandler
        {
            _insertRule.Call<THandler>(CUDOperation.CREATE);
            _updateRule.Call<THandler>(CUDOperation.UPDATE);
            _deleteRule.Call<THandler>(CUDOperation.DELETE);
            return this;
        }

        public ModifyRuleBuidler<TEntity> Call<T>(Func<T, Func<RepositoryBusContext, Task>> action)
            where T : class
        {
            _insertRule.Call<T>(action, CUDOperation.CREATE);
            _updateRule.Call<T>(action, CUDOperation.UPDATE);
            _deleteRule.Call<T>(action, CUDOperation.DELETE);
            return this;
        }

        public ModifyRuleBuidler<TEntity> Call<T>(Func<T, RepositoryBusContext, Task> action)
            where T : class
        {
            _insertRule.Call<T>(action, CUDOperation.CREATE);
            _updateRule.Call<T>(action, CUDOperation.UPDATE);
            _deleteRule.Call<T>(action, CUDOperation.DELETE);
            return this;
        }

        public ModifyRuleBuidler<TEntity> FireAndForget()
        {
            _insertRule.FireAndForget = true;
            _updateRule.FireAndForget = true;
            _deleteRule.FireAndForget = true;
            return this;
        }
    }
}