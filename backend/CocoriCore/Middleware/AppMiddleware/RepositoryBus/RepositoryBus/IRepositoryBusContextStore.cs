﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRepositoryBusContextStore
    {
        IEnumerable<RepositoryBusContext> PendingContexts { get; }

        void Clear();
        void CreateNewContainer();
        Task Notify(RepositoryBusContext context);
        void NotifyDelete(IEntity entity);
        void NotifyInsert(IEntity entity);
        TEntity NotifyLoad<TEntity>(TEntity entity) where TEntity : IEntity;
        IQueryable<T> NotifyQuery<T>(IQueryable<T> queryable);
        void NotifyUpdate(IEntity entity);
    }
}