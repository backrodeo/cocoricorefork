using System;

namespace CocoriCore
{
    public static class VirtualPropertyChecker
    {
        public static void CheckAllVirtualSetter(Type type)
        {
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (property.CanWrite && !property.GetSetMethod().IsVirtual)
                {
                    throw new ArgumentException($"The setter of property {property.Name} for type {type} must be virtual.");
                }
            }
        }
    }
}