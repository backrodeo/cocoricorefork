using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class UpdateRuleBuidler<TEntity>
        where TEntity : class, IEntity
    {
        private RepositoryBusRule _rule;
        public UpdateRuleBuidler(RepositoryBusRule rule)
        {
            _rule = rule;
        }

        public UpdateRuleBuidler<TEntity> Call<THandler>()
            where THandler : class, IRepositoryBusHandler
        {
            _rule.Call<THandler>(CUDOperation.UPDATE);
            return this;
        }

        public UpdateRuleBuidler<TEntity> Call<T>(Func<T, Func<RepositoryBusContext, Task>> action)
            where T : class
        {
            _rule.Call<T>(action, CUDOperation.UPDATE);
            return this;
        }

        public UpdateRuleBuidler<TEntity> Call<T>(Func<T, RepositoryBusContext, Task> action)
            where T : class
        {
            _rule.Call<T>(action, CUDOperation.UPDATE);
            return this;
        }

        public UpdateRuleBuidler<TEntity> FireAndForget()
        {
            _rule.FireAndForget = true;
            return this;
        }
    }
}