using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRepositoryBusHandler
    {
        Task ExecuteAsync(RepositoryBusContext context);
    }
}