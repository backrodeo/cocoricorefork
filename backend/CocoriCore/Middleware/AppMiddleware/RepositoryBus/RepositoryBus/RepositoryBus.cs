using System.Threading.Tasks;

namespace CocoriCore
{
    public class RepositoryBus : BusBase<RepositoryBusContext, RepositoryBusRule, RepositoryBusOptions>, IRepositoryBus
    {
        private IRepositoryBusContextStore _contextStore;
        public RepositoryBus(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, 
            RepositoryBusOptions options, IRepositoryBusContextStore contextStore)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
            _contextStore = contextStore;
        }

        public async Task ApplyRulesAsync()
        {
            foreach (var context in _contextStore.PendingContexts)
            {
                await base.HandleAsync(context);
            }
        }
    }
}