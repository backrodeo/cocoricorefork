namespace CocoriCore
{
    public class RepositoryBusContext
    {
        public string TraceId { get; }
        public CUDOperation Operation { get; set; }
        public IEntity Entity { get; set; }
        public IState State { get; set; }

        public State<TEntity> GetState<TEntity>()
            where TEntity : class, IEntity
        {
            return State?.Cast<TEntity>();
        }

        public TEntity GetEntity<TEntity>()
                  where TEntity : IEntity
        {
            return (TEntity)Entity;
        }
    }
}