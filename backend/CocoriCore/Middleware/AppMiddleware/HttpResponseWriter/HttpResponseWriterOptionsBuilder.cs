using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpResponseWriterOptionsBuilder : BusOptionsBuilder<HttpResponseWriterContext, HttpResponseWriterRule, HttpResponseWriterOptions>
    {
        public HttpResponseWriterOptionsBuilder()
        { }

        public new HttpResponseWriterOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler<THandler>(unexpectedExceptionHandler);
            return this;
        }

        public HttpResponseWriterRule For<TResponse>()
        {
            var rule = AddRule<TResponse>();
            rule.StopPropagation = true;
            return rule;
        }
    }
}