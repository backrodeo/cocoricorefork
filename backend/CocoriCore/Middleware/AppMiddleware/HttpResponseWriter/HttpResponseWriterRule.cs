using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpResponseWriterRule : BusRule<HttpResponseWriterContext>
    {
        public HttpResponseWriterRule()
        {
            AllowMultipleHandler = false;
        }

        public HttpResponseWriterRule Call<THandler>()
            where THandler : class, IHttpReponseWriterHandler
        {
            return Call<THandler>((handler, context) => handler.WriteResponseAsync(context));
        }

        public HttpResponseWriterRule Call<T>(Func<T, Func<HttpResponseWriterContext, Task>> action)
            where T : class
        {
            return Call<T>((handler, context) => action(handler)(context));
        }

        public HttpResponseWriterRule Call<T>(Func<T, HttpResponseWriterContext, Task> action)
            where T : class
        {
            AddHandler<T>(action);
            return this;
        }
    }
}