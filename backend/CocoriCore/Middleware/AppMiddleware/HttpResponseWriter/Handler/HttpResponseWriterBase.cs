﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class HttpResponseWriterBase
    {
        protected JsonSerializer _jsonSerializer;

        public HttpResponseWriterBase(JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public async Task WriteResponseAsync(HttpResponse httpResponse, object obj)
        {
            using (var stringWriter = new StringWriter())
            {
                httpResponse.ContentType = "application/json; charset=utf-8";
                _jsonSerializer.Serialize(stringWriter, obj);
                await httpResponse.WriteAsync(stringWriter.ToString());
            }
        }
    }
}