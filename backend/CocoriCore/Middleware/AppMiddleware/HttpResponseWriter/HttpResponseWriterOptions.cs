using System;

namespace CocoriCore
{
    public class HttpResponseWriterOptions : BusOptions<HttpResponseWriterContext, HttpResponseWriterRule>
    {
        public HttpResponseWriterOptions()
        {
            AllowMultipleRuleForType = false;
        }

        public override Type GetTargetType(HttpResponseWriterContext context)
        {
            var targetType = context?.Response?.GetType();
            return targetType ?? typeof(object);
        }
    }
}