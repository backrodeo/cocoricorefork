namespace CocoriCore
{
    public class DefaultHttpResponseWriterOptionsBuilder : HttpResponseWriterOptionsBuilder
    {
        public DefaultHttpResponseWriterOptionsBuilder()
        {
            //TODO rendre plus clair avec qq chose comme ca : StopAfterFirstRule = true;

            For<FileResponse>().Call<HttpResponseWriterFileHandler>();
            For<object>().Call<HttpResponseWriterDefaultHandler>();
        }
    }
}