﻿namespace CocoriCore
{
    public static class HttpResponseWriterOptionsBuilderExtension
    {
        public static HttpResponseWriterOptionsBuilder UseErrorBusForFireAndForgetException(this HttpResponseWriterOptionsBuilder builder)
        {
            builder.SetFireAndForgetExceptionHandler<IErrorBus>((b, e) => b.HandleAsync(e));
            return builder;
        }
    }
}