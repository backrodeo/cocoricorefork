using System;

namespace CocoriCore
{
    public interface IRoute
    {
        Type MessageType { get; }
    }
}