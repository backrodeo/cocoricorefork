using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CocoriCore
{
    public class JWTSerializer : JsonSerializer
    {
        protected JWTContractResolver _contractResolver;

        public JWTSerializer()
        {
            _contractResolver = new JWTContractResolver();
            ContractResolver = _contractResolver;
        }

        protected void DefineFeatureConverter<T>(Func<string, T> readJson, Func<T, string> writeJson)
        {
            //TODO : si on peut éviter de devoir définir ceci en se basant sur les opérateurs implicit/excplicite/enum/constructeur
            var featureConverter = new FeatureConverter(typeof(T), x => readJson((string)x), x => writeJson((T)x));
            Converters.Add(featureConverter);
        }
    }

    public class JWTContractResolver : CamelCasePropertyNamesContractResolver
    {
        //TODO rendre configurable le renommage des propriétés
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var jsonProperty = base.CreateProperty(member, memberSerialization);
            if (member.DeclaringType.IsAssignableTo<AccessClaims>() && member.Name == nameof(AccessClaims.UserId))
            {
                jsonProperty.PropertyName = "usid";
                jsonProperty.Converter = new GuidConverter();
            }
            if (member.DeclaringType.IsAssignableTo<IWithFeatures>() && member.Name == nameof(IWithFeatures<object>.Features))
            {
                jsonProperty.PropertyName = "feat";
            }
            if (member.DeclaringType.IsAssignableTo<IClaims>() && member.Name == nameof(IClaims.ExpireAt))
            {
                jsonProperty.PropertyName = "exp";
                jsonProperty.Converter = new UnixDateTimeConverter();
            }
            return jsonProperty;
        }
    }

    public class GuidConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Guid);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var base64Guid = (string)reader.Value;
            return new Guid(Convert.FromBase64String(base64Guid));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var guid = (Guid)value;
            var guidBytes = guid.ToByteArray();
            writer.WriteValue(Convert.ToBase64String(guidBytes));
        }
    }

    public class FeatureConverter : JsonConverter
    {
        private Type _featureType;
        private Func<object, object> _readJson;
        private Func<object, object> _writeJson;

        public FeatureConverter(Type featureType, Func<object, object> readJson, Func<object, object> writeJson)
        {
            _featureType = featureType;
            _readJson = readJson;
            _writeJson = writeJson;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == _featureType;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return _readJson(reader.Value);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(_writeJson(value));
        }
    }

    public class UnixDateTimeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return DateTimeOffset.FromUnixTimeSeconds((long)reader.Value).UtcDateTime;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            DateTimeOffset dateTimeOffset = (DateTime)value;
            writer.WriteValue(dateTimeOffset.ToUnixTimeSeconds());
        }
    }
}