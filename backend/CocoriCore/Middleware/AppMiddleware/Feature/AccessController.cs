﻿using CocoriCore.Middleware.Authentication;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    //TODO suppr, trop complexe à réutiliser
    public class AccessController
    {
        private IClaimsProvider _claimsProvider;
        private FeatureOptions _featureOptions;
        private IAuthenticationController _authenticationController;

        public AccessController(IClaimsProvider claimsProvider, FeatureOptions featureOptions,
            IAuthenticationController authenticationController)
        {
            _claimsProvider = claimsProvider;
            _featureOptions = featureOptions;
            _authenticationController = authenticationController;
        }

        public virtual Task CheckUserAccesRights(IMessage message)
        {
            var messageType = message.GetType();
            return CheckUserAccesRights(messageType);
        }

        public virtual Task CheckUserAccesRights(Type messageType)
        {
            if (_authenticationController.RequireAuthentication(messageType))
            {
                //TODO ca va pas marche sauf si on utilise castle proxy pour instancier une interface
                var claims = _claimsProvider.GetClaims<IWithFeatures>();
                var messageFeatures = _featureOptions.GetFeatures(messageType);
                if (messageFeatures.Count() > 0 && messageFeatures.Intersect(claims.Features).Count() == 0)
                {
                    var errorCause = string.Empty;
                    if (claims.Features.Count() == 0)
                    {
                        errorCause = $"Claims's features are empty.";
                    }
                    else
                    {
                        errorCause = $"User must have any of features : [{string.Join(", ", messageFeatures)}].";
                    }
                    throw new ForbiddenAccessException($"Access forbidden : {errorCause}");//TODO traduire en erreur http sans reprendre le message (en mettre un générique), mettre détails dans l'erreur si débug 
                }
            }
            return Task.CompletedTask;
        }
    }
}
