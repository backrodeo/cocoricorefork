using System.Collections.Generic;

namespace CocoriCore
{
    public interface IWithFeatures
    {
        IEnumerable<object> Features { get; }
    }

    public interface IWithFeatures<T> : IWithFeatures
    {
        new IEnumerable<T> Features { get; set; }
    }
}