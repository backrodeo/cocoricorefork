using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class AppMiddleware : IMiddleware
    {
        private IRouter _router;
        private IAuthenticationController _authenticationController;

        public AppMiddleware(IRouter router, IAuthenticationController authenticationController)
        {
            _router = router;
            _authenticationController = authenticationController;
        }

        public virtual async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var unitOfWork = context.GetUnitOfWork();
            var route = _router.TryFindRoute(context.Request);
            if (route != null)
            {
                if (_authenticationController.RequireAuthentication(route.MessageType))
                {
                    var authenticator = unitOfWork.Resolve<IHttpAuthenticator>();
                    await authenticator.AuthenticateAsync(context);
                }
                var messageBus = unitOfWork.Resolve<IMessageBus>();
                var message = await _router.DeserializeMessageAsync<IMessage>(context.Request, route);
                var response = await messageBus.ExecuteAsync(message);
                var responseWriter = unitOfWork.Resolve<IHttpResponseWriter>();
                await responseWriter.WriteResponseAsync(response, context.Response);
            }
            else
            {
                await next(context);
            }
        }
    }
}