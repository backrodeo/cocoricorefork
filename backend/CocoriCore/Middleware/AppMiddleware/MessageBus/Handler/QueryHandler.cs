using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class QueryHandler<TQuery, TResponse> : IHandler<TQuery, TResponse>, IQueryHandler
        where TQuery : IQuery
    {
        public async Task<object> HandleAsync(IMessage query)
        {
            return await ExecuteAsync((TQuery)query);
        }

        public abstract Task<TResponse> ExecuteAsync(TQuery query);
    }
}