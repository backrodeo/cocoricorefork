using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class CreateCommandHandler<TCommand> : IHandler<TCommand, Guid>, ICommandHandler
        where TCommand : ICommand
    {
        public async Task<object> HandleAsync(IMessage command)
        {
            return await ExecuteAsync((TCommand)command);
        }

        public abstract Task<Guid> ExecuteAsync(TCommand command);
    }
}