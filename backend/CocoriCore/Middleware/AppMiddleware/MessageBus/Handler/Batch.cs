﻿using System.Collections.Generic;

namespace CocoriCore
{
    public class Batch<TCommand> : List<TCommand>, ICommand
        where TCommand : ICommand
    {
    }
}
