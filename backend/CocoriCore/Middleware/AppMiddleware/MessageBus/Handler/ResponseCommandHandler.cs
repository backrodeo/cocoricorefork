using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class ResponseCommandHandler<TCommand, TResponse> : IHandler<TCommand, TResponse>, ICommandHandler
        where TCommand : ICommand
    {
        public async Task<object> HandleAsync(IMessage command)
        {
            return await ExecuteAsync((TCommand)command);
        }

        public abstract Task<TResponse> ExecuteAsync(TCommand command);
    }
}