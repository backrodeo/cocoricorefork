using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class VoidQueryHandler<TQuery> : IHandler<TQuery>, IQueryHandler
        where TQuery : IQuery
    {
        public async Task<object> HandleAsync(IMessage query)
        {
            await ExecuteAsync((TQuery)query);
            return null;
        }

        public abstract Task ExecuteAsync(TQuery query);
    }
}