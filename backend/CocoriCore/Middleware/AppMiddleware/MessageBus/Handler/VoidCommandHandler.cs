using System.Threading.Tasks;

namespace CocoriCore
{
    public abstract class VoidCommandHandler<TCommand> : IHandler<TCommand>, ICommandHandler
        where TCommand : ICommand
    {
        public async Task<object> HandleAsync(IMessage command)
        {
            await ExecuteAsync((TCommand)command);
            return null;
        }

        public abstract Task ExecuteAsync(TCommand command);
    }
}