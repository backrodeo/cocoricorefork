﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IMessageBus
    {
        Task<object> ExecuteAsync(IMessage message);
    }
}