using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class MessageBus : BusBase<MessageBusContext, MessageBusRule, MessageBusOptions>, IMessageBus
    {
        private static readonly object DefaultResponse = new object();

        public MessageBus(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, MessageBusOptions options)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
        }

        public async Task<object> ExecuteAsync(IMessage message)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            try
            {
                MessageBusContext context = new MessageBusContext()
                {
                    Message = message,
                    Response = DefaultResponse
                };
                await base.HandleAsync(context);
                if (context.Response == DefaultResponse)
                    throw new ConfigurationException("Response has not been set, ensure you have "
                        + $"defined a handler for message of type {message?.GetType()}.");
                return context.Response;
            }
            catch(Exception exception)
            {
                exception.Data[GetType().Name] = new
                {
                    MessageType = message?.GetType()?.FullName,
                    Message = message
                };                
                throw;
            }
        }

        // TODO : si on configure la dérivation pour : 
        //      quand j'insert une entitéA, je projette dans projectionA en FnF
        //      quand j'insert une entitéA, je projette dans projectionB (pas Fnf) 
        // si la projectionB synchrone plante et qu'on a une seule transaction alors on rollback tout y compris l'entité
        // la projectionA elle continue de s'exécuter et ne correspondra à rien...

        // Solution 1 (pas terrible) : 
        // Dans le messageBus appeler le handler de message puis premier commit
        // Appeler la dérivation puis second commit
        
        // Solution 2 (mieu) : 
        // Le bus de dérivation préapre les taches FnF mais ne les lance pas.
        // Une fois l'unitOfWork principale terminée et le commit fait déclencher tous les traitements asynchrones préparés
    }
}