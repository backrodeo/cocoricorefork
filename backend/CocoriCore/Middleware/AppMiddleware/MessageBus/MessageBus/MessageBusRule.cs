using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class MessageBusRule : BusRule<MessageBusContext>
    {
        public MessageBusRule()
        {
            AllowMultipleHandler = true;
        }

        public MessageBusRule Call<THandler>()
            where THandler : class, IHandler
        {
            return Call<THandler>(async (handler, context) => context.Response = await handler.HandleAsync(context.Message));
        }

        public MessageBusRule Call<T>(Func<T, Func<MessageBusContext, Task>> action)
            where T : class
        {
            return Call<T>((t, e) => action(t)(e));
        }

        public MessageBusRule Call<T>(Func<T, MessageBusContext, Task> action)
            where T : class
        {
            AddHandler<T>(action);
            return this;
        }
    }
}