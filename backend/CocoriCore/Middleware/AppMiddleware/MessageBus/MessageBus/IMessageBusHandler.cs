using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IMessageBusHandler
    {
        Task ExecuteAsync(MessageBusContext context);
    }
}