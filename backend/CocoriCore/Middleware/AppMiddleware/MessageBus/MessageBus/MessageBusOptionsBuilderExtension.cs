﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public static class MessageBusOptionsBuilderExtension
    {
        public static IEnumerable<MessageBusRule> CallMessageHandlers(this IEnumerable<MessageBusRule> rules,
            params System.Reflection.Assembly[] assemblies)
        {
            if (assemblies.Count() == 0)
            {
                throw new ConfigurationException("You must pass at least one assembly.");
            }
            var messageHandlerTypes = assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t => typeof(IHandler).IsAssignableFrom(t) && t.IsConcrete())
                .ToDictionary(t =>
                {
                    var messageType = t.GetGenericArguments(typeof(IHandler<>))[0];
                    if (messageType.IsGenericParameter)
                        messageType = messageType.BaseType.GetGenericTypeDefinition();
                    if (messageType.IsGenericType)
                        messageType = messageType.GetGenericTypeDefinition();
                    return messageType;
                }, t => t);
            foreach (var rule in rules)
            {
                var messageType = rule.TargetType;
                if (messageHandlerTypes.ContainsKey(messageType))
                    rule.AddHandler(messageHandlerTypes[messageType], async (h, c, u) => c.Response = await ((IHandler)h).HandleAsync(c.Message));
            }
            return rules;
        }

        public static IEnumerable<MessageBusRule> Call<THandler>(this IEnumerable<MessageBusRule> rules,
           Func<THandler, MessageBusContext, Task> action)
               where THandler : class
        {
            foreach (var rule in rules)
            {
                rule.AddHandler<THandler>((h, c) => action(h, c));
            }
            return rules;
        }

        public static IEnumerable<MessageBusRule> Call<THandler>(this IEnumerable<MessageBusRule> rules)
            where THandler : class, IMessageBusHandler
        {
            foreach (var rule in rules)
            {
                rule.AddHandler<THandler>((h, c) => h.ExecuteAsync(c));
            }
            return rules;
        }

        public static MessageBusOptionsBuilder UseErrorBusForFireAndForgetException(this MessageBusOptionsBuilder builder)
        {
            builder.SetFireAndForgetExceptionHandler<IErrorBus>((b, e) => b.HandleAsync(e));
            return builder;
        }
    }
}