using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class MessageBusOptionsBuilder : BusOptionsBuilder<MessageBusContext, MessageBusRule, MessageBusOptions>
    {
        public new MessageBusOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler(unexpectedExceptionHandler);
            return this;
        }

        public MessageBusRule For<TMessage>()
        {
            return AddRule(typeof(TMessage));
        }

        public IEnumerable<MessageBusRule> ForAll<TMessage>(params System.Reflection.Assembly[] assemblies)
            where TMessage : IMessage
        {
            if (assemblies.Count() == 0)
            {
                throw new ConfigurationException("You must pass at least one assembly.");
            }
            var messagesTypes = assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t => typeof(TMessage).IsAssignableFrom(t) && t.IsConcrete());
            var ruleList = new List<MessageBusRule>();
            foreach (var type in messagesTypes)
            {
                MessageBusRule rule;
                if (type.IsGenericTypeDefinition)
                    rule = AddRule(type.GetGenericTypeDefinition());
                else
                    rule = AddRule(type);
                ruleList.Add(rule);
            }
            return ruleList;
        }
    }
}