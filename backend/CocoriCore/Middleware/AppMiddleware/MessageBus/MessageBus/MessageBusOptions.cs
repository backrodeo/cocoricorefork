using System;

namespace CocoriCore
{
    public class MessageBusOptions : BusOptions<MessageBusContext, MessageBusRule>
    {
        public MessageBusOptions()
        {
            this.ExceptionIfNoRuleDefined = true;
        }

        public override Type GetTargetType(MessageBusContext context)
        {
            return context.Message.GetType();
        }
    }
}