using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore
{
    public class MessageBusContext : IGenericContext
    {
        public MessageBusContext()
        {
        }

        public IMessage Message;
        public object Response;
        public Dictionary<string, object> Data;

        public Type[] GetGenericParameters(Type genericHandlerType)
        {
            var genericParameters = genericHandlerType.GetGenericArguments();
            var messageType = Message.GetType();
            var candidateTypes = messageType.GetGenericArguments().Append(messageType).ToArray();
            for (var i = 0; i < genericParameters.Length; i++)
            {
                genericParameters[i] = GetTypeForHandlerParamater(genericHandlerType, genericParameters[i], candidateTypes);
            }
            return genericParameters;
        }

        private Type GetTypeForHandlerParamater(Type handlerType, Type handlerParameter, IEnumerable<Type> candidateTypes)
        {
            foreach (var candidate in candidateTypes)
            {
                if (handlerParameter.IsGenericParameter)
                {
                    if (handlerParameter.GetInterfaces().Any(x => candidate.IsAssignableTo(x)))
                    {
                        return candidate;
                    }
                }
                if (candidate.IsAssignableTo(handlerParameter))
                {
                    return candidate;
                }
            }
            throw new InvalidOperationException($"Unable to find matching type for parameter {handlerParameter} of "
                + $"generic handler {handlerType} among [{string.Join(", ", candidateTypes)}]");
        }
    }
}