using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    //TODO à tester/finir
    public class StaticFileMiddleware : IMiddleware
    {
        private string _rootPath;

        public StaticFileMiddleware(string rootPath)
        {
            _rootPath = rootPath;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var urlPath = context.Request.Path.ToString();
            urlPath = urlPath.Substring(1, urlPath.Length - 1);
            var filePath = System.IO.Path.Combine(_rootPath, urlPath.Replace('/', System.IO.Path.DirectorySeparatorChar));
            if (System.IO.File.Exists(filePath))
                using (var fileStream = System.IO.File.OpenRead(filePath))
                {
                    await fileStream.CopyToAsync(context.Response.Body);
                }
            else
                await next(context);
        }
    }
}