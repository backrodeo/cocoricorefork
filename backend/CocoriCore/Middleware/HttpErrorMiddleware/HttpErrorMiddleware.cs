using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpErrorMiddleware : IMiddleware
    {

        public HttpErrorMiddleware()
        {
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception e)
            {
                var errorWriter = context.GetUnitOfWork().Resolve<IHttpErrorWriter>();
                await errorWriter.WriteErrorAsync(e, context.Response);
            }
        }
    }
}