﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpErrorWriter : BusBase<HttpErrorWriterContext, HttpErrorWriterRule, HttpErrorWriterOptions>, IHttpErrorWriter
    {
        public HttpErrorWriter(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, HttpErrorWriterOptions options)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
        }

        public async Task WriteErrorAsync(Exception exception, HttpResponse httpResponse)
        {
            httpResponse.StatusCode = 500;
            HttpErrorWriterContext context = new HttpErrorWriterContext()
            {
                Exception = exception,
                HttpResponse = httpResponse,
                DebugMode = Options.DebugMode
            };
            await base.HandleAsync(context);
        }
    }
}