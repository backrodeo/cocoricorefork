using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpErrorWriterRule : BusRule<HttpErrorWriterContext>
    {
        public HttpErrorWriterRule()
        {
            AllowMultipleHandler = false;
        }

        public HttpErrorWriterRule Call<THandler>()
            where THandler : class, IHttpErrorWriterHandler
        {
            return Call<THandler>((h, c) => h.WriteResponseAsync(c));
        }

        public HttpErrorWriterRule Call<T>(Func<T, Func<HttpErrorWriterContext, Task>> action)
            where T : class
        {
            return Call<T>((h, c) => action(h)(c));
        }

        public HttpErrorWriterRule Call<T>(Func<T, HttpErrorWriterContext, Task> action)
            where T : class
        {
            AddHandler<T>(action);
            return this;
        }

        public Type GetGenericParameter()
        {
            throw new InvalidOperationException();
        }
    }
}