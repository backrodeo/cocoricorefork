using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Dynamic;

namespace CocoriCore
{
    //TODO tester et factoriser le code de cette classe avec ErrorResponse ?
    public class ErrorFormatter
    {
        protected JsonSerializer _jsonSerializer;

        public ErrorFormatter()
        {
            _jsonSerializer = new JsonSerializer();
            _jsonSerializer.Converters.Add(new StringEnumConverter());
            _jsonSerializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
            _jsonSerializer.Formatting = Formatting.Indented;
        }

        public virtual string ConstructMessage(Exception exception)
        {
            var message = string.Join("\n", GetMessages(exception));
            object datas = GetDatas(exception);
            var jsonDatas = exception.Data != null ? _jsonSerializer.Serialize(datas) : null;
            jsonDatas = "   DebugDatas: " + jsonDatas.Replace("\n", "\n   ");
            var stackTrace = string.Join("\n", GetStackTraces(exception));
            return $"{exception.GetType()}: {message}\n{jsonDatas}\n{stackTrace}\n";
        }

        protected virtual IEnumerable<string> GetMessages(Exception exception)
        {
            var messages = new List<string> { exception.Message };
            exception = exception.InnerException;
            while (exception != null)
            {
                messages.Add("--- Inner message :\n");
                messages.Add($"{exception.Message}\n");
                exception = exception.InnerException;
            }
            return messages;
        }

        protected virtual IEnumerable<string> GetStackTraces(Exception exception)
        {
            var messages = new List<string> { exception.StackTrace };
            exception = exception.InnerException;
            while (exception != null)
            {
                messages.Insert(0, $"{exception.StackTrace}\n");
                messages.Insert(0, "--- End of the stack trace.\n");
                exception = exception.InnerException;
            }
            return messages;
        }

        protected virtual dynamic GetDatas(Exception exception)
        {
            var datas = new ExpandoObject() as IDictionary<string, object>;
            if (exception.Data != null)
            {
                foreach (var key in exception.Data.Keys)
                {
                    datas[key.ToString()] = exception.Data[key];
                }
                if (exception.InnerException != null && exception.InnerException.Data.Count > 0)
                {
                    datas["InnerDatas"] = GetDatas(exception.InnerException);
                }
            }
            return datas;
        }
    }
}