using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class HttpErrorWriterOptionsBuilder : BusOptionsBuilder<HttpErrorWriterContext, HttpErrorWriterRule, HttpErrorWriterOptions>
    {
        public HttpErrorWriterOptionsBuilder(Func<bool> debugMode = null)
        {
            debugMode = debugMode ?? (() => false);
            SetDebugMode(debugMode);
        }

        public new HttpErrorWriterOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler<THandler>(unexpectedExceptionHandler);
            return this;
        }

        public HttpErrorWriterRule For<TException>()
            where TException : Exception
        {
            var rule = AddRule<TException>();
            rule.StopPropagation = true;
            return rule;
        }

        public HttpErrorWriterOptionsBuilder SetDebugMode(Func<bool> debugMode)
        {
            _options.DebugMode = debugMode;
            return this;
        }
    }
}