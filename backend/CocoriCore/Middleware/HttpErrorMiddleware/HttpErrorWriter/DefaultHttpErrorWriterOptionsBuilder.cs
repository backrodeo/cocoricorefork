using System;

namespace CocoriCore
{
    public class DefaultHttpErrorWriterOptionsBuilder : HttpErrorWriterOptionsBuilder
    {
        public DefaultHttpErrorWriterOptionsBuilder(Func<bool> debugMode = null)
            : base(debugMode)
        {
            For<RouteNotFoundException>().Call<RouteNotFoundExceptionWriter>();
            For<Exception>().Call<DefaultExceptionWriter>();
        }
    }
}