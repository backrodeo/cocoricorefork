﻿namespace CocoriCore
{
    public static class HttpErrorWriterOptionsBuilderExtension
    {
        public static HttpErrorWriterOptionsBuilder UseErrorBusForFireAndForgetException(this HttpErrorWriterOptionsBuilder builder)
        {
            builder.SetFireAndForgetExceptionHandler<IErrorBus>((b, e) => b.HandleAsync(e));
            return builder;
        }
    }
}