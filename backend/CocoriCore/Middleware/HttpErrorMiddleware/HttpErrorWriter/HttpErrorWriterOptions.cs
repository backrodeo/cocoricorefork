using System;

namespace CocoriCore
{
    public class HttpErrorWriterOptions : BusOptions<HttpErrorWriterContext, HttpErrorWriterRule>
    {
        public Func<bool> DebugMode;

        public HttpErrorWriterOptions()
        {
            AllowMultipleRuleForType = false;
            DebugMode = null;
        }

        public override Type GetTargetType(HttpErrorWriterContext context)
        {
            return context.Exception.GetType();
        }

        public override void ValidateConfiguration()
        {
            base.ValidateConfiguration();
            if (DebugMode == null)
            {
                throw new ConfigurationException($"No debug mode function defined.");
            }
        }
    }
}