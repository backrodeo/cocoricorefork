
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CocoriCore
{
    /// <summary>
    /// <para>Set Status Code to 498. Can be used for exception of type <see cref="InvalidTokenException"/>.</para>
    /// <para>If DebugMode is enabled response body is a json with <see cref="ErrorResponse"/> structure else response body contains "Token expired/invalid".</para>
    /// </summary>
    public class InvalidTokenExceptionWriter : HttpResponseWriterBase, IHttpErrorWriterHandler
    {
        public InvalidTokenExceptionWriter(JsonSerializer jsonSerializer)
            : base(jsonSerializer)
        {
        }

        public async Task WriteResponseAsync(HttpErrorWriterContext context)
        {
            context.HttpResponse.StatusCode = 498;
            var response = new ErrorResponse(context.Exception, context.DebugMode());
            await WriteResponseAsync(context.HttpResponse, response);
        }
    }
}