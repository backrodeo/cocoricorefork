
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CocoriCore
{
    //TODO simplifier les noms des handlers dans les bus
    /// <summary>
    /// <para>Set Status Code to 401. Can be used for exception of type <see cref="AuthenticationException"/>.</para>
    /// <para>If DebugMode is enabled response body is a json with <see cref="ErrorResponse"/> structure else response body contains "Unauthorized".</para>
    /// </summary>
    public class AuthenticationExceptionWriter : HttpResponseWriterBase, IHttpErrorWriterHandler
    {
        public AuthenticationExceptionWriter(JsonSerializer jsonSerializer)
            : base(jsonSerializer)
        {
        }

        public async Task WriteResponseAsync(HttpErrorWriterContext context)
        {
            context.HttpResponse.StatusCode = 401;
            var response = new ErrorResponse(context.Exception, context.DebugMode());
            await WriteResponseAsync(context.HttpResponse, response);
        }
    }
}