using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CocoriCore
{
    //TODO simplifier les noms
    public class RouteNotFoundExceptionWriter : HttpResponseWriterBase, IHttpErrorWriterHandler
    {
        public RouteNotFoundExceptionWriter(JsonSerializer jsonSerializer)
            : base(jsonSerializer)
        {
        }

        public Task WriteResponseAsync(HttpErrorWriterContext context)
        {
            context.HttpResponse.StatusCode = 404;
            var errorResponse = new ErrorResponse(context.Exception, context.DebugMode());
            return WriteResponseAsync(context.HttpResponse, errorResponse);
        }
    }
}