﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IHttpErrorWriterHandler
    {
        Task WriteResponseAsync(HttpErrorWriterContext context);
    }
}