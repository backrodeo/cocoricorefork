using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class DefaultExceptionWriter : HttpResponseWriterBase, IHttpErrorWriterHandler
    {
        public DefaultExceptionWriter(JsonSerializer jsonSerializer)
            : base(jsonSerializer)
        {
        }

        public virtual Task WriteResponseAsync(HttpErrorWriterContext context)
        {
            context.HttpResponse.StatusCode = 500;
            var errorResponse = new ErrorResponse(context.Exception, context.DebugMode());
            return WriteResponseAsync(context.HttpResponse, errorResponse);
        }
    }
}