using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpErrorWriterContext
    {
        public Func<bool> DebugMode;
        public Exception Exception;
        public HttpResponse HttpResponse;
        public Dictionary<string, object> Data;
    }
}