using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace CocoriCore
{
    public class ErrorResponse
    {
        public string Message;
        public object Datas;
        public DebugResponse Debug;

        private Exception _exception;

        public ErrorResponse(Exception exception, bool debugMode = false)
            : this(exception, exception?.Message, debugMode)
        {
        }

        public ErrorResponse(Exception exception, string message, bool debugMode = false)
        {
            _exception = exception;
            Message = message;

            if (debugMode)
            {
                Debug = new DebugResponse
                {
                    Id = exception.GetId(),
                    Type = exception?.GetType()?.FullName,
                    Messages = GetMessages(exception),
                    StackTraces = GetStackTraces(exception),
                    Datas = CopyDatas(exception)
                };
            }
        }

        private string[] GetMessages(Exception exception)
        {
            var message = exception?.Message ?? string.Empty;
            var messages = new List<string>(message.Split("\n"));
            exception = exception?.InnerException;
            while (exception != null)
            {
                messages.Add("--- Inner message :");
                var innerMessage = exception?.Message ?? string.Empty;
                messages.AddRange(innerMessage.Split("\n"));
                exception = exception?.InnerException;
            }
            return messages
                .Select(x => x != null ? x.Replace("\r", string.Empty) : x)
                .ToArray();
        }

        private string[] GetStackTraces(Exception exception)
        {
            var stackTrace = exception?.StackTrace ?? string.Empty;
            var stackTraces = new List<string>(stackTrace.Split("\n"));
            exception = exception?.InnerException;
            while (exception != null)
            {
                stackTraces.Insert(0, "--- End of the stack trace.");
                var innerStackTrace = exception?.StackTrace ?? string.Empty;
                stackTraces.InsertRange(0, innerStackTrace.Split("\n"));
                exception = exception?.InnerException;
            }
            return stackTraces
                .Select(x => x != null ? x.Replace("\r", string.Empty) : x)
                .ToArray();
        }

        private dynamic CopyDatas(Exception exception)
        {
            var datas = new ExpandoObject() as IDictionary<string, object>;
            if (exception?.Data != null)
            {
                foreach (var key in exception.Data.Keys)
                {
                    if (Equals(key, ExceptionExtension.ExceptionIdKey))
                    {
                        continue;
                    }
                    datas[key.ToString()] = exception.Data[key];
                }
                if (exception.InnerException != null && exception.InnerException.Data.Count > 0)
                {
                    datas["InnerDatas"] = CopyDatas(exception.InnerException);
                }
            }
            return datas;
        }

        public class DebugResponse
        {
            public Guid? Id;
            public string Type;
            public string[] Messages;
            public dynamic Datas;
            public string[] StackTraces;
        }
    }
}