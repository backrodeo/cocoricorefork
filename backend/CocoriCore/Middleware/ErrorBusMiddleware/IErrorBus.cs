﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IErrorBus
    {
        Task HandleAsync(Exception exception);
    }
}