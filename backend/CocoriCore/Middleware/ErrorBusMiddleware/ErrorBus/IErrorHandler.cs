using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IErrorHandler
    {
        Task HandlerErrorAsync(Exception exception);
    }

    public interface IErrorHandler<TException> : IErrorHandler
    {
        Task HandlerErrorAsync(TException exception);
    }

}