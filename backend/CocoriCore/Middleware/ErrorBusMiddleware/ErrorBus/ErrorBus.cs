﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ErrorBus : BusBase<Exception, ErrorBusRule, ErrorBusOptions>, IErrorBus
    {
        public ErrorBus(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork, ErrorBusOptions options)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
            if (options.UnexceptedExceptionHandler == null)
                throw new ConfigurationException("No action defined in case of exception during error handling."
                    + $" Please use {nameof(ErrorBusOptionsBuilder.SetUnexpectedExceptionHandler)}() to define one.");
        }


        public override async Task HandleAsync(Exception exception)
        {
            try
            {
                await base.HandleAsync(exception);
            }
            catch (Exception ue)
            {
                await Options.UnexceptedExceptionHandler(ue, exception);
            }
        }

        protected override async Task HandleInternalAsync(Exception exception)
        {
            exception.GenerateId();
            await base.HandleInternalAsync(exception);
        }
    }
}