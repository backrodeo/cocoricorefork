using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    // TODO : peut être dans un package séparé
    public static class ErrorBusRuleExtension
    {
        public static ErrorBusRule TraceBdd<TException>(this ErrorBusOptionsBuilder<TException> builder)
            where TException : Exception
        {
            return builder.Call<ErrorTracerBdd>(t => t.TraceBdd);
        }

        public class ErrorTracerBdd
        {
            private IRepository _repository;

            public ErrorTracerBdd(IRepository repository)
            {
                _repository = repository;
            }

            public async Task TraceBdd(Exception exception)
            {
                var error = new ErrorTrace(exception);
                await _repository.InsertAsync((IEntity)error);
                //TODO +commit
            }
        }
    }
}