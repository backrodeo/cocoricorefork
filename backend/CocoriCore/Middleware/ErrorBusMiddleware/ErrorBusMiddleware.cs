using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore
{
    public class ErrorBusMiddleware : IMiddleware
    {
        public ErrorBusMiddleware()
        {
        }

        public async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
        {
            var start = DateTime.Now;
            try
            {
                await next(httpContext);
            }
            catch (Exception exception)
            {
                var errorBus = httpContext.GetUnitOfWork().Resolve<IErrorBus>();
                exception.Data["HttpTrace"] = new
                {
                    TraceId = httpContext.TraceIdentifier,
                    Method = httpContext.Request?.Method,
                    Url = httpContext.Request?.GetDisplayUrl(),
                    Start = start,
                    End = DateTime.Now,
                    Protocol = httpContext.Request?.Protocol,
                    StatusCode = httpContext.Response?.StatusCode,
                    RequestHeaders = GetHeaders(httpContext.Request?.Headers)
                };
                await errorBus.HandleAsync(exception);
                throw;
            }
        }
        
        protected virtual dynamic GetHeaders(IHeaderDictionary headerDictionary)
        {
            if (headerDictionary == null)
                return null;
            var headers = new ExpandoObject() as IDictionary<string, object>;
            foreach(var header in headerDictionary)
            {
                headers[header.Key] = header.Value.ToString();
            }
            return headers;
        }
    }
}