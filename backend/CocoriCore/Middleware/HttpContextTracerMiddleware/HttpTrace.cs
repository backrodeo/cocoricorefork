using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore 
{
    public class HttpTrace
    {
        public string Id;
        public DateTime Start;
        public DateTime End;
        public string Url;
        public string Protocol;
        public int StatusCode;

        //request
        //  body
        //  headers
        //response
        //  body
        //  headers
        //ip hash
        public HttpTrace(HttpContext httpContext, DateTime start, DateTime end)
        {
            Id = httpContext.TraceIdentifier;
            Start = start;
            End = end;
            Url = httpContext.Request.GetDisplayUrl();
            Protocol = httpContext.Request.Protocol;
            StatusCode = httpContext.Response.StatusCode;    
        }
    }
}