using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    //TODO transformer tracer en bus configurable pour construire nos propres objets trace, persister dans nlog, bdd, en async, anonymiser etc ?
    public class HttpContextTracerMiddleware : IMiddleware
    {
        private ITracer _tracer;

        public HttpContextTracerMiddleware(ITracer traceBus)
        {
            _tracer = traceBus;
        }

        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var start = DateTime.Now;
            await next(context);
            var trace = new HttpTrace(context, start, DateTime.Now);
            await _tracer.Trace(trace);
        }
    }
}