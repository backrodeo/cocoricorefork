﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ConsoleTraceBus : ITracer
    {
        public async Task Trace(object obj)
        {
            await Console.Out.WriteLineAsync(obj.ToString());
        }
    }
}
