﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface ITracer
    {
        Task Trace(object obj);
    }
}
