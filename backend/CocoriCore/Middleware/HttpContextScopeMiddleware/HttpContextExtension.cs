using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public static class HttpContextExtension
    {
        public const string HttpContextUnitOfWorkKey = "HTTP_CONTEXT_UNIT_OF_WORK";

        public static void SetUnitOfWork(this HttpContext httpContext, IUnitOfWork unitOfWork)
        {
            httpContext.Items[HttpContextUnitOfWorkKey] = unitOfWork;
        }

        public static IUnitOfWork GetUnitOfWork(this HttpContext httpContext)
        {
            return (IUnitOfWork)httpContext.Items[HttpContextUnitOfWorkKey];
        }
    }
}