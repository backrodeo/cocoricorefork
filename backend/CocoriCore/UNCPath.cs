namespace CocoriCore
{
    public class UNCPath : Path
    {
        public UNCPath(string path)
        {
            path = System.IO.Path.GetFullPath(path);
            Segments = GetSegments(path);
        }

        protected UNCPath(params object[] paths)
        {
            Segments = GetSegments(paths);
        }

        public override string ToString()
        {
            return $@"\\{base.ToString()}";
        }

        public override Path Append(params object[] paths)
        {
            return new UNCPath(Segments, paths);
        }

        public static bool IsUNCPath(string path)
        {
            return path.StartsWith(@"\\");
        }

        public static implicit operator UNCPath(string value)
        {
            return new UNCPath(value);
        }

        public static implicit operator string(UNCPath path)
        {
            return path?.ToString();
        }
    }
}
