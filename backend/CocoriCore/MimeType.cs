﻿namespace CocoriCore
{

    public class MimeType : ValueObject<string>
    {
        public MimeType()
        {
        }

        public MimeType(string value) : base(value)
        {
        }

        public static implicit operator MimeType(string value)
        {
            return new MimeType(value);
        }

        public static implicit operator string(MimeType value)
        {
            return value?.ToString();
        }
    }
}
