﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IFileSystem
    {
        Path TempPath { get; }

        Task<bool> FileExistsAsync(Path path);

        Task CreateBinaryFileAsync(Path path, byte[] binaryContent);

        Task CreateBinaryFileAsync(Path path, string base64Content);

        Task<bool> DeleteFileAsync(Path path);

        Task<byte[]> ReadAsBinaryAsync(Path path);

        Task<string> ReadAsBase64Async(Path path);

        Task CreateTextFileAsync(Path path, string textContent);

        Task<string> ReadAsTextAsync(Path path);

        Task CreateDirectoryAsync(Path path);

        Task<bool> DirectoryExistsAsync(Path path);

        Task<IEnumerable<Path>> ListFilesAsync(Path path);

        Task<DateTime> GetCreationDateTimeAsync(Path path);

        Path GetUniqueTempPath();
        Task MoveFileAsync(Path path, Path destinationFile);
        Task DeleteDirectoryAsync(Path folderPath);
    }
}
