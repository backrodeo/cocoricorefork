﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class EmailService : IEmailService
    {
        private SmtpConfiguration _configuration;

        public EmailService(SmtpConfiguration configuration)
        {
            _configuration = configuration;
        }

        public virtual async Task SendAsync(MailMessage message)
        {
            using (var client = new SmtpClient())
            {
                client.Host = _configuration.Host;
                if (_configuration.Port.HasValue)
                {
                    client.Port = _configuration.Port.Value;
                }
                if (_configuration.User != null)
                {
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(_configuration.User, _configuration.Password);
                }
                if (message.From == null)
                {
                    message.From = new MailAddress(_configuration.DefaultFrom);
                }
                await client.SendMailAsync(message);
            }
        }
    }
}
