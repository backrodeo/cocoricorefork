﻿using System;

namespace CocoriCore
{
    public interface IEnumObject : IValueObject
    {
        Type EnumType { get; }
    }
}
