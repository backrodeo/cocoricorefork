using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ResourceProviderOptions
    {
        private Dictionary<Type, Func<IUnitOfWork, object, Task<Stream>>> _rules;

        public ResourceProviderOptions()
        {
            _rules = new Dictionary<Type, Func<IUnitOfWork, object, Task<Stream>>>();
        }

        public void AddRule(Type resourceKeyType, Func<IUnitOfWork, object, Task<Stream>> GetResourceAsync)
        {
            if (GetResourceAsync == null)
            {
                throw new ArgumentNullException(nameof(GetResourceAsync));
            }
            if (resourceKeyType == null)
            {
                throw new ArgumentNullException(nameof(resourceKeyType));
            }
            if (_rules.ContainsKey(resourceKeyType))
            {
                throw new ConfigurationException($"A rule with the same resource key '{resourceKeyType}' already exists.");
            }
            _rules[resourceKeyType] = GetResourceAsync;
        }

        public Func<IUnitOfWork, object, Task<Stream>> this[Type type]
        {
            get
            {
                if (!_rules.ContainsKey(type))
                {
                    throw new InvalidOperationException($"No rule defined for type {type}");
                }
                return _rules[type];
            }
        }
    }
}