﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRenderer
    {
        Task<string> RenderAsync(object templateKey, dynamic variables);
    }

}
