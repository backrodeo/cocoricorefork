using System.Collections.Generic;

namespace CocoriCore
{
    public class ResourceProviderOptionsBuilder
    {
        private List<ResourceProviderRule> _rules;

        public ResourceProviderOptionsBuilder()
        {
            _rules = new List<ResourceProviderRule>();
        }

        public ResourceProviderOptions Options
        {
            get
            {
                var options = new ResourceProviderOptions();
                _rules.ForEach(r => options.AddRule(r.ResourceKeyType, r.GetResourceAsync));
                return options;
            }
        }

        public ResourceProviderRuleBuilder<TKey> For<TKey>()
        {
            var rule = new ResourceProviderRule();
            _rules.Add(rule);
            return new ResourceProviderRuleBuilder<TKey>(rule);
        }
    }
}