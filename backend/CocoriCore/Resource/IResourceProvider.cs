﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IResourceProvider
    {
        Task<string> ReadAsTextAsync(object resourceKey);
        Task<byte[]> ReadAsBinaryAsync(object resourceKey);
        Task<string> ReadAsBase64Async(object resourceKey);
    }
}
