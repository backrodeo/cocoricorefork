using System;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class EmbeddedResourceProviderRuleBuilder<TKey>
    {
        private ResourceProviderRule _rule;
        private Assembly _resourceAssembly;

        public EmbeddedResourceProviderRuleBuilder(ResourceProviderRule rule, Assembly resourceAssembly)
        {
            _rule = rule;
            _resourceAssembly = resourceAssembly;
        }

        public void GetPathUsing(Func<TKey, Path> resolvePath)
        {
            _rule.GetResourceAsync = (u, k) =>
            {
                var provider = u.Resolve<EmbeddedResourceProvider>();
                var path = resolvePath((TKey)k);
                return provider.GetStreamAsync(_resourceAssembly, path);
            };
        }

        public void GetPathUsing<TService>(Func<TService, TKey, Task<EmbeddedResourcePath>> resolvePathAsync)
        {
            _rule.GetResourceAsync = async (u, k) =>
            {
                var provider = u.Resolve<EmbeddedResourceProvider>();
                var service = u.Resolve<TService>();
                var path = await resolvePathAsync(service, (TKey)k);
                return await provider.GetStreamAsync(path);
            };
        }
    }
}