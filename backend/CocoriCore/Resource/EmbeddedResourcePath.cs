﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CocoriCore
{
    public class EmbeddedResourcePath : Path
    {
        public Assembly ResourceAssembly { get; }

        public EmbeddedResourcePath(Assembly resourceAssembly, string path)
        {
            ResourceAssembly = resourceAssembly;
            Segments = GetSegments(path);
        }

        public EmbeddedResourcePath(Assembly resourceAssembly, params object[] paths)
        {
            ResourceAssembly = resourceAssembly;
            Segments = GetSegments(paths);
        }

        public override string ToString()
        {
            return $"{ResourceAssembly.GetName().Name}.{string.Join(".", Segments)}";
        }
    }
}
