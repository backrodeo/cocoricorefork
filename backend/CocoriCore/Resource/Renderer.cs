﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public class Renderer : IRenderer
    {
        private IResourceProvider _resourceProvider;

        public Renderer(IResourceProvider resourceProvider)
        {
            _resourceProvider = resourceProvider;
        }

        public async Task<string> RenderAsync(object templateKey, object variables)
        {
            var template = await _resourceProvider.ReadAsTextAsync(templateKey);
            return ReplaceVariablesIntoTemplate(variables, template);
        }

        private string ReplaceVariablesIntoTemplate(object variables, string template)
        {
            foreach (var member in variables.GetType().GetPropertiesAndFields())
            {
                var variableKey = member.Name;
                var variableValue = member.InvokeGetter(variables);
                template = template.Replace($"{{{{{variableKey}}}}}", $"{variableValue}");
            }
            return template;
        }
    }
}
