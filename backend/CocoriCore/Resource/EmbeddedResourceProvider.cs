﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class EmbeddedResourceProvider
    {
        public Task<Stream> GetStreamAsync(Assembly resourceAssembly, Path path)
        {
            var embeddedResourcePath = new EmbeddedResourcePath(resourceAssembly, path);
            return GetStreamAsync(embeddedResourcePath);
        }

        public Task<Stream> GetStreamAsync(EmbeddedResourcePath path)
        {
            var resourceAssembly = path.ResourceAssembly;
            if (resourceAssembly.GetManifestResourceNames().Any(x => x == path.ToString()))
            {
                var stream = resourceAssembly.GetManifestResourceStream(path);
                return Task.FromResult(stream);
            }
            else
            {
                throw new InvalidOperationException($"Embedded resource '{path}' not found."
                    + "Ensure the path is correct and file is configured as embebded resource.");
            }
        }
    }
}
