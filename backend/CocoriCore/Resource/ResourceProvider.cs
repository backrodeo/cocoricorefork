using System;
using System.IO;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ResourceProvider : IResourceProvider
    {
        private ResourceProviderOptions _options;
        private IUnitOfWork _unitOfWork;

        public ResourceProvider(ResourceProviderOptions options, IUnitOfWork unitOfWork)
        {
            _options = options;
            _unitOfWork = unitOfWork;
        }

        public async Task<string> ReadAsBase64Async(object resourceKey)
        {
            var binaryContent = await ReadAsBinaryAsync(resourceKey);
            return Convert.ToBase64String(binaryContent);
        }

        public async Task<byte[]> ReadAsBinaryAsync(object resourceKey)
        {
            if (resourceKey == null)
            {
                throw new ArgumentNullException(nameof(resourceKey));
            }
            var keyType = resourceKey.GetType();
            var stream = await _options[keyType](_unitOfWork, resourceKey);
            return stream.ReadAllBytes();
        }

        public async Task<string> ReadAsTextAsync(object resourceKey)
        {
            if (resourceKey == null)
            {
                throw new ArgumentNullException(nameof(resourceKey));
            }
            var keyType = resourceKey.GetType();
            using (var stream = await _options[keyType](_unitOfWork, resourceKey))
            using (var reader = new StreamReader(stream))

            {
                return await reader.ReadToEndAsync();
            }
        }

        public Task<string> ReadAsTextAsync(Path path)
        {
            throw new NotImplementedException();
        }
    }
}