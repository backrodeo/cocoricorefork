using System;
using System.IO;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ResourceProviderRule
    {
        public Type ResourceKeyType { get; set; }
        public Func<IUnitOfWork, object, Task<Stream>> GetResourceAsync { get; set; }
    }
}