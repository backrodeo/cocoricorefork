﻿using System.Threading.Tasks;

namespace CocoriCore
{
    public interface ITokenService
    {
        void CheckIfExpired(string payload);
        Task<string> CheckIntegrityAndExpirationAsync(string token, string salt = null);
        Task CheckIntegrityAsync(string token, string salt = null);
        T GetPayload<T>(string token);
        Task<string> GenerateTokenAsync<T>(T payload, string extraSalt = null) where T : IClaims;
    }
}