﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface ITransactionHolder : IDisposable
    {
        Task CommitAsync();
        Task RollbackAsync();
    }
}
