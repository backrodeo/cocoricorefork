using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IRandomService
    {
        Task<string> GenerateRandomString();
    }
}