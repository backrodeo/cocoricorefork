namespace CocoriCore
{
    public interface IMessage
    {
    }

    public interface ICommand : IMessage
    {
    }

    public interface IQuery : IMessage
    {
    }
}