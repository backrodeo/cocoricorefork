﻿
using Jose;
using Newtonsoft.Json;
using System;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class TokenService : ITokenService
    {
        private IClock _clock;
        private JWTConfiguration _configuration;
        private JsonSerializer _jsonSerializer;
        private IFactory _factory;

        public TokenService(IClock clock, JWTConfiguration configuration,
            JWTSerializer jsonSerializer, IFactory factory)
        {
            _clock = clock;
            _configuration = configuration;
            _jsonSerializer = jsonSerializer;
            _factory = factory;
        }

        public virtual Task<string> GenerateTokenAsync<T>(T payload, string extraSalt = null)
            where T : IClaims
        {
            var key = _configuration.SecretBytes;
            if (extraSalt != null)
            {
                key = key.Concat(Encoding.UTF8.GetBytes(extraSalt)).ToArray();
            }
            var token = JWT.Encode(_jsonSerializer.Serialize(payload), key, _configuration.Algorithm);
            return Task.FromResult(token);
        }

        public async Task<string> CheckIntegrityAndExpirationAsync(string token, string salt = null)
        {
            await CheckIntegrityAsync(token, salt);
            CheckIfExpired(token);
            return token;
        }

        public Task CheckIntegrityAsync(string token, string salt = null)
        {
            var key = _configuration.SecretBytes;
            if (salt != null)
            {
                key = key.Concat(Encoding.UTF8.GetBytes(salt)).ToArray();
            }
            var algorithm = _configuration.Algorithm;
            try
            {
                JWT.Decode(token, key, algorithm);
                return Task.CompletedTask;
            }
            catch (Exception e)
            {
                throw new InvalidTokenException("Can't decode token.", e);
            }
        }

        public void CheckIfExpired(string token)
        {
            var payload = JWT.Payload(token);
            var claims = _jsonSerializer.Deserialize<Claims>(payload);
            if (claims.ExpireAt <= _clock.Now)
            {
                throw new InvalidTokenException("Token is expired.");
            }
        }

        public T GetPayload<T>(string token)
        {
            var payload = JWT.Payload(token);
            return _jsonSerializer.Deserialize<T>(payload);
        }


        class Claims : IClaims
        {
            public DateTime ExpireAt { get; set; }
        }
    }
}
