﻿namespace CocoriCore
{
    public abstract class ValueObject<T> : IValueObject<T>
    {
        protected T _value;

        protected ValueObject()
        {
        }

        public ValueObject(T value)
        {
            _value = value;
        }

        public T Value => _value;

        public virtual string ValidationRegex => ".*";

        object IValueObject.Value => _value;

        bool IValueObject.HasValue => _value != null;

        public static bool operator ==(ValueObject<T> objet1, ValueObject<T> objet2)
        {
            if (objet1 as object == null && objet2 as object == null)
                return true;
            else if (objet1 as object != null && objet2 as object == null)
                return false;
            else if (objet1 as object == null && objet2 as object != null)
                return false;
            return Equals(objet1._value, objet2._value);
        }

        public static bool operator !=(ValueObject<T> objet1, ValueObject<T> objet2)
        {
            return !(objet1 == objet2);
        }

        public override string ToString()
        {
            return _value.ToString();
        }

        public override bool Equals(object objet)
        {
            return objet is ValueObject<T> && Equals(((ValueObject<T>)objet)._value, _value);
        }

        public override int GetHashCode()
        {
            return 13 ^ _value.GetHashCode();
        }
    }
}
