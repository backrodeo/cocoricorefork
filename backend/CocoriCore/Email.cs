﻿namespace CocoriCore
{

    public class Email : ValueObject<string>
    {
        public Email(string value) : base(value)
        {
        }

        public static implicit operator Email(string value)
        {
            return value == null ? null : new Email(value);
        }

        public static implicit operator string(Email value)
        {
            return value?.ToString();
        }
    }
}
