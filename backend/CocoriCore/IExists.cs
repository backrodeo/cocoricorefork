﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IExists
    {
        Task<bool> ExistsAsync(Type entityType, Guid id);

        Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value);

        Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection);
    }
}
