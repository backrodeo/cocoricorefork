﻿namespace CocoriCore
{
    public interface ICryptoService
    {
        string Decrypt(byte[] cryptedBinaryText);
        byte[] Encrypt(string text);
        void InstallKey(byte[] binaryKey);
        void InstallKey(string base64Key);
        string InstallNewKey();
        void UninstallKey();
    }
}