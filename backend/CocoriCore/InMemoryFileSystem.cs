﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore
{
    //TODO déplacer dans CocoriCore.Testutils ?
    public class InMemoryFileSystem : IFileSystem
    {
        private Dictionary<Path, byte[]> _files;
        private HashSet<Path> _directories;
        private Dictionary<Path, DateTime> _fileCreationDateTimes;
        private Dictionary<Path, DateTime> _directoryCreationDateTimes;
        private IClock _clock;

        public Path TempPath => "tmp";

        public InMemoryFileSystem(IClock clock)
        {
            _files = new Dictionary<Path, byte[]>();
            _directories = new HashSet<Path>();
            _fileCreationDateTimes = new Dictionary<Path, DateTime>();
            _directoryCreationDateTimes = new Dictionary<Path, DateTime>();
            _clock = clock;
            _directories.Add(new Path());
        }

        public Task<bool> FileExistsAsync(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            return Task.FromResult(_files.ContainsKey(path));
        }

        public async Task CreateBinaryFileAsync(Path path, byte[] binaryContent)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (binaryContent == null)
                throw new ArgumentNullException(nameof(binaryContent));
            if (!await DirectoryExistsAsync(path.GetParent()))
                throw new DirectoryNotFoundException($"Can't create file {path} because parent directory doesn't exist.");
            if (await DirectoryExistsAsync(path))
                throw new UnauthorizedAccessException($"Can't create file {path} because a directory with same name already exists.");
            _files[path] = binaryContent;
            _fileCreationDateTimes[path] = _clock.Now;
        }

        public Task CreateBinaryFileAsync(Path path, string base64Content)
        {
            return CreateBinaryFileAsync(path, Convert.FromBase64String(base64Content));
        }

        public Task<byte[]> ReadAsBinaryAsync(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            if (!_files.ContainsKey(path))
                throw new FileNotFoundException($"File {path} does not exist.");
            return Task.FromResult(_files[path]);
        }

        public async Task<string> ReadAsBase64Async(Path path)
        {
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            var binaryContent = await ReadAsBinaryAsync(path);
            return Convert.ToBase64String(binaryContent);
        }

        public Task CreateTextFileAsync(Path path, string textContent)
        {
            return CreateBinaryFileAsync(path, Encoding.UTF8.GetBytes(textContent));
        }

        public async Task<string> ReadAsTextAsync(Path path)
        {
            var binaryContent = await ReadAsBinaryAsync(path);
            return Encoding.UTF8.GetString(binaryContent);
        }

        public Task CreateDirectoryAsync(Path path)
        {
            _directories.Add(path);
            while (!path.IsEmpty())
            {
                path = path.GetParent();
                _directories.Add(path);
                _directoryCreationDateTimes[path] = _clock.Now;
            }
            return Task.CompletedTask;
        }

        public Task<bool> DirectoryExistsAsync(Path path)
        {
            return Task.FromResult(_directories.Contains(path));
        }

        public Task<bool> DeleteFileAsync(Path path)
        {
            return Task.FromResult(_files.Remove(path));
        }

        public async Task<IEnumerable<Path>> ListFilesAsync(Path path)
        {
            if (await DirectoryExistsAsync(TempPath))
                return _files.Where(x => x.Key.GetParent() == path).Select(x => x.Key);
            else
                return new Path[0];
        }

        public Task<DateTime> GetCreationDateTimeAsync(Path path)
        {
            if (_files.ContainsKey(path))
                return Task.FromResult(_fileCreationDateTimes[path]);
            if (_directories.Contains(path))
                return Task.FromResult(_directoryCreationDateTimes[path]);
            throw new InvalidOperationException($"Path {path} does not exists.");
        }

        public Path GetUniqueTempPath()
        {
            var uniqueDirectoryName = System.IO.Path.GetRandomFileName().Replace(".", string.Empty);
            return TempPath.Append(uniqueDirectoryName);
        }

        public async Task MoveFileAsync(Path sourceFile, Path destinationFile)
        {
            if (sourceFile == destinationFile)
                throw new ArgumentException($"{nameof(sourceFile)} and {nameof(destinationFile)} must be different.");
            var fileContent = _files[sourceFile];
            _files.Remove(sourceFile);
            _fileCreationDateTimes.Remove(sourceFile);
            if (!await DirectoryExistsAsync(destinationFile.GetParent()))//TODO TECH voir si l'api File de Microsoft fonctionne pareil
                throw new DirectoryNotFoundException($"Can't create file {destinationFile} because parent directory doesn't exist.");
            _files[destinationFile] = fileContent;
            _fileCreationDateTimes[destinationFile] = _clock.Now;
        }

        public Task DeleteDirectoryAsync(Path folderPath)
        {
            _directories.Remove(folderPath);
            _directoryCreationDateTimes.Remove(folderPath);
            return Task.CompletedTask;
        }
    }
}
