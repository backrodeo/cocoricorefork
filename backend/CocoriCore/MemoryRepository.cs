using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    //TODO déplacer dans CocoriCore.Testutils
    public class MemoryRepository : IRepository, IReloader
    {
        private IInMemoryEntityStore _entityStore;
        private HashSet<Type> _insertedOrUpdatedTypes;

        public MemoryRepository(IInMemoryEntityStore entityStore)
        {
            _entityStore = entityStore;
            _insertedOrUpdatedTypes = new HashSet<Type>();
        }

        public virtual Task InsertAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();//TODO rendre paramétrable ?
            _entityStore.Add(entity);
            _insertedOrUpdatedTypes.Add(entity.GetType());
            return Task.CompletedTask;
        }

        public virtual Task UpdateAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            _entityStore.Set(entity);
            _insertedOrUpdatedTypes.Add(entity.GetType());
            return Task.CompletedTask;
        }

        public virtual Task DeleteAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            _entityStore.Remove(entity.GetType(), entity.Id);
            return Task.CompletedTask;
        }

        public virtual async Task<TEntity> LoadAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            var type = typeof(TEntity);
            return (TEntity)await LoadAsync(typeof(TEntity), id);
        }

        public virtual Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMemberExpression, object value)
            where TEntity : class, IEntity
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }
            var memberInfo = uniqueMemberExpression.GetMemberInfo();
            value = value.ImplicitConvertTo(memberInfo.GetMemberType());
            var memberFunc = uniqueMemberExpression.Compile();
            var loadedEntities = _entityStore
                .Get(typeof(TEntity))
                .Cast<TEntity>()
                .Where(x => Equals(memberFunc(x), value))
                .ToList();
            if (loadedEntities.Count() == 0)
                throw new InvalidOperationException($"There is no entity of type {typeof(TEntity)} with value {value} "
                    + $"for unique field {uniqueMemberExpression.GetMemberInfo().Name}");
            if (loadedEntities.Count() > 1)
                throw new InvalidOperationException($"There are several entity of {typeof(TEntity)} with value {value} "
                    + $"for unique field {uniqueMemberExpression.GetMemberInfo().Name}, "
                    + $"use {nameof(Query)}() method instead.");
            return Task.FromResult(loadedEntities.First());
        }

        public virtual Task<object> LoadAsync(Type type, Guid id)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Given id is empty");
            var loadedEntities = _entityStore.Get(type, id);
            if (loadedEntities.Count() == 0)
                throw new InvalidOperationException($"There is no entity of type {type} with id {id}");
            if (loadedEntities.Count() > 1)
                throw new InvalidOperationException($"There are several entity of type {type} with id {id}, " +
                    $"use {nameof(MemoryRepository.Query)}() method instead.");
            return Task.FromResult((object)loadedEntities.First());
        }

        public virtual Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var result = ids.SelectMany(x => _entityStore.Get(type, x)).Cast<IEntity>().ToArray();
            return Task.FromResult(result);
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            var queryable = _entityStore.Get(typeof(TEntity)).Cast<TEntity>().AsQueryable();
            return new FakeAsyncEnumerable<TEntity>(queryable);
        }

        public virtual IQueryable<IEntity> Query(Type type)
        {
            var queryable = _entityStore.Get(type).AsQueryable();
            return new FakeAsyncEnumerable<IEntity>(queryable);
        }

        public virtual Task<bool> ExistsAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return ExistsAsync(typeof(TEntity), id);
        }

        public virtual Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            if (id == Guid.Empty)
                throw new ArgumentException("Given id is empty");
            return Task.FromResult(_entityStore.Contains(entityType, id));
        }

        public Task<bool> ExistsAsync(Type entityType, MemberInfo memberInfo, object value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            value = value.ImplicitConvertTo(memberInfo.GetMemberType());
            var loadedEntities = _entityStore
                .Get(entityType)
                .Where(x => Equals(memberInfo.InvokeGetter(x), value))
                .ToList();
            return Task.FromResult(loadedEntities.Count == 1);
        }

        public Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            if (collection == null)
                throw new ArgumentNullException(nameof(collection));
            foreach (var id in collection)
            {
                if (!_entityStore.Contains(entityType, id))
                    return Task.FromResult(false);
            }
            return Task.FromResult(true);
        }

        public Task ClearAsync()
        {
            _entityStore.Clear();
            return Task.CompletedTask;
        }

        public Task<TEntity> ReloadAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            return LoadAsync<TEntity>(entity.Id);
        }

        public Task<TEntity> ReloadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return LoadAsync<TEntity>(id);
        }

        public void Dispose()
        {
        }
    }
}