﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace CocoriCore
{
    public class CryptoService : ICryptoService
    {
        private string _keyContainerName;
        private int _keySize;

        public CryptoService(string keyContainerName, int keySize = 2048, string base64Key = null)
        {
            _keyContainerName = keyContainerName;
            _keySize = keySize;
            if (base64Key != null)
            {
                if (AKeyIsAlreadyInstalled())
                {
                    UninstallKey();
                }
                InstallKey(base64Key);
            }
        }

        public virtual string InstallNewKey()
        {
            byte[] key = GenerateNewKey();
            InstallKey(key);
            return Convert.ToBase64String(key);
        }

        protected virtual byte[] GenerateNewKey()
        {
            using (var rsa = new RSACryptoServiceProvider(_keySize))
            {
                return rsa.ExportCspBlob(true);
            }
        }

        protected virtual RSACryptoServiceProvider CreateCryptoServiceProvider(CspProviderFlags flags)
        {
            try
            {
                var containerParameters = new CspParameters();
                containerParameters.Flags = flags;
                containerParameters.KeyContainerName = _keyContainerName;
                return new RSACryptoServiceProvider(containerParameters);
            }
            catch (CryptographicException e)
            {
                if (e.Message.Contains("Le jeu de clés n’existe pas") ||
                    e.Message.Contains("Keyset does not exist"))
                    throw new ConfigurationException($"Key container is empty, you must install a key into this container before encrypt/decrypt datas.\n"
                        + $"You can install an existing key using {GetType().Name}.{nameof(InstallKey)}() "
                        + $"or you can generate and install a new one using {GetType().Name}.{nameof(InstallNewKey)}().", e);
                else if (e.Message.Contains("System cannot find specified file") ||
                    e.Message.Contains("Le fichier spécifié est introuvable"))
                    throw new ConfigurationException("You have to configure application pool to load user profil.\n"
                        + "On IIS Manager open application pool then into advanced settings/Process Model set Load User Profile to True", e);
                else
                    throw;
            }
        }

        public virtual void InstallKey(string base64Key)
        {
            InstallKey(Convert.FromBase64String(base64Key));
        }

        public void InstallKey(byte[] binaryKey)
        {
            if (AKeyIsAlreadyInstalled())
            {
                throw new ConfigurationException($"A key is already installed in container {_keyContainerName}."
                    + $"Clear container using {GetType().Name}.{nameof(UninstallKey)}() first if you want to install another key.");
            }
            else
            {
                using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.NoFlags))
                {
                    rsa.ImportCspBlob(binaryKey);
                }
            }
        }

        protected virtual bool AKeyIsAlreadyInstalled()
        {
            try
            {
                var containerParameters = new CspParameters();
                containerParameters.KeyContainerName = _keyContainerName;
                containerParameters.Flags = CspProviderFlags.UseExistingKey;
                using (var rsa = new RSACryptoServiceProvider(containerParameters)) { }
                return true;
            }
            catch (CryptographicException)
            {
                return false;
            }
        }

        public virtual byte[] Encrypt(string text)
        {
            if (text == null)
            {
                return null;
            }
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
            {
                var utf8Bytes = Encoding.UTF8.GetBytes(text);
                return rsa.Encrypt(utf8Bytes, true);
            }
        }

        public virtual string Decrypt(byte[] encryptedBinaryText)
        {
            if (encryptedBinaryText == null)
            {
                return null;
            }
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
            {
                byte[] binaryText = rsa.Decrypt(encryptedBinaryText, true);
                return Encoding.UTF8.GetString(binaryText);
            }
        }

        public virtual void UninstallKey()
        {
            using (var rsa = CreateCryptoServiceProvider(CspProviderFlags.UseExistingKey))
            {
                rsa.PersistKeyInCsp = false;
            }
        }
    }
}
