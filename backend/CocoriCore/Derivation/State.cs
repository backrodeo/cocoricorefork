using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Utf8Json;

namespace CocoriCore
{
    public class State : IState
    {
        protected object _previousEntity;
        protected byte[] _previousState;

        public State(object entity)
        {
            _previousEntity = Utf8Json.CreateCopy(entity, out _previousState);
        }

        public State(object previousEntity, byte[] previousState)
        {
            _previousEntity = previousEntity;
            _previousState = previousState;
        }

        public object PreviousEntity => _previousEntity;

        public State<TCast> Cast<TCast>()
            where TCast : class
        {
            return new State<TCast>((TCast)_previousEntity, _previousState);
        }

        public TProperty PreviousValue<TEntity, TProperty>(Expression<Func<TEntity, TProperty>> propertyExpression)
        {
            return propertyExpression.Compile()((TEntity)_previousEntity);
        }

        public TProperty PreviousValue<TProperty>(MemberInfo memberInfo)
        {
            return (TProperty)memberInfo.InvokeGetter(_previousEntity);
        }

        public bool HasChanged(object entity)
        {
            var currentState = Utf8Json.Serialize(entity);
            return !_previousState.SequenceEqual(currentState);
        }
    }

    public class State<T> : State
    {
        public State(T previousEntity, byte[] previousState)
            : base(previousEntity, previousState)
        {
        }

        public TProperty PreviousValue<TProperty>(Expression<Func<T, TProperty>> propertyExpression)
        {
            return PreviousValue<T, TProperty>(propertyExpression);
        }
    }
}