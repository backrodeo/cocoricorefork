﻿using System.Collections.Generic;

namespace CocoriCore
{
    public interface IStateStore
    {
        void Clear();
        IEnumerator<IState> GetEnumerator();
        IState GetState(object entity);
        bool HasState(object entity);
        T SaveState<T>(T entity);
        T SaveStateAndCreateCopy<T>(T entity);
    }
}