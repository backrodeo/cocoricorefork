namespace CocoriCore
{
    public interface IState
    {
        State<TCast> Cast<TCast>() where TCast : class;
        object PreviousEntity { get; }
        bool HasChanged(object entity);
    }
}