using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore
{
    public class StateStore : IEnumerable<IState>, IStateStore
    {
        private Dictionary<object, IState> _stateDictionary;
        private IUIDProvider _UIDProvider;

        public StateStore(IUIDProvider UIDProvider)
        {
            _stateDictionary = new Dictionary<object, IState>();
            _UIDProvider = UIDProvider;
        }

        public virtual T SaveStateAndCreateCopy<T>(T entity)
        {
            var uid = _UIDProvider.GetUID(entity);
            var entityType = entity.GetType();
            if (!_stateDictionary.ContainsKey(uid))
            {
                var previousEntity = Utf8Json.CreateCopy(entity, out byte[] previousState);
                _stateDictionary[uid] = new State(previousEntity, previousState);
                return (T)Utf8Json.Deserialize(entityType, previousState);
            }
            else
            {
                return (T)Utf8Json.CreateCopy(entity);
            }
        }

        public virtual T SaveState<T>(T entity)
        {
            var uid = _UIDProvider.GetUID(entity);
            _stateDictionary[uid] = new State(entity);
            return entity;
        }

        public virtual IState GetState(object entity)
        {
            var uid = _UIDProvider.GetUID(entity);
            if (!StateExistsForUID(uid))
            {
                throw new InvalidOperationException("There is no state associated with this entity, ensure you loaded the entity before");
            }
            return _stateDictionary[uid];
        }

        protected virtual bool StateExistsForUID(object uid)
        {
            return _stateDictionary.ContainsKey(uid);
        }

        public virtual bool HasState(object entity)
        {
            var uid = _UIDProvider.GetUID(entity);
            return StateExistsForUID(uid);
        }

        public virtual void Clear()
        {
            _stateDictionary.Clear();
        }

        public virtual IEnumerator<IState> GetEnumerator()
        {
            return _stateDictionary.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _stateDictionary.Values.GetEnumerator();
        }
    }
}