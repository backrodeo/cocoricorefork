using System;

namespace CocoriCore
{
    public interface IEntity 
    {
        Guid Id { get; set; }
    }
}