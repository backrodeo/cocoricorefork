using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IPasswordService
    {
        Task<string> GeneratePasswordAsync();
        Task<bool> CheckPasswordAsync(string password, string hash);
        Task<string> HashPasswordAsync(string password);
        Task<bool> PasswordIsWeakAsync(string password);
    }
}