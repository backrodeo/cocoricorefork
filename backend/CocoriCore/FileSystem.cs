﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class FileSystem : IFileSystem
    {
        //TODO écrire ce code en version async car ici on utilise les appels bloquants
        private Path _root;
        public Path TempPath { get; }

        public FileSystem(string rootPath)
        {
            _root = GetRootPath(rootPath);
            TempPath = new Path("tmp");//TODO à rendre configurable


            //TODO si on passe "c:\files" alors ca donne un caractère spécial, comment s'en prévenir ?
            System.IO.Directory.CreateDirectory(_root);
            System.IO.Directory.CreateDirectory(TempPath);
        }

        protected virtual Path GetRootPath(string rootPath)
        {
            var absolutePath = System.IO.Path.GetFullPath(rootPath);
            if (System.IO.Path.DirectorySeparatorChar == '\\')
            {
                if (WindowsDrivePath.IsWindowsDrivePath(absolutePath))
                {
                    return new WindowsDrivePath(absolutePath);
                }
                else if (UNCPath.IsUNCPath(absolutePath))
                {
                    return new UNCPath(absolutePath);
                }
                else
                {
                    throw new ArgumentException($"Not supported windows path type : '{rootPath}'.");
                }
            }
            else
            {
                if (LinuxPath.IsLinuxPath(absolutePath))
                {
                    return new LinuxPath(absolutePath);
                }
                else
                {
                    throw new ArgumentException($"Not supported linux path type : '{rootPath}'.");
                }
            }
        }

        public async Task CreateBinaryFileAsync(Path path, string base64Content)
        {
            var binaryContent = Convert.FromBase64String(base64Content);
            await CreateBinaryFileAsync(path, binaryContent);
        }

        public async Task CreateBinaryFileAsync(Path path, byte[] binaryContent)
        {
            var absolutePath = _root.Append(path);
            using (var stream = System.IO.File.Create(absolutePath))
            {
                await stream.WriteAsync(binaryContent, 0, binaryContent.Length);
            }
        }

        public Task CreateTextFileAsync(Path path, string textContent)
        {
            var binaryContent = System.Text.Encoding.UTF8.GetBytes(textContent);
            return CreateBinaryFileAsync(path, binaryContent);
        }

        public Task CreateDirectoryAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            System.IO.Directory.CreateDirectory(absolutePath);
            return Task.CompletedTask;
        }

        public Task DeleteDirectoryAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            System.IO.Directory.Delete(absolutePath);
            return Task.CompletedTask;
        }

        public Task<bool> DeleteFileAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            System.IO.File.Delete(absolutePath);
            return Task.FromResult(true);
        }

        public Task<bool> DirectoryExistsAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            var directoryExists = System.IO.Directory.Exists(absolutePath);
            return Task.FromResult(directoryExists);
        }

        public Task<bool> FileExistsAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            var fileExists = System.IO.File.Exists(absolutePath);
            return Task.FromResult(fileExists);
        }

        public Task<DateTime> GetCreationDateTimeAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            if (System.IO.Directory.Exists(absolutePath))
            {
                return Task.FromResult(System.IO.Directory.GetCreationTime(absolutePath));
            }
            if (System.IO.File.Exists(absolutePath))
            {
                return Task.FromResult(System.IO.File.GetCreationTime(absolutePath));
            }
            throw new InvalidOperationException();
        }

        public Path GetUniqueTempPath()
        {
            return System.IO.Path.Combine(TempPath, Guid.NewGuid().ToString());
        }

        public Task<IEnumerable<Path>> ListFilesAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            var files = System.IO.Directory.GetFiles(absolutePath);
            var pathCollection = files.Select(x => new Path(System.IO.Path.GetRelativePath(_root, x)));
            return Task.FromResult(pathCollection);
        }

        public Task MoveFileAsync(Path sourcePath, Path destinationFile)
        {
            var absoluteSourcePath = _root.Append(sourcePath);
            var absoluteDestinationPath = _root.Append(destinationFile);
            System.IO.File.Move(absoluteSourcePath, absoluteDestinationPath);
            return Task.CompletedTask;
        }

        public async Task<string> ReadAsBase64Async(Path path)
        {
            var binaryContent = await ReadAsBinaryAsync(path);
            return Convert.ToBase64String(binaryContent);
        }

        public Task<byte[]> ReadAsBinaryAsync(Path path)
        {
            var absolutePath = _root.Append(path);
            using (var stream = System.IO.File.OpenRead(absolutePath))
            {
                return Task.FromResult(stream.ReadAllBytes());
            }
        }

        public async Task<string> ReadAsTextAsync(Path path)
        {
            var bynaryContent = await ReadAsBinaryAsync(path);
            var textContent = System.Text.Encoding.UTF8.GetString(bynaryContent);
            return textContent;
        }
    }
}
