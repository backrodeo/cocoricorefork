using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IBufferedRepository
    {
        Task FlushAsync();
    }
}