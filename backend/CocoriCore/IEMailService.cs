﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IEmailService
    {
        Task SendAsync(MailMessage message);
    }
}
