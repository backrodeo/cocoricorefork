﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IReloader
    {
        Task<TEntity> ReloadAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task<TEntity> ReloadAsync<TEntity>(Guid id) where TEntity : class, IEntity;
    }
}
