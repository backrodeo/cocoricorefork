namespace CocoriCore
{
    public class LinuxPath : Path
    {
        public LinuxPath(string path)
        {
            path = System.IO.Path.GetFullPath(path);
            Segments = GetSegments(path);
        }

        protected LinuxPath(params object[] paths)
        {
            Segments = GetSegments(paths);
        }

        public override string ToString()
        {
            return $"/{base.ToString()}";
        }

        public override Path Append(params object[] paths)
        {
            return new LinuxPath(Segments, paths);
        }

        public static bool IsLinuxPath(string path)
        {
            return path.StartsWith("/");
        }

        public static implicit operator LinuxPath(string value)
        {
            return new LinuxPath(value);
        }

        public static implicit operator string(LinuxPath path)
        {
            return path?.ToString();
        }
    }
}
