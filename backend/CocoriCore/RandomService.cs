using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class RandomService : IRandomService
    {
        public Task<string> GenerateRandomString()
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                byte[] bytes = new byte[32];
                provider.GetBytes(bytes);
                return Task.FromResult(Convert.ToBase64String(bytes));
            }
        }
    }
}