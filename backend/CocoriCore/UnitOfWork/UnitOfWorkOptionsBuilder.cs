using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class UnitOfWorkOptionsBuilder
    {
        public UnitOfWorkOptions Options { get; }

        public UnitOfWorkOptionsBuilder()
        {
            Options = new UnitOfWorkOptions();
        }

        public UnitOfWorkOptionsBuilder Call<T>(Func<T, Task> action)
        {
            Options.Actions.Add(new ActionDefinition(typeof(T), o => action((T)o)));
            return this;
        }
    }
}