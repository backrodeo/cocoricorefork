using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class ActionDefinition
    {
        public Type Type;
        public Func<object, Task> Action;

        public ActionDefinition(Type type, Func<object, Task> action)
        {
            Type = type;
            Action = action;
        }
    }
}