﻿namespace CocoriCore
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork NewUnitOfWork();
    }
}