using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IUnitOfWork : IDisposable
    {
        T Resolve<T>();

        object Resolve(Type type);

        Task FinishAsync();
    }
}