using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class TransactionalMemoryRepository : IRepository, ITransactionHolder, IReloader, IBufferedRepository
    {
        private MemoryRepository _repository;
        private TransactionDecorator _decorator;

        public TransactionalMemoryRepository(IInMemoryEntityStore entityStore, IStateStore stateStore, bool buffered = false)
        {
            _repository = new MemoryRepository(entityStore);
            _decorator = new TransactionDecorator(_repository, stateStore, buffered);
        }

        public virtual Task InsertAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            return _decorator.InsertAsync(entity);
        }

        public virtual Task UpdateAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            return _decorator.UpdateAsync(entity);
        }

        public virtual Task DeleteAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            return _decorator.DeleteAsync(entity);
        }

        public virtual Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return _decorator.ExistsAsync(entityType, id);
        }

        public virtual Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            return _decorator.ExistsAsync(entityType, member, value);
        }

        public virtual Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            return _decorator.ExistsAsync(entityType, collection);
        }

        public virtual Task<bool> ExistsAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return _decorator.ExistsAsync<TEntity>(id);
        }

        public virtual Task<object> LoadAsync(Type type, Guid id)
        {
            return _decorator.LoadAsync(type, id);
        }

        public virtual Task<TEntity> LoadAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return _decorator.LoadAsync<TEntity>(id);
        }

        public virtual Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
            where TEntity : class, IEntity
        {
            return _decorator.LoadAsync(uniqueMember, value);
        }

        public virtual Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            return _decorator.LoadAsync(type, ids);
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            return _decorator.Query<TEntity>();
        }

        public virtual IQueryable<IEntity> Query(Type type)
        {
            return _decorator.Query(type);
        }

        public virtual Task<TEntity> ReloadAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            return _repository.ReloadAsync(entity);
        }

        public virtual Task<TEntity> ReloadAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return _repository.ReloadAsync<TEntity>(id);
        }

        public virtual Task CommitAsync()
        {
            return _decorator.CommitAsync();
        }

        public virtual Task RollbackAsync()
        {
            return _decorator.RollbackAsync();
        }

        public virtual void Dispose()
        {
            _decorator.Dispose();
        }

        public Task FlushAsync()
        {
            return ((IBufferedRepository)_decorator).FlushAsync();
        }
    }
}