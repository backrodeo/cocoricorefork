namespace CocoriCore
{
    public class FileResponse
    {
        public byte[] Content;
        public MimeType MimeType;
        public string FileName;
    }
}