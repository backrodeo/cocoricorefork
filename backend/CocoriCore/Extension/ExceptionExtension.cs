using System;
using System.Collections.Generic;

namespace CocoriCore
{
    public static class ExceptionExtension
    {
        public const string ExceptionIdKey = "exceptionId";
        public const string TraceIdKey = "traceId";
        public const string BusContextKey = "busContext";


        public static void GenerateId(this Exception exception)
        {
            exception.SetId(Guid.NewGuid());
        }

        public static void SetId(this Exception exception, Guid id)
        {
            exception.Data[ExceptionIdKey] = id;
        }

        public static Guid? GetId(this Exception exception)
        {
            if (exception != null && exception.Data.Contains(ExceptionIdKey))
                return (Guid)exception.Data[ExceptionIdKey];
            else
                return null;
        }

        public static void SetTraceId(this Exception exception, string traceId)
        {
            exception.Data[TraceIdKey] = traceId;
        }

        public static string GetTraceId(this Exception exception)
        {
            if (exception != null && exception.Data.Contains(TraceIdKey))
                return (string)exception.Data[TraceIdKey];
            else
                return null;
        }
    }
}