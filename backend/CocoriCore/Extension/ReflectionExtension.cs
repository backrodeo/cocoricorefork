using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public static class ReflectionExtension
    {
        public static Type GetMemberType(this MemberInfo memberInfo)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.PropertyType;
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.FieldType;
            }
            else
            {
                throw new NotImplementedException("Unknown member info type : " + memberInfo.GetType());
            }
        }

        public static Type GetMemberType(this LambdaExpression expression, bool allowDepth = false)
        {
            return expression.GetMemberInfo().GetMemberType();
        }

        public static MemberInfo GetMemberInfo<T>(this Expression<Func<T, object>> expression, bool allowDepth = false)
        {
            return GetMemberInfo((LambdaExpression)expression, allowDepth);
        }

        public static MemberInfo GetMemberInfo(this Expression expression, bool allowDepth = false)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));
            if (expression is LambdaExpression)
            {
                if (((LambdaExpression)expression).Body == null)
                    throw new InvalidOperationException("expression body is null");
                return ((LambdaExpression)expression).Body.GetMemberInfo(allowDepth);
            }

            if (expression is UnaryExpression unaryBody)
            {
                var memberExpr = unaryBody.Operand as MemberExpression;
                return memberExpr.Member;
            }
            else if (expression is MemberExpression memberBody)
            {
                if (!allowDepth && expression.ToString().Split('.').Length > 2)
                    throw new InvalidOperationException("No depth allowed for expression " + expression);
                return memberBody.Member;
            }
            else
            {
                throw new NotImplementedException("Unknown expression body type " + expression.GetType());
            }
        }

        public static T InvokeGetter<T>(this MemberInfo memberInfo, object obj)
        {
            return (T)memberInfo.InvokeGetter(obj);
        }

        public static object InvokeGetter(this MemberInfo memberInfo, object obj)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                return propInfo.GetGetMethod().Invoke(obj, new object[0]);
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                return fieldInfo.GetValue(obj);
            }
            else
            {
                throw new InvalidOperationException($"Only {nameof(PropertyInfo)} and {nameof(FieldInfo)} are supported.");
            }
        }

        public static void InvokeSetter(this MemberInfo memberInfo, object obj, object value)
        {
            if (memberInfo is PropertyInfo propInfo)
            {
                propInfo.SetValue(obj, value);
            }
            else if (memberInfo is FieldInfo fieldInfo)
            {
                fieldInfo.SetValue(obj, value);
            }
            else
            {
                throw new InvalidOperationException($"Only {nameof(PropertyInfo)} and {nameof(FieldInfo)} are supported.");
            }
        }

        public static bool IsMethodCall(this Expression expression, Type type, string methodName)
        {
            if (!(expression is MethodCallExpression))
                return false;

            var methodCall = (MethodCallExpression)expression;
            return (methodCall.Method.DeclaringType == type && methodCall.Method.Name == methodName);
        }
    }
}