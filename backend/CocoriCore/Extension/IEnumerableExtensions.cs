﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public static class IEnumerableExtensions
    {
        public static void ForEach<T>(this T[] array, Action<T> action)
        {
            foreach (var element in array)
            {
                action(element);
            }
        }

        public static void ForEachDouble<T1, T2>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2, Action<T1, T2> action)
        {
            var enumerator1 = enumerable1.GetEnumerator();
            var enumerator2 = enumerable2.GetEnumerator();
            while (enumerator1.MoveNext() && enumerator2.MoveNext())
            {
                var value1 = enumerator1.Current;
                var value2 = enumerator2.Current;
                action(value1, value2);
            }
        }

        public static void ForEachTriple<T1, T2, T3>(this IEnumerable<T1> enumerable1, IEnumerable<T2> enumerable2,
            IEnumerable<T3> enumerable3, Action<T1, T2, T3> action)
        {
            var enumerator1 = enumerable1.GetEnumerator();
            var enumerator2 = enumerable2.GetEnumerator();
            var enumerator3 = enumerable3.GetEnumerator();
            while (enumerator1.MoveNext() && enumerator2.MoveNext() && enumerator3.MoveNext())
            {
                var value1 = enumerator1.Current;
                var value2 = enumerator2.Current;
                var value3 = enumerator3.Current;
                action(value1, value2, value3);
            }
        }

        public static bool IsUnique<T>(this IEnumerable<T> enumerable, Func<T, dynamic> member)
        {
            dynamic memberValue = null;
            return IsUnique(enumerable, member, out memberValue);
        }

        public static bool IsUnique<T>(this IEnumerable<T> enumerable, MemberInfo memberInfo)
        {
            dynamic memberValue = null;
            Func<T, dynamic> accessorFunc = x => memberInfo.InvokeGetter(x);
            return IsUnique(enumerable, accessorFunc, out memberValue);
        }

        public static bool IsUnique<TElement, TMemberValue>(this IEnumerable<TElement> enumerable,
            Func<TElement, TMemberValue> member, out TMemberValue duplicate)
        {
            HashSet<TMemberValue> set = new HashSet<TMemberValue>();
            foreach (var element in enumerable)
            {
                var memberValue = member.Invoke(element);
                if (!set.Add(memberValue))
                {
                    duplicate = memberValue;
                    return false;
                }
            }
            duplicate = default(TMemberValue);
            return true;
        }

        public static T[] RemoveAt<T>(this T[] array, int index)
            where T : class
        {
            array[index] = null;
            return array.Where(x => x != null).ToArray();
        }

        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> collection)
        {
            return new Dictionary<TKey, TValue>(collection);
        }

        public static Dictionary<Type, Type> IndexTypesByGenericParameter(this IEnumerable<System.Reflection.Assembly> assemblies, Type genericType)
        {
            if (!genericType.IsGenericTypeDefinition)
            {
                throw new ArgumentException($"{genericType} must be a generic type definition.");
            }
            if (genericType.GetGenericArguments().Count() != 1)
            {
                throw new ArgumentException($"{genericType} must have one and only one generic argument.");
            }
            return assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t => t.IsAssignableToGeneric(genericType) && t.IsConcrete())
                .ToDictionary(t => t.GetGenericArguments(genericType).First(), t => t);
        }

        public static IEnumerable<T> OrderBy<T, TProperty>(this IEnumerable<T> enumerable,
            Func<T, TProperty> memberSelector, IEnumerable<TProperty> collection)
        {
            var order = new Dictionary<TProperty, int>();
            int position = 0;
            foreach (var element in collection)
            {
                order[element] = position++;
            }
            return enumerable.OrderBy(memberSelector, new KeyOrderComparer<TProperty>(order));
        }

        class KeyOrderComparer<TKey> : IComparer<TKey>
        {
            private Dictionary<TKey, int> _order;

            public KeyOrderComparer(Dictionary<TKey, int> order)
            {
                _order = order;
            }

            public int Compare(TKey x, TKey y)
            {
                return _order[x].CompareTo(_order[y]);
            }
        }

        public static IndexedLists<TKey, TValue> ToIndexedLists<TKey, TValue>(this IEnumerable<TValue> enumerable, Func<TValue, TKey> keySelector)
        {
            var indexedLists = new IndexedLists<TKey, TValue>();
            foreach (var element in enumerable)
            {
                var key = keySelector(element);
                indexedLists.Add(key, element);
            }
            return indexedLists;
        }

        public static bool HasSingle<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate, out T result)
            where T : class
        {
            var results = enumerable.Where(predicate);
            if (results.Count() == 1)
            {
                result = results.First();
                return true;
            }
            result = null;
            return false;
        }

        public static TValue TryGetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary,
            TKey key, TValue defaultValue)
        {
            TValue value = default(TValue);
            if (dictionary.TryGetValue(key, out value))
            {
                return value;
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
