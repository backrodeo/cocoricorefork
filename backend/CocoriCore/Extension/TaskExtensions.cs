using System.Threading.Tasks;

namespace CocoriCore
{
    public static class TaskExtensions
    {
        public static async Task<T> GetResultAsync<T>(this Task task)
        {
            await task;
            return (T)task.GetType().GetProperty(nameof(Task<object>.Result)).InvokeGetter(task);
        }
    }
}