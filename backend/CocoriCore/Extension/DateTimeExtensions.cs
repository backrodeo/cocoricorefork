﻿using System;

namespace CocoriCore
{
    public static class DateTimeExtensions
    {
        public static bool IsDefault(this DateTime dateTime)
        {
            return dateTime == default(DateTime);
        }

        public static bool IsDateWithoutTime(this DateTime dateTime)
        {
            return dateTime.TimeOfDay == TimeSpan.Zero;
        }
    }
}
