using System;
using System.IO;
using System.Threading.Tasks;

namespace CocoriCore
{
    public static class StreamExtension
    {
        public static async Task<string> ReadBodyAsStringAsync(this Stream body)
        {
            var sr = new StreamReader(body);
            string bodyStr = await sr.ReadToEndAsync();
            return bodyStr;
        }

        public static byte[] ReadAllBytes(this Stream stream)
        {
            byte[] bytes;
            using (stream)
            {
                int index = 0;
                long fileLength = stream.Length;
                if (fileLength > Int32.MaxValue)
                    throw new IOException("File is too big to be read once a time.");
                int count = (int)fileLength;
                bytes = new byte[count];
                while (count > 0)
                {
                    int n = stream.Read(bytes, index, count);
                    if (n == 0)
                        throw new IOException("End of file.");
                    index += n;
                    count -= n;
                }
            }
            return bytes;
        }
    }
}