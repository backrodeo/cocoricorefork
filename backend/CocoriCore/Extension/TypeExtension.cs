using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore
{
    public static class TypeExtension
    {
        public static IEnumerable<Type> GetTypeHierarchy(this object obj)
        {
            if (obj == null)
                return new Type[0];
            return obj.GetType().GetTypeHierarchy();
        }

        public static IEnumerable<Type> GetTypeHierarchy(this Type type)
        {
            List<Type> types = new List<Type>();
            types.Add(type);
            types.AddRange(type.GetBaseTypes());
            types.AddRange(type.GetInterfaces());
            return types;
        }

        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            List<Type> superTypes = new List<Type>();
            type = type.BaseType;
            while (type != null)
            {
                superTypes.Add(type);
                type = type.BaseType;
            }
            return superTypes;
        }

        public static Type GetFirstBaseType(this Type type, Type baseType)
        {
            return type.GetBaseTypes().First(t => t.IsGenericType && t.GetGenericTypeDefinition() == baseType);
        }

        public static bool IsAssignableToGeneric(this Type type, Type genericType)
        {
            return type
                .GetTypeHierarchy()
                .Any(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == genericType);
        }

        public static bool IsAssignableTo(this Type currentType, Type type)
        {
            return type.IsAssignableFrom(currentType);
        }

        public static bool IsAssignableTo<T>(this Type currentType)
        {
            return typeof(T).IsAssignableFrom(currentType);
        }

        public static bool IsNullable(this Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }

        public static bool IsNullable(this Type type, out Type nonNullableType)
        {
            var underlyingType = Nullable.GetUnderlyingType(type);
            nonNullableType = underlyingType ?? type;
            return underlyingType != null;
        }

        public static Type[] GetGenericArguments(this Type type, Type genericSuperType)
        {
            return type
                .GetTypeHierarchy()
                .First(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == genericSuperType)
                .GenericTypeArguments;
        }

        public static Type GetInterface(this Type type, Type genericInterfaceType)
        {
            IEnumerable<Type> interfaces = type.GetInterfaces();
            if (type.IsInterface)
            {
                interfaces = interfaces.Append(type);
            }
            return interfaces.First(t => t.IsGenericType && t.GetGenericTypeDefinition() == genericInterfaceType);
        }

        public static ReturnType CreateGenericInstance<ReturnType>(this Type type, Type contentType, params object[] parameters)
        {
            var _genericType = type.MakeGenericType(contentType);
            return (ReturnType)Activator.CreateInstance(_genericType, parameters);
        }

        public static MemberInfo GetPropertyOrField(this Type type, string name, BindingFlags bindingAttr)
        {
            MemberInfo memberInfo = type.GetPropertyOrFieldOrDefault(name, bindingAttr);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            throw new MissingMemberException($"No property neither field named '{name}' found for type {type}");
        }

        public static MemberInfo GetPropertyOrFieldOrDefault(this Type type, string name, BindingFlags bindingAttr)
        {
            MemberInfo memberInfo = type.GetProperty(name, bindingAttr);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            memberInfo = type.GetField(name, bindingAttr);
            if (memberInfo != null)
            {
                return memberInfo;
            }
            return null;
        }

        public static IEnumerable<MemberInfo> GetPropertiesAndFields(this Type type)
        {
            List<MemberInfo> propertiesAndFields = new List<MemberInfo>();
            propertiesAndFields.AddRange(type.GetProperties());
            propertiesAndFields.AddRange(type.GetFields());
            return propertiesAndFields;
        }

        public static bool IsConcrete(this Type type)
        {
            return !type.IsAbstract && !type.IsInterface;
        }

        public static object ImplicitConvertTo(this object value, Type type)
        {
            if (!value.GetType().IsAssignableTo(type))
            {
                var method = type.GetMethod("op_Implicit", new Type[] { value.GetType() });
                if (method == null)
                {
                    throw new NotImplementedException($"No implicit operator found to convert object from {value?.GetType()?.FullName} to {type?.FullName}.");
                }
                value = method.Invoke(null, new object[] { value });
            }
            return value;
        }

        public static object GetDefault(this Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static string GetPrettyName(this Type type)
        {
            var typeName = type.Name;
            if (type.IsGenericType)
            {
                typeName = type.Name.Substring(0, type.Name.IndexOf("`"));
                var genericParamaters = type
                    .GetGenericArguments()
                    .Select(x => x.Name)
                    .ToArray();
                typeName += $"<{string.Join(", ", genericParamaters)}>";
            }
            return typeName;
        }
    }
}