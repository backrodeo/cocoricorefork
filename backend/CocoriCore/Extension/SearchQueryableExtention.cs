﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public enum SearchBehavior
    {
        MultiKeywordsOr,
        MultiKeywordsAnd,
    }

    public static class SearchQueryableExtention
    {
        private enum AppendBehavior
        {
            Or,
            And,
        }

        private static MethodInfo _toLowerMethod = typeof(string).GetMethod(nameof(string.ToLower), new Type[0]);
        private static MethodInfo _containsMethod = typeof(string).GetMethod(nameof(string.Contains), new[] { typeof(string) });

        public static IQueryable<T> Search<T>(this IQueryable<T> queryable, string keywords, params Expression<Func<T, string>>[] fields)
        {
            return queryable.Search(keywords, SearchBehavior.MultiKeywordsOr, fields);
        }

        public static IQueryable<T> Search<T>(this IQueryable<T> queryable, string keywords, SearchBehavior behavior,
            params Expression<Func<T, string>>[] fields)
        {
            if (string.IsNullOrEmpty(keywords))
            {
                return queryable;
            }
            var keywordsArray = ExtractKeywords(keywords);
            Expression searchExpression = null;
            var parameter = Expression.Parameter(typeof(T), "x");
            for (var i = 0; i < keywordsArray.Length; i++)
            {
                var keyword = keywordsArray[i];
                Expression keywordExpression = null;
                foreach (Expression<Func<T, string>> field in fields)
                {
                    var andExpression = ConstructFieldContainsExpression(parameter, field, keyword);
                    keywordExpression = AppendExpression(keywordExpression, AppendBehavior.Or, andExpression);
                }
                var appendBehavior = GetAppendBehavior(behavior);
                searchExpression = AppendExpression(searchExpression, appendBehavior, keywordExpression);
            }
            var predicate = Expression.Lambda<Func<T, bool>>(searchExpression, parameter);
            return queryable.Where(predicate);
        }

        private static string[] ExtractKeywords(string keywords)
        {
            return keywords
                .Split(' ')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .ToArray();
        }

        private static Expression ConstructFieldContainsExpression<T>(ParameterExpression parameter,
            Expression<Func<T, string>> field, string keyword)
        {
            var keywordExpression = Expression.Constant(keyword.ToLower());
            var fieldExpression = Expression.MakeMemberAccess(parameter, field.GetMemberInfo());
            var notNullFieldExpression = Expression.NotEqual(fieldExpression, Expression.Constant(null, typeof(string)));
            var fieldToLowerExpression = Expression.Call(fieldExpression, _toLowerMethod);
            var containsExpression = Expression.Call(fieldToLowerExpression, _containsMethod, new[] { keywordExpression });
            var andExpression = Expression.AndAlso(notNullFieldExpression, containsExpression);
            return andExpression;
        }

        private static Expression AppendExpression(Expression expression, AppendBehavior behavior, Expression expressionToAppend)
        {
            if (expression == null)
            {
                return expressionToAppend;
            }
            else if (behavior == AppendBehavior.Or)
            {
                return Expression.OrElse(expression, expressionToAppend);
            }
            else if (behavior == AppendBehavior.And)
            {
                return Expression.AndAlso(expression, expressionToAppend);
            }
            else
            {
                throw new NotImplementedException(behavior.ToString());
            }
        }

        private static AppendBehavior GetAppendBehavior(SearchBehavior behavior)
        {
            if (behavior == SearchBehavior.MultiKeywordsAnd)
            {
                return AppendBehavior.And;
            }
            else if (behavior == SearchBehavior.MultiKeywordsOr)
            {
                return AppendBehavior.Or;
            }
            else
            {
                throw new NotImplementedException(behavior.ToString());
            }
        }
    }
}
