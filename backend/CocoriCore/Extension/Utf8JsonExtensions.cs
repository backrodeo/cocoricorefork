﻿using AutoMapper;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using Utf8Json;

namespace CocoriCore
{
    public static class Utf8Json
    {
        private static MethodInfo _deserializeMethod = typeof(JsonSerializer)
            .GetMethods()
            .Where(m =>
                m.Name == nameof(JsonSerializer.Deserialize) &&
                m.GetParameters().Count() == 1 &&
                m.GetParameters()[0].ParameterType == typeof(byte[]))
            .Single();

        public static byte[] Serialize(object obj)
        {
            return JsonSerializer.Serialize(obj);
        }

        public static T Deserialize<T>(byte[] bytes)
        {
            return JsonSerializer.Deserialize<T>(bytes);
        }

        public static object Deserialize(Type type, byte[] bytes)
        {
            var deserializeMethod = _deserializeMethod.MakeGenericMethod(type);
            return deserializeMethod.Invoke(null, new[] { bytes });
        }

        public static object CreateCopy(object obj)
        {
            return CreateCopy(obj, out byte[] state);
        }

        public static object CreateCopy(object obj, out byte[] jsonBytes)
        {
            jsonBytes = null;
            try
            {
                jsonBytes = Serialize(obj);
                return Deserialize(obj.GetType(), jsonBytes);
            }
            catch (Exception e)
            {
                var exception = new SerializationException("Error while creating an object copy, see inner and Data for details.", e);
                exception.Data["json"] = jsonBytes == null ? null : Encoding.UTF8.GetString(jsonBytes);
                exception.Data["type"] = obj?.GetType();
                throw exception;
            }
        }
    }
}
