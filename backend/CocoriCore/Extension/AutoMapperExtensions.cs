﻿using AutoMapper;
using System;
using System.Linq.Expressions;

namespace CocoriCore
{
    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> MapMemberFrom<TSource, TDestination, TSourceField, TDestinationField>(
            this IMappingExpression<TSource, TDestination> mappingExpression,
            Expression<Func<TDestination, TDestinationField>> destinationMember,
            Expression<Func<TSource, TSourceField>> sourceMember)
        {
            return mappingExpression.ForMember(destinationMember, o => o.MapFrom(sourceMember));
        }
    }
}
