using System;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public class NotUniqueException : Exception
    {
        public MemberInfo[] Members { get; }
        public string FieldName { get; }

        public NotUniqueException(string message, string fieldName, params MemberInfo[] members)
        : base(message)
        {
            Members = members;
            FieldName = fieldName;
        }

        public NotUniqueException(string message, MemberInfo member)
        : base(message)
        {
            Members = new[] { member };
            FieldName = member.Name;
        }
    }

    public class NotUniqueException<T> : NotUniqueException
    {
        public NotUniqueException(string message, Expression<Func<T, object>> expression)
            : base(message, expression.GetMemberInfo())
        {
        }
    }
}
