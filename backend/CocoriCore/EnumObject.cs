﻿using System;

namespace CocoriCore
{
    public class EnumObject<TEnum> : ValueObject<TEnum>, IEnumObject
            where TEnum : struct, IComparable, IFormattable, IConvertible
    {
        public Type EnumType => typeof(TEnum);

        public EnumObject(TEnum value)
        {
            _value = value;
        }
        public EnumObject()
        {
        }

        object IValueObject.Value => _value;

        public EnumObject(string value)
        {
            _value = (TEnum)Enum.Parse(typeof(TEnum), value);
        }

        public static implicit operator EnumObject<TEnum>(TEnum value)
        {
            return new EnumObject<TEnum>(value);
        }

        public static implicit operator string(EnumObject<TEnum> value)
        {
            return value.Value.ToString();
        }

        public static bool operator ==(EnumObject<TEnum> enumObject, TEnum enumValue)
        {
            if (enumObject == null)
                return false;
            return object.Equals(enumObject.Value, enumValue);
        }

        public static bool operator !=(EnumObject<TEnum> enumObject, TEnum enumValue)
        {
            if (enumObject == null)
                return true;
            return !object.Equals(enumObject.Value, enumValue);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

}
