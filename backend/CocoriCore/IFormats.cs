﻿namespace CocoriCore
{
    public interface IFormats
    {
        string Get<T>();
        void Set<T>(string format);
        bool DateTimeInUTC { get; }
    }
}