using System;

namespace CocoriCore
{
    public interface IClaims
    {
        DateTime ExpireAt { get; }
    }
}