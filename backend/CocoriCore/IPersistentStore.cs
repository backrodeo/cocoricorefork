using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IPersistentStore
    {
        void Clear();
    }
}