﻿using System;

namespace CocoriCore
{
    public class Clock : IClock
    {
        public DateTime Now => DateTime.UtcNow;

        public DateTime Today => Now.Date;
    }
}
