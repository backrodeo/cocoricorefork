using Microsoft.AspNetCore.Hosting;
using NLog;
using NLog.Config;
using CocoriCore;

namespace CocoriCore.NLog
{
    public class NLogFactory : ILoggerFactory
    {
        public NLogFactory(string nLogConfigPath)
        {
            if (!System.IO.File.Exists(nLogConfigPath))
                throw new ConfigurationException($"NLog configuration file not found at {nLogConfigPath}.");
            LogManager.Configuration = new XmlLoggingConfiguration(nLogConfigPath);
        }

        public ILogger GetLogger(string name)
        {
            return LogManager.GetLogger(name);
        }
    }
}