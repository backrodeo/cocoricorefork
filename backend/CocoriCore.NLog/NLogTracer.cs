﻿
using CocoriCore;
using NLog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.NLog
{
    public class NLogTracer : ITracer
    {
        private ILogger _logger;

        public NLogTracer(ILogger logger)
        {
            _logger = logger;
        }

        public Task Trace(object obj)
        {
            _logger.Trace(obj.ToString());
            return Task.CompletedTask;
        }
    }
}
