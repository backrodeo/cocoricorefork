using System;
using System.Linq;
using Autofac;
using Autofac.Core;
using NLog;

namespace CocoriCore.NLog
{
    public static class ContainerBuilderExtension
    {
        public static ContainerBuilder ConfigureNLog(this ContainerBuilder builder, ILoggerFactory factory)
        {
            builder.RegisterModule<LoggerModule>();
            builder.RegisterInstance(factory).As<ILoggerFactory>();
            builder.Register((c, p) =>
            {
                //TODO à tester, c'est pour le cas ou on fait un scope.Resolve<ILogger>() qu'il faut un nom par défaut
                var typeParameter = p.OfType<TypedParameter>().FirstOrDefault();
                var loggerName = " CocoriCore.DefaultLogger";
                if (typeParameter != null && typeParameter.Value is Type typeValue)
                    loggerName = typeValue.FullName;

                return factory.GetLogger(loggerName);
            }).As<ILogger>();
            return builder;
        }
    }

    public class LoggerModule : Module
    {
        protected override void AttachToComponentRegistration(
            IComponentRegistry registry, IComponentRegistration registration)
        {
            registration.Preparing +=
                (sender, args) =>
                {
                    var forType = args.Component.Activator.LimitType;

                    var logParameter = new ResolvedParameter(
                        (p, c) => p.ParameterType == typeof(ILogger),
                        (p, c) => c.Resolve<ILogger>(TypedParameter.From(forType)));

                    args.Parameters = args.Parameters.Union(new[] { logParameter });
                };
        }
    }
}
