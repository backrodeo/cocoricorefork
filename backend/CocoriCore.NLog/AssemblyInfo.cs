﻿using System.Reflection;

namespace CocoriCore.NLog
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
