using System.Threading.Tasks;
using NLog;

namespace CocoriCore.NLog
{
    public static class ErrorBusRuleExtension
    {
        public static ErrorBusRule TraceLog(this ErrorBusRule conf)
        {
            return conf.Call<ILogger>((l, e) => { l.Error(e); return Task.CompletedTask; });
        }

    }
}