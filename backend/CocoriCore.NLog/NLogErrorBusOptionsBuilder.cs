using System;
using System.Threading.Tasks;
using NLog;

namespace CocoriCore.NLog
{
    public class NLogErrorBusOptionsBuilder : ErrorBusOptionsBuilder
    {
        public NLogErrorBusOptionsBuilder(ILogger logger)
        {
            For<Exception>().Call<ILogger>((log, context) =>
            {
                log.Error(context);
                return Task.CompletedTask;
            });

            SetUnexpectedExceptionHandler((ue, e) =>
            {
                logger.Fatal(ue);
                if (e != null)
                    logger.Error(e);
                return Task.CompletedTask;
            });
        }
    }
}