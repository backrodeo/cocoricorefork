using NLog;

namespace CocoriCore 
{
    public interface ILoggerFactory
    {
        ILogger GetLogger(string name);
    }
}