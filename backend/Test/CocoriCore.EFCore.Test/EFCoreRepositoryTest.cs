using System;
using System.Data;
using System.Threading.Tasks;
using CocoriCore.TestUtils;
using FluentAssertions;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Xunit;
using CocoriCore;
using CocoriCore.Linq.Async;
using System.Linq;

namespace CocoriCore.EFCore.Test
{
    public class EFCoreRepositoryTest
    {
        private DbContextOptions _options;
        private IMemoryCache _cache;

        public EFCoreRepositoryTest()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var builder = new DbContextOptionsBuilder().UseSqlite(connection);
            _cache = new MemoryCache(new MemoryCacheOptions());
            _options = builder.Options;

            using (var repository = new EFCoreRepository(_options, _cache, DefineMappings))
            {
                repository.Database.EnsureCreated();
            };
        }

        private EFCoreRepository CreateRepository(bool buffered = false, bool useCocoriCoreCache = false)
        {
            return new EFCoreRepository(_options, _cache, DefineMappings, buffered, useCocoriCoreCache);
        }

        [Fact]
        public async Task CanInsertEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);

                var exists = await repository.ExistsAsync<AnEntity>(id);
                exists.Should().BeTrue();

                var entity2 = await repository.LoadAsync<AnEntity>(id);
                entity2.Name.Should().Be("toto");
                entity2.Should().BeSameAs(entity);

                var entity3 = (AnEntity)await repository.LoadAsync(typeof(AnEntity), id);
                entity3.Name.Should().Be("toto");
                entity3.Should().BeSameAs(entity);


                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("toto");
            }
        }

        [Fact]
        public async Task CanUpdateEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("titi");
            }
        }

        [Fact]
        public async Task CanDeleteEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.DeleteAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var exists = await repository.ExistsAsync<AnEntity>(id);
                exists.Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackTransaction()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = "titi";
                await repository.UpdateAsync(entity);
                await repository.RollbackAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name.Should().Be("toto");
            }
        }

        [Fact]
        public async Task FlushAndCommitAfterRollbackHasNoEffect()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository(true, true))
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await repository.RollbackAsync();
                await repository.FlushAsync();
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var exists = await repository.ExistsAsync<AnEntity>(id);
                exists.Should().BeFalse();
            }
        }

        [Fact]
        public async Task ExceptionIfNoEntityWithProviderId()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var wrongId = Guid.NewGuid();
                Func<Task<AnEntity>> action1 = () => repository.LoadAsync<AnEntity>(wrongId);
                action1.Should().Throw<InvalidOperationException>()
                    .Which.Message.Should().Be($"There is no entity of type {typeof(AnEntity)} with id {wrongId}.");
                Func<Task<object>> action2 = () => repository.LoadAsync(typeof(AnEntity), wrongId);
                action2.Should().Throw<InvalidOperationException>()
                    .Which.Message.Should().Be($"There is no entity of type {typeof(AnEntity)} with id {wrongId}.");
            }
        }

        [Fact]
        public async Task CanLoadByUniqueMember()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity = new AnEntity();
                entity.Id = id;
                entity.Name = "toto";
                entity.NullableId = id;
                await repository.InsertAsync(entity);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity1 = await repository.LoadAsync<AnEntity>(x => x.Name, "toto");
                entity1.Id.Should().Be(id);
                var entity2 = await repository.LoadAsync<AnEntity>(x => x.NullableId, id);
                entity2.Id.Should().Be(id);
            }
        }

        [Fact]
        public async Task ExceptionIfZeroOrMoreThanOneResultForloadByUniqueMember()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity();
                entity1.Id = Guid.NewGuid();
                entity1.Name = "toto";
                var entity2 = new AnEntity();
                entity2.Id = Guid.NewGuid();
                entity2.Name = "toto";
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                Func<Task<AnEntity>> action1 = () => repository.LoadAsync<AnEntity>(x => x.Name, "titi");
                action1.Should().Throw<InvalidOperationException>()
                    .Which.Message.Should().Be($"There is no entity of type {typeof(AnEntity)} with value 'titi' "
                    + $"for unique field Name.");
                Func<Task<AnEntity>> action2 = () => repository.LoadAsync<AnEntity>(x => x.Name, "toto");
                action2.Should().Throw<InvalidOperationException>()
                    .Which.Message.Should().Be($"There are several entities of type {typeof(AnEntity)} with value 'toto' "
                    + $"for unique field Name, use Query() method instead.");
            }
        }

        [Fact]
        public async Task CanCheckIfEntityExistsOrNot()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity();
                entity1.Id = id;
                entity1.Name = "toto";
                await repository.InsertAsync(entity1);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var exists1 = await repository.ExistsAsync<AnEntity>(id);
                exists1.Should().BeTrue();
                var exists2 = await repository.ExistsAsync(typeof(AnEntity), id);
                exists2.Should().BeTrue();
                var exists3 = await repository.ExistsAsync(typeof(AnEntity), typeof(AnEntity).GetProperty("Name"), "toto");
                exists3.Should().BeTrue();
            }

            using (var repository = CreateRepository())
            {
                var exists1 = await repository.ExistsAsync<AnEntity>(Guid.NewGuid());
                exists1.Should().BeFalse();
                var exists2 = await repository.ExistsAsync(typeof(AnEntity), Guid.NewGuid());
                exists2.Should().BeFalse();
                var exists3 = await repository.ExistsAsync(typeof(AnEntity), typeof(AnEntity).GetProperty("Name"), "titi");
                exists3.Should().BeFalse();
                var exists4 = await repository.ExistsAsync(typeof(AnEntity), typeof(AnEntity).GetProperty("Name"), null);
                exists4.Should().BeFalse();
            }
        }

        [Fact]
        public async Task CheckExistenceForIdCollection()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity();
                entity1.Id = id1;
                entity1.Name = null;
                var entity2 = new AnEntity();
                entity2.Id = id2;
                entity2.Name = null;
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var exists1 = await repository.ExistsAsync(typeof(AnEntity), new[] { id1, id2, Guid.NewGuid() });
                exists1.Should().BeFalse();
                var exists2 = await repository.ExistsAsync(typeof(AnEntity), new[] { id1, id2 });
                exists2.Should().BeTrue();
                var exists3 = await repository.ExistsAsync(typeof(AnEntity), new[] { id1, id2, id2 });
                exists3.Should().BeTrue();
            }
        }

        [Fact]
        public async Task CanReloadEntity()
        {
            var id = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity();
                entity1.Id = id;
                entity1.Name = "toto";
                var entity2 = new AnotherEntity();
                entity2.Id = id;
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                var anotherEntity = await repository.LoadAsync<AnotherEntity>(id);
                anotherEntity.Name = "test";

                await UpdateEntityName(id, "titi");
                await repository.UpdateAsync(anotherEntity);

                entity.Name.Should().Be("toto");
                var entityReloaded1 = await repository.ReloadAsync<AnEntity>(id);
                entityReloaded1.Name.Should().Be("titi");
                var entityReloaded2 = await repository.ReloadAsync(entity);
                entityReloaded2.Name.Should().Be("titi");
            }
        }

        private async Task UpdateEntityName(Guid id, string name)
        {
            using (var repository = CreateRepository())
            {
                var entity = await repository.LoadAsync<AnEntity>(id);
                entity.Name = name;
                await repository.UpdateAsync(entity);
                await repository.CommitAsync();
            }
        }

        [Fact]
        public async Task CanQueryEntity()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AnEntity().WithId();
                entity1.Name = "toto";
                var entity2 = new AnEntity().WithId();
                entity2.Name = "titi";
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<AnEntity>().Where(x => x.Name != null).OrderBy(x => x.Name).Take(10);
                var entities = await AsyncQueryableExtensions.ToArrayAsync(request);
                entities.Select(x => x.Name).Should().BeEquivalentTo("toto", "titi");
            }
        }

        [Fact]
        public async Task CanQueryAbstractClass()
        {
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1().WithId();
                entity1.Name = "toto";
                var entity2 = new AConcreteEntity2().WithId();
                entity2.Name = "titi";
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<AnAbstractEntity>().Where(x => x.Name != null).OrderBy(x => x.Name);
                var entities = await AsyncQueryableExtensions.ToArrayAsync(request);
                entities.Select(x => x.Name).Should().BeEquivalentTo("titi", "toto");
            }
        }

        [Fact]
        public async Task CanQueryInterface()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var id3 = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1();
                entity1.Id = id1;
                entity1.Name = "toto";
                var entity2 = new AConcreteEntity2();
                entity2.Id = id2;
                entity2.Name = "titi";
                var entity3 = new AnEntity();
                entity3.Id = id3;
                entity3.Name = "tutu";
                await repository.InsertManyAsync(entity1, entity2, entity3);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var request = repository.Query<IEntity>().Where(x => x.Id != default);
                var entities = await AsyncQueryableExtensions.ToArrayAsync(request);
                entities.Select(x => x.Id).Should().BeEquivalentTo(id1, id2, id3);
            }
        }

        [Fact]
        public async Task CanLoadAbstractClass()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            using (var repository = CreateRepository())
            {
                var entity1 = new AConcreteEntity1();
                entity1.Id = id1;
                entity1.Name = "toto";
                var entity2 = new AConcreteEntity2();
                entity2.Id = id2;
                entity2.Name = "titi";
                await repository.InsertManyAsync(entity1, entity2);
                await repository.CommitAsync();
            }

            using (var repository = CreateRepository())
            {
                var exists1 = await repository.ExistsAsync<AnAbstractEntity>(id1);
                exists1.Should().BeTrue();
                var exists2 = await repository.ExistsAsync<AnAbstractEntity>(id2);
                exists2.Should().BeTrue();
                var exists3 = await repository.ExistsAsync<AnAbstractEntity>(Guid.NewGuid());
                exists3.Should().BeFalse();
            }
        }

        private void DefineMappings(ModelBuilder buidler)
        {
            buidler.Entity<AnEntity>();
            buidler.Entity<AnotherEntity>();
            buidler.Entity<AnAbstractEntity>()
                .HasDiscriminator<int>("type")
                .HasValue<AConcreteEntity1>(1)
                .HasValue<AConcreteEntity2>(2);
            buidler.Entity<AConcreteEntity1>();
            buidler.Entity<AConcreteEntity2>();
        }
    }

    public class AnEntity : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid? NullableId { get; set; }
    }

    public abstract class AnAbstractEntity : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class AConcreteEntity1 : AnAbstractEntity
    {
        public DateTime Date { get; set; }
    }

    public class AConcreteEntity2 : AnAbstractEntity
    {
        public int Age { get; set; }
    }


    public class AnotherEntity : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
