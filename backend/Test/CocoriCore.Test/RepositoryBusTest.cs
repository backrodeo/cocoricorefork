using System;
using Xunit;
using CocoriCore;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Autofac;
using System.Diagnostics;
using System.Linq;
using Autofac.Core;
using System.Threading;
using CocoriCore.Linq.Async;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class RepositoryBusTest : BusTestBase
    {
        private RepositoryBusOptionsBuilder _builder;

        public RepositoryBusTest()
        {
            _builder = new RepositoryBusOptionsBuilder();
            _unitOfWorkBuilder.Call<IRepositoryBus>(b => b.ApplyRulesAsync());

            ContainerBuilder.RegisterInstance(_builder).AsSelf();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<TransactionalMemoryRepository>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusDecorator>()
                .As<IRepository>().AsSelf()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<TransactionalMemoryRepository>()))
                .WithParameter(new TypedParameter<ITransactionHolder>(c => c.Resolve<TransactionalMemoryRepository>()))
                .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _builder.Options).As<RepositoryBusOptions>();
            ContainerBuilder.RegisterType<RepositoryBus>().AsSelf().As<IRepositoryBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusContextStore>().As<IRepositoryBusContextStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            ContainerBuilder.RegisterType<SpyFnFExceptionHandler>().AsSelf().SingleInstance();
        }

        protected IInMemoryEntityStore MemoryStore => RootScope.Resolve<IInMemoryEntityStore>();

        [Fact]
        public async Task WhenInsertEntityCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenInsert<AnEntity>().Call<ASpyHandler>(h => h.DoSomethingAsync);
            _builder.WhenInsert<AnEntity>().Call<ASpyHandler>((h, c) => h.DoSomethingAsync(c));
            _builder.WhenInsert<AnEntity>().Call<ASpyHandler>();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(3);
        }

        [Fact]
        public async Task WhenUpdateEntityCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<AnEntity>().Call<ASpyHandler>(h => h.DoSomethingAsync);
            _builder.WhenUpdate<AnEntity>().Call<ASpyHandler>((h, c) => h.DoSomethingAsync(c));
            _builder.WhenUpdate<AnEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(3);
        }

        [Fact]
        public async Task WhenDeleteEntityCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenDelete<AnEntity>().Call<ASpyHandler>(h => h.DoSomethingAsync);
            _builder.WhenDelete<AnEntity>().Call<ASpyHandler>((h, c) => h.DoSomethingAsync(c));
            _builder.WhenDelete<AnEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(3);
        }

        [Fact]
        public async Task WhenModifyEntityCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenModify<AnEntity>().Call<ASpyHandler>(h => h.DoSomethingAsync);
            _builder.WhenModify<AnEntity>().Call<ASpyHandler>((h, c) => h.DoSomethingAsync(c));
            _builder.WhenModify<AnEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(9);
        }

        [Fact]
        public async Task WhenModifyInterfaceCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenModify<IEntity>().Call<ASpyHandler>();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(1);
        }

        [Fact]
        public async Task WhenModifyAbstractCallActions()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenModify<AnAbstractEntity>().Call<ASpyHandler>();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(1);
        }

        [Fact]
        public async Task WhenInsertEntityCallActionAndForget()
        {
            var handler = new ASpyHandler();
            handler.Delay = 50;
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _builder.WhenInsert<IEntity>().Call<ASpyHandler>().FireAndForget();

            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(0);
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());
            handler.NbCall.Should().Be(1);
            RootScope.Resolve<SpyFnFExceptionHandler>().NbCall.Should().Be(0);
        }

        [Fact]
        public async Task WhenUpdateEntityCallActionAndForget()
        {
            var handler = new ASpyHandler();
            handler.Delay = 50;
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>().FireAndForget();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(0);
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());
            handler.NbCall.Should().Be(1);
            RootScope.Resolve<SpyFnFExceptionHandler>().NbCall.Should().Be(0);
        }

        [Fact]
        public async Task WhenDeleteEntityCallActionAndForget()
        {
            var handler = new ASpyHandler();
            handler.Delay = 50;
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _builder.WhenDelete<IEntity>().Call<ASpyHandler>().FireAndForget();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(0);
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());
            handler.NbCall.Should().Be(1);
            RootScope.Resolve<SpyFnFExceptionHandler>().NbCall.Should().Be(0);
        }

        // [Fact]
        // public async Task WhenModifyEntityCallActionAndForget()
        // {
        //     var handler = new ASpyHandler();
        //     handler.Delay = 50;
        //     ContainerBuilder.RegisterInstance(handler).AsSelf();
        //     _builder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
        //     _builder.WhenModify<IEntity>().Call<ASpyHandler>().FireAndForget();

        //     RepositoryBus repositoryBus = null;
        //     using (var uow = UnitOfWorkFactory.NewUnitOfWork())
        //     {
        //         var repository = uow.Resolve<IRepository>();
        //         repositoryBus = uow.Resolve<RepositoryBus>();
        //         var entity = new AnEntity().WithId();
        //         await repository.InsertAsync(entity);
        //         entity = await repository.LoadAsync<AnEntity>(entity.Id);
        //         await repository.UpdateAsync(entity);
        //         await repository.DeleteAsync(entity);
        //         await uow.FinishAsync();
        //     }

        //     handler.NbCall.Should().Be(0);
        //     Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());
        //     handler.NbCall.Should().Be(3);
        //     RootScope.Resolve<SpyFnFExceptionHandler>().NbCall.Should().Be(0);
        // }

        [Fact]
        public async Task StateIsNullWhenInsertEntity()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenInsert<IEntity>().Call<ASpyHandler>();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            handler.PassedContext.GetState<AnEntity>().Should().BeNull();
        }

        [Fact]
        public async Task HandlerNotCalledWhenStateNotChanged()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.NbCall.Should().Be(0);
        }

        [Fact]
        public async Task StateIsNotNullWheDeleteEntity()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenDelete<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            handler.PassedContext.GetState<AnEntity>().Should().NotBeNull();
        }

        [Fact]
        public async Task StateContainsPreviousValueWhenUpdateEntity()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "mugron";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            entity.Name.Should().Be("bruges");
            handler.PassedContext.GetState<AnEntity>().PreviousValue(x => x.Name).Should().Be("mugron");
        }

        [Fact]
        public async Task SavePreviousValueOnLoad()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "mugron";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.PassedContext.GetState<AnEntity>().PreviousValue(x => x.Name).Should().Be("mugron");
        }

        [Fact]
        public async Task SaveStateWhenQuerySingleEntity()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "mugron";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.Query<AnEntity>().Where(x => x.Id == entity.Id).SingleAsync();
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.PassedContext.GetState<AnEntity>().PreviousValue(x => x.Name).Should().Be("mugron");
        }

        [Fact]
        public async Task SaveStateWhenQueryListEntity()
        {
            var handler = new ASpyHandler();
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenUpdate<IEntity>().Call<ASpyHandler>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "mugron";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var list = await repository.Query<AnEntity>().Where(x => x.Id == entity.Id).ToListAsync();
                entity = list.First();
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            handler.PassedContext.GetState<AnEntity>().PreviousValue(x => x.Name).Should().Be("mugron");
        }

        [Fact]
        public async Task WhenQueryWithPredicateNoFoundResultReturnEmptyListEntity()
        {
            var repository = RootScope.Resolve<IRepository>();

            var entities = await repository.Query<AnEntity>().Where(x => x.Id == Guid.NewGuid()).ToListAsync();
            entities.Count.Should().Be(0);

            entities = repository.Query<AnEntity>().Where(x => x.Id == Guid.NewGuid()).ToList();
            entities.Count.Should().Be(0);
        }

        [Fact]
        public async Task CanRollbackAnInsertAfterLoad()
        {
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackAnUpdateAfterLoad()
        {
            AnEntity entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bruges");
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bordeaux");
            }
        }

        [Fact]
        public async Task CanRollbackAnUpdateAfterQuery()
        {
            AnEntity entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.Query<AnEntity>().Where(x => x.Id == entity.Id).SingleAsync();
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bruges");
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bordeaux");
            }
        }

        [Fact]
        public async Task CanRollbackEntityModificationWithoutUpdate()
        {
            AnEntity entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bordeaux");
            }
        }

        [Fact]
        public async Task EntityIsNotModifiedWhileNoCommitted()
        {
            AnEntity entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bordeaux");
            }
        }

        [Fact]
        public async Task RollbackSinceLastCommit()
        {
            var entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "bordeaux";
                await repository.InsertAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await uow.Resolve<ITransactionHolder>().CommitAsync();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "ruelle";
                await repository.UpdateAsync(entity);
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                ((AnEntity)MemoryStore.Get(typeof(AnEntity), entity.Id).Single()).Name.Should().Be("bruges");
            }
        }

        [Fact]
        public async Task CanRollbackMultipleOperationsOnSameEntity()
        {
            var entity = new AnEntity().WithId();
            entity.Name = "bordeaux";
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "bordeaux";
                await repository.InsertAsync(entity);
                entity.Name = "bruges";
                await repository.UpdateAsync(entity);
                await repository.DeleteAsync(entity);
                await repository.InsertAsync(entity);
                await repository.DeleteAsync(entity);
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();
            }
        }

        [Fact]
        public async Task CanRollbackADeleteAfterLoad()
        {
            var entity = new AnEntity().WithId();
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();
            }
        }

        [Fact]
        public async Task CanRollbackADeleteAfterQuery()
        {
            var entity = new AnEntity().WithId();
            MemoryStore.Add(entity);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.Query<AnEntity>().Where(x => x.Id == entity.Id).SingleAsync();
                await repository.DeleteAsync(entity);
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeFalse();
                await uow.Resolve<ITransactionHolder>().RollbackAsync();
                MemoryStore.Contains(typeof(AnEntity), entity.Id).Should().BeTrue();
            }
        }

        public class ASpyHandler : IRepositoryBusHandler
        {
            public int NbCall = 0;
            public int Delay = 0;
            public RepositoryBusContext PassedContext;

            public async Task DoSomethingAsync(RepositoryBusContext context)
            {
                await Task.Delay(Delay);
                Interlocked.Increment(ref NbCall);
                PassedContext = context;
            }

            public Task ExecuteAsync(RepositoryBusContext context)
            {
                return DoSomethingAsync(context);
            }
        }
    }
}

