using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using AutoMapper;
using Castle.DynamicProxy;
using CocoriCore.Test;
using CocoriCore;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class DataSets : DataSetsBase
    {
        public DataSets(IUnitOfWorkFactory unitOfWorkFactory)
            : base(unitOfWorkFactory)
        {
        }
    }
}