using System;
using Xunit;
using CocoriCore;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using System.Threading.Tasks;
using Moq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class TraceHttpTest
    {
        [Fact]
        public void ConstructHttpTraceFromHttpContext()
        {
            var httpContext = new HttpContextFake().Default(
                x => x.Request.Method = "GET",
                x => x.Request.Scheme = "http",
                x => x.Request.Host = new HostString("myHost", 8080),
                x => x.Request.Path = "/myPath/truc",
                x => x.Request.QueryString = new QueryString("?val1=titi&val2=tata"),
                x => x.Request.Protocol = "HTTP/1.1",
                x => x.Response.StatusCode = 200);
            //just here to show how url is build and string interpolation
            var url = $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";
            httpContext.Request.GetDisplayUrl().Should().Be(url);

            var start = DateTime.Now;
            var end = start.AddMilliseconds(100);
            var httpTrace = new HttpTrace(httpContext, start, end);

            httpTrace.Start.Should().Be(start);
            httpTrace.End.Should().Be(end);
            httpTrace.Url.Should().Be("http://myHost:8080/myPath/truc?val1=titi&val2=tata");
            httpTrace.Protocol.Should().Be("HTTP/1.1");
            httpTrace.StatusCode.Should().Be(200);
        }
    }
}
