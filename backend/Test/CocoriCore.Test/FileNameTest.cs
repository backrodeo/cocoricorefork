﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CocoriCore.Test
{
    public class FileNameTest
    {
        [Theory]
        [InlineData("toto.jpg")]
        [InlineData("toto(1).jpg")]
        [InlineData("toto")]
        [InlineData("toto.titi.jpg")]
        [InlineData("toto titi.jpg")]
        [InlineData("toto titi.tu tu.jpg")]
        [InlineData("visual-reverse-image-search-v2_intro-2019-04-24 09.46.36.jpg")]
        public void FileNameIsValid(string filename)
        {
            var valueObject = new FileName(filename);

            valueObject.Value.Should().Be(filename);
        }

        [Theory]
        [InlineData("toto.jpg", "toto(1).jpg")]
        [InlineData("to to.ti ti.jpg", "to to.ti ti(1).jpg")]
        [InlineData("toto(25).jpg", "toto(26).jpg")]
        [InlineData("toto", "toto(1)")]
        [InlineData("toto_1234(1)", "toto_1234(2)")]
        public void CanIncrementFileName(string filename, string expected)
        {
            var valueObject = new FileName(filename);

            valueObject.Increment().Value.Should().Be(expected);
        }

        [Theory]
        [InlineData(@"to*to.jpg")]
        [InlineData(@"to\to.jpg")]
        [InlineData(@"to?to.jpg")]
        [InlineData(@"to%to.jpg")]
        public void ErrorIfFileNameContainsIlelgalCaracters(string filename)
        {
            Action action = () => new FileName(filename);

            action.Should().Throw<ArgumentException>().WithMessage($"'{filename}' is not a valid filename.");
        }
    }
}
