using System;
using Xunit;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using System.Text;
using Newtonsoft.Json;
using Autofac;
using System.Collections.Generic;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class HttpErrorBusTest : BusTestBase
    {
        private JsonSerializer _jsonSerializer;

        public HttpErrorBusTest()
        {
            _jsonSerializer = new JsonSerializer();
            ContainerBuilder.RegisterInstance(_jsonSerializer);
        }

        [Fact]
        public async Task UseDefaultErrorResponseHandler()
        {
            ContainerBuilder.RegisterType<DefaultExceptionWriter>().AsSelf();
            var optionsBuilder = new DefaultHttpErrorWriterOptionsBuilder(() => false);
            var responseWriter = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var exception = new ArgumentOutOfRangeException("totoParameter");
            exception.GenerateId();
            exception.Data["toto"] = "tutu";
            var httpResponse = new HttpResponseFake();

            await responseWriter.WriteErrorAsync(exception, httpResponse);

            httpResponse.StatusCode.Should().Be(500);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");
            var error = new ErrorResponse(exception);
            var stringWriter = new StringWriter();
            _jsonSerializer.Serialize(stringWriter, error);
            httpResponse.Body.Should().Be(stringWriter.ToString());
        }

        [Fact]
        public async Task UseDefaultErrorHandlerForRouteNotFoundException()
        {
            ContainerBuilder.RegisterType<RouteNotFoundExceptionWriter>().AsSelf();

            var optionsBuilder = new DefaultHttpErrorWriterOptionsBuilder(() => true);
            var responseWriter = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/toto/titi");

            var exception = new RouteNotFoundException(httpRequest.HttpContext);
            exception.GenerateId();
            exception.SetTraceId(httpRequest.HttpContext.TraceIdentifier);

            var httpResponse = new HttpResponseFake();

            await responseWriter.WriteErrorAsync(exception, httpResponse);

            httpResponse.StatusCode.Should().Be(404);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");

            var error = new ErrorResponse(exception, true);
            var stringWriter = new StringWriter();
            _jsonSerializer.Serialize(stringWriter, error);
            httpResponse.Body.Should().Be(stringWriter.ToString());
        }

        [Fact]
        public async Task UseCustomHandlerToWriteErrorSetStatusCodeAndContentType()
        {
            var errorWriter = new ConfigurationExceptionErrorWriter1(_jsonSerializer);
            ContainerBuilder.RegisterInstance(errorWriter).AsSelf();

            var optionsBuilder = new HttpErrorWriterOptionsBuilder(() => false);
            optionsBuilder.For<ConfigurationException>().Call<ConfigurationExceptionErrorWriter1>();

            var responseWriter = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var httpResponse = new HttpResponseFake();

            await responseWriter.WriteErrorAsync(new ConfigurationException("bad conf"), httpResponse);

            errorWriter.MethodCall.Should().BeTrue();
            httpResponse.StatusCode.Should().Be(500);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");
        }

        [Fact]
        public async Task UseCustomHandlerToWriteAnError()
        {
            ContainerBuilder.RegisterType<ConfigurationExceptionErrorWriter2>().AsSelf();
            var optionsBuilder = new HttpErrorWriterOptionsBuilder().SetDebugMode(() => false);
            optionsBuilder.For<ConfigurationException>().Call<ConfigurationExceptionErrorWriter2>();

            var responseWriter = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var httpResponse = new HttpResponseFake();

            await responseWriter.WriteErrorAsync(new ConfigurationException("bad conf"), httpResponse);

            httpResponse.StatusCode.Should().Be(501);
            httpResponse.ContentType.Should().Be("application/json; charset=CN-BIG5");
        }

        [Fact]
        public void ExceptionIfSuperTypeRuleBeforeSpecificRule()
        {
            var optionsBuilder = new HttpErrorWriterOptionsBuilder();
            optionsBuilder.SetDebugMode(() => false);
            optionsBuilder.For<Exception>().Call<ConfigurationExceptionErrorWriter1>();
            optionsBuilder.For<ConfigurationException>().Call<ConfigurationExceptionErrorWriter1>();

            HttpErrorWriterOptions option = null;
            Action action = () => option = optionsBuilder.Options;

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Rule for {typeof(Exception).FullName} is less specific than "
                + $"rule for {typeof(ConfigurationException).FullName} and should be defined after.");
        }

        [Fact]
        public void ExceptionIfTwoRulesForSameType()
        {
            var optionsBuilder = new HttpErrorWriterOptionsBuilder();
            optionsBuilder.SetDebugMode(() => false);
            optionsBuilder.For<Exception>().Call<ConfigurationExceptionErrorWriter1>();
            Action action = () => optionsBuilder.For<Exception>().Call<ConfigurationExceptionErrorWriter1>();

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Can't add a rule for type {typeof(Exception).FullName}"
                        + $" because a previous one is already defined and this rule doesn't allow multiple handlers.");
        }

        [Fact]
        public void ExceptionIfTwoHandlerForASameRule()
        {
            var optionsBuilder = new HttpErrorWriterOptionsBuilder();
            optionsBuilder.SetDebugMode(() => false);
            Action action = () => optionsBuilder.For<Exception>()
                .Call<ConfigurationExceptionErrorWriter1>()
                .Call<ConfigurationExceptionErrorWriter2>();

            var exception = action.Should().Throw<ConfigurationException>().Which;
            exception.Message.Should().Be($"Can't add handler [{typeof(ConfigurationExceptionErrorWriter2).FullName}]"
                         + $" because a previous one is already defined and this rule doesn't allow multiple handlers.");
        }

        [Fact]
        public async Task CanDisplayDebugInfos()
        {
            var displayDebugInfo = false;
            ContainerBuilder.RegisterType<DefaultExceptionWriter>().AsSelf();
            var optionsBuilder = new DefaultHttpErrorWriterOptionsBuilder(() => displayDebugInfo);
            var responseWriter = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);

            var httpResponse = new HttpResponseFake();
            var exception = GetExceptionWithStackTrace("something bas happend.");
            exception.GenerateId();

            await responseWriter.WriteErrorAsync(exception, httpResponse);

            httpResponse.StatusCode.Should().Be(500);
            httpResponse.ContentType.Should().Be("application/json; charset=utf-8");
            var error = new ErrorResponse(exception, displayDebugInfo);
            var stringWriter = new StringWriter();
            _jsonSerializer.Serialize(stringWriter, error);
            httpResponse.Body.Should().Be(stringWriter.ToString());
            httpResponse.Body.Should().Contain("something bas happend.");
        }

        [Fact]
        public void ThrowExceptionIfExceptionDuringHandlerProcess()
        {
            ContainerBuilder.RegisterType<AErrorWriter>().AsSelf();
            var optionsBuilder = new HttpErrorWriterOptionsBuilder();
            optionsBuilder.SetDebugMode(() => true);
            var exception = new Exception("something bad happened");
            optionsBuilder.For<Exception>().Call<AErrorWriter>(c => throw exception);
            var errorWriterBus = new HttpErrorWriter(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new HttpResponseFake();

            Func<Task> action = async () => await errorWriterBus.WriteErrorAsync(new Exception(), httpResponse);

            action.Should().Throw<Exception>().Which.Should().Be(exception);
        }

        private Exception GetExceptionWithStackTrace(string message = null)
        {
            try
            {
                AThrowingErrorFunction(message);
            }
            catch (Exception e)
            {
                return e;
            }
            return null;
        }

        private void AThrowingErrorFunction(string message = null)
        {
            message = message ?? "it is null !";
            throw new NullReferenceException(message);
        }

        public class ConfigurationExceptionErrorWriter1 : DefaultExceptionWriter
        {
            public bool MethodCall;

            public ConfigurationExceptionErrorWriter1(JsonSerializer jsonSerializer)
                : base(jsonSerializer)
            {
                MethodCall = false;
            }

            public override Task WriteResponseAsync(HttpErrorWriterContext context)
            {
                MethodCall = true;
                context.Exception.Should().BeOfType<ConfigurationException>();
                context.HttpResponse.Should().BeAssignableTo<HttpResponse>();
                return base.WriteResponseAsync(context);
            }
        }

        public class ConfigurationExceptionErrorWriter2 : IHttpErrorWriterHandler
        {
            public Task WriteResponseAsync(HttpErrorWriterContext context)
            {
                context.HttpResponse.StatusCode = 501;
                context.HttpResponse.ContentType = "application/json; charset=CN-BIG5";
                return Task.CompletedTask;
            }
        }

        public class AErrorWriter
        {

        }
    }
}