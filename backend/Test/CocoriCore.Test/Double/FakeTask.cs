using System;
using System.Threading.Tasks;

namespace CocoriCore.Test
{
    public class FakeTask
    {
        public static Task From(Action action)
        {
            action();
            return Task.CompletedTask;
        }
    }
}