using System;
using Xunit;
using CocoriCore;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Autofac;
using System.Diagnostics;
using System.Linq;
using Autofac.Core;
using System.Threading;
using System.Collections.Generic;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class RepositoryBusErrorTest : BusTestBase
    {
        private RepositoryBusOptionsBuilder _builder;
        private FakeRepositoryConfiguration _fakeRepositoryConfiguration;

        public RepositoryBusErrorTest()
        {
            _builder = new RepositoryBusOptionsBuilder();
            _unitOfWorkBuilder.Call<IRepositoryBus>(b => b.ApplyRulesAsync());

            ContainerBuilder.RegisterInstance(_builder).AsSelf();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<TransactionalMemoryRepository>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusDecorator>()
                .As<IRepository>().AsSelf()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<TransactionalMemoryRepository>()))
                .WithParameter(new TypedParameter<ITransactionHolder>(c => c.Resolve<TransactionalMemoryRepository>()))
                .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _builder.Options).As<RepositoryBusOptions>();
            ContainerBuilder.RegisterType<RepositoryBus>().AsSelf().As<IRepositoryBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusContextStore>().As<IRepositoryBusContextStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>();
            ContainerBuilder.RegisterType<SpyFnFExceptionHandler>().AsSelf().SingleInstance();
        }

        [Fact]
        public void ThrowConfigurationExceptionIfNoFireAndForgetHandlerDefined()
        {
            var handler = new ASpyHandler();
            handler.Delay = 50;
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _builder.WhenInsert<IEntity>().Call<ASpyHandler>().FireAndForget();

            RepositoryBusOptions options = null;
            Action action1 = () => options = _builder.Options;

            action1.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Contain("No fire and forget exception handler");

            RepositoryBus repoBus = null;
            Action action2 = () => repoBus = RootScope.Resolve<RepositoryBus>();

            var innerError = action2.Should().Throw<DependencyResolutionException>()
                .Which.InnerException;
            innerError.Should().BeOfType<ConfigurationException>()
                .Which.Message.Should().Contain("No fire and forget exception handler");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForInsertEntity()
        {
            Action action = () => _builder.WhenInsert<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForUpdateEntity()
        {
            Action action = () => _builder.WhenUpdate<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForDeleteEntity()
        {
            Action action = () => _builder.WhenDelete<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForModifyEntity()
        {
            Action action = () => _builder.WhenModify<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public async Task ThrowExceptionIfEntityNotLoadedBeforeUpdate()
        {
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            _builder.WhenModify<AnEntity>().Call<AMockHandler>((h, c) => h.ExecuteAsync(c));

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            action.Should().Throw<InvalidOperationException>()
                .WithMessage("No state associated with this entity, ensure you have loaded it before any update or delete.");
        }

        [Fact]
        public void NoExceptionIfNoRuleDefinedForEntity()
        {
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ThrowErrorWhenInsertEntity()
        {
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            _builder.WhenInsert<AnEntity>().Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."));

            AnEntity entity = null;
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<RepositoryBusException>().Which;
            exception.Message.Should().Be("Exception occured while applying rule for CREATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.State.Should().BeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ThrowErrorWhenUpdateEntity()
        {
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            _builder.WhenUpdate<AnEntity>().Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."));

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    entity.Name = "toto";
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<RepositoryBusException>().Which;
            exception.Message.Should().Be("Exception occured while applying rule for UPDATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.UPDATE);
            exception.State.Should().NotBeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ThrowErrorWhenDeleteEntity()
        {
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            _builder.WhenDelete<AnEntity>().Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."));

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    await repository.DeleteAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<RepositoryBusException>().Which;
            exception.Message.Should().Be("Exception occured while applying rule for DELETE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.DELETE);
            exception.State.Should().NotBeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public void ThrowErrorWhenModifyEntity()
        {
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            _builder.WhenModify<AnEntity>().Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."));

            AnEntity entity = null;
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<RepositoryBusException>().Which;
            exception.Message.Should().Be("Exception occured while applying rule for CREATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.State.Should().BeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ExceptionHandlerCatchErrorWhenInsertEntityFireAndForget()
        {
            var exceptionHandler = new ASpyExceptionHandler();
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            ContainerBuilder.RegisterInstance(exceptionHandler);
            _builder.SetFireAndForgetExceptionHandler<ASpyExceptionHandler>((h, e) => h.HandleErrorAsync(e));
            _builder.WhenInsert<AnEntity>()
                .Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."))
                .FireAndForget();

            AnEntity entity = null;
            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());

            var exception = (RepositoryBusException)exceptionHandler.Exceptions.Should().ContainSingle().Which;
            exception.Message.Should().Be("Exception occured while applying rule for CREATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.State.Should().BeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ExceptionHandlerCatchErrorWhenUpdateEntityFireAndForget()
        {
            var exceptionHandler = new ASpyExceptionHandler();
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            ContainerBuilder.RegisterInstance(exceptionHandler);
            _builder.SetFireAndForgetExceptionHandler<ASpyExceptionHandler>((h, e) => h.HandleErrorAsync(e));
            _builder.WhenUpdate<AnEntity>()
                .Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."))
                .FireAndForget();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());

            var exception = (RepositoryBusException)exceptionHandler.Exceptions.Should().ContainSingle().Which;
            exception.Message.Should().Be("Exception occured while applying rule for UPDATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.UPDATE);
            exception.State.Should().NotBeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ExceptionHandlerCatchErrorWhenDeleteEntityFireAndForget()
        {
            var exceptionHandler = new ASpyExceptionHandler();
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            ContainerBuilder.RegisterInstance(exceptionHandler);
            _builder.SetFireAndForgetExceptionHandler<ASpyExceptionHandler>((h, e) => h.HandleErrorAsync(e));
            _builder.WhenDelete<AnEntity>()
                .Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."))
                .FireAndForget();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());

            var exception = (RepositoryBusException)exceptionHandler.Exceptions.Should().ContainSingle().Which;
            exception.Message.Should().Be("Exception occured while applying rule for DELETE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.DELETE);
            exception.State.Should().NotBeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        [Fact]
        public async Task ExceptionHandlerCatchErrorWhenModifyEntityFireAndForget()
        {
            var exceptionHandler = new ASpyExceptionHandler();
            ContainerBuilder.RegisterType<AMockHandler>().AsSelf();
            ContainerBuilder.RegisterInstance(exceptionHandler);
            _builder.SetFireAndForgetExceptionHandler<ASpyExceptionHandler>((h, e) => h.HandleErrorAsync(e));
            _builder.WhenModify<AnEntity>()
                .Call<AMockHandler>((h, c) => throw new Exception("Something bad happend."))
                .FireAndForget();

            AnEntity entity = null;
            RepositoryBus repositoryBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                repositoryBus = uow.Resolve<RepositoryBus>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }
            Task.WaitAll(repositoryBus.FireAndForgetTasks.ToArray());

            var exception = (RepositoryBusException)exceptionHandler.Exceptions.Should().ContainSingle().Which;
            exception.Message.Should().Be("Exception occured while applying rule for CREATE entity of type CocoriCore.Test.AnEntity.");
            exception.Entity.Should().Be(entity);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.State.Should().BeNull();
            exception.Handler.Should().Contain("AMockHandler");
            exception.InnerException.Message.Should().Be("Something bad happend.");
        }

        public class AMockHandler
        {
            public Task ExecuteAsync(RepositoryBusContext context)
            {
                return Task.CompletedTask;
            }
        }

        public class ASpyExceptionHandler
        {
            public List<Exception> Exceptions;

            public ASpyExceptionHandler()
            {
                Exceptions = new List<Exception>();
            }

            public Task HandleErrorAsync(Exception exception)
            {
                Exceptions.Add(exception);
                return Task.CompletedTask;
            }
        }

        public class AnInvalidEntity : IEntity
        {
            public virtual Guid Id { get; set; }
            public string ANotVirtualProperty { get; set; }
            public virtual string AVirtualProperty { get; set; }
        }

        public class ASpyHandler : IRepositoryBusHandler
        {
            public int NbCall = 0;
            public int Delay = 0;
            public RepositoryBusContext PassedContext;

            public async Task DoSomethingAsync(RepositoryBusContext context)
            {
                await Task.Delay(Delay);
                Interlocked.Increment(ref NbCall);
                PassedContext = context;
            }

            public Task ExecuteAsync(RepositoryBusContext context)
            {
                return DoSomethingAsync(context);
            }
        }
    }
}

