﻿using System;
using System.Reflection;
using System.Runtime.Loader;

namespace CocoriCore.Test
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
