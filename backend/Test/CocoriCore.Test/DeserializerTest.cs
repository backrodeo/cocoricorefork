﻿using CocoriCore;
using FluentAssertions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace CocoriCore.Test
{
    public class DeserializerTest
    {
        //TODO au propre et tester qu'avec des valeurs dans json on les écrase pas, et aussi avec des champs non enumerable
        [Theory]
        [InlineData(@"{""array"": null, ""list"": null, ""hashSet"": null, ""iList"": null, ""iEnumerable"": null, ""iCollection"": null}")]
        [InlineData(@"{}")]
        public void Test(string json)
        {
            var serializer = new DefaultJsonSerializer(new Formats());
            var deserializedObject = serializer.Deserialize<WithCollection>(new JsonTextReader(new StringReader(json)));

            deserializedObject.array.Should().NotBeNull();
            deserializedObject.list.Should().NotBeNull();
            deserializedObject.hashSet.Should().NotBeNull();
            deserializedObject.iList.Should().NotBeNull();
            deserializedObject.iEnumerable.Should().NotBeNull();
            deserializedObject.iCollection.Should().NotBeNull();
        }

        //TODO tester qu'avec des valeurs dans les tableau ca marche aussi

        class WithCollection
        {
            public object[] array = null;
            public List<object> list = null;
            public HashSet<object> hashSet = null;
            public IList<object> iList = null;
            public IEnumerable<object> iEnumerable = null;
            public ICollection<object> iCollection = null;
        }
    }
}
