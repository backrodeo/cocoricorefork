using System;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace CocoriCore.Test
{
    public class ExceptionExtensionTest
    {
        [Fact]
        public void ExceptionIdIsNullIfExceptionIsNull()
        {
            Exception exception = null;
            exception.GetId().Should().BeNull();
        }

        [Fact]
        public void ExceptionIdIsNullIfExceptionDataDoenstContainsExceptionId()
        {
            Exception exception = new Exception();
            exception.GetId().Should().BeNull();
        }

        [Fact]
        public void ExceptionIdIsNotEmptyIfExceptionDataContainsExceptionId()
        {
            Exception exception = new Exception();
            exception.GenerateId();
            exception.GetId().Should().NotBeEmpty();
        }
    }
}