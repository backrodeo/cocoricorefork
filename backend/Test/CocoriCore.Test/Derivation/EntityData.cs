using System;
using CocoriCore;

namespace CocoriCore.Test 
{
    public class ADependencyEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Town { get; set; }
    }

    public class ANotVirtualEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public string ANotVirtualProperty { get; set; }
    }

    public class AnEntity : AnAbstractEntity
    {
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid IdDependency { get; set; }
    }

    public class AnotherEntity : AnAbstractEntity
    {
    }

    public class AProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid IdDependency { get; set; }
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual TimeSpan Duration { get; set; }
        public virtual string DependencyName { get; set; }
        public virtual string EntityName { get; set; }
        public virtual string Town { get; set; }
    }

    public class AnOtherProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual TimeSpan Duration { get; set; }
    }

    public abstract class AnAbstractEntity : IEntity
    {
        public virtual Guid Id { get; set; }
    }
}