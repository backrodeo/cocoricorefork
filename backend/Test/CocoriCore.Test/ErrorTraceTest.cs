using System;
using Xunit;
using CocoriCore;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using System.Threading.Tasks;
using Moq;
using Microsoft.AspNetCore.Hosting;
using CocoriCore.TestUtils;

namespace CocoriCore.Test
{
    public class ErrorTraceTest
    {
        [Fact]
        public void ConstructErrorTraceFromException()
        {
            Action action = () => AMethodWithExceptionAndInnerException("fatal error", "low level error");
            var exception = action.CatchException();

            ErrorTrace trace = new ErrorTrace(exception);

            trace.DateTime.Should().BeCloseTo(DateTime.Now);
            trace.Message.Should().Contain("fatal error");
            trace.StackTrace.Should().Contain("AMethodWithExceptionAndInnerException");
            trace.Type.Should().Be(typeof(AnException).FullName);
            var innerTrace = trace.InnerExceptions.Should().ContainSingle().Which;
            innerTrace.Message.Should().Be("low level error");
            innerTrace.StackTrace.Should().Contain("AMethodWithInnerException");
            innerTrace.Type.Should().Be(typeof(AnInnerException).FullName);
            innerTrace.InnerExceptions.Should().BeNull();
        }

        [Fact]
        public void ConstructErrorTraceFromAggregateException()
        {
            Action action = () => AMethodWithAggregateException("anError1", "anError2");
            var exception = action.CatchException();

            ErrorTrace trace = new ErrorTrace(exception);

            trace.DateTime.Should().BeCloseTo(DateTime.Now, 1000);
            trace.Message.Should().Contain("anError1").And.Contain("anError2");
            trace.StackTrace.Should().Contain("AMethodWithAggregateException");
            trace.Type.Should().Be(typeof(AggregateException).FullName);
            trace.InnerExceptions.Should().HaveCount(2);
            foreach (var innerTrace in trace.InnerExceptions)
            {
                innerTrace.Message.Should().Contain("anError");
                innerTrace.StackTrace.Should().Contain("AMethodWithInnerException");
                innerTrace.Type.Should().Be(typeof(AnInnerException).FullName);
                innerTrace.InnerExceptions.Should().BeNull();
            }
        }

        private void AMethodWithAggregateException(string message1, string message2)
        {
            Task action1 = Task.Run(() => AMethodWithInnerException(message1));
            Task action2 = Task.Run(() => AMethodWithInnerException(message2));
            Task.WaitAll(action1, action2);
        }

        private void AMethodWithExceptionAndInnerException(string message, string innerMessage)
        {
            try
            {
                AMethodWithInnerException(innerMessage);
            }
            catch (Exception e)
            {
                throw new AnException(message, e);
            }
        }

        private void AMethodWithInnerException(string message)
        {
            throw new AnInnerException(message);
        }

        class AnException : Exception
        {
            public AnException(string message) : base(message)
            {
            }

            public AnException(string message, Exception innerException)
                : base(message, innerException)
            {
            }
        }

        class AnInnerException : Exception
        {
            public AnInnerException(string message) : base(message)
            {
            }
        }

        class AnotherErrorTrace : ErrorTrace
        {
            public AnotherErrorTrace(Exception exception) : base(exception)
            {
            }
        }
    }
}
