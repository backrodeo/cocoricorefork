using System;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using CocoriCore;
using CocoriCore.Linq.Async;
using CocoriCore.TestUtils;
using FluentAssertions;
using Xunit;

namespace CocoriCore.Test
{
    public class MemoryRepositoryTest
    {
        private ILifetimeScope _scope;

        public MemoryRepositoryTest()
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            containerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            containerBuilder.RegisterType<MemoryRepository>().As<IRepository>().SingleInstance();
            _scope = containerBuilder.Build();
        }

        [Fact]
        public async Task InsertEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var anEntity = await repository.LoadAsync<AnEntity>(entity.Id);
            var anAbstractEntity = await repository.LoadAsync<AnAbstractEntity>(entity.Id);
            var iEntity = await repository.LoadAsync<IEntity>(entity.Id);

            (await repository.LoadAsync<AnEntity>(entity.Id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<AnAbstractEntity>(entity.Id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<IEntity>(entity.Id)).Should().BeEquivalentTo(entity);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(1);
        }

        [Fact]
        public async Task InsertTwoEntitiesDifferentTypeButSameId()
        {
            var id = Guid.NewGuid();
            var entity = new AnEntity { Id = id };
            var projection = new AProjection { Id = id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);
            await repository.InsertAsync(projection);

            (await repository.LoadAsync<AnEntity>(id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<AnAbstractEntity>(id)).Should().BeEquivalentTo(entity);
            (await repository.LoadAsync<AProjection>(id)).Should().BeEquivalentTo(projection);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(2);
            (await repository.ExistsAsync<IEntity>(id)).Should().BeTrue();
        }

        [Fact]
        public async Task UpdateEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            IEntity iEntity = anEntity;
            AnAbstractEntity abstractEntity = anEntity;

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);

            anEntity.Name = "toto";
            await repository.UpdateAsync(anEntity);
            await repository.UpdateAsync(abstractEntity);
            await repository.UpdateAsync(iEntity);

            (await repository.LoadAsync<AnEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.LoadAsync<AnAbstractEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(anEntity);
            (await repository.Query<AnEntity>().ToListAsync()).Should().ContainSingle().Which.Name.Should().Be("toto");
            (await repository.Query<AnAbstractEntity>().ToListAsync()).Should().ContainSingle();
            (await repository.Query<IEntity>().ToListAsync()).Should().ContainSingle();
        }

        [Fact]
        public async Task RemoveAnEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            AProjection projection = new AProjection { Id = anEntity.Id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);
            await repository.InsertAsync(projection);

            await repository.DeleteAsync<AnEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeTrue();
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(projection);
        }

        [Fact]
        public async Task RemoveAnAbstractEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();
            AProjection projection = new AProjection { Id = anEntity.Id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);
            await repository.InsertAsync(projection);

            await repository.DeleteAsync<AnAbstractEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeTrue();
            (await repository.LoadAsync<IEntity>(anEntity.Id)).Should().BeEquivalentTo(projection);
        }

        [Fact]
        public async Task RemoveIEntity()
        {
            AnEntity anEntity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(anEntity);

            await repository.DeleteAsync<IEntity>(anEntity);

            (await repository.ExistsAsync<AnEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.ExistsAsync<IEntity>(anEntity.Id)).Should().BeFalse();
            (await repository.Query<IEntity>().ToListAsync()).Should().BeEmpty();
        }

        [Fact]
        public async Task QueryEntitiesAsync()
        {
            var entity1 = new AnEntity().WithId();
            var entity2 = new AnotherEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity1);
            await repository.InsertAsync(entity2);

            (await repository.Query<AnEntity>().ToListAsync()).Should().ContainSingle().Which.Should().BeEquivalentTo(entity1);
            (await repository.Query<AnotherEntity>().ToListAsync()).Should().ContainSingle().Which.Should().BeEquivalentTo(entity2);
            (await repository.Query<IEntity>().ToListAsync()).Should().HaveCount(2);
        }

        [Fact]
        public async Task QueryEntities()
        {
            var entity1 = new AnEntity().WithId();
            var entity2 = new AnotherEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity1);
            await repository.InsertAsync(entity2);

            repository.Query<AnEntity>().Should().ContainSingle().Which.Should().BeEquivalentTo(entity1);
            repository.Query<AnotherEntity>().Should().ContainSingle().Which.Should().BeEquivalentTo(entity2);
            repository.Query<IEntity>().Should().HaveCount(2);
        }

        [Fact]
        public async Task QueryWhenNoEntity()
        {
            var repository = _scope.Resolve<IRepository>();

            (await repository.Query<IEntity>().ToListAsync()).Should().BeEmpty();
        }

        [Fact]
        public async Task ExistsEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            (await repository.ExistsAsync<AnEntity>(entity.Id)).Should().BeTrue();
            (await repository.ExistsAsync<AnAbstractEntity>(entity.Id)).Should().BeTrue();
            (await repository.ExistsAsync<IEntity>(entity.Id)).Should().BeTrue();
        }


        [Fact]
        public void ExceptionIfInsertTwoEntitiesSameType()
        {
            var id = Guid.NewGuid();
            var entity1 = new AnEntity { Id = id };
            var entity2 = new AnEntity { Id = id };

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.InsertManyAsync(entity1, entity2);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is already an entity of type {typeof(AnEntity)} with id {id}");
        }

        [Fact]
        public void ExceptionIfInsertTwoEntitiesSameTypeButDifferentDeclaredType()
        {
            var id = Guid.NewGuid();
            AnEntity entity1 = new AnEntity { Id = id };
            IEntity entity2 = new AnEntity { Id = id };

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.InsertManyAsync(entity1, entity2);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is already an entity of type {typeof(AnEntity)} with id {id}");
        }

        [Fact]
        public void ExceptionIfUpdateNonExistingEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.UpdateAsync(entity);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public void ExceptionIfDeleteNonExistingEntity()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.DeleteAsync(entity);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public void ExceptionIfDeleteNonExistingTypeAndId()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            Func<Task> action = () => repository.DeleteAsync<AnEntity>(entity);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is no entity of type {typeof(AnEntity)} with id {entity.Id}");
        }

        [Fact]
        public async Task ExceptionIfLoadTwoEntitiesSameId()
        {
            var id = Guid.NewGuid();
            var entity = new AnEntity { Id = id };
            var projection = new AProjection { Id = id };

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);
            await repository.InsertAsync(projection);

            Func<Task> action = () => repository.LoadAsync<IEntity>(id);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There are several entity of type {typeof(IEntity)} " +
                $"with id {id}, use Query() method instead.");
        }

        [Fact]
        public async Task ExceptionIfNoEntityMatchingType()
        {
            var projection = new AProjection().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(projection);

            Func<Task> action = () => repository.LoadAsync<AnEntity>(projection.Id);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is no entity of type {typeof(AnEntity)} with id {projection.Id}");
        }

        [Fact]
        public async Task ExceptionIfNoEntityMatchingId()
        {
            var entity = new AnEntity().WithId();

            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var invalidId = Guid.NewGuid();
            Func<Task> action = () => repository.LoadAsync<AnEntity>(invalidId);

            action.Should().Throw<InvalidOperationException>().WithMessage($"There is no entity of type {typeof(AnEntity)} with id {invalidId}");
        }

        //TODO tests d'insert/update avec des objets proxifi�s
    }
}