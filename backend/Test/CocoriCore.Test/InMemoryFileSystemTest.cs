using System;
using Xunit;
using CocoriCore;
using FluentAssertions;
using System.Threading.Tasks;
using System.Text;
using System.IO;

namespace CocoriCore.Test
{
    public class InMemoryFileSystemTest
    {
        [Fact]
        public async Task CreateAndReadEmptyFile()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            await fileSystem.CreateBinaryFileAsync("myFile.txt", new byte[0]);

            (await fileSystem.FileExistsAsync("myFile.txt")).Should().BeTrue();
            (await fileSystem.ReadAsBinaryAsync("myFile.txt")).Should().BeEmpty();
            (await fileSystem.FileExistsAsync("myFile.bin")).Should().BeFalse();
        }

        [Fact]
        public async Task CreateBinaryFileFromBytes()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());
            await fileSystem.CreateBinaryFileAsync("myFile.bin", Encoding.UTF8.GetBytes("hello"));

            var contentRead = await fileSystem.ReadAsBinaryAsync("myFile.bin");

            Encoding.UTF8.GetString(contentRead).Should().Be("hello");
        }

        [Fact]
        public async Task CreateBinaryFileFromBase64String()
        {
            var base64String = "SGVsbG8gaXQncyBDb2NvcmlDb3JlICE=";
            var fileSystem = new InMemoryFileSystem(new Clock());
            await fileSystem.CreateBinaryFileAsync("myFile.bin", base64String);

            var contentRead = await fileSystem.ReadAsBase64Async("myFile.bin");

            contentRead.Should().Be(base64String);
        }

        [Fact]
        public async Task IfCreateExistingFileReplaceContent()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());
            var firstContent = Encoding.UTF8.GetBytes("hello");
            var secondContent = Encoding.UTF8.GetBytes("goodbye");
            await fileSystem.CreateBinaryFileAsync("myFile.txt", firstContent);
            await fileSystem.CreateBinaryFileAsync("myFile.txt", secondContent);

            (await fileSystem.ReadAsBinaryAsync("myFile.txt")).Should().BeEquivalentTo(secondContent);
        }

        [Fact]
        public async Task CreateTextFile()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            await fileSystem.CreateTextFileAsync("myFile.txt", "hello i'm CocoriCore");

            (await fileSystem.ReadAsTextAsync("myFile.txt")).Should().Be("hello i'm CocoriCore");
        }

        [Fact]
        public async Task CreateDirectory()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            await fileSystem.CreateDirectoryAsync("dir1/dir2");

            (await fileSystem.DirectoryExistsAsync("dir1")).Should().BeTrue();
            (await fileSystem.DirectoryExistsAsync("dir1/dir2")).Should().BeTrue();
            (await fileSystem.DirectoryExistsAsync("dir1/dir2/dir3")).Should().BeFalse();
        }

        [Fact]
        public async Task CreateFileIfParentDirectoryExists()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());
            await fileSystem.CreateDirectoryAsync("dir1/dir2");

            await fileSystem.CreateTextFileAsync("dir1/dir2/myFile.text", "hello");

            (await fileSystem.FileExistsAsync("/dir1/dir2/myFile.text")).Should().BeTrue();
        }

        [Fact]
        public async Task ExceptionIfCreateFileWhenDirectoryWithSameNameExists()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());
            await fileSystem.CreateDirectoryAsync("dir1/toto");

            Func<Task> action = async () => await fileSystem.CreateTextFileAsync("dir1/toto", "hello");

            action.Should().Throw<UnauthorizedAccessException>();
        }

        [Fact]
        public async Task ExceptionIfCreateFileWithoutExistingParentDirectory()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());
            await fileSystem.CreateDirectoryAsync("dir1");

            Func<Task> action = async () => await fileSystem.CreateTextFileAsync("dir1/dir2/myFile.text", "hello");

            action.Should().Throw<DirectoryNotFoundException>();
        }

        [Fact]
        public void ExceptionIfPathNullFileExistsAsync()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task<bool>> action = async () => await fileSystem.FileExistsAsync(null);

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfPathNullCreateBinaryFileAsync()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.CreateBinaryFileAsync(null, new byte[0]);

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfContentNullCreateBinaryFileAsync()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.CreateBinaryFileAsync("myFile.bin", (byte[])null);

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfPathNullCreateBinaryFileBase64Async()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.CreateBinaryFileAsync(null, "SGVsbG8gaXQncyBDb2NvcmlDb3JlICE=");

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfContentNullCreateBinaryFileBase64Async()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.CreateBinaryFileAsync("myFile.bin", (string)null);

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfPathNullReadAsBinaryAsync()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.ReadAsBinaryAsync(null);

            action.Should().Throw<ArgumentNullException>();
        }

        [Fact]
        public void ExceptionIfPathNullReadAsBase64Async()
        {
            var fileSystem = new InMemoryFileSystem(new Clock());

            Func<Task> action = async () => await fileSystem.ReadAsBase64Async(null);

            action.Should().Throw<ArgumentNullException>();
        }
    }
}
