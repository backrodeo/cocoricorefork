using System;
using System.Linq;
using Autofac;
using FluentAssertions;
using Moq;
using Xunit;

namespace CocoriCore.Test
{
    public class AuthenticationControllerTest
    {
        private ILifetimeScope _scope;
        private ContainerBuilder _containerBuilder;

        public AuthenticationControllerTest()
        {
            _containerBuilder = new ContainerBuilder();
            _containerBuilder.RegisterType<AuthenticationController>().As<IAuthenticationController>();
            _containerBuilder.RegisterType<MyFeatureOptions>().AsSelf();
            _containerBuilder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>();
            _containerBuilder.RegisterType<UnitOfWorkOptions>().AsSelf();
        }

        protected ILifetimeScope Scope
        {
            get
            {
                if (_scope == null)
                {
                    _scope = _containerBuilder.Build();
                }
                return _scope;
            }
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromType()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPrivate()
                .Public<PublicCommand>()
                .Public<PublicQuery>();
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromType()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPublic()
                .Private<PrivateCommand>()
                .Private<PrivateQuery>();
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromInterface()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPrivate()
                .Public<IPublic>();
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromInterface()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPublic()
                .Private<IPrivate>();
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromExpression()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPrivate()
                .Public(t => t.Name.Contains("Public"));
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromExpression()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPublic()
                .Private(t => t.Name.Contains("Private"));
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPrivateFromExpressionWithType()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPrivate()
                .Public<MyFeatureOptions>((s, t) => s.IsPublic(t));
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }

        [Fact]
        public void ConfigureAuthenticationDefaultPublicFromExpressionWithType()
        {
            var builder = new AuthenticationOptionsBuilder()
                .DefaultPublic()
                .Private<MyFeatureOptions>((s, t) => s.IsPrivate(t));
            _containerBuilder.RegisterInstance(builder.Options);
            var authenticationController = Scope.Resolve<IAuthenticationController>();

            authenticationController.RequireAuthentication(typeof(PublicCommand)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PublicQuery)).Should().BeFalse();
            authenticationController.RequireAuthentication(typeof(PrivateCommand)).Should().BeTrue();
            authenticationController.RequireAuthentication(typeof(PrivateQuery)).Should().BeTrue();
        }
    }

    public enum MyFeatures { Public, Private };

    public class MyFeatureOptions : FeatureOptions
    {
        public MyFeatureOptions()
        {
            DefineFeature(MyFeatures.Public)
                .Add<PublicCommand>()
                .Add<PublicQuery>();

            DefineFeature(MyFeatures.Private)
                .Add<PrivateCommand>()
                .Add<PrivateQuery>();
        }

        public bool IsPublic(Type messageType)
        {
            return GetFeatures(messageType).Contains(MyFeatures.Public);
        }

        public bool IsPrivate(Type messageType)
        {
            return GetFeatures(messageType).Contains(MyFeatures.Private);
        }
    }

    public interface IPublic
    {
    }

    public interface IPrivate
    {
    }

    public class PublicCommand : IPublic
    {
    }

    public class PublicQuery : IPublic
    {
    }

    public class PrivateCommand : IPrivate
    {
    }

    public class PrivateQuery : IPrivate
    {
    }
}