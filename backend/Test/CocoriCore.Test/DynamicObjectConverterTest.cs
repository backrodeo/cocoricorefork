using System;
using Xunit;
using CocoriCore;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using System.IO;
using System.Globalization;
using Microsoft.AspNetCore.Http.Internal;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Newtonsoft.Json;

namespace CocoriCore.Test
{
    public class DynamicObjectConverterTest
    {
        [Fact]
        public void CanDeserializeRecursivelyDynamicField()
        {
            var json = @"{""stringField"": ""toto"", 
                          ""dynamicField"": {""intField"": 2, 
                                             ""dateTimeField"": ""2018-05-23T21:51:45.508Z"", 
                                             ""arrayField"": [{""aField"": 3}, ""stringValue""]}}";
            var anObject = JsonConvert.DeserializeObject<AnObject>(json, new DynamicObjectConverter());

            anObject.stringField.Should().Be("toto");
            var dynamicValue = anObject.dynamicField;
            ((object)dynamicValue).Should().NotBeNull();
            ((object)dynamicValue.intField).Should().Be(2L);
            ((object)dynamicValue.dateTimeField).Should().Be(DateTime.Parse("2018-05-23T21:51:45.508Z").ToUniversalTime());
            ((object)dynamicValue.arrayField[0].aField).Should().Be(3L);
            ((object)dynamicValue.arrayField[1]).Should().Be("stringValue");
        }

        class AnObject
        {
            public string stringField = null;
            public dynamic dynamicField = null;
        }
    }
}
