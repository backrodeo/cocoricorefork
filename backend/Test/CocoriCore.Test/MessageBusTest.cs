using System;
using Xunit;
using CocoriCore;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Runtime.Loader;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Dynamic;
using CocoriCore.TestUtils;
using Moq;

namespace CocoriCore.Test
{
    public class MessageBusTest : BusTestBase
    {
        private MessageBusOptionsBuilder _optionBuilder;
        private IMessageBus _messageBus;

        public MessageBusTest()
        {
            _optionBuilder = new MessageBusOptionsBuilder();
            var assembly = this.GetType().Assembly;
            _optionBuilder
                .ForAll<IMessage>(assembly)
                .CallMessageHandlers(assembly);

            ContainerBuilder.RegisterInstance(_optionBuilder.Options).AsSelf();
            ContainerBuilder.RegisterType<MessageBus>().As<IMessageBus>();
            ContainerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<MemoryRepository>().As<IRepository>().SingleInstance();
        }

        public IMessageBus MessageBus
        {
            get
            {
                if (_messageBus == null)
                    _messageBus = RootScope.Resolve<IMessageBus>();
                return _messageBus;
            }
        }

        [Fact]
        public async Task ExecuteHandlerForQuery()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new AQueryHandler(() => nbCall++)).AsSelf();

            var response = await MessageBus.ExecuteAsync(new AQuery());

            nbCall.Should().Be(1);
            response.Should().Be(1234);
        }

        [Fact]
        public async Task ExecuteHandlerForVoidCommand()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new AVoidCommandHandler(() => nbCall++)).AsSelf();

            var response = await MessageBus.ExecuteAsync(new AVoidCommand());

            nbCall.Should().Be(1);
            response.Should().BeNull();
        }

        [Fact]
        public async Task ExecuteHandlerForCreateCommand()
        {
            var nbCall = 0;
            Action action = () => nbCall++;
            ContainerBuilder.RegisterInstance(new ACreateCommandHandler(() => nbCall++)).AsSelf();

            var id = await MessageBus.ExecuteAsync(new ACreateCommand());

            nbCall.Should().Be(1);
            id.Should().NotBe(default(Guid));
        }

        [Fact]
        public void ExceptionIfNoHandlerDefinedForMessage()
        {
            Func<Task> action = async () => await MessageBus.ExecuteAsync(new AMessageWithoutHandler());

            action.Should()
             .Throw<ConfigurationException>()
             .WithMessage("Response has not been set, ensure you have defined a handler "
                + $"for message of type {typeof(AMessageWithoutHandler).FullName}.");
        }

        [Fact]
        public async Task CallHandlerFollowingDeclarationOrder()
        {
            var handler1 = new AHandler();
            var handler2 = new AnotherHandler();
            ContainerBuilder.RegisterInstance(handler1).AsSelf().SingleInstance();
            ContainerBuilder.RegisterInstance(handler2).AsSelf().SingleInstance();

            _optionBuilder
                .ForAll<IMessage>(this.GetType().Assembly)
                .Call<AHandler>()
                .Call<AnotherHandler>((h, c) => h.Execute());

            await MessageBus.ExecuteAsync(new AMessage());

            handler1.CallDateTime.Should().NotBeNull();
            handler2.CallDateTime.Should().NotBeNull();
            handler1.CallDateTime.Should().BeBefore(handler2.CallDateTime.Value);
        }

        [Fact]
        public void ThrowExceptionIfExceptionDuringHandlerProcess()
        {
            ContainerBuilder.RegisterType<AVoidCommandHandler>().AsSelf();
            var optionsBuilder = new MessageBusOptionsBuilder();
            var exception = new Exception("something bad happened");
            optionsBuilder.For<AVoidCommand>().Call<AVoidCommandHandler>(c => throw exception);
            var messageBus = new MessageBus(UnitOfWorkFactory, UnitOfWork, optionsBuilder.Options);
            var httpResponse = new HttpResponseFake();

            Func<Task> action = async () => await messageBus.ExecuteAsync(new AVoidCommand());

            action.Should().Throw<Exception>().Which.Should().Be(exception);
        }

        [Fact]
        public async Task CanHandleGenericCommandWithGenericCommandAndEntityParameter()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler1<,>)).AsSelf();

            var anEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand1<AnEntity>());
            var anotherEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand1<AnotherEntity>());

            var repository = _rootScope.Resolve<IRepository>();
            repository.ExistsAsync<AnEntity>(anEntityId).Result.Should().BeTrue();
            repository.ExistsAsync<AnotherEntity>(anotherEntityId).Result.Should().BeTrue();
        }

        [Fact]
        public async Task CanHandleGenericCommandWithGenericEntityParameter()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler2<>)).AsSelf();

            var anEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand2<AnEntity>());
            var anotherEntityId = (Guid)await MessageBus.ExecuteAsync(new AGenericCommand2<AnotherEntity>());

            var repository = _rootScope.Resolve<IRepository>();
            repository.ExistsAsync<AnEntity>(anEntityId).Result.Should().BeTrue();
            repository.ExistsAsync<AnotherEntity>(anotherEntityId).Result.Should().BeTrue();
        }

        [Fact]
        public void ExceptionIfCantFindMatchingTypeFromMessage()
        {
            ContainerBuilder.RegisterGeneric(typeof(GenericCommandHandler3<,>)).AsSelf();

            Func<Task> action = () => MessageBus.ExecuteAsync(new AGenericCommand3<AnEntity>());

            action.Should().Throw<InvalidOperationException>()
                .WithMessage($"Unable to find matching type for parameter T of "
                + $"generic handler {typeof(GenericCommandHandler3<,>)} among [{typeof(AnEntity)}, {typeof(AGenericCommand3<AnEntity>)}]");
        }
    }
}
