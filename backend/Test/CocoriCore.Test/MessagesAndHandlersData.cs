using System;
using Xunit;
using CocoriCore;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Runtime.Loader;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Dynamic;
using CocoriCore.TestUtils;
using Moq;

namespace CocoriCore.Test
{
    public class AVoidCommandHandler : VoidCommandHandler<AVoidCommand>
    {
        private Action _action;

        public AVoidCommandHandler()
        {
        }

        public AVoidCommandHandler(Action action)
        {
            _action = action;
        }

        public override Task ExecuteAsync(AVoidCommand message)
        {
            _action();
            return Task.CompletedTask;
        }
    }

    public class AVoidCommand : ICommand
    {
        public AVoidCommand()
        {
        }
    }

    public class ACreateCommandHandler : CreateCommandHandler<ACreateCommand>
    {
        private Action _action;

        public ACreateCommandHandler(Action action)
        {
            _action = action;
        }

        public override Task<Guid> ExecuteAsync(ACreateCommand message)
        {
            _action();
            return Task.FromResult(Guid.NewGuid());
        }
    }

    public class ACreateCommand : ICommand
    {
        public ACreateCommand()
        {
        }
    }

    public class AQueryHandler : QueryHandler<AQuery, int>
    {
        private Action _action;

        public AQueryHandler(Action action)
        {
            _action = action;
        }

        public override Task<int> ExecuteAsync(AQuery message)
        {
            _action();
            return Task.FromResult(1234);
        }
    }

    public class AQuery : IQuery
    {
        public AQuery()
        {
        }
    }

    public class AMessageWithoutHandler : IMessage
    {
        public AMessageWithoutHandler()
        {
        }
    }

    public class AHandler : IMessageBusHandler
    {
        public DateTime? CallDateTime;

        public Task ExecuteAsync(MessageBusContext context)
        {
            CallDateTime = DateTime.Now;
            context.Response = null;
            return Task.CompletedTask;
        }
    }

    public class AMessage : IMessage
    {
    }

    public class AnotherHandler
    {
        public DateTime? CallDateTime;

        public Task Execute()
        {
            CallDateTime = DateTime.Now;
            return Task.CompletedTask;
        }
    }

    public class AGenericCommand1<TEntity> : ICommand
        where TEntity : IEntity
    {
        public Guid Id;
    }

    public class GenericCommandHandler1<TCommand, TEntity> : CreateCommandHandler<TCommand>
        where TCommand : AGenericCommand1<TEntity>
        where TEntity : class, IEntity, new()
    {
        private IRepository _repository;
        public GenericCommandHandler1(IRepository repository)
        {
            _repository = repository;
        }

        public override async Task<Guid> ExecuteAsync(TCommand command)
        {
            var entity = new TEntity();
            await _repository.InsertAsync(entity);
            return entity.Id;
        }
    }

    public class AGenericCommand2<TEntity> : ICommand
        where TEntity : IEntity
    {
        public Guid Id;
    }

    public class GenericCommandHandler2<TEntity> : CreateCommandHandler<AGenericCommand2<TEntity>>
        where TEntity : class, IEntity, new()
    {
        private IRepository _repository;
        public GenericCommandHandler2(IRepository repository)
        {
            _repository = repository;
        }

        public override async Task<Guid> ExecuteAsync(AGenericCommand2<TEntity> command)
        {
            var entity = new TEntity();
            await _repository.InsertAsync(entity);
            return entity.Id;
        }
    }


    public class AGenericCommand3<TEntity> : ICommand
        where TEntity : IEntity
    {
        public Guid Id;
    }

    public class GenericCommandHandler3<TEntity, T> : CreateCommandHandler<AGenericCommand3<TEntity>>
        where TEntity : class, IEntity, new()
    {
        private IRepository _repository;
        public GenericCommandHandler3(IRepository repository)
        {
            _repository = repository;
        }

        public override async Task<Guid> ExecuteAsync(AGenericCommand3<TEntity> command)
        {
            var entity = new TEntity();
            await _repository.InsertAsync(entity);
            return entity.Id;
        }
    }
}
