
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class AManyProjectionSynchronizer : IGroupSynchronizer<AOneManyProjection, AManyProjection>
    {
        public int CallCount;
        private IRepository _repository;

        public AManyProjectionSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task GroupUpdateAsync(Guid groupKey, AManyProjection projection)
        {
            CallCount++;
            var result = await _repository
                 .Query<AOneManyProjection>()
                 .Where(x => x.ManyId == groupKey)
                 .ToListAsync();
            projection.OneCount = result.Count;
        }
    }
}