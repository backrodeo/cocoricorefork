using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class ACustomSynchronizer : IInsertSynchronizer<AnEntity, AProjectionWithCustomConstruction>
    {
        private IRepository _repository;

        public ACustomSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task OnInsertAsync(AnEntity entity)
        {
            var projection = new AProjectionWithCustomConstruction();
            projection.EntityId = entity.Id;
            await _repository.InsertAsync(projection);
        }
    }
}