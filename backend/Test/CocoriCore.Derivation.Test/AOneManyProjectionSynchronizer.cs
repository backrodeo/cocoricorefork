
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class AOneManyProjectionSynchronizer : IGroupSynchronizer<AOneEntity, AOneManyProjection>
    {
        private IRepository _repository;

        public AOneManyProjectionSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task GroupUpdateAsync(Guid groupKey, AOneManyProjection projection)
        {
            var result = await _repository
                 .Query<AOneEntity>()
                 .Where(x => x.ParentId == groupKey)
                 .ToListAsync();
            projection.OneCount = result.Count;
        }
    }
}