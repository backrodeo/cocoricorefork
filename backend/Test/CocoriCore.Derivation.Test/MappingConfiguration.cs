using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;

namespace CocoriCore.Derivation.Test
{
    public class MappingConfiguration : MappingConfigurationBase
    {
        public MappingConfiguration()
        {
            Map<AnEntity>().To<AProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
                p.EntityName = e.Name;
                p.Duration = e.End - e.Start;
            });
            Map<DependencyEntityA>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyAName = e.Name;
                p.Town = e?.Town;
                p.DependencyAVersion = e.Version;
                p.DependencyBId = e.DependencyBId;
                p.DependencyCId = e.DependencyCId;
            });
            Map<INamedEntity>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyAName = e.Name;
            });
            Map<DependencyEntityA>("error").To<AProjection>().Using((e, p) =>
            {
                throw new Exception("Fake synchronizer error.");
            });
            Map<DependencyEntityB>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyBName = e.Name;
            });
            Map<DependencyEntityC>().To<AProjection>().Using((e, p) =>
            {
                p.DependencyCName = e.Name;
            });
            Map<AnEntity>().To<AGroupProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AOneEntity>().To<AOneManyProjection>().Using((e, p) =>
            {
                p.OneId = e.Id;
                p.ParentId = e.ParentId;
                p.OneName = e.Name;
            });
            Map<AManyEntity>().To<AOneManyProjection>().Using((e, p) =>
            {
                p.ManyId = e.Id;
                p.ManyName = e.Name;
            });
            Map<AOneEntity>("many").To<AOneManyProjection>().Using((e, p) =>
            {
                p.OneId = e.Id;
                p.ParentId = e.ParentId;
                p.OneName = e.Name;
            });
            Map<AOneEntity>().To<AOneProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AManyEntity>().To<AManyProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AnAssociationEntity>().To<AManyProjection>().Using((e, p) =>
            {
                p.OneId = e.OneId;
            });
            Map<AOneEntity>().To<AManyProjection>().Using((e, p) =>
            {
                p.OneName = e.Name;
            });
            Map<AParentChildEntity>().To<AParentChildProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
            });
            Map<AParentChildEntity>("parent").To<AParentChildProjection>().Using((e, p) =>
            {
                p.ParentDepth = e.Depth;
                p.ParentName = e.Name;
            })
            .ResetUsing(p =>
            {
                p.ParentDepth = null;
                p.ParentName = null;
            });
            Map<AParentChildEntity>().To<AnotherParentChildProjection>().Using((e, p) =>
            {
                AutoMap(e, p);
                if (e.Depth == 0)
                {
                    p.RootId = e.Id;
                }
            });
            Map<AnotherParentChildProjection>("parent").To<AnotherParentChildProjection>().Using((e, p) =>
            {
                p.ParentName = e.Name;
                p.ParentDepth = e.Depth;
                p.RootId = e.RootId;
            })
            .ResetUsing(p =>
            {
                p.ParentDepth = null;
                p.ParentName = null;
            });
            Map<AParentChildEntity>("root").To<AnotherParentChildProjection>().Using((e, p) =>
            {
                p.RootName = e.Name;
            });

            Map<AnEntity>().To<AProjectionWithCustomConstruction>().Using((e, p) =>
            {
                p.EntityName = e.Name;
                p.DepAId = e.DependencyAId;
            });
            Map<DependencyEntityA>().To<AProjectionWithCustomConstruction>().Using((e, p) =>
            {
                p.DepAName = e.Name;
            });

            Map<AnEntityWithPolymorphicDependency>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.Name = e.Name;
                p.DependencyId = e.DependencyId;
            });
            Map<DependencyEntityA>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.DependencyName = e.Name;
                p.Town = e.Town;
            });
            Map<DependencyEntityB>().To<AProjectionWithPolymorphicDependency>().Using((e, p) =>
            {
                p.DependencyName = e.Name;
            });
        }
    }
}