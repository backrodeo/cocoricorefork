using System;
using Autofac;

namespace CocoriCore.Derivation.Test
{
    public class BusTestBase
    {
        protected ContainerBuilder ContainerBuilder { get; }
        protected UnitOfWorkOptionsBuilder _unitOfWorkBuilder;
        protected ILifetimeScope _rootScope;
        protected IUnitOfWorkFactory UnitOfWorkFactory => RootScope.Resolve<IUnitOfWorkFactory>();

        public BusTestBase()
        {
            ContainerBuilder = new ContainerBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();

            ContainerBuilder.Register<Func<ILifetimeScope>>(c => _rootScope.BeginLifetimeScope).AsSelf();
            ContainerBuilder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            ContainerBuilder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            ContainerBuilder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
        }

        protected ILifetimeScope RootScope
        {
            get
            {
                if (_rootScope == null)
                {
                    _rootScope = ContainerBuilder.Build();
                }
                return _rootScope;
            }
        }
    }
}