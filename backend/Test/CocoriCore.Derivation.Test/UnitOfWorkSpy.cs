using System;
using System.Threading.Tasks;
using Autofac;
using CocoriCore;

namespace CocoriCore.Derivation.Test
{
    public class UnitOfWorkSpy : AutofacUnitOfWork
    {
        private SpyCounter _counter;

        public UnitOfWorkSpy(UnitOfWorkOptions options, Func<ILifetimeScope> scopeFactory, ILifetimeScope scope, SpyCounter counter)
            : base(options, scope)
        {
            _counter = counter;
            _counter.InstanceCounter++;
        }

        public override async Task FinishAsync()
        {
            _counter.FinishCounter++;
            await base.FinishAsync();
        }
    }

    public class SpyCounter
    {
        public int InstanceCounter;
        public int FinishCounter;
    }
}