using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class GroupProjectionSynchronizer : IGroupSynchronizer<DependencyEntityD, AGroupProjection>
    {
        private IRepository _repository;

        public GroupProjectionSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task GroupUpdateAsync(Guid groupKey, AGroupProjection projection)
        {
            var count = await _repository
                .Query<DependencyEntityD>()
                .Where(x => x.AnEntityId == groupKey)
                .CountAsync();
            projection.DependencyDCount = count;
        }
    }
}