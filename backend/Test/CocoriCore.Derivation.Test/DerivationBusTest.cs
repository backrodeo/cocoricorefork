﻿using System;
using Xunit;
using CocoriCore;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Diagnostics;
using System.Linq;
using Autofac.Core;
using CocoriCore.Test;
using System.Linq.Expressions;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class DerivationBusTest : BusTestBase
    {
        private DerivationBusOptionsBuilder _derivationBuilder;
        private FakeRepositoryConfiguration _fakeRepositoryConfiguration;
        private MemoryRepositoryFake RepositoryFake => RootScope.Resolve<MemoryRepositoryFake>();
        private IInMemoryEntityStore EntityStore => RootScope.Resolve<IInMemoryEntityStore>();

        private SpyFnFExceptionHandler FnFErrorHandler => RootScope.Resolve<SpyFnFExceptionHandler>();

        public DerivationBusTest()
        {
            _derivationBuilder = new DerivationBusOptionsBuilder();
            _derivationBuilder.ConfigureMappings(this.GetType().Assembly);
            _unitOfWorkBuilder.Call<IDerivationBus>(b => b.ApplyRulesAsync());
            _fakeRepositoryConfiguration = new FakeRepositoryConfiguration();

            ContainerBuilder.RegisterInstance(_derivationBuilder).AsSelf();
            ContainerBuilder.RegisterInstance(_fakeRepositoryConfiguration).AsSelf();
            ContainerBuilder.RegisterType<Factory>().As<IFactory>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<MemoryRepositoryFake>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MyRepository>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<MemoryRepositoryFake>()))
                .WithParameter(new TypedParameter<ITransactionHolder>(c => c.Resolve<MemoryRepositoryFake>()))
                .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _derivationBuilder.Options).As<DerivationBusOptions>().SingleInstance();
            ContainerBuilder.RegisterType<DerivationBus>().AsSelf().As<IDerivationBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusContextStore>().As<IRepositoryBusContextStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            ContainerBuilder.RegisterType<GroupProjectionSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<AOneProjectionSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<AManyProjectionSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<AnAssociationSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<AOneManyProjectionSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<ACustomSynchronizer>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<ProjectionHandler>().As<IProjectionHandler>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MappingSynchronizer>().As<IMappingSynchronizer>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<InsertHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UpdateHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<DeleteHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<DependencyHandler>().As<IDependencyHandler>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<GroupDependencyHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<ManyProjectionHandler>().As<IManyProjectionHandler>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<SpyFnFExceptionHandler>().AsSelf().SingleInstance();

            ContainerBuilder.RegisterType<AProjection>().AsSelf();
            ContainerBuilder.RegisterType<AProjectionWithPolymorphicDependency>().AsSelf();
            ContainerBuilder.RegisterType<AGroupProjection>().AsSelf();
            ContainerBuilder.RegisterType<AOneManyProjection>().AsSelf();
            ContainerBuilder.RegisterType<AOneProjection>().AsSelf();
            ContainerBuilder.RegisterType<AManyProjection>().AsSelf();
            ContainerBuilder.RegisterType<AParentChildProjection>().AsSelf();
            ContainerBuilder.RegisterType<AnotherParentChildProjection>().AsSelf();
        }

        #region projection

        [Fact]
        public async Task WhenInsertEntityInsertProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                entity.Start = DateTime.Now;
                entity.End = entity.Start.AddHours(1);
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }
            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.Should().NotBeNull();
            projection.EntityName.Should().Be("toto");
            projection.Start.Should().Be(entity.Start);
            projection.End.Should().Be(entity.End);
            projection.Duration.TotalHours.Should().Be(1);
        }

        [Fact]
        public async Task WhenUpdateEntityUpdateProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Start = DateTime.Now;
                entity.End = entity.Start.AddHours(1);
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.End = entity.End.AddHours(1);
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.Should().NotBeNull();
            projection.Start.Should().Be(entity.Start);
            projection.End.Should().Be(entity.End);
            projection.Duration.TotalHours.Should().Be(2);
        }

        [Fact]
        public async Task WhenDeleteEntityDeleteProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeTrue();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeFalse();
        }

        [Fact]
        public async Task WhenInsertEntityStartSynchronizeAndForget()
        {
            _fakeRepositoryConfiguration.DelayInMilliseconds = 50;
            _derivationBuilder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                derivationBus = uow.Resolve<DerivationBus>();
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            FnFErrorHandler.NbCall.Should().Be(0);
            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().ContainSingle();
            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeFalse();
            Task.WaitAll(fnfTasks);
            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeTrue();
        }

        [Fact]
        public async Task WhenUpdateEntityStartSynchronizeAndForget()
        {
            _fakeRepositoryConfiguration.DelayInMilliseconds = 50;
            _derivationBuilder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                derivationBus = uow.Resolve<DerivationBus>();
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().ContainSingle();
            Task.WaitAll(fnfTasks);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                derivationBus = uow.Resolve<DerivationBus>();
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto2";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            FnFErrorHandler.NbCall.Should().Be(0);
            fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().ContainSingle();
            //TODO a rendre plus lisible, utiliser des méthodes d'extenions ?
            //TODO ou sinon pouvoir configurer un Task.Await au début de la tache de dérivation ?
            var projection = (AProjection)EntityStore.Get(typeof(AProjection), entity.Id).Should().ContainSingle().Which;
            projection.EntityName.Should().Be("toto");
            Task.WaitAll(fnfTasks);
            projection = (AProjection)EntityStore.Get(typeof(AProjection), entity.Id).Should().ContainSingle().Which;
            projection.EntityName.Should().Be("toto2");
        }

        [Fact]
        public async Task WhenDeleteEntityStartSynchronizeAndForget()
        {
            _fakeRepositoryConfiguration.DelayInMilliseconds = 50;
            _derivationBuilder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                derivationBus = uow.Resolve<DerivationBus>();
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().ContainSingle();
            Task.WaitAll(fnfTasks);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                derivationBus = uow.Resolve<DerivationBus>();
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            FnFErrorHandler.NbCall.Should().Be(0);
            fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().ContainSingle();
            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeTrue();
            Task.WaitAll(fnfTasks);
            RepositoryFake.ExistsAsync<AProjection>(entity.Id).Result.Should().BeFalse();
        }

        [Fact]
        public async Task CanCreateProjectionLikeLeftJoin()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            //TODO on devrait pouvoir passer un discriminator ici 'one'
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>("many").Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.Name = "toto";
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity1.Id)
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity1.Id);
            projection1.Id.Should().Be(oneEntity1.Id);
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            projection1.OneId.Should().Be(oneEntity1.Id);
            projection1.OneName.Should().Be("toto");
            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.ManyId.Should().Be(manyEntity1.Id);
            projection2.ManyName.Should().Be("titi");
            projection2.OneId.Should().Be(oneEntity1.Id);
            projection2.OneName.Should().Be("toto");
            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection3.ManyId.Should().Be(manyEntity2.Id);
            projection3.ManyName.Should().Be("tutu");
            projection3.OneId.Should().Be(oneEntity1.Id);
            projection3.OneName.Should().Be("toto");
        }

        [Fact]
        public async Task CanUpdateProjectionLikeLeftJoin()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>("many").Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = await repository.LoadAsync<AManyEntity>(manyEntity1.Id);
                manyEntity1.Name = "titi";
                manyEntity2 = await repository.LoadAsync<AManyEntity>(manyEntity2.Id);
                manyEntity2.Name = "tutu";
                oneEntity1 = await repository.LoadAsync<AOneEntity>(oneEntity1.Id);
                oneEntity1.Name = "toto";
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.UpdateManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity1.Id)
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity1.Id);
            projection1.Id.Should().Be(oneEntity1.Id);
            projection1.ManyId.Should().BeNull();
            projection1.ManyName.Should().BeNull();
            projection1.OneId.Should().Be(oneEntity1.Id);
            projection1.OneName.Should().Be("toto");
            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.ManyId.Should().Be(manyEntity1.Id);
            projection2.ManyName.Should().Be("titi");
            projection2.OneId.Should().Be(oneEntity1.Id);
            projection2.OneName.Should().Be("toto");
            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection3.ManyId.Should().Be(manyEntity2.Id);
            projection3.ManyName.Should().Be("tutu");
            projection3.OneId.Should().Be(oneEntity1.Id);
            projection3.OneName.Should().Be("toto");
        }

        [Fact]
        public async Task CanUseOtherIdPropertyForGroupUpdateJoin()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>("many").Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .GroupUpdate<AOneManyProjection>((e, p) => e.ParentId == p.ParentId)
                .Using<AOneManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AOneEntity oneEntity2 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var parentId = Guid.NewGuid();
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ParentId = parentId;
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                oneEntity2 = new AOneEntity().WithId();
                oneEntity2.ParentId = parentId;
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity1.Id)
                .ToArrayAsync();

            projections.Should().HaveCount(3);
            var projection1 = projections.Single(x => x.Id == oneEntity1.Id);
            projection1.OneCount.Should().Be(2);

            var projection2 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection2.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.OneCount.Should().Be(2);

            var projection3 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection3.Id.Should().NotBe(manyEntity1.Id)
                .And.NotBe(manyEntity2.Id)
                .And.NotBe(oneEntity1.Id);
            projection2.OneCount.Should().Be(2);
        }

        #endregion

        #region dependency

        [Fact]
        public async Task WhenUpdateEntitysPropertySynchronizeProjection()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>(x => x.Name, x => x.Town)
                .Update<AProjection>((e, p) => e.Id == p.DependencyAId)
                .UsingMapping();

            DependencyEntityA dependency = null;
            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Name = "luc";
                dependency.Town = "dax";
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(1);
            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.Town.Should().Be("dax");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Town = "bruges";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(2);
            projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.Town.Should().Be("bruges");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                dependency.Name = "toto";
                await repository.UpdateAsync(dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.TotalNbQueryCall.Should().Be(3);
            projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("toto");
            projection.Town.Should().Be("bruges");
        }

        [Fact]
        public async Task InFireAndForgetCallFinishOnUnitOfWork()
        {
            _fakeRepositoryConfiguration.DelayInMilliseconds = 50;
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();
            _derivationBuilder.SetFireAndForgetExceptionHandler<SpyFnFExceptionHandler>((h, e) => h.HandleError(e));
            _unitOfWorkBuilder.Call<MemoryRepositoryFake>(repository => repository.Commit());

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                derivationBus = uow.Resolve<DerivationBus>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            FnFErrorHandler.NbCall.Should().Be(0);
            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            _fakeRepositoryConfiguration.TotalCommitCall.Should().Be(1);
            Task.WaitAll(fnfTasks);
            _fakeRepositoryConfiguration.TotalCommitCall.Should().Be(2);
        }

        [Fact]
        public async Task CanResetWithInterfaceDependency()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<INamedEntity>()
                .Update<AProjection>((e, p) => e.Id == p.DependencyAId)
                .UsingMapping();

            DependencyEntityA dependency = null;
            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Name = "luc";
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().Be("luc");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                await repository.DeleteAsync(dependency);
                await uow.FinishAsync();
            }

            projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAName.Should().BeNull();
        }

        #endregion

        #region mapper

        [Fact]
        public async Task WhenInsertEntityWithDependencyCallDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(entity, dependencyA, dependencyB);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("toto");
        }

        [Fact]
        public async Task WhenRemoveDependencyForEntityWithDependencyCallDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(entity, dependencyA, dependencyB);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.DependencyAId = Guid.Empty;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().BeEmpty();
            projection.DependencyAName.Should().BeNull();
            projection.DependencyBId.Should().BeNull();
            projection.DependencyBName.Should().BeNull();
        }

        [Fact]
        public async Task WhenUpdateEntityWithDependencyCallOnlyNeededDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA1 = null;
            DependencyEntityA dependencyA2 = null;
            DependencyEntityB dependencyB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyA1 = new DependencyEntityA().WithId();
                dependencyA1.Name = "luc";
                dependencyA1.DependencyBId = dependencyB.Id;
                dependencyA2 = new DependencyEntityA().WithId();
                dependencyA2.Name = "etienne";
                dependencyA2.DependencyBId = dependencyB.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA1.Id;
                await repository.InsertManyAsync(entity, dependencyA1, dependencyA2, dependencyB);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.Reset();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.DependencyAId = dependencyA2.Id;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.LoadedUIDs.Should().NotContain(new EntityUID(typeof(DependencyEntityB), dependencyB.Id));
            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA2.Id);
            projection.DependencyAName.Should().Be("etienne");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("toto");
        }

        [Fact]
        public async Task WhenUpdateDependencyCallNeededDependenciesMappers()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var dependencyB1 = new DependencyEntityB().WithId();
                dependencyB1.Name = "toto";
                dependencyB2 = new DependencyEntityB().WithId();
                dependencyB2.Name = "titi";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB1.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(dependencyB1, dependencyB2, dependencyA, entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyA = await repository.LoadAsync<DependencyEntityA>(dependencyA.Id);
                dependencyA.Name = "amandine";
                dependencyA.DependencyBId = dependencyB2.Id;
                await repository.UpdateAsync(dependencyA);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("amandine");
            projection.DependencyBId.Should().Be(dependencyB2.Id);
            projection.DependencyBName.Should().Be("titi");
        }

        [Fact]
        public async Task CallMappersForAllEntityDependencies()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityB>().Update<AProjection>((e, p) => e.Id == p.DependencyBId).UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityC>().Update<AProjection>((e, p) => e.Id == p.DependencyCId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyA = null;
            DependencyEntityB dependencyB = null;
            DependencyEntityC dependencyC = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyB = new DependencyEntityB().WithId();
                dependencyB.Name = "toto";
                dependencyC = new DependencyEntityC().WithId();
                dependencyC.Name = "titi";
                dependencyA = new DependencyEntityA().WithId();
                dependencyA.Name = "luc";
                dependencyA.DependencyBId = dependencyB.Id;
                dependencyA.DependencyCId = dependencyC.Id;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependencyA.Id;
                await repository.InsertManyAsync(dependencyB, dependencyC, dependencyA, entity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AProjection>(entity.Id);
            projection.DependencyAId.Should().Be(dependencyA.Id);
            projection.DependencyAName.Should().Be("luc");
            projection.DependencyBId.Should().Be(dependencyB.Id);
            projection.DependencyBName.Should().Be("toto");
            projection.DependencyCId.Should().Be(dependencyC.Id);
            projection.DependencyCName.Should().Be("titi");
        }

        [Fact]
        public async Task GroupUpdateAfterInsert()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .GroupUpdate<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                var dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity.Id;
                var dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity.Id;
                await repository.InsertManyAsync(dependencyD1, dependencyD2, entity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AGroupProjection>(entity.Id);
            projection.Name.Should().Be("toto");
            projection.DependencyDCount.Should().Be(2);
        }

        [Fact]
        public async Task GroupUpdateAfterUpdate()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .GroupUpdate<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity1 = null;
            AnEntity entity2 = null;
            DependencyEntityD dependencyD1 = null;
            DependencyEntityD dependencyD2 = null;
            DependencyEntityD dependencyD3 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntity().WithId();
                entity2 = new AnEntity().WithId();
                dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity1.Id;
                dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity1.Id;
                dependencyD3 = new DependencyEntityD().WithId();
                dependencyD3.AnEntityId = entity2.Id;
                await repository.InsertManyAsync(dependencyD1, dependencyD2, dependencyD3, entity1, entity2);
                await uow.FinishAsync();
            }

            var projection1 = await RepositoryFake.LoadAsync<AGroupProjection>(entity1.Id);
            projection1.DependencyDCount.Should().Be(2);
            var projection2 = await RepositoryFake.LoadAsync<AGroupProjection>(entity2.Id);
            projection2.DependencyDCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyD1 = await repository.LoadAsync<DependencyEntityD>(dependencyD1.Id);
                dependencyD1.AnEntityId = entity2.Id;//TODO tester aussi avec un id qui n'existe plus
                await repository.UpdateAsync(dependencyD1);
                await uow.FinishAsync();
            }

            //TODO a faire fonctionner
            // projection1 = await RepositoryFake.LoadAsync<AGroupProjection>(entity1.Id);
            // projection1.DependencyDCount.Should().Be(1);
            projection2 = await RepositoryFake.LoadAsync<AGroupProjection>(entity2.Id);
            projection2.DependencyDCount.Should().Be(2);
        }

        //TODO after insert + update en meme temps pour compter le nb d'appels au count

        [Fact]
        public async Task GroupUpdateAfterDelete()
        {
            _derivationBuilder.Project<AnEntity>().Into<AGroupProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityD>()
                .GroupUpdate<AGroupProjection>((e, p) => e.AnEntityId == p.Id)
                .Using<GroupProjectionSynchronizer>();

            AnEntity entity = null;
            DependencyEntityD dependencyD1 = null;
            DependencyEntityD dependencyD2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                dependencyD1 = new DependencyEntityD().WithId();
                dependencyD1.AnEntityId = entity.Id;
                dependencyD2 = new DependencyEntityD().WithId();
                dependencyD2.AnEntityId = entity.Id;
                await repository.InsertManyAsync(dependencyD1, dependencyD2, entity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyD1 = await repository.LoadAsync<DependencyEntityD>(dependencyD1.Id);
                await repository.DeleteAsync(dependencyD1);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AGroupProjection>(entity.Id);
            projection.Name.Should().Be("toto");
            projection.DependencyDCount.Should().Be(1);
        }

        [Fact]
        public async Task ProjectEntityIntoManyProjectionsWhenInsertEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("toto");
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("titi");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("toto");
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task ProjectEntityIntoManyProjectionsWhenAddEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, oneEntity);
                await uow.FinishAsync();
            }

            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.ManyIds = oneEntity.ManyIds.Append(manyEntity2.Id);
                await repository.InsertAsync(manyEntity2);
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().HaveCount(2);
            var projection1 = projections.Single(x => x.ManyId == manyEntity1.Id);
            projection1.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be("toto");
            projection1.ManyId.Should().Be(manyEntity1.Id);
            projection1.ManyName.Should().Be("titi");
            var projection2 = projections.Single(x => x.ManyId == manyEntity2.Id);
            projection2.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be("toto");
            projection2.ManyId.Should().Be(manyEntity2.Id);
            projection2.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task ProjectEntityIntoManyProjectionsWhenRemoveEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                oneEntity.ManyIds = new Guid[] { manyEntity2.Id };
                await repository.UpdateAsync(oneEntity);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            var projection = projections.Should().ContainSingle().Which;
            projection.Id.Should().NotBe(oneEntity.Id).And.NotBe(manyEntity1.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be("toto");
            projection.ManyId.Should().Be(manyEntity2.Id);
            projection.ManyName.Should().Be("tutu");
        }

        [Fact]
        public async Task DeleteManyProjectionsWhenDeleteOneEntity()
        {
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            var projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().HaveCount(2);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = await repository.LoadAsync<AOneEntity>(oneEntity.Id);
                await repository.DeleteAsync(oneEntity);
                await uow.FinishAsync();
            }

            projections = await RepositoryFake
                .Query<AOneManyProjection>()
                .Where(x => x.OneId == oneEntity.Id)
                .ToListAsync();
            projections.Should().BeEmpty();
        }

        [Fact]
        public async Task CascadeDerivationOneMany()
        {
            _derivationBuilder.Project<AOneEntity>().Into<AOneProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .GroupUpdate<AOneProjection>((e, p) => e.OneId == p.Id)
                .Using<AOneProjectionSynchronizer>();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();

            AOneEntity oneEntity = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AOneProjectionSynchronizer synchronizer = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                synchronizer = uow.Resolve<AOneProjectionSynchronizer>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity1.Name = "titi";
                manyEntity2 = new AManyEntity().WithId();
                manyEntity2.Name = "tutu";
                oneEntity = new AOneEntity().WithId();
                oneEntity.Name = "toto";
                oneEntity.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AOneProjection>(oneEntity.Id);
            projection.Name.Should().Be(oneEntity.Name);
            projection.ManyCount.Should().Be(2);
            synchronizer.CallCount.Should().Be(1);
        }

        [Fact]
        public async Task CascadeDerivationManyMany()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .GroupUpdate<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            AOneEntity oneEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity2 = new AOneEntity().WithId();
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(oneEntity2);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(2);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);
        }

        [Fact]
        public async Task CascadeDerivationProjectIntoManyWhenUpdate()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder.Project<AOneEntity, AManyEntity>(e => e.ManyIds).IntoMany<AOneManyProjection>().UsingMapping();
            _derivationBuilder.ForDependency<AOneEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.OneId).UsingMapping();
            //TODO message d'erreur clair si on oublie de mettre la ligne ci-dessus ou/et ci-dessous
            _derivationBuilder.ForDependency<AManyEntity>().Update<AOneManyProjection>((e, p) => e.Id == p.ManyId).UsingMapping();

            _derivationBuilder
                .ForDependency<AOneManyProjection>()
                .GroupUpdate<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AManyProjectionSynchronizer>();

            AOneEntity oneEntity1 = null;
            AOneEntity oneEntity2 = null;
            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                oneEntity1 = new AOneEntity().WithId();
                oneEntity2 = new AOneEntity().WithId();
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id, manyEntity2.Id };
                oneEntity2.ManyIds = new Guid[] { manyEntity1.Id };
                await repository.InsertManyAsync(manyEntity1, manyEntity2, oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(2);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity1 = await repository.LoadAsync<AOneEntity>(oneEntity1.Id);
                oneEntity2 = await repository.LoadAsync<AOneEntity>(oneEntity2.Id);
                oneEntity1.ManyIds = new Guid[] { manyEntity1.Id };
                oneEntity2.ManyIds = new Guid[] { manyEntity2.Id };
                await repository.UpdateManyAsync(oneEntity1, oneEntity2);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);
        }

        [Fact]
        public async Task CascadeDerivationWhenUpdateAssociation()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .GroupUpdate<AManyProjection>((e, p) => e.ManyId == p.Id)
                .Using<AnAssociationSynchronizer>();

            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AnAssociationEntity associationEntity1 = null;
            AnAssociationEntity associationEntity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var oneEntity1 = new AOneEntity().WithId();
                var oneEntity2 = new AOneEntity().WithId();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                associationEntity1 = new AnAssociationEntity().WithId();
                associationEntity2 = new AnAssociationEntity().WithId();
                associationEntity1.OneId = oneEntity1.Id;
                associationEntity1.ManyId = manyEntity1.Id;
                associationEntity2.OneId = oneEntity2.Id;
                associationEntity2.ManyId = manyEntity2.Id;
                await repository.InsertManyAsync(oneEntity1, oneEntity2, manyEntity1, manyEntity2, associationEntity1, associationEntity2);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(1);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(1);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity1 = await repository.LoadAsync<AnAssociationEntity>(associationEntity1.Id);
                associationEntity1.ManyId = manyEntity2.Id;
                await repository.UpdateAsync(associationEntity1);
                await uow.FinishAsync();
            }

            RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id).Result.OneCount.Should().Be(0);
            RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id).Result.OneCount.Should().Be(2);
        }

        [Fact]
        public async Task CreateProjectionWithAssociationEntityAsDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity, associationEntity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenInsertDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                await repository.InsertManyAsync(oneEntity, manyEntity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().BeNull();
            projection.OneName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenDeleteDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity, associationEntity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().Be(oneEntity.Id);
            projection.OneName.Should().Be(oneEntity.Name);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity = await repository.LoadAsync<AnAssociationEntity>(associationEntity.Id);
                await repository.DeleteAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity.Id);
            projection.OneId.Should().BeNull();
            projection.OneName.Should().BeNull();
        }

        [Fact]
        public async Task UpdateProjectionWithAssociationEntityAsDependencyWhenUpdateDependency()
        {
            _derivationBuilder.Project<AManyEntity>().Into<AManyProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnAssociationEntity>()
                .Update<AManyProjection>((e, p) => e.ManyId == p.Id)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AOneEntity>()
                .Update<AManyProjection>((e, p) => e.Id == p.OneId)
                .UsingMapping();

            AManyEntity manyEntity1 = null;
            AManyEntity manyEntity2 = null;
            AOneEntity oneEntity = null;
            AnAssociationEntity associationEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                oneEntity = new AOneEntity().WithId();
                manyEntity1 = new AManyEntity().WithId();
                manyEntity2 = new AManyEntity().WithId();
                associationEntity = new AnAssociationEntity().WithId();
                associationEntity.OneId = oneEntity.Id;
                associationEntity.ManyId = manyEntity1.Id;
                await repository.InsertManyAsync(oneEntity, manyEntity1, manyEntity2, associationEntity);
                await uow.FinishAsync();
            }

            var projection1 = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id);
            projection1.OneId.Should().Be(oneEntity.Id);
            projection1.OneName.Should().Be(oneEntity.Name);
            var projection2 = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id);
            projection2.OneId.Should().BeNull();
            projection2.OneName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                associationEntity = await repository.LoadAsync<AnAssociationEntity>(associationEntity.Id);
                associationEntity.ManyId = manyEntity2.Id;
                await repository.UpdateAsync(associationEntity);
                await uow.FinishAsync();
            }

            projection1 = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity1.Id);
            projection1.OneId.Should().BeNull();
            projection1.OneName.Should().BeNull();
            projection2 = await RepositoryFake.LoadAsync<AManyProjection>(manyEntity2.Id);
            projection2.OneId.Should().Be(oneEntity.Id);
            projection2.OneName.Should().Be(oneEntity.Name);

        }

        [Fact]
        public async Task CanUseResetActionForDependency()
        {
            _derivationBuilder.Project<AParentChildEntity>().Into<AParentChildProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AParentChildEntity>("parent")
                .Update<AParentChildProjection>((e, p) => e.Id == p.ParentId)
                .UsingMapping();

            AParentChildEntity childEntity = null;
            AParentChildEntity parentEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity = new AParentChildEntity().WithId();
                parentEntity = new AParentChildEntity().WithId();
                childEntity.Depth = 1;
                childEntity.Name = "toto";
                childEntity.ParentId = parentEntity.Id;
                parentEntity.Depth = 0;
                parentEntity.Name = "titi";
                await repository.InsertManyAsync(childEntity, parentEntity);
                await uow.FinishAsync();
            }

            var projection1 = await RepositoryFake.LoadAsync<AParentChildProjection>(childEntity.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().Be(parentEntity.Id);
            projection1.ParentDepth.Should().Be(0);
            projection1.ParentName.Should().Be("titi");
            var projection2 = await RepositoryFake.LoadAsync<AParentChildProjection>(parentEntity.Id);
            projection2.Depth.Should().Be(0);
            projection2.Name.Should().Be("titi");
            projection2.ParentId.Should().BeNull();
            projection2.ParentDepth.Should().BeNull();
            projection2.ParentName.Should().BeNull();

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity = await repository.LoadAsync<AParentChildEntity>(childEntity.Id);
                childEntity.ParentId = null;
                await repository.UpdateAsync(childEntity);
                await uow.FinishAsync();
            }

            projection1 = await RepositoryFake.LoadAsync<AParentChildProjection>(childEntity.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().BeNull();
            projection1.ParentDepth.Should().BeNull();
            projection1.ParentName.Should().BeNull();
        }

        [Fact]
        public async Task CanHaveRecursiveDependency()
        {
            _derivationBuilder.Project<AParentChildEntity>().Into<AnotherParentChildProjection>().UsingMapping();
            _derivationBuilder
                .ForDependency<AnotherParentChildProjection>("parent")
                .Update<AnotherParentChildProjection>((e, p) => e.Id == p.ParentId)
                .UsingMapping();
            _derivationBuilder
                .ForDependency<AParentChildEntity>("root")
                .Update<AnotherParentChildProjection>((e, p) => e.Id == p.RootId)
                .UsingMapping();

            AParentChildEntity childEntity1 = null;
            AParentChildEntity childEntity2 = null;
            AParentChildEntity rootEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                childEntity1 = new AParentChildEntity().WithId();
                childEntity2 = new AParentChildEntity().WithId();
                rootEntity = new AParentChildEntity().WithId();
                childEntity2.Depth = 2;
                childEntity2.Name = "tutu";
                childEntity2.ParentId = childEntity1.Id;
                childEntity1.Depth = 1;
                childEntity1.Name = "toto";
                childEntity1.ParentId = rootEntity.Id;
                rootEntity.Depth = 0;
                rootEntity.Name = "titi";
                await repository.InsertManyAsync(childEntity1, childEntity2, rootEntity);
                await uow.FinishAsync();
            }

            var rootProjection = await RepositoryFake.LoadAsync<AnotherParentChildProjection>(rootEntity.Id);
            rootProjection.Depth.Should().Be(0);
            rootProjection.Name.Should().Be("titi");
            rootProjection.ParentId.Should().BeNull();
            rootProjection.ParentDepth.Should().BeNull();
            rootProjection.ParentName.Should().BeNull();
            var projection1 = await RepositoryFake.LoadAsync<AnotherParentChildProjection>(childEntity1.Id);
            projection1.Depth.Should().Be(1);
            projection1.Name.Should().Be("toto");
            projection1.ParentId.Should().Be(rootEntity.Id);
            projection1.ParentDepth.Should().Be(0);
            projection1.ParentName.Should().Be("titi");
            var projection2 = await RepositoryFake.LoadAsync<AnotherParentChildProjection>(childEntity2.Id);
            projection2.Depth.Should().Be(2);
            projection2.Name.Should().Be("tutu");
            projection2.ParentId.Should().Be(childEntity1.Id);
            projection2.ParentDepth.Should().Be(1);
            projection2.ParentName.Should().Be("toto");
        }

        [Fact]
        public async Task CallMappingSynchronizerWhenUsingCustomSynchronizer()
        {
            _derivationBuilder.WhenInsert<AnEntity>().For<AProjectionWithCustomConstruction>().Call<ACustomSynchronizer>();
            _derivationBuilder
                .ForDependency<AnEntity>()
                .Update<AProjectionWithCustomConstruction>((e, p) => e.Id == p.EntityId)
                .UsingMapping();
            _derivationBuilder
               .ForDependency<DependencyEntityA>()
               .Update<AProjectionWithCustomConstruction>((e, p) => e.Id == p.DepAId)
               .UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependencyEntity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyEntity = new DependencyEntityA().WithId();
                dependencyEntity.Name = "depA";
                await repository.InsertAsync(dependencyEntity);
                await uow.FinishAsync();
            }
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                entity.Name = "toto";
                entity.DependencyAId = dependencyEntity.Id;
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var projection = await RepositoryFake
                .Query<AProjectionWithCustomConstruction>()
                .Where(x => x.EntityId == entity.Id)
                .SingleAsync();

            projection.EntityId.Should().Be(entity.Id);
            projection.EntityName.Should().Be("toto");
            projection.DepAId.Should().Be(dependencyEntity.Id);
            projection.DepAName.Should().Be("depA");
        }

        [Fact]
        public async Task CanHaveDiffrentDependencyTypesForSameProjectionProperty()
        {
            //TODO si on oublie de mettre UsingMapping() il faudrait lever une erreur de configuration.
            _derivationBuilder.Project<AnEntityWithPolymorphicDependency>().Into<AProjectionWithPolymorphicDependency>().UsingMapping();
            _derivationBuilder
                .ForDependency<DependencyEntityA>()
                .Update<AProjectionWithPolymorphicDependency>((e, p) => e.Id == p.DependencyId)
                .UsingMapping();
            _derivationBuilder
               .ForDependency<DependencyEntityB>()
               .Update<AProjectionWithPolymorphicDependency>((e, p) => e.Id == p.DependencyId)
               .UsingMapping();

            DependencyEntityA dependencyEntityA = null;
            DependencyEntityB dependencyEntityB = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependencyEntityA = new DependencyEntityA().WithId();
                dependencyEntityA.Name = "depA";
                dependencyEntityA.Town = "Bordeaux";
                dependencyEntityB = new DependencyEntityB().WithId();
                dependencyEntityB.Name = "depB";
                await repository.InsertManyAsync(dependencyEntityA, dependencyEntityB);
                await uow.FinishAsync();
            }

            AnEntityWithPolymorphicDependency entity1 = null;
            AnEntityWithPolymorphicDependency entity2 = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity1 = new AnEntityWithPolymorphicDependency().WithId();
                entity1.Name = "toto1";
                entity1.DependencyId = dependencyEntityA.Id;
                entity2 = new AnEntityWithPolymorphicDependency().WithId();
                entity2.Name = "toto2";
                entity2.DependencyId = dependencyEntityB.Id;
                await repository.InsertManyAsync(entity1, entity2);
                await uow.FinishAsync();
            }

            var projection1 = await RepositoryFake.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("toto1");
            projection1.Town.Should().Be("Bordeaux");
            projection1.DependencyId.Should().Be(dependencyEntityA.Id);
            projection1.DependencyName.Should().Be("depA");/////////////////
            var projection2 = await RepositoryFake.LoadAsync<AProjectionWithPolymorphicDependency>(entity2.Id);
            projection2.Name.Should().Be("toto2");
            projection2.Town.Should().BeNull();
            projection2.DependencyId.Should().Be(dependencyEntityB.Id);
            projection2.DependencyName.Should().Be("depB");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntityWithPolymorphicDependency>(entity1.Id);
                entity.DependencyId = dependencyEntityB.Id;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            projection1 = await RepositoryFake.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("toto1");
            projection1.Town.Should().BeNull();
            projection1.DependencyId.Should().Be(dependencyEntityB.Id);
            projection1.DependencyName.Should().Be("depB");

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                var entity = await repository.LoadAsync<AnEntityWithPolymorphicDependency>(entity1.Id);
                entity.Name = "titi";
                entity.DependencyId = null;
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            projection1 = await RepositoryFake.LoadAsync<AProjectionWithPolymorphicDependency>(entity1.Id);
            projection1.Name.Should().Be("titi");
            projection1.Town.Should().BeNull();
            projection1.DependencyId.Should().BeNull();
            projection1.DependencyName.Should().BeNull();
        }

        //TODO peut-on faire cohabiter une règle d'update dep qui appelle synchronizer spécifique optimisé (UPDATE WHERE) avec
        //des mappers utilisés pour l'insert/update de la projection ?
        /*
            _derivationBuilder.WhenInsert<AnEntity>().For<AProjection>().AutoMap();//autre nom : .AutoMapAllDependencies() ?
            _derivationBuilder.WhenUpdate<ADependencyEntity>().For<AProjection>(p => p.DependencyId).Call<CustomSynchronizer>();//synchronizer optimisé
            _derivationBuilder.WhenUpdate<AnotherDependencyEntity>().For<AProjection>(p => p.AnotherDependencyId).AutoMap().Batch(...)//optimisation du batch mode
            //Comme l'insert de la projection a besoin de mappers et qu'on a définir deux dépendance fonctionnelles
            //on vérifie qu'il y bien les mappeur de définis pour chauqe dépendance.
            //Le mapper de ADependencyEntity->AProjection ne sera appelé qu'a l'insert de la projection
            //Le synchronizeur optimisé lui sera utilisé pour les update de dépendances.
         */

        #endregion
    }
}

