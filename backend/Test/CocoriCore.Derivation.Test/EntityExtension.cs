using System;
using System.Collections.Generic;
using CocoriCore;

namespace CocoriCore.Derivation.Test
{
    public static class EntityExtension
    {
        public static TEntity WithId<TEntity>(this TEntity entity) where TEntity : IEntity
        {
            entity.Id = Guid.NewGuid();
            return entity;
        }
    }
}