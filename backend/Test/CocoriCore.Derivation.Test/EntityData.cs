using System;
using System.Collections.Generic;
using CocoriCore;

namespace CocoriCore.Derivation.Test
{
    public interface INamedEntity : IEntity
    {
        string Name { get; }
    }

    public class DependencyEntityB : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
    }

    public class DependencyEntityC : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
    }

    public class DependencyEntityD : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid AnEntityId { get; set; }
    }

    public class DependencyEntityA : INamedEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Town { get; set; }
        public virtual int Version { get; set; }
        public virtual Guid? DependencyBId { get; set; }
        public virtual Guid DependencyCId { get; set; }
    }

    public class ANotVirtualEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public string ANotVirtualProperty { get; set; }
    }

    public class AnEntity : AnAbstractEntity
    {
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid DependencyAId { get; set; }
    }

    public class AnEntityWithPolymorphicDependency : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid? DependencyId { get; set; }
    }

    public class AnotherEntity : AnAbstractEntity
    {
    }

    public abstract class AnAbstractEntity : IEntity
    {
        public virtual Guid Id { get; set; }
    }

    public class AOneEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid ParentId { get; set; }
        public virtual IEnumerable<Guid> ManyIds { get; set; }
        public virtual string Name { get; set; }
    }

    public class AManyEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
    }

    public class AnAssociationEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid OneId { get; set; }
        public virtual Guid ManyId { get; set; }
    }

    public class AProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid DependencyAId { get; set; }
        public virtual string DependencyAName { get; set; }
        public virtual int DependencyAVersion { get; set; }
        public virtual Guid? DependencyBId { get; set; }
        public virtual string DependencyBName { get; set; }
        public virtual Guid DependencyCId { get; set; }
        public virtual string DependencyCName { get; set; }
        public virtual DateTime Start { get; set; }
        public virtual DateTime End { get; set; }
        public virtual TimeSpan Duration { get; set; }
        public virtual string EntityName { get; set; }
        public virtual string Town { get; set; }
    }

    public class AnOtherProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual TimeSpan Duration { get; set; }
    }

    public class AGroupProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual int DependencyDCount { get; set; }
        public virtual string Name { get; set; }
    }

    public class AOneManyProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid OneId { get; set; }
        public virtual Guid ParentId { get; set; }
        public virtual Guid? ManyId { get; set; }
        public virtual string ManyName { get; set; }
        public virtual string OneName { get; set; }
        public virtual int OneCount { get; set; }
    }

    public class AManyProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual int OneCount { get; set; }
        public virtual Guid? OneId { get; set; }
        public virtual string OneName { get; set; }
    }

    public class AOneProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int ManyCount { get; set; }
    }

    public class AParentChildEntity : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual int Depth { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid? ParentId { get; set; }
    }

    public class AParentChildProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual int Depth { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid? ParentId { get; set; }
        public virtual string ParentName { get; set; }
        public virtual int? ParentDepth { get; set; }
    }

    public class AnotherParentChildProjection : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual int Depth { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid? ParentId { get; set; }
        public virtual string ParentName { get; set; }
        public virtual int? ParentDepth { get; set; }
        public virtual Guid RootId { get; set; }
        public virtual string RootName { get; set; }
    }

    public class AProjectionWithCustomConstruction : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid EntityId { get; set; }
        public virtual string EntityName { get; set; }
        public virtual Guid DepAId { get; set; }
        public virtual string DepAName { get; set; }
    }

    public class AProjectionWithPolymorphicDependency : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual string Name { get; set; }
        public virtual Guid? DependencyId { get; set; }
        public virtual string DependencyName { get; set; }
        public virtual string Town { get; set; }
    }
}