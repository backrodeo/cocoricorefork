using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class AOneProjectionSynchronizer : IGroupSynchronizer<AOneManyProjection, AOneProjection>
    {
        public int CallCount;
        private IRepository _repository;

        public AOneProjectionSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task GroupUpdateAsync(Guid groupKey, AOneProjection projection)
        {
            CallCount++;
            var result = await _repository
                 .Query<AOneManyProjection>()
                 .Where(x => x.OneId == groupKey)
                 .ToListAsync();
            projection.ManyCount = result.Count;
        }
    }
}