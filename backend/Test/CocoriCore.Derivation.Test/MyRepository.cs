namespace CocoriCore.Derivation
{
    public class MyRepository : RepositoryBusDecorator, IDerivationRepository
    {
        public MyRepository(IRepository repository, IRepositoryBusContextStore contextStore, IUIDProvider UIDProvider) 
            : base(repository, contextStore, UIDProvider)
        {
        }
    }
}