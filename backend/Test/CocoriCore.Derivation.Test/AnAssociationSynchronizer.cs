using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation.Test
{
    public class AnAssociationSynchronizer :
        IGroupSynchronizer<AnAssociationEntity, AManyProjection>
    {
        public int CallCount;
        private IRepository _repository;

        public AnAssociationSynchronizer(IRepository repository)
        {
            _repository = repository;
        }

        public async Task GroupUpdateAsync(Guid groupKey, AManyProjection projection)
        {
            var count = await _repository
                 .Query<AnAssociationEntity>()
                 .Where(x => x.ManyId == groupKey)
                 .CountAsync();
            projection.OneCount = count;
        }
    }
}