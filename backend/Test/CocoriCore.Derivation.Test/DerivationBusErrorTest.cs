﻿using System;
using Xunit;
using CocoriCore;
using FluentAssertions;
using System.Threading.Tasks;
using Autofac;
using System.Diagnostics;
using System.Linq;
using Autofac.Core;
using System.Threading;
using System.Collections.Generic;
using CocoriCore.Test;
using CocoriCore.Linq.Async;
using Utf8Json;
using Newtonsoft.Json.Converters;

namespace CocoriCore.Derivation.Test
{
    public class DerivationBusErrorTest : BusTestBase
    {
        private DerivationBusOptionsBuilder _derivationBuilder;
        private ErrorBusOptionsBuilder _errorBuilder;
        private FakeRepositoryConfiguration _fakeRepositoryConfiguration;
        private MemoryRepositoryFake RepositoryFake => RootScope.Resolve<MemoryRepositoryFake>();

        public DerivationBusErrorTest()
        {
            _derivationBuilder = new DerivationBusOptionsBuilder();
            _derivationBuilder.ConfigureMappings(this.GetType().Assembly);
            _errorBuilder = new ErrorBusOptionsBuilder();
            _unitOfWorkBuilder.Call<IDerivationBus>(b => b.ApplyRulesAsync());
            _fakeRepositoryConfiguration = new FakeRepositoryConfiguration();

            ContainerBuilder.RegisterInstance(_derivationBuilder).AsSelf();
            ContainerBuilder.RegisterInstance(_fakeRepositoryConfiguration).AsSelf();
            ContainerBuilder.RegisterType<Factory>().As<IFactory>().SingleInstance();
            ContainerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            ContainerBuilder.RegisterType<MemoryRepositoryFake>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MyRepository>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<MemoryRepositoryFake>()))
                .WithParameter(new TypedParameter<ITransactionHolder>(c => c.Resolve<MemoryRepositoryFake>()))
                .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _derivationBuilder.Options).As<DerivationBusOptions>();
            ContainerBuilder.RegisterType<DerivationBus>().AsSelf().As<IDerivationBus>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<RepositoryBusContextStore>().As<IRepositoryBusContextStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            ContainerBuilder.RegisterType<ProjectionHandler>().As<IProjectionHandler>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<DependencyHandler>().As<IDependencyHandler>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<MappingSynchronizer>().As<IMappingSynchronizer>().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<InsertHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<UpdateHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<DeleteHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<SpyFnFExceptionHandler>().AsSelf().InstancePerLifetimeScope();
            ContainerBuilder.RegisterType<DerivationErrorHandler>()
                .AsSelf()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<MemoryRepositoryFake>()))
                .InstancePerLifetimeScope();
            ContainerBuilder.Register(c => _errorBuilder.Options).As<ErrorBusOptions>();
            ContainerBuilder.RegisterType<ErrorBus>().As<IErrorBus>().SingleInstance();

            ContainerBuilder.RegisterType<AProjection>().AsSelf();
        }

        [Fact]
        public void ThrowConfigurationExceptionIfNoFireAndForgetHandlerDefined()
        {
            var handler = new ASpyHandler();
            handler.Delay = 50;
            ContainerBuilder.RegisterInstance(handler).AsSelf();
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            DerivationBusOptions options = null;
            Action action1 = () => options = _derivationBuilder.Options;

            action1.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Contain("No fire and forget exception handler");

            DerivationBus derivationBus = null;
            Action action2 = () => derivationBus = RootScope.Resolve<DerivationBus>();

            var innerError = action2.Should().Throw<DependencyResolutionException>()
                .Which.InnerException;
            innerError.Should().BeOfType<ConfigurationException>()
                .Which.Message.Should().Contain("No fire and forget exception handler");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForInsertEntity()
        {
            Action action = () => _derivationBuilder.WhenInsert<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForUpdateEntity()
        {
            Action action = () => _derivationBuilder.WhenUpdate<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForDeleteEntity()
        {
            Action action = () => _derivationBuilder.WhenDelete<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualProject()
        {
            Action action = () => _derivationBuilder.Project<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public void ThrowExceptionIfPropertiesNotVirtualForDependency()
        {
            Action action = () => _derivationBuilder.ForDependency<AnInvalidEntity>();

            action.Should().Throw<ArgumentException>()
                .Which.Message.Should().Contain($"The setter of property {nameof(AnInvalidEntity.ANotVirtualProperty)} for type {typeof(AnInvalidEntity)} must be virtual.");
        }

        [Fact]
        public async Task ThrowExceptionIfEntityNotLoadedBeforeUpdate()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            action.Should().Throw<InvalidOperationException>()
                .WithMessage("No state associated with this entity, ensure you have loaded it before any update or delete.");
        }

        [Fact]
        public void NoExceptionIfNoRuleDefinedForEntity()
        {
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    var entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ThrowErrorWhenInsertEntity()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.CREATE && e is AProjection);
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = new AnEntity().WithId();
                    await repository.InsertAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<DerivationException>().Which;
            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(AnEntity)} to {typeof(AProjection)} with empty discriminator "
                + $"using {typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(AnEntity));
            exception.EntityId.Should().Be(entity.Id);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Fake insert error.");
        }

        [Fact]
        public async Task ThrowErrorWhenUpdateEntity()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.UPDATE && e is AProjection);
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    entity.Name = "toto";
                    await repository.UpdateAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<DerivationException>().Which;
            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(AnEntity)} to {typeof(AProjection)} with empty discriminator "
                + $"using {typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(AnEntity));
            exception.EntityId.Should().Be(entity.Id);
            exception.Operation.Should().Be(CUDOperation.UPDATE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Fake update error.");
        }

        [Fact]
        public async Task ThrowErrorWhenDeleteEntity()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.DELETE && e is AProjection);
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            AnEntity entity = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    entity = await repository.LoadAsync<AnEntity>(entity.Id);
                    await repository.DeleteAsync(entity);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<DerivationException>().Which;
            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(AnEntity)} to {typeof(AProjection)} with empty discriminator "
                + $"using {typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(AnEntity));
            exception.EntityId.Should().Be(entity.Id);
            exception.Operation.Should().Be(CUDOperation.DELETE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Fake delete error.");
        }

        [Fact]
        public async Task ThrowUpdateErrorWhenUpdateDependencyEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Version = 1;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.UPDATE && e is AProjection);

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                    dependency.Version = 2;
                    await repository.UpdateAsync(dependency);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<DerivationException>().Which;
            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(DependencyEntityA)} to {typeof(AProjection)} with empty discriminator using "
                + $"{typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(DependencyEntityA));
            exception.EntityId.Should().Be(dependency.Id);
            exception.Operation.Should().Be(CUDOperation.UPDATE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Fake update error.");
        }

        [Fact]
        public void ThrowSynchronizerErrorWhenUpdateDependencyEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>("error").Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            Func<Task> action = async () =>
             {
                 using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                 {
                     var repository = uow.Resolve<IRepository>();
                     dependency = new DependencyEntityA().WithId();
                     entity = new AnEntity().WithId();
                     entity.DependencyAId = dependency.Id;
                     await repository.InsertManyAsync(entity, dependency);
                     await uow.FinishAsync();
                 }
             };

            var exception = action.Should().Throw<DerivationException>().Which;

            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(AnEntity)} to {typeof(AProjection)} with empty discriminator "
                + $"using {typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(AnEntity));
            exception.EntityId.Should().Be(entity.Id);
            exception.Operation.Should().Be(CUDOperation.CREATE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Derivation mapping with 'error' discriminator from "
                + $"{typeof(DependencyEntityA)} to projection {typeof(AProjection)} throw an exception, see inner for details.");
            exception.InnerException.InnerException.Message.Should().Be("Fake synchronizer error.");
            _fakeRepositoryConfiguration.TotalNbUpdateCall.Should().Be(0);
        }

        [Fact]
        public async Task ThrowQueryErrorWhenUpdateDependencyEntity()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>().Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            AnEntity entity = null;
            DependencyEntityA dependency = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                dependency = new DependencyEntityA().WithId();
                dependency.Version = 1;
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            _fakeRepositoryConfiguration.ErrorWhenQuery(t => typeof(AProjection).IsAssignableFrom(t));

            Func<Task> action = async () =>
            {
                using (var uow = UnitOfWorkFactory.NewUnitOfWork())
                {
                    var repository = uow.Resolve<IRepository>();
                    dependency = await repository.LoadAsync<DependencyEntityA>(dependency.Id);
                    dependency.Name = "toto";
                    await repository.UpdateAsync(dependency);
                    await uow.FinishAsync();
                }
            };

            var exception = action.Should().Throw<DerivationException>().Which;
            exception.Message.Should().Be("Error while executing derivation rule from "
                + $"{typeof(DependencyEntityA)} to {typeof(AProjection)} with empty discriminator using "
                + $"{typeof(IMappingSynchronizer)}, see inner for details.");
            exception.EntityType.Should().Be(typeof(DependencyEntityA));
            exception.EntityId.Should().Be(dependency.Id);
            exception.Operation.Should().Be(CUDOperation.UPDATE);
            exception.ProjectionType.Should().Be(typeof(AProjection));
            exception.ProjectionId.Should().BeNull();
            exception.SynchronizerType.Should().Be(typeof(IMappingSynchronizer));
            exception.InnerException.Message.Should().Be("Fake query error.");
        }

        [Fact]
        public async Task DerivationErrorHandlerSaveErrorWhenInsertEntityFireAndForget()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.CREATE && e is AProjection);
            _errorBuilder.For<DerivationException>().Call<DerivationErrorHandler>(h => h.HandleErrorAsync);
            _errorBuilder.SetUnexpectedExceptionHandler((ue, e) => { Console.Error.WriteLine(ue); return Task.CompletedTask; });
            _derivationBuilder.SetFireAndForgetExceptionHandler<IErrorBus>((h, e) => h.HandleAsync(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                derivationBus = uow.Resolve<DerivationBus>();
                var dependency = new DependencyEntityA().WithId();
                entity = new AnEntity().WithId();
                entity.DependencyAId = dependency.Id;
                await repository.InsertManyAsync(entity, dependency);
                await uow.FinishAsync();
            }

            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            fnfTasks.Should().NotBeEmpty();
            Task.WaitAll(fnfTasks);

            var errorData = await RepositoryFake.Query<DerivationError>().SingleAsync();
            errorData.EntityId.Should().Be(entity.Id);
            errorData.EntityType.Should().Be(typeof(AnEntity));
            errorData.ExceptionId.Should().NotBeEmpty();
            errorData.Operation.Should().Be(CUDOperation.CREATE);
            errorData.ProjectionType.Should().Be(typeof(AProjection));
            errorData.ProjectionId.Should().BeNull();
        }

        [Fact]
        public async Task DerivationErrorHandlerSaveErrorWhenUpdateEntityFireAndForget()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.UPDATE && e is AProjection);
            _errorBuilder.For<DerivationException>().Call<DerivationErrorHandler>(h => h.HandleErrorAsync);
            _errorBuilder.SetUnexpectedExceptionHandler((ue, e) => { Console.Error.WriteLine(ue); return Task.CompletedTask; });
            _derivationBuilder.SetFireAndForgetExceptionHandler<IErrorBus>((h, e) => h.HandleAsync(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                derivationBus = uow.Resolve<DerivationBus>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            Task.WaitAll(fnfTasks);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                derivationBus = uow.Resolve<DerivationBus>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                entity.Name = "toto";
                await repository.UpdateAsync(entity);
                await uow.FinishAsync();
            }

            fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            Task.WaitAll(fnfTasks);

            var errorData = await RepositoryFake.Query<DerivationError>().SingleAsync();
            errorData.EntityId.Should().Be(entity.Id);
            errorData.EntityType.Should().Be(typeof(AnEntity));
            errorData.ExceptionId.Should().NotBeEmpty();
            errorData.Operation.Should().Be(CUDOperation.UPDATE);
            errorData.ProjectionType.Should().Be(typeof(AProjection));
            errorData.ProjectionId.Should().BeNull();
        }

        [Fact]
        public async Task DerivationErrorHandlerSaveErrorWhenDeleteEntityFireAndForget()
        {
            _fakeRepositoryConfiguration.ErrorWhen((o, e) => o == CUDOperation.DELETE && e is AProjection);
            _errorBuilder.For<DerivationException>().Call<DerivationErrorHandler>(h => h.HandleErrorAsync);
            _errorBuilder.SetUnexpectedExceptionHandler((ue, e) => { Console.Error.WriteLine(ue); return Task.CompletedTask; });
            _derivationBuilder.SetFireAndForgetExceptionHandler<IErrorBus>((h, e) => h.HandleAsync(e));
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping().FireAndForget();

            AnEntity entity = null;
            DerivationBus derivationBus = null;
            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                derivationBus = uow.Resolve<DerivationBus>();
                entity = new AnEntity().WithId();
                await repository.InsertAsync(entity);
                await uow.FinishAsync();
            }

            var fnfTasks = derivationBus.FireAndForgetTasks.ToArray();
            Task.WaitAll(fnfTasks);

            using (var uow = UnitOfWorkFactory.NewUnitOfWork())
            {
                var repository = uow.Resolve<IRepository>();
                entity = await repository.LoadAsync<AnEntity>(entity.Id);
                await repository.DeleteAsync(entity);
                await uow.FinishAsync();
            }

            var errorData = await RepositoryFake.Query<DerivationError>().SingleAsync();
            errorData.EntityId.Should().Be(entity.Id);
            errorData.EntityType.Should().Be(typeof(AnEntity));
            errorData.ExceptionId.Should().NotBeEmpty();
            errorData.Operation.Should().Be(CUDOperation.DELETE);
            errorData.ProjectionType.Should().Be(typeof(AProjection));
            errorData.ProjectionId.Should().BeNull();
        }

        [Fact]
        public void ConfigurationExceptionIfRuleUsingMappingButNoMappingDefined()
        {
            _derivationBuilder.Options.MappingRules.Clear();
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Options;

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"No mapping defined from entity {typeof(AnEntity)} to projection {typeof(AProjection)} with discriminator ''. "
                + $"Ensure you have configured mappings using {nameof(DerivationBusMappingExtension.ConfigureMappings)}() "
                + $"and defined a mapping action for this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.");
        }

        [Fact]
        public void ConfigurationExceptionIfDuplicateDependencyWithSameDiscriminator()
        {
            _derivationBuilder.Options.MappingRules.Clear();
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();

            Action action = () => _derivationBuilder.ForDependency<AnEntity>().Update<AProjection>((e, p) => true);

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"Projection {typeof(AProjection)} already have a dependency {typeof(AnEntity)} "
                + $"with an empty discriminator. Set discriminator using builder.{nameof(DerivationBusOptionsBuilder.ForDependency)}(\"discriminator\").");
        }

        [Fact]
        public void ConfigurationExceptionIfTwoRulesForSameDependencyProjectionAndDiscriminator()
        {
            var rule1 = new MappingRule(typeof(AnEntity));
            rule1.ProjectionType = typeof(AProjection);
            rule1.Discriminator = "toto";
            var rule2 = new MappingRule(typeof(AnEntity));
            rule2.ProjectionType = typeof(AProjection);
            rule2.Discriminator = "toto";
            _derivationBuilder.Options.MappingRules.Clear();
            _derivationBuilder.Options.AddMappingRule(rule1);

            Action action = () => _derivationBuilder.Options.AddMappingRule(rule2);

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"Duplicate mapping defined for entity {typeof(AnEntity)}, projection {typeof(AProjection)} and discriminator 'toto'.");
        }

        [Fact]
        public void ConfigurationExceptionIfDependencyWithDiscriminorButNoMappingDefinedWithSameDiscriminator()
        {
            _derivationBuilder.Project<AnEntity>().Into<AProjection>().UsingMapping();
            _derivationBuilder.ForDependency<DependencyEntityA>("toto").Update<AProjection>((e, p) => e.Id == p.DependencyAId).UsingMapping();

            DerivationBusOptions options = null;
            Action action = () => options = _derivationBuilder.Options;

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should().Be($"No mapping defined from entity {typeof(DependencyEntityA)} to projection {typeof(AProjection)} with discriminator 'toto'. "
                + $"Ensure you have configured mappings using {nameof(DerivationBusMappingExtension.ConfigureMappings)}() "
                + $"and defined a mapping action for this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.");
        }

        // TODO : reste fix error (retry derivation) / save dependencies error

        public class AMockHandler
        {
            public Task ExecuteAsync(RepositoryBusContext context)
            {
                return Task.CompletedTask;
            }
        }

        public class ASpyExceptionHandler
        {
            public List<Exception> Exceptions;

            public ASpyExceptionHandler()
            {
                Exceptions = new List<Exception>();
            }

            public Task HandleErrorAsync(Exception exception)
            {
                Exceptions.Add(exception);
                return Task.CompletedTask;
            }
        }

        public class AnInvalidEntity : IEntity
        {
            public virtual Guid Id { get; set; }
            public string ANotVirtualProperty { get; set; }
            public virtual string AVirtualProperty { get; set; }
        }

        public class ASpyHandler : IRepositoryBusHandler
        {
            public int NbCall = 0;
            public int Delay = 0;
            public RepositoryBusContext PassedContext;

            public async Task DoSomethingAsync(RepositoryBusContext context)
            {
                await Task.Delay(Delay);
                Interlocked.Increment(ref NbCall);
                PassedContext = context;
            }

            public Task ExecuteAsync(RepositoryBusContext context)
            {
                return DoSomethingAsync(context);
            }
        }
    }
}

