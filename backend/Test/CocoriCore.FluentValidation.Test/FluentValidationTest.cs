using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using FluentValidation;
using FluentValidation.Results;
using Xunit;

namespace CocoriCore.FluentValidation.Test
{
    public class FluentValidationTest
    {
        private ILifetimeScope _scope;
        private ContainerBuilder _builder;
        private MessageBusOptionsBuilder _optionBuilder;
        private UnitOfWorkOptionsBuilder _unitOfWorkBuilder;
        private IMessageBus _messageBus;

        public FluentValidationTest()
        {
            _builder = new ContainerBuilder();
            _optionBuilder = new MessageBusOptionsBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();
            var assembly = this.GetType().Assembly;
            _optionBuilder.ForAll<IMessage>(assembly)
                .CallValidators(assembly)
                .CallMessageHandlers(assembly);

            _builder.RegisterInstance(_optionBuilder.Options).AsSelf();
            _builder.RegisterType<MessageBus>().As<IMessageBus>();

            _builder.Register<Func<ILifetimeScope>>(c => _scope.BeginLifetimeScope).AsSelf();
            _builder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
        }

        public ILifetimeScope Scope
        {
            get
            {
                if (_scope == null)
                    _scope = _builder.Build();
                return _scope;
            }
        }

        public IMessageBus MessageBus
        {
            get
            {
                if (_messageBus == null)
                    _messageBus = Scope.Resolve<IMessageBus>();
                return _messageBus;
            }
        }

        [Fact]
        public async Task CallACommandValidator()
        {
            var validator = new ACommandValidator();
            _builder.RegisterInstance(validator);
            _builder.RegisterType<ACommandHandler>().AsSelf();
            var command = new ACommand();
            command.Required = "ABC";

            await MessageBus.ExecuteAsync(command);

            validator.Executed.Should().BeTrue();
        }

        [Fact]
        public void ACommandValidatorThrowsValidationException()
        {
            var validator = new ACommandValidator();
            _builder.RegisterInstance(validator);
            var command = new ACommand();
            command.Required = null;

            Func<Task> action = () => MessageBus.ExecuteAsync(command);

            action.Should().Throw<ValidationException>();
            validator.Executed.Should().BeTrue();
        }

        [Fact]
        public async Task CallAnAbstractCommandValidator()
        {
            var validator = new AnAbstractCommandValidator();
            _builder.RegisterInstance(validator);
            _builder.RegisterType<AnotherCommandHandler>().AsSelf();
            var command = new AnotherCommand();
            command.Required = "ABC";

            await MessageBus.ExecuteAsync(command);

            validator.Executed.Should().BeTrue();
        }

        [Fact]
        public void AnAbstractCommandValidatorThrowsValidationException()
        {
            var validator = new AnAbstractCommandValidator();
            _builder.RegisterInstance(validator);
            var command = new AnotherCommand();
            command.Required = null;

            Func<Task> action = () => MessageBus.ExecuteAsync(command);

            action.Should().Throw<ValidationException>();
            validator.Executed.Should().BeTrue();
        }
    }


    public class ACommand : ICommand
    {
        public string Required;
    }

    public abstract class AnAbstractCommand
    {
        public string Required;
    }

    public class AnotherCommand : AnAbstractCommand, ICommand
    {
    }

    public class ACommandHandler : IHandler<ACommand>
    {
        public Task<object> HandleAsync(IMessage message)
        {
            return Task.FromResult<object>(null);
        }
    }

    public class ACommandValidator : AbstractValidator<ACommand>
    {
        public bool Executed = false;

        public ACommandValidator()
        {
            RuleFor(x => x.Required).NotEmpty();
        }

        public async override Task<ValidationResult> ValidateAsync(ValidationContext<ACommand> context, CancellationToken cancellation = default(CancellationToken))
        {
            Executed = true;
            return await base.ValidateAsync(context, cancellation);
        }
    }

    public class AnotherCommandHandler : IHandler<AnotherCommand>
    {
        public Task<object> HandleAsync(IMessage message)
        {
            return Task.FromResult<object>(null);
        }
    }

    public class AnAbstractCommandValidator : AbstractValidator<AnAbstractCommand>
    {
        public bool Executed = false;

        public AnAbstractCommandValidator()
        {
            RuleFor(x => x.Required).NotEmpty();
        }

        public async override Task<ValidationResult> ValidateAsync(ValidationContext<AnAbstractCommand> context, CancellationToken cancellation = default(CancellationToken))
        {
            Executed = true;
            return await base.ValidateAsync(context, cancellation);
        }
    }
}