using System;
using System.Collections.Generic;
using System.Linq;
using CocoriCore.OData.Linq;
using FluentAssertions;
using Xunit;

namespace CocoriCore.OData.Test
{
    public class ODataQueryableTest
    {
        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByPropertyAsc(string propertyName)
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "b" },
                new ODataEntityProperty { Name = "a" },
                new ODataEntityProperty { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = propertyName;

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Name);
        }

        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByFieldAsc(string fieldName)
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "b" },
                new ODataEntityField { Name = "a" },
                new ODataEntityField { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = fieldName;

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Name);
        }

        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByPropertyDesc(string propertyName)
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "b" },
                new ODataEntityProperty { Name = "a" },
                new ODataEntityProperty { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Desc;
            order.Member = propertyName;

            queryable.OrderBy(order).Should().BeInDescendingOrder(x => x.Name);
        }

        [Theory]
        [InlineData("Name")]
        [InlineData("name")]
        public void OrderByFieldDesc(string fieldName)
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "b" },
                new ODataEntityField { Name = "a" },
                new ODataEntityField { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Desc;
            order.Member = fieldName;

            queryable.OrderBy(order).Should().BeInDescendingOrder(x => x.Name);
        }

        [Fact]
        public void OrderByIntField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Price = 45 },
                new ODataEntityField { Price = 23 },
                new ODataEntityField { Price = 64 },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "price";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Price);
        }

        [Fact]
        public void OrderByBoolField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Archived = true },
                new ODataEntityField { Archived = false },
                new ODataEntityField { Archived = true },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "archived";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Archived);
        }

        [Fact]
        public void OrderByDateTimeField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Date = new DateTime(2018, 12, 1) },
                new ODataEntityField { Date = new DateTime(2018, 11, 30) },
                new ODataEntityField { Date = new DateTime(2018, 12, 2) },
            };
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Asc;
            order.Member = "date";

            queryable.OrderBy(order).Should().BeInAscendingOrder(x => x.Date);
        }

        [Fact]
        public void ThrowExceptionIfNoMemberFound()
        {
            var collection = new ODataEntityProperty[0];
            var queryable = collection.AsQueryable();

            var order = new ODataOrderBy();
            order.Direction = OrderDirection.Desc;
            order.Member = "toto";
            Action action = () => queryable.OrderBy(order).ToList();

            action.Should().Throw<MissingMemberException>()
                .WithMessage($"No property neither field named 'toto' found for type {typeof(ODataEntityProperty)}");
        }

        [Fact]
        public void DoNothingIfOrderIsNull()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "b" },
                new ODataEntityField { Name = "a" },
                new ODataEntityField { Name = "c" },
            };
            var queryable = collection.AsQueryable();

            queryable.OrderBy(null).Select(x => x.Name).Should().ContainInOrder("b", "a", "c");
        }

        [Fact]
        public void FilterEqOnOneProperty()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand" },
                new ODataEntityProperty { Name = "antho" },
                new ODataEntityProperty { Name = "luc" },
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("Name", "antho");
            filter.Eq.Add("Name", "bertrand");
            filter.Eq.Add("Name", "c");

            queryable.Where(filter).Select(x => x.Name).Should().BeEquivalentTo("antho", "bertrand");
        }

        [Fact]
        public void FilterEqOnOneStringField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand" },
                new ODataEntityField { Name = "antho" },
                new ODataEntityField { Name = "luc" },
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("Name", "antho");
            filter.Eq.Add("Name", "bertrand");
            filter.Eq.Add("Name", "c");

            queryable.Where(filter).Select(x => x.Name).Should().BeEquivalentTo("antho", "bertrand");
        }

        [Theory]
        [InlineData("begaar")]
        [InlineData("BégaAr")]
        public void FilterEqReplaceField(string search)
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "Bégaar", _Name = "begaar"},
                new ODataEntityField { Name = "Babar", _Name = "babar"},
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("Name", search);
            filter.TransformValue("Name", x => x.RemoveDiacritics().ToLower());
            filter.ReplaceKey("Name", "_Name");

            queryable.Where(filter).Select(x => x.Name).Should().ContainSingle().Which.Should().Be("Bégaar");
        }

        [Fact]
        public void FilterEqOnOneIntField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Price = 12 },
                new ODataEntityField { Price = 19 },
                new ODataEntityField { Price = 25 },
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("price", "12");
            filter.Eq.Add("price", "25");

            queryable.Where(filter).Select(x => x.Price).Should().BeEquivalentTo(12, 25);
        }

        [Fact]
        public void FilterEqOnOneBoolField()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "a", Archived = true },
                new ODataEntityField { Name = "b", Archived = false },
                new ODataEntityField { Name = "c", Archived = true },
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("archived", "true");

            queryable.Where(filter).Select(x => x.Name).Should().BeEquivalentTo("a", "c");
        }

        [Fact]
        public void FilterEqOnOneGuidField()
        {
            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "a", AnId = id1 },
                new ODataEntityField { Name = "b", AnId = id2 },
                new ODataEntityField { Name = "c", AnId = id1 },
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("anid", id1.ToString());

            queryable.Where(filter).Select(x => x.Name).Should().BeEquivalentTo("a", "c");
        }

        //TODO add datetime support for filter

        [Fact]
        public void FilterEqOnManyField()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "33520"},
                new ODataEntityProperty { Name = "antho"   , Code = "16600"},
                new ODataEntityProperty { Name = "luc"     , Code = "40250"},
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Eq.Add("Name", "antho");
            filter.Eq.Add("Name", "luc");
            filter.Eq.Add("Code", "40250");
            filter.Eq.Add("Code", "33520");

            queryable.Where(filter).Should().ContainSingle().Which.Name.Should().Be("luc");
        }

        [Fact]
        public void FilterContainsOnManyField()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "33520"},
                new ODataEntityProperty { Name = "antho"   , Code = "16600"},
                new ODataEntityProperty { Name = "luc"     , Code = "40250"},
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Contains.Add("Name", "an");
            filter.Contains.Add("Code", "5");

            queryable.Where(filter).Should().ContainSingle().Which.Name.Should().Be("bertrand");
        }

        [Theory]
        [InlineData("beg")]
        [InlineData("Bég")]
        public void FilterContainsReplaceField(string search)
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "Bégaar", _Name = "begaar"},
                new ODataEntityField { Name = "Babar", _Name = "babar"},
            };
            var queryable = collection.AsQueryable();

            var filter = new ODataFilter();
            filter.Contains.Add("Name", search);
            filter.TransformValue("Name", x => x.RemoveDiacritics().ToLower());
            filter.ReplaceKey("Name", "_Name");

            queryable.Where(filter).Select(x => x.Name).Should().ContainSingle().Which.Should().Be("Bégaar");
        }

        [Fact]
        public void SelectAllFieldsIfNullFieldResponse()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntityField { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntityField, FieldResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectAllPropertiesIfNullFieldResponse()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntityProperty, FieldResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectAllFieldsIfNullPropertyResponse()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntityField { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntityField, PropertyResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectAllPropertiesIfNullPropertyResponse()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var result = queryable.Select<ODataEntityProperty, PropertyResponse>();

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).AnId.Should().BeEmpty();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).AnId.Should().BeEmpty();
        }

        [Fact]
        public void SelectFieldPropertyResponse()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "o" },
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "o" },
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntityProperty, PropertyResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
        }

        [Fact]
        public void SelectFieldFieldResponse()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand", Code = "A", Other = "o"},
                new ODataEntityField { Name = "antho"   , Code = "B", Other = "o"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntityField, FieldResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
        }

        [Fact]
        public void SelectFieldWithEntityTypeAsResponse()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntityField { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(0).Other.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
            result.ElementAt(1).Other.Should().BeNull();
        }

        [Fact]
        public void SelectPropertyWithEntityTypeAsResponse()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name");
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(0).Other.Should().BeNull();
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
            result.ElementAt(1).Other.Should().BeNull();
        }

        [Fact]
        public void SelectAlFieldsWithEntityTypeAsResponse()
        {
            var collection = new ODataEntityField[]
            {
                new ODataEntityField { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntityField { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            ODataSelect select = null;
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).Other.Should().Be("c");
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).Other.Should().Be("d");
        }

        [Fact]
        public void SelectAllPropertiesWithEntityTypeAsResponse()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            ODataSelect select = null;
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).Other.Should().Be("c");
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).Other.Should().Be("d");
        }

        [Fact]
        public void SelectAllPropertiesWhenEmptySelect()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", Other = "c"},
                new ODataEntityProperty { Name = "antho"   , Code = "B", Other = "d"},
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect();
            var result = queryable.Select(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().Be("A");
            result.ElementAt(0).Other.Should().Be("c");
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().Be("B");
            result.ElementAt(1).Other.Should().Be("d");
        }

        [Fact]
        public void SelectNotNullableToNullable()
        {
            Guid id1 = Guid.NewGuid();
            Guid id2 = Guid.NewGuid();
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", Code = "A", AnId = id1 },
                new ODataEntityProperty { Name = "antho"   , Code = "B", AnId = id2 },
            };
            var queryable = collection.AsQueryable();

            var select = new ODataSelect("name", "anid");
            var result = queryable.Select<ODataEntityProperty, PropertyResponse>(select);

            result.ElementAt(0).Name.Should().Be("bertrand");
            result.ElementAt(0).Code.Should().BeNull();
            result.ElementAt(0).AnId.Should().Be(id1);
            result.ElementAt(1).Name.Should().Be("antho");
            result.ElementAt(1).Code.Should().BeNull();
            result.ElementAt(1).AnId.Should().Be(id2);
        }

        [Fact]
        public void ApplyDistinctOnSelectedFields()
        {
            var collection = new ODataEntityProperty[]
            {
                new ODataEntityProperty { Name = "bertrand", AnId = Guid.NewGuid() },
                new ODataEntityProperty { Name = "antho"   , AnId = Guid.NewGuid() },
                new ODataEntityProperty { Name = "antho"   , AnId = Guid.NewGuid() },
            };
            var queryable = new DistinctEnumerable<ODataEntityProperty>(collection.AsQueryable());

            var select = new ODataSelect("name");
            var result = queryable.Select<ODataEntityProperty, FieldResponse>(select).Distinct(true);

            result.Select(x => x.Name).Should().BeEquivalentTo("bertrand", "antho");
        }
    }

    public class ODataEntityProperty
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Other { get; set; }
        public Guid AnId { get; set; }
        public int Price { get; set; }
    }

    public class ODataEntityField
    {
        public string Name;
        public string _Name;
        public string Code;
        public string Other;
        public int Price;
        public bool Archived;
        public Guid AnId;
        public DateTime Date;
    }

    public class PropertyResponse
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public Guid? AnId { get; set; }
    }

    public class FieldResponse
    {
        public string Name;
        public string Code;
        public Guid? AnId;
    }
}