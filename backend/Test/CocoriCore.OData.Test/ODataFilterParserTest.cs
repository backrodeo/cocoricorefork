using System;
using System.IO;
using System.Linq;
using CocoriCore.OData.Converter;
using CocoriCore.OData.Linq;
using CocoriCore.OData.Parser;
using FluentAssertions;
using Newtonsoft.Json;
using Xunit;

namespace CocoriCore.OData.Test
{
    public class ODataFilterParserTest
    {
        [Fact]
        public void ParseODataFilterEqWithOneExpression()
        {
            string odataInput = "name eq 'toto'";
            var parser = new ODataFilterParser();

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Eq.Keys.Should().ContainSingle().Which.Should().Be("Name");
            odataFilter.Eq.Get("Name").Should().ContainSingle().Which.Should().Be("toto");
        }

        [Fact]
        public void ParseODataFilterEqWithManyExpression()
        {
            string odataInput = "(name eq 'toto') or (name eq 'titi')";
            var parser = new ODataFilterParser();

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Eq.Keys.Should().ContainSingle().Which.Should().Be("Name");
            odataFilter.Eq.Get("Name").Should().HaveCount(2);
            odataFilter.Eq.Get("Name").Should().Contain("toto", "titi");
        }

        [Fact]
        public void CanCustomizeEqKeyName()
        {
            string odataInput = "name eq 'toto'";
            var parser = new ODataFilterParser(k => $"_{k}");

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Eq.Keys.Should().ContainSingle().Which.Should().Be("_name");
            odataFilter.Eq.Get("_name").Should().ContainSingle().Which.Should().Be("toto");
        }

        [Fact]
        public void UnescapeQuotes()
        {
            string odataInput = "name eq 'piment d''espelette'";
            var parser = new ODataFilterParser();

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Eq.Keys.Should().ContainSingle().Which.Should().Be("Name");
            odataFilter.Eq.Get("Name").Should().ContainSingle().Which.Should().Be("piment d'espelette");
        }

        [Theory]
        [InlineData("contains(name,'to')")]
        [InlineData("contains(name, 'to')")]
        public void ParseODataFilterContainOneExpression(string contains)
        {
            string odataInput = contains;
            var parser = new ODataFilterParser();

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Contains.Keys.Should().ContainSingle().Which.Should().Be("Name");
            odataFilter.Contains.Get("Name").Should().ContainSingle().Which.Should().Be("to");
        }

        [Fact]
        public void ParseODataFilterContainsWithManyExpression()
        {
            string odataInput = "contains(name,'to') or contains(name, 'ti')";
            var parser = new ODataFilterParser();

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Contains.Keys.Should().ContainSingle().Which.Should().Be("Name");
            odataFilter.Contains.Get("Name").Should().HaveCount(2);
            odataFilter.Contains.Get("Name").Should().Contain("to", "ti");
        }

        [Fact]
        public void CanCustomizeContainsKeyName()
        {
            string odataInput = "contains(name,'to')";
            var parser = new ODataFilterParser(k => $"_{k}");

            var odataFilter = parser.Parse(odataInput);

            odataFilter.Contains.Keys.Should().ContainSingle().Which.Should().Be("_name");
            odataFilter.Contains.Get("_name").Should().ContainSingle().Which.Should().Be("to");
        }
    }
}