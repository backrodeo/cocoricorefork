using CocoriCore;
using FluentAssertions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    public class TestCreateFileCommand : TestWithDataSets<DataSetsBase, DependencyInjection>
    {
        [Fact]
        public async Task CreateFile()
        {
            var command = new CreateFileCommand();
            command.MimeType = "text/plain";
            command.FileName = "myFile.txt";
            command.Base64Content = Convert.ToBase64String(Encoding.UTF8.GetBytes("toto"));
            command.FolderPath = "/truc/toto";

            var id = (Guid)await _.ExecuteAsync(command);

            var file = await _.Repository.LoadAsync<File>(id);
            await file.ShouldBeEquivalentToAsync(command);
        }

        [Fact]
        public async Task CanCreateFileIntoRootFolder()
        {
            var command = new CreateFileCommand();
            command.MimeType = "text/plain";
            command.FileName = "myFile.txt";
            command.Base64Content = Convert.ToBase64String(Encoding.UTF8.GetBytes("toto"));
            command.FolderPath = "/";

            var id = (Guid)await _.ExecuteAsync(command);

            var file = await _.Repository.LoadAsync<File>(id);
            await file.ShouldBeEquivalentToAsync(command);
        }
    }
}
