﻿using Autofac;
using CocoriCore;
using FluentAssertions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    public class TestFileQuery : TestWithDataSets<DataSetsBase, DependencyInjection>
    {
        [Fact]
        public async Task GetFile()
        {
            var file = await _.CreateFileAsync();

            var query = new GetFileQuery() { Id = file.Id };
            var response = (FileResponse)await _.ExecuteAsync(query);

            await file.ShouldBeEquivalentToAsync(response);
        }
    }
}
