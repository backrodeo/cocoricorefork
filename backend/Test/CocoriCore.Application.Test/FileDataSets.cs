using Castle.DynamicProxy;
using CocoriCore;
using CocoriCore.TestUtils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.Application.Test
{
    public static class FileDataSets
    {
        public static async Task<File> CreateFileAsync(this DataSetsBase _,
            params Action<CreateFileCommand>[] modifications)
        {
            var command = new CreateFileCommand().Default(modifications);
            return await _.CreateFileAsync(command);
        }

        public static async Task<File> CreateFileAsync(this DataSetsBase _, CreateFileCommand command)
        {
            var id = (Guid)await _.ExecuteAsync(command);
            return await _.Repository.LoadAsync<File>(id);
        }
    }
}
