using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Text;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    public static class FileDefault
    {
        public static CreateFileCommand Default(this CreateFileCommand command,
            params Action<CreateFileCommand>[] modifications)
        {
            command.Base64Content = Convert.ToBase64String(Encoding.UTF8.GetBytes("toto"));
            command.FileName = "myFile.txt";
            command.FolderPath = "/aFolder/aSubFolder";
            command.MimeType = "text/plain";
            modifications.ApplyOn(command);
            return command;
        }
    }
}
