using Autofac;
using AutoMapper;
using CocoriCore;
using FluentValidation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    //TODO : factoriser code pour l'injection de dépendances
    public class DependencyInjection : IDependencyInjection
    {
        protected ILifetimeScope _rootScope;
        protected ContainerBuilder _builder;
        private UnitOfWorkOptionsBuilder _unitOfWorkBuilder;

        public ILifetimeScope RootScope
        {
            get
            {
                if (_rootScope == null)
                    _rootScope = _builder.Build();
                return _rootScope;
            }
        }

        public ContainerBuilder Builder => _builder;

        public DependencyInjection()//TODO faire du ménage ici pour ne garder que ce qui est necessaire
        {
            _builder = new ContainerBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();

            var AssemblyContext = AssemblyLoadContext.Default;
            var applicationAssembly = AssemblyContext.LoadFromAssemblyName(typeof(CocoriCore.Application.AssemblyInfo).Assembly.GetName());
            var domainAssembly = AssemblyContext.LoadFromAssemblyName(typeof(CocoriCore.Application.AssemblyInfo).Assembly.GetName());

            _builder.RegisterAssemblyTypes(applicationAssembly)
                    .AssignableTo<IHandler>()
                    .AsSelf();

            _builder.RegisterAssemblyTypes(domainAssembly)
                    .AssignableTo<IEntity>()
                    .AsSelf();

            _builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            _builder.RegisterType<TransactionalMemoryRepository>()
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            _builder.RegisterType<StateStore>().As<IStateStore>().InstancePerLifetimeScope();
            _builder.RegisterType<DataSetsDecorator>()
                .AsSelf()
                .As<IRepository>()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<TransactionalMemoryRepository>()))
                .WithParameter(new TypedParameter<ITransactionHolder>(c => c.Resolve<TransactionalMemoryRepository>()))
                .InstancePerLifetimeScope();
            _builder.RegisterType<DataSetsBase>().As<IDataSets>().SingleInstance();
            _builder.RegisterType<Factory>().As<IFactory>().SingleInstance();

            var formats = new Dictionary<Type, string>();
            _builder.RegisterType<DefaultJsonSerializer>().As<DefaultJsonSerializer, JsonSerializer>().SingleInstance()
                .WithParameter(new TypedParameter(typeof(Dictionary<Type, string>), formats));
            _builder.RegisterType<Formats>().AsSelf().SingleInstance()
                .WithParameter(new TypedParameter(typeof(Dictionary<Type, string>), formats));

            var autoMapperConfiguration = new MapperConfiguration(x => x.AddMaps(applicationAssembly));
            _builder.RegisterAssemblyTypes(applicationAssembly)
               .AssignableTo<Profile>()
               .AsSelf();

            _builder.Register(c => autoMapperConfiguration.CreateMapper(c.Resolve<IComponentContext>().Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();

            _builder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();

            _builder.RegisterType<InMemoryFileSystem>().As<IFileSystem>().SingleInstance();

            _builder.Register<Func<ILifetimeScope>>(c => _rootScope.BeginLifetimeScope).AsSelf();
            _builder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();

            _builder.Register<Func<Type, object>>(c => _rootScope.Resolve).AsSelf().SingleInstance();

            var messageBusOptionsBuilder = new MessageBusOptionsBuilder();
            messageBusOptionsBuilder
                .ForAll<IMessage>(CocoriCore.Application.AssemblyInfo.Assembly)
                .CallMessageHandlers(CocoriCore.Application.AssemblyInfo.Assembly);

            _builder.RegisterInstance(messageBusOptionsBuilder);
            _builder.Register(c => new MessageBus(c.Resolve<IUnitOfWorkFactory>(), c.Resolve<IUnitOfWork>(), c.Resolve<MessageBusOptionsBuilder>().Options))
                .As<IMessageBus>()
                .SingleInstance();

            _builder.RegisterType<Clock>().As<IClock>().SingleInstance();
        }

        public void Dispose()
        {
        }
    }
}
