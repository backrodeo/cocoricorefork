using CocoriCore;
using CocoriCore.TestUtils;

namespace CocoriCore.Application.Test
{
    public static class EntityWithDataSetsExtention
    {
        public static DataSetsBase GetDataSets(this IEntity entity)
        {
            return (DataSetsBase)((entity as IWithDataSets).DataSets);
        }
    }
}
