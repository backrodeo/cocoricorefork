using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System.Collections.Generic;
using CocoriCore.TestUtils;
using System.Linq;
using Soltys.ChangeCase;

namespace CocoriCore.Router.Test
{
    public class ClientCodeGenerationTest
    {
        private Router _router;
        private RouterOptionsBuilder _routerBuilder;
        private ClientCodeGeneratorOptionsBuilder _builder;
        private ResponseTypeProvider _responseTypeProvider;

        public ClientCodeGenerationTest()
        {
            _routerBuilder = new RouterOptionsBuilder();
            _builder = new ClientCodeGeneratorOptionsBuilder();
            _responseTypeProvider = new ResponseTypeProvider(this.GetType().Assembly);
        }

        public IEnumerable<RouteDescriptor> RouteDescriptors => _routerBuilder
            .Options
            .AllRoutes
            .Select(x => x.GetDescriptor());

        //TODO presets de correspondance de type
        //TODO ajouter fichier index.ts

        [Fact]
        public void GenerateFlatTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.RoutePath.Should().Be("admin/users");
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQuery));
            clientDescriptor.MessageTypeIsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("OptionalId", "Guid", true));
            clientDescriptor.MessageType.Imports.Should().BeEmpty();
            clientDescriptor.MessageType.IsEnum.Should().BeFalse();
            clientDescriptor.MessageType.Values.Should().BeEmpty();
            clientDescriptor.MessageType.InnerTypes.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseTypeIsArray.Should().BeFalse();
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true));
            clientDescriptor.ResponseType.Imports.Should().BeEmpty();
            clientDescriptor.ResponseType.IsEnum.Should().BeFalse();
            clientDescriptor.ResponseType.Values.Should().BeEmpty();
            clientDescriptor.ResponseType.InnerTypes.Should().BeEmpty();
        }

        [Fact]
        public void CanSetFieldNameTransformation()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _builder.TransformFieldNames(f => f.CamelCase());

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(
                new Field("id", "Guid"),
                new Field("optionalId", "Guid", true));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("id", "Guid"),
                new Field("text", "String"),
                new Field("date", "DateTime"),
                new Field("count", "Int32"),
                new Field("average", "Single"),
                new Field("time", "TimeSpan", false, true),
                new Field("optionalId", "Guid", true));
        }

        [Fact]
        public void CanSetFieldTypeTransformation()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _builder.TransformFieldTypes(t => t.CamelCase());

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(
                new Field("Id", "guid"),
                new Field("OptionalId", "guid", true));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Id", "guid"),
                new Field("Text", "string"),
                new Field("Date", "dateTime"),
                new Field("Count", "int32"),
                new Field("Average", "single"),
                new Field("Time", "timeSpan", false, true),
                new Field("OptionalId", "guid", true));
        }

        [Fact]
        public void CanDefineTypeCorrespondences()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _builder
                .AddTypeCorrespondence(typeof(Guid), "Guid")
                .AddTypeCorrespondence(typeof(DateTime), "Date")
                .AddTypeCorrespondence(typeof(float), "Number")
                .AddTypeCorrespondence(typeof(TimeSpan), "String");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.RoutePath.Should().Be("admin/users");
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQuery));
            clientDescriptor.MessageType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("OptionalId", "Guid", true));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "Date"),
                new Field("Count", "Int32"),
                new Field("Average", "Number"),
                new Field("Time", "String", false, true),
                new Field("OptionalId", "Guid", true));
            clientDescriptor.ResponseType.Values.Should().BeEmpty();
        }

        [Fact]
        public void CanDefineImports()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("admin")
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _builder
                .AddConditionalImport(nameof(Guid), "guid-typescript")
                .AddConditionalImport(nameof(TimeSpan), "time-import");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Imports.Should()
                .Contain(new Import(nameof(Guid), "guid-typescript"))
                .And.NotContain(new Import(nameof(Decimal), "time-import"));

            clientDescriptor.ResponseType.Imports.Should()
                .Contain(new Import(nameof(Guid), "guid-typescript"))
                .And.Contain(new Import(nameof(TimeSpan), "time-import"));
        }

        [Fact]
        public void GenerateEnumerableTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AEnumerableQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AEnumerableQuery));
            clientDescriptor.MessageTypeIsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseTypeIsArray.Should().BeTrue();
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true));
        }

        [Fact]
        public void GenerateArrayTypeWithDefaultOptions()
        {
            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AnArrayQuery));
            clientDescriptor.MessageTypeIsArray.Should().BeFalse();
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(AResponse));
            clientDescriptor.ResponseTypeIsArray.Should().BeTrue();
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Id", "Guid"),
                new Field("Text", "String"),
                new Field("Date", "DateTime"),
                new Field("Count", "Int32"),
                new Field("Average", "Single"),
                new Field("Time", "TimeSpan", false, true),
                new Field("OptionalId", "Guid", true));
        }

        [Fact]
        public void RoutePathStopAtFirstParameterizedSegment()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            clientDescriptors.Single(x => x.MessageType.Name == nameof(AQuery))
                .RoutePath.Should().Be(new Path("users"));
            clientDescriptors.Single(x => x.MessageType.Name == nameof(AnArrayQuery))
               .RoutePath.Should().Be(new Path("admin", "users"));
        }

        [Fact]
        public void CanExcludeRoute()
        {
            _routerBuilder.Get<AQuery>()
                 .AddPath("users")
                 .AddPathParameter(x => x.Id);

            _routerBuilder.Get<AnArrayQuery>()
                 .AddPath("admin")
                 .AddPath("users");

            _builder.ExcludeRouteWhen(d => d.ParameterizedUrl.Contains("admin"));

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            clientDescriptors.Should().ContainSingle().Which.RoutePath.Should().Be(new Path("users"));
        }

        [Fact]
        public void GenerateClassesRecursively()
        {
            _routerBuilder.Get<AQueryWithDeepResponse>();

            _builder.AddConditionalImport("Guid", "guid-typescript");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AQueryWithDeepResponse));
            clientDescriptor.MessageType.Fields.Should().BeEmpty();
            clientDescriptor.ResponseType.Name.Should().Be(nameof(RootResponse));
            clientDescriptor.ResponseType.Fields.Should().BeEquivalentTo(
                new Field("Name", "String"),
                new Field("Data", "SubResponse"),
                new Field("Childrens", "SubResponse", false, true));
            clientDescriptor.ResponseType.InnerTypes.Should().ContainSingle()
                .Which.Fields.Should().BeEquivalentTo(
                    new Field("Id", "Guid"),
                    new Field("Childrens", "SubResponse", false, true));
            clientDescriptor.ResponseType.Imports.Should().ContainSingle().Which
                .Should().Be(new Import("Guid", "guid-typescript"));
        }

        [Fact]
        public void GenerateTypesForVoidCommand()
        {
            _routerBuilder.Post<AVoidCommand>();

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AVoidCommand));
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateTypesForCreateCommand()
        {
            _routerBuilder.Post<ACreateCommand>();

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(ACreateCommand));
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateEnums()
        {
            _routerBuilder.Post<AEnumCommand>()
                .AddPath("users");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be(nameof(AEnumCommand));
            var enumType = clientDescriptor.MessageType.InnerTypes.Should().ContainSingle().Which;
            enumType.Name.Should().Be(nameof(AnEnum));
            enumType.IsEnum.Should().BeTrue();
            enumType.Fields.Should().BeEmpty();
            enumType.Values.Should().BeEquivalentTo(
                new EnumValue("A", 0),
                new EnumValue("B", 1),
                new EnumValue("C", 2));
            enumType.Imports.Should().BeEmpty();
            clientDescriptor.ResponseType.Should().BeNull();
        }

        [Fact]
        public void GenerateGenericTypes()
        {
            _routerBuilder.Post<AGenericCommand<DateTime, string>>()
                .AddPath("users");

            _builder.AddTypeCorrespondence(typeof(DateTime), "Date");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Name.Should().Be("AGenericCommand<Date, String>");
        }

        [Fact]
        public void GenerateMethodUrlAndQueryStringForQuery()
        {
            _routerBuilder.Get<AQuery>()
                .AddPath("admin")
                .AddPath("users")
                .AddPathParameter(x => x.Id)
                .UseQuery();

            _builder.TransformFieldNames(n => n.ToUpper());
            _builder.RenderPathParameter(n => $"${{this.{n}}}");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Method.Should().Be("GET");
            clientDescriptor.MessageType.Url.Should().Be("/admin/users/${this.ID}");
            clientDescriptor.MessageType.QueryStringFields.Should().ContainSingle().Which.Should().Be("OPTIONALID");
            clientDescriptor.MessageType.BodyFields.Should().BeNull();
        }

        [Fact]
        public void GenerateMethodUrlAndQueryStringForCommand()
        {
            _routerBuilder.Post<AVoidCommand>()
                .AddPath("admin")
                .AddPath("users")
                .AddPathParameter(x => x.Id)
                .UseBody();

            _builder.TransformFieldNames(n => n.CamelCase());
            _builder.RenderPathParameter(n => $"${{this.{n}}}");

            var clientCodeGenerator = new ClientCodeGenerator(_builder.Options, _responseTypeProvider);
            var clientDescriptors = clientCodeGenerator.GenerateClientDescriptors(RouteDescriptors);

            var clientDescriptor = clientDescriptors.Should().ContainSingle().Which;
            clientDescriptor.MessageType.Method.Should().Be("POST");
            clientDescriptor.MessageType.Url.Should().Be("/admin/users/${this.id}");
            clientDescriptor.MessageType.QueryStringFields.Should().BeNull();
            clientDescriptor.MessageType.BodyFields.Should().ContainSingle().Which.Should().Be("name");
        }

        public class AQuery : IQuery
        {
            public Guid Id;
            public Guid? OptionalId;
        }

        public class AQueryHandler : QueryHandler<AQuery, AResponse>
        {
            public override Task<AResponse> ExecuteAsync(AQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AEnumerableQuery : IQuery
        {
        }

        public class AEnumerableQueryHandler : QueryHandler<AEnumerableQuery, IEnumerable<AResponse>>
        {
            public override Task<IEnumerable<AResponse>> ExecuteAsync(AEnumerableQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AnArrayQuery : IQuery
        {
        }

        public class AnArrayQueryHandler : QueryHandler<AnArrayQuery, AResponse[]>
        {
            public override Task<AResponse[]> ExecuteAsync(AnArrayQuery query)
            {
                throw new NotImplementedException();
            }
        }

        public class AQueryWithDeepResponse : IQuery
        {
        }

        public class ADeepResponseQueryHandler : QueryHandler<AQueryWithDeepResponse, RootResponse>
        {
            public override Task<RootResponse> ExecuteAsync(AQueryWithDeepResponse query)
            {
                throw new NotImplementedException();
            }
        }

        public class AResponse
        {
            public Guid Id;
            public string Text;
            public DateTime Date;
            public int Count;
            public float Average;
            public TimeSpan[] Time;
            public Guid? OptionalId;
        }

        public class RootResponse
        {
            public string Name;
            public SubResponse Data;
            public SubResponse[] Childrens;

            public class SubResponse
            {
                public Guid Id;
                public SubResponse[] Childrens;
            }
        }

        public class ACreateCommand : ICommand
        {
        }

        public class ACreateCommandHandler : CreateCommandHandler<ACreateCommand>
        {
            public override Task<Guid> ExecuteAsync(ACreateCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public class AVoidCommand : ICommand
        {
            public Guid Id;
            public string Name;
        }

        public class AVoidCommandHandler : VoidCommandHandler<AVoidCommand>
        {
            public override Task ExecuteAsync(AVoidCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public class AEnumCommand : ICommand
        {
            public AnEnum Enum;
        }

        public class AEnumCommandHandler : VoidCommandHandler<AEnumCommand>
        {
            public override Task ExecuteAsync(AEnumCommand command)
            {
                throw new NotImplementedException();
            }
        }

        public enum AnEnum
        {
            A, B, C
        }

        public class AGenericCommand<T1, T2> : ICommand
        {
        }

        public class AGenericCommandHandler<T1, T2> : VoidCommandHandler<AGenericCommand<T1, T2>>
        {
            public override Task ExecuteAsync(AGenericCommand<T1, T2> command)
            {
                throw new NotImplementedException();
            }
        }
    }
}
