using System;
using Xunit;
using FluentAssertions;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router.Test
{
    public class ApiFootprintTest
    {
        private Router _router;
        private RouterOptionsBuilder _builder;
        private MessageDeserializer _messageDeserializer;
        private ResponseTypeProvider _responseTypeProvider;

        public ApiFootprintTest()
        {
            var jsonSerializer = new DefaultJsonSerializer(new Formats());
            _messageDeserializer = new MessageDeserializer(jsonSerializer);
            _builder = new RouterOptionsBuilder();
            _responseTypeProvider = new ResponseTypeProvider(this.GetType().Assembly);
        }

        private Router Router
        {
            get
            {
                if (_router == null)
                {
                    _router = new Router(_builder.Options, _messageDeserializer);
                }
                return _router;
            }
        }


        [Fact]
        public void RouteWithoutQueryString_Get()
        {
            var route = _builder.Get<GetSomethingQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            var response = (Dictionary<string, object>)routeFootprint.Response;
            response.Should().HaveCount(4);
            response["Id"].Should().Be("guid");
            response["Label"].Should().Be("string");
            response["RelatedId"].Should().Be("guid?");
            response["Gender"].Should().Be("gender");

            footPrint.Enums.Should().HaveCount(1);
            var gender = footPrint.Enums["Gender"];
            gender["None"].Should().Be(0);
            gender["Male"].Should().Be(1);
            gender["Female"].Should().Be(2);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void RouteWithQueryString_Get(int deep)
        {
            var route = _builder.Get<SearchSomethingQuery>()
                 .AddPath("somethings")
                 .AddPath("search")
                 .UseQuery();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, deep);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/search");
            routeFootprint.Method.Should().Be("GET");
            var queryString = (Dictionary<string, object>)routeFootprint.QueryString;
            queryString["Keywords"].Should().Be("string");
            var response = (object[])routeFootprint.Response;
            var firstResponse = (Dictionary<string, object>)response.Should().ContainSingle().Which;
            firstResponse.Should().HaveCount(4);
            firstResponse["Id"].Should().Be("guid");
            firstResponse["Label"].Should().Be("string");
            firstResponse["RelatedId"].Should().Be("guid?");
            firstResponse["Gender"].Should().Be("gender");
        }

        [Fact]
        public void RouteWithQueryString_Deep0()
        {
            var route = _builder.Get<SearchSomethingQuery>()
                 .AddPath("somethings")
                 .AddPath("search")
                 .UseQuery();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, 0);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/search");
            routeFootprint.Method.Should().Be("GET");
            var queryString = (Dictionary<string, object>)routeFootprint.QueryString;
            queryString["Keywords"].Should().Be("string");
            routeFootprint.Response.Should().Be("aresponse[]");

            var types = footPrint.Types;
            types.Should().HaveCount(1);
            var firstType = (Dictionary<string, string>)types["AResponse"];
            firstType["Id"].Should().Be("guid");
            firstType["Label"].Should().Be("string");
            firstType["RelatedId"].Should().Be("guid?");
            firstType["Gender"].Should().Be("gender");
        }

        [Fact]
        public void RouteWithBody_Put()
        {
            var route = _builder.Put<CreateSomethingCommand>()
                 .AddPath("somethings")
                 .UseBody();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings");
            routeFootprint.Method.Should().Be("PUT");
            routeFootprint.QueryString.Should().BeNull();
            var body = (Dictionary<string, object>)routeFootprint.Body;
            body.Should().HaveCount(2);
            body["Name"].Should().Be("string");
            body["BirthDate"].Should().Be("dateTime");
            routeFootprint.Response.Should().Be("guid");
        }

        [Fact]
        public void RouteWithBody_Post()
        {
            var route = _builder.Post<UpdateSomethingCommand>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            var body = (Dictionary<string, object>)routeFootprint.Body;
            body.Should().HaveCount(1);
            body["Name"].Should().Be("string");
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithGenericCommand_Post()
        {
            var route = _builder.Post<UpdateSomethingGenericCommand<AnEntity>>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().BeNull();
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithGenericAndConstraintCommand_Post()
        {
            var route = _builder.Post<DoSomethingGenericCommand<AnEntity>>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id)
                 .UseBody();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("POST");
            routeFootprint.QueryString.Should().BeNull();
            routeFootprint.Body.Should().BeNull();
            routeFootprint.Response.Should().BeNull();
        }

        [Fact]
        public void RouteWithResponseContainingEnumerable_Get()
        {
            var route = _builder.Get<GetSomethingWithEnumerableQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            var response = (Dictionary<string, object>)routeFootprint.Response;
            response.Should().HaveCount(2);
            response["Labels"].Should().Be("string[]");
            response["Items"].Should().Be("itemResponse[]");
        }

        [Fact]
        public void RouteWithResponseContainingEnumerable_Deep2()
        {
            var route = _builder.Get<GetSomethingWithEnumerableQuery>()
                 .AddPath("somethings")
                 .AddPathParameter(x => x.Id);

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, 2);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/somethings/id:guid");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.QueryString.Should().BeNull();
            var response = (Dictionary<string, object>)routeFootprint.Response;
            response.Should().HaveCount(2);
            response["Labels"].Should().Be("string[]");
            var itemsResponse = (object[])response["Items"];
            itemsResponse.Should().ContainSingle();
            var items = (Dictionary<string, object>)itemsResponse[0];
            items["Age"].Should().Be("int32");
            items["Name"].Should().Be("string");
        }

        [Fact]
        public void ExceptionIfDuplicateFieldIntoDataStructure()
        {
            var route = _builder.Get<ADuplicateResponseFieldQuery>().AddPath("stuff");

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, 1);

            Action action = () => footPrintGenerator.Generate(descriptors);

            action.Should().Throw<Exception>()
                .WithMessage("Error during footprint generation for route : GET /stuff")
                .WithInnerException<InvalidOperationException>()
                .WithMessage($"Duplicate field or property name for type {typeof(AResponseWithDuplicateField).FullName} : \nId\nId");
        }

        [Fact]
        public void ExceptionIfUseQueryWithEmtptyQuery()
        {
            var route = _builder.Get<EmtptyQuery>().AddPath("empty").UseQuery();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, 1);

            Action action = () => footPrintGenerator.Generate(descriptors);

            action.Should().Throw<Exception>()
                .WithMessage("Error during footprint generation for route : GET /empty")
                .WithInnerException<ConfigurationException>()
                .WithMessage("Query string is empty for route GET /empty, remove call to .UseQuery().");
        }

        [Fact]
        public void CanDefineFinalTypesWithDescription()
        {
            var route = _builder.Get<WithPrimitiveTypesQuery>()
                 .AddPath("test")
                 .UseQuery();

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var finalTypes = FootprintGenerator.DEFAULT_FINAL_TYPES;
            finalTypes.Add(typeof(AClassConsideredAsFinalType), "object handled by deserializer");
            finalTypes.Add(typeof(Uri), null);
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider, 2, finalTypes);
            var footPrint = footPrintGenerator.Generate(descriptors);

            footPrint.Types.Should().HaveCount(1);
            var firstType = footPrint.Types.ElementAt(0);
            firstType.Key.Should().Be("AClassConsideredAsFinalType");
            firstType.Value.Should().Be("object handled by deserializer");
            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("GET");
            var queryString = (Dictionary<string, object>)routeFootprint.QueryString;
            queryString.Should().HaveCount(2);
            queryString.ElementAt(0).Key.Should().Be("MyFinalObject");
            queryString.ElementAt(0).Value.Should().Be("aclassConsideredAsFinalType");
            queryString.ElementAt(1).Key.Should().Be("Url");
            queryString.ElementAt(1).Value.Should().Be("uri");
        }

        [Fact]
        public void CanDefineRouteDescription()
        {
            var route = _builder.Get<EmtptyQuery>()
                 .AddPath("test")
                 .SetDescritpion("a short description for this route");

            var routes = Router.AllRoutes;
            var descriptors = routes.Select(x => x.GetDescriptor());
            var footPrintGenerator = new FootprintGenerator(_responseTypeProvider);
            var footPrint = footPrintGenerator.Generate(descriptors);

            var routeFootprint = footPrint.Routes.Should().ContainSingle().Which;
            routeFootprint.Url.Should().Be("/test");
            routeFootprint.Method.Should().Be("GET");
            routeFootprint.Description.Should().Be("a short description for this route");
        }
    }

#pragma warning disable 0649

    class AnEntity : IEntity
    {
        public Guid Id { get; set; }
    }

    class GetSomethingQuery : IQuery
    {
        public Guid Id;
    }

    class SearchSomethingQuery : IQuery
    {
        public string Keywords;
    }

    class GetSomethingWithEnumerableQuery : IQuery
    {
        public Guid Id;
    }

    class AResponse : IQuery
    {
        public Guid Id;
        public string Label;
        public Guid? RelatedId;
        public Gender Gender;
    }

    class AResponseWithDuplicateField : AResponseWithDuplicateFieldBase
    {
        public new Guid Id;
    }

    class AResponseWithDuplicateFieldBase : IQuery
    {
        public Guid Id;
    }

    class AResponseWithEnumerable : IQuery
    {
        public string[] Labels;
        public List<ItemResponse> Items;
    }

    class ItemResponse
    {
        public int Age;
        public string Name;
    }

    public enum Gender
    {
        None = 0,
        Male = 1,
        Female = 2
    }

    class ADuplicateResponseFieldQuery : IQuery
    {
    }

    class EmtptyQuery : IQuery
    {
    }

    class CreateSomethingCommand : ICommand
    {
        public string Name;
        public DateTime BirthDate;
    }

    class UpdateSomethingCommand : ICommand
    {
        public Guid Id;
        public string Name;
    }

    class UpdateSomethingGenericCommand<T> : ICommand
        where T : IEntity
    {
        public Guid Id;
    }

    class DoSomethingGenericCommand<T> : ICommand
        where T : IEntity
    {
        public Guid Id;
    }

    class WithPrimitiveTypesQuery : IQuery
    {
        public AClassConsideredAsFinalType MyFinalObject;
        public Uri Url;
    }

    public class AClassConsideredAsFinalType
    {
        public object AField;
    }

#pragma warning restore 0649

    class AFootprintQueryHandler : QueryHandler<GetSomethingQuery, AResponse>
    {
        public override Task<AResponse> ExecuteAsync(GetSomethingQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintWithEnumerableQueryHandler : QueryHandler<GetSomethingWithEnumerableQuery, AResponseWithEnumerable>
    {
        public override Task<AResponseWithEnumerable> ExecuteAsync(GetSomethingWithEnumerableQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintQueryIEnumerableHandler : QueryHandler<SearchSomethingQuery, IEnumerable<AResponse>>
    {
        public override Task<IEnumerable<AResponse>> ExecuteAsync(SearchSomethingQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintCreateCommandHandler : CreateCommandHandler<CreateSomethingCommand>
    {
        public override Task<Guid> ExecuteAsync(CreateSomethingCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintVoidCommandHandler : VoidCommandHandler<UpdateSomethingCommand>
    {
        public override Task ExecuteAsync(UpdateSomethingCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintGenericVoidCommandHandler<T> : VoidCommandHandler<UpdateSomethingGenericCommand<T>>
        where T : IEntity
    {
        public override Task ExecuteAsync(UpdateSomethingGenericCommand<T> command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintGenericWithConstraintVoidCommandHandler<TCommand, T> : VoidCommandHandler<TCommand>
        where TCommand : DoSomethingGenericCommand<T>
        where T : IEntity
    {
        public override Task ExecuteAsync(TCommand command)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintQueryWithDuplicateHandler : QueryHandler<ADuplicateResponseFieldQuery, AResponseWithDuplicateField>
    {
        public override Task<AResponseWithDuplicateField> ExecuteAsync(ADuplicateResponseFieldQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintEmptyQueryHandler : QueryHandler<EmtptyQuery, object>
    {
        public override Task<object> ExecuteAsync(EmtptyQuery query)
        {
            throw new NotImplementedException();
        }
    }

    class AFootprintWithPrimitiveTypeQueryHandler : QueryHandler<WithPrimitiveTypesQuery, object>
    {
        public override Task<object> ExecuteAsync(WithPrimitiveTypesQuery query)
        {
            throw new NotImplementedException();
        }
    }
}
