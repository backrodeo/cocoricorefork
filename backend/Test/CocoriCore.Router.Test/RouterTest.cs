using System;
using Xunit;
using Microsoft.AspNetCore.Http;
using FluentAssertions;
using Microsoft.AspNetCore.Http.Internal;
using System.Threading.Tasks;
using System.Collections.Generic;
using CocoriCore.OData.Converter;
using CocoriCore.OData.Parser;
using Microsoft.AspNetCore.Http.Extensions;
using CocoriCore.TestUtils;

namespace CocoriCore.Router.Test
{
    public class RouterTest
    {
        private Router _router;
        private MessageDeserializer _messageDeserializer;
        private RouterOptionsBuilder _builder;

        public RouterTest()
        {
            var jsonSerializer = new DefaultJsonSerializer(new Formats());
            jsonSerializer.Converters.Add(new ODataFilterConverter(new ODataFilterParser()));
            jsonSerializer.Converters.Add(new ODataOrderByConverter());
            _messageDeserializer = new MessageDeserializer(jsonSerializer);
            _builder = new RouterOptionsBuilder();
        }

        private Router Router
        {
            get
            {
                if (_router == null)
                {
                    _router = new Router(_builder.Options, _messageDeserializer);
                }
                return _router;
            }
        }

        private async Task<object> FindMessageAsync(HttpRequest request)
        {
            var route = Router.TryFindRoute(request);
            if (route != null)
            {
                return await Router.DeserializeMessageAsync<object>(request, route);
            }
            return null;
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrl()
        {
            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .AddPathParameter(m => m.nb);

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();
            int nb = 666;

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/user/" + guid1 + "/edit/" + guid2 + "/" + nb);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(nb);
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrlAndQuery()
        {
            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .UseQuery();

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/user/" + guid1 + "/edit/" + guid2,
                x => x.QueryString = new QueryString("?nb=666&lat=44.825&p=unused"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(666);
            query.lat.Should().Be(44.825);
        }

        [Fact]
        public async Task CreateMessageForRouteWithParametersFromUrlAndBody()
        {
            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id)
                 .UseBody();

            Guid guid2 = Guid.NewGuid();
            Guid guid1 = Guid.NewGuid();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/user/" + guid1 + "/edit/" + guid2,
                x => x.Body = "{\"lat\": 44.825, \"nb\": 666}");

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid1);
            query.id.Should().Be(guid2);
            query.nb.Should().Be(666);
            query.lat.Should().Be(44.825);
        }

        [Fact]
        public async Task KeepPathParameterWhenEmptyQueryString()
        {
            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .UseQuery();

            Guid guid = Guid.NewGuid();

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/user/" + guid);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(guid);
        }

        [Fact]
        public async Task CanSetPathWithString()
        {
            _builder.Get<AnotherCommand>().SetPath("test/path");
            var httpRequest = new HttpRequestFake().Default(x => x.Path = "/test/path");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
        }

        [Fact]
        public async Task CanSetPathWithSimpleStringFunc()
        {
            _builder.Get<AnotherCommand>().SetPath(x => "test/path");
            var httpRequest = new HttpRequestFake().Default(x => x.Path = "/test/path");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
        }

        [Fact]
        public async Task CanSetPathWithInterpolatedString()
        {
            _builder.Get<AnotherCommand>().SetPath(x => $"test/{x.Data}");
            var httpRequest = new HttpRequestFake().Default(x => x.Path = "/test/12");
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnotherCommand>();
            ((AnotherCommand)message).Data.Should().Be("12");
        }

        [Fact]
        public void CannotSetPathWithSomethingElseThanInterpolatedString()
        {
            Action action = () => _builder.Get<AnotherCommand>().SetPath(x => x == null ? "/a" : "/b");
            action.Should().Throw<InvalidOperationException>();

            action = () => _builder.Post<AnotherCommand>().SetPath(x => "test".Substring(1, 2));
            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void AllowSameRoutesWhenMethodIsDifferent()
        {
            _builder.Get<AQuery>()
                 .AddPath("user");

            _builder.Post<ACommand>()
                 .AddPath("user");

            _builder.Put<ACommand>()
                 .AddPath("user");

            _builder.Delete<ACommand>()
                 .AddPath("user");

            Action action = () => Router.CheckDuplicatesRoutes();

            action.Should().NotThrow<Exception>();
        }

        [Fact]
        public void ExceptionIfDuplicateRoutes()
        {
            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.idReferent)
                 .AddPath("edit")
                 .AddPathParameter(m => m.id);

            _builder.Get<AQuery>()
                 .AddPath("user")
                 .AddPathParameter(m => m.id)
                 .AddPath("edit")
                 .AddPathParameter(m => m.idReferent);

            Action action = () => Router.CheckDuplicatesRoutes();

            var exception = action.Should().Throw<DuplicateRoutesException>().Which;
            exception.Message.Should().Contain("edit");
            exception.Message.Should().Contain("user");
        }


        [Theory]
        [InlineData(null)]
        [InlineData("truc")]
        [InlineData("POST")]
        public async Task NoErrorIfMethodIsNotSupported(string method)
        {
            var httpRequest = new HttpRequestFake().Default(x => x.Method = method);
            var message = await FindMessageAsync(httpRequest);
            message.Should().BeNull();
        }

        [Fact]
        public void ThrowExceptionIfCantDeserializeMessage()
        {
            _builder.Post<ACommand>()
                 .UseBody();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Method = "POST",
                x => x.Body = "{\"nb\": NaN}");

            Func<Task> action = async () => await FindMessageAsync(httpRequest);

            action.Should().Throw<DeserializeMessageException>();
        }

        [Fact]
        public async Task CreateMessageForRouteWithEmptyBody()
        {
            _builder.Get<AQuery>()
                 .UseBody();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Body = string.Empty);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.idReferent.Should().Be(Guid.Empty);
            query.id.Should().Be(Guid.Empty);
            query.nb.Should().Be(0);
            query.lat.Should().Be(0);
        }

        [Fact]
        public async Task CreateMessageForRouteWithValueObjectParametersFromUrl()
        {
            _builder.Get<AQuery>()
                 .AddPath("toto")
                 .AddPathParameter(m => m.stringObject);

            string str = "mugron";

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/toto/" + str);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.stringObject.Value.Should().Be(str);
        }

        [Fact]
        public async Task CreateMessageForRouteWithValueObjectBody()
        {
            _builder.Post<ACommand>()
                 .AddPath("toto")
                 .UseBody();

            string str = "mugron";

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/toto",
                         x => x.Method = HttpMethods.Post,
                         x => x.Body = "{\"stringObject\": \"" + str + "\"}");

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ACommand>();

            ACommand command = (ACommand)message;
            command.stringObject.Value.Should().Be(str);
        }

        [Fact]
        public void ThrowExceptionIfMessageExpressionWithdepth()
        {
            Action action = () => _builder.Get<AQuery>()
                 .AddPathParameter(m => m.stringObject.Value);

            var exception = action.Should().Throw<InvalidOperationException>().Which;
            exception.Message.Should().Contain("stringObject.Value");
            exception.Message.Should().Contain("depth");
        }

        [Fact]
        public async Task CanAddRegexForCustomType()
        {
            _builder.SetPatternForType("[a-z]+", typeof(CustomType));

            _builder.Get<AQuery>()
                 .AddPathParameter(m => m.customTypeValue);

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/toto");

            AMessage message = (AMessage)await FindMessageAsync(httpRequest);
            message.customTypeValue.ToString().Should().Be("toto");
        }

        [Fact]
        public async Task CantFindRouteIfRegexDoesntMatchForCustomType()
        {
            _builder.SetPatternForType("[a-z]+", typeof(CustomType));

            _builder.Get<AQuery>()
                 .AddPathParameter(m => m.customTypeValue);

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/toto123");

            (await FindMessageAsync(httpRequest)).Should().BeNull();
        }

        [Fact]
        public void ShowExplicitErrorMessageIfNoParameterlessConstructorForValueObject()
        {
            Action action = () => _builder
                .Get<InvalidQuery>()
                .AddPathParameter(m => m.invalidValueObject);

            var exception = action.Should().Throw<InvalidOperationException>().Which;
            exception.Message.Should().Contain("Can't instanciate type");
            exception.Message.Should().Contain("ValueObjetNoParameterlessConstructor");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringFirstWay()
        {
            _builder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab[]=t&tab[]=o&tab[]=t&tab[]=o"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringSecondWay()
        {
            _builder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab=[\"t\",\"o\",\"t\",\"o\"]"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassArrayIntoQueryStringThirdWay()
        {
            _builder.Get<AQuery>()
                 .AddPath("testArray")
                 .UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/testArray",
                    x => x.QueryString = new QueryString("?tab=['t','o','t','o']"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.tab.Should().HaveCount(4);
            string.Join("", query.tab).Should().Be("toto");
        }

        [Fact]
        public async Task CanPassHeader()
        {
            _builder.Get<AQuery>()
                 .AddPath("testHeader")
                 .AddHeader("test-header", x => x.name);

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/testHeader",
                    x => x.Headers.Add("test-header", "test value"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();

            AQuery query = (AQuery)message;
            query.name.Should().Be("test value");
        }

        [Fact]
        public async Task CanUseCookie()
        {
            _builder.Get<ATokenQuery>()
                 .AddPath("testCookie")
                 .UseCookies();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/testCookie",
                    x => x.Cookies = RequestCookieCollection.Parse(new List<string>
                    {
                        "refreshToken=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9; Secure; HttpOnly;"
                    }));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ATokenQuery>();

            ATokenQuery query = (ATokenQuery)message;
            query.refreshToken.Should().Be("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9");
        }

        [Fact]
        public async Task UrlWithoutAndWithUseQueryAreDifferent()
        {
            _builder.Get<GetAllQuery>().AddPath("toto");
            _builder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            Action action = () => Router.CheckDuplicatesRoutes();
            action.Should().NotThrow<DuplicateRoutesException>();

            var httpRequest1 = new HttpRequestFake().Default(
                    x => x.Path = "/toto");
            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<GetAllQuery>();

            var httpRequest2 = new HttpRequestFake().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString("?keywords=truc"));
            var message2 = await FindMessageAsync(httpRequest2);
            message2.Should().NotBeNull();
            message2.Should().BeOfType<ASearchQuery>();
        }

        [Fact]
        public async Task RouteUsingQueryCanMatchIfNoOtherRouteWhenNoQueryString()
        {
            _builder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            var httpRequest1 = new HttpRequestFake().Default(
                    x => x.Path = "/toto");
            var message1 = await FindMessageAsync(httpRequest1);
            message1.Should().NotBeNull();
            message1.Should().BeOfType<ASearchQuery>();

            var httpRequest2 = new HttpRequestFake().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString("?keywords=truc"));
            var message2 = await FindMessageAsync(httpRequest2);
            message2.Should().NotBeNull();
            message2.Should().BeOfType<ASearchQuery>();
        }

        [Fact]
        public void ThrowExceptionWhenSeveralRoutesMathUrl()
        {
            _builder.Get<ASearchQuery>().AddPath("toto").UseQuery();
            _builder.Get<AQuery>().AddPath("toto").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/toto");

            Func<Task<object>> action = () => FindMessageAsync(httpRequest);

            var exception = action.Should().Throw<DuplicateRoutesException>().Which;
            exception.Message.Should().Contain(httpRequest.GetDisplayUrl())
                .And.Contain("/toto")
                .And.Contain(nameof(ASearchQuery))
                .And.Contain(nameof(AQuery))
                .And.Contain(nameof(Router.CheckDuplicatesRoutes));
        }

        [Theory]
        [InlineData("truc+machin")]
        [InlineData("truc machin")]
        [InlineData("truc%20machin")]
        public async Task ReplacePlusBySpaceForQueryStringParameters(string keywords)
        {
            _builder.Get<ASearchQuery>().AddPath("toto").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                   x => x.Path = "/toto",
                   x => x.QueryString = new QueryString($"?keywords={keywords}"));
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ASearchQuery>().Subject.keywords.Should().Be("truc machin");
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task NoErrorIfEmptyBodyAndUseBody(string body)
        {
            _builder.Post<AnotherCommand>()
                 .AddPath("toto")
                 .UseBody();

            var httpRequest = new HttpRequestFake()
                .Default(x => x.Path = "/toto",
                         x => x.Method = HttpMethods.Post,
                         x => x.Body = body);

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnotherCommand>();

            AnotherCommand command = (AnotherCommand)message;
            command.Data.Should().BeNull();
        }


        [Theory]
        [InlineData("toto")]
        [InlineData("TOTO")]
        [InlineData("tOTo")]
        public async Task RoutesAreNotCaseSensitive(string path)
        {
            _builder.Get<AQuery>().AddPath("toto");

            Action action = () => Router.CheckDuplicatesRoutes();
            action.Should().NotThrow<DuplicateRoutesException>();

            var httpRequest = new HttpRequestFake().Default(
                    x => x.Path = "/" + path);
            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AQuery>();
        }

        [Fact]
        public async Task TestDollarTopAndSkip()
        {
            _builder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString("?$top=5&$skip=10"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            query.Top.Should().Be(5);
            query.Skip.Should().Be(10);
        }

        [Theory]
        [InlineData("?$orderBy=myField", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField asc", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField Asc", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField ASC", OrderDirection.Asc)]
        [InlineData("?$orderBy=myField desc", OrderDirection.Desc)]
        [InlineData("?$orderBy=myField Desc", OrderDirection.Desc)]
        [InlineData("?$orderBy=myField DESC", OrderDirection.Desc)]
        public async Task TestODataOrderBy(string queryString, OrderDirection direction)
        {
            _builder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString(queryString));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            query.OrderBy.Direction.Should().Be(direction);
            query.OrderBy.Member.Should().Be("myField");
        }

        [Theory]
        [InlineData("?$filter=(Prop eq 'mugron')", "mugron")]
        [InlineData("?$filter=Prop eq 'mugron'", "mugron")]
        [InlineData("?$filter=(Prop eq 'Île-d''Aix')", "Île-d'Aix")]
        [InlineData("?$filter=(Prop eq 123)", "123")]
        [InlineData("?$filter=(Prop eq true)", "true")]
        public async Task TestODataFilter(string queryString, string expected)
        {
            _builder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString(queryString));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            var nameFilter = query.Filter.Eq["Prop"];
            nameFilter.Should().ContainSingle().Which.Should().Be(expected);
        }

        [Theory]
        [InlineData("?$filter=(Name eq 'mugron') or (Name eq 'ti ti')")]
        [InlineData("?$filter=(Name eq 'mugron') and (Name eq 'ti ti')")]
        [InlineData("?$filter=(Name eq 'mugron') toto (Name eq 'ti ti')")]
        public async Task TestODataFilterTwoValuesSameField(string queryString)
        {
            _builder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString(queryString));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            var nameFilter = query.Filter.Eq["Name"];
            nameFilter.Should().HaveCount(2).And.BeEquivalentTo("mugron", "ti ti");
        }

        [Fact]
        public async Task TestODataFilterDifferentFields()
        {
            _builder.Get<AnODataQuery>().AddPath("odata").UseQuery();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/odata",
                x => x.QueryString = new QueryString("?$filter=((Code eq 33520) or (Code eq 40250)) and ((Name eq 'mugron') or (Name eq 'bruges'))"));

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<AnODataQuery>();

            AnODataQuery query = (AnODataQuery)message;
            var nameFilter = query.Filter.Eq["Name"];
            nameFilter.Should().HaveCount(2).And.BeEquivalentTo("mugron", "bruges");
            var codeFilter = query.Filter.Eq["Code"];
            codeFilter.Should().HaveCount(2).And.BeEquivalentTo("33520", "40250");
        }

        [Fact]
        public async Task CanDeserializeMessageWithLeadingOrTrailingWhiteSpace()
        {
            _builder.Get<ACommand>().AddPath("test").UseBody();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/test",
                x => x.Body = " {\"name\":\"toto\"} ");

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<ACommand>();

            ACommand command = (ACommand)message;
            command.name.Should().Be("toto");
        }

        [Fact]
        public async Task CanDeserializeArrayMessage()
        {
            _builder.Get<Batch<ACommand>>().AddPath("test").UseBody();

            var httpRequest = new HttpRequestFake().Default(
                x => x.Path = "/test",
                x => x.Body = "[{\"name\":\"toto\"}, {\"name\":\"titi\"}]");

            var message = await FindMessageAsync(httpRequest);
            message.Should().NotBeNull();
            message.Should().BeOfType<Batch<ACommand>>();

            Batch<ACommand> commands = (Batch<ACommand>)message;
            commands[0].name.Should().Be("toto");
            commands[1].name.Should().Be("titi");
        }

        public class AnotherCommand : ICommand
        {
            public string Data;
        }

        public class ACommand : AMessage, ICommand
        {
        }

        public class AQuery : AMessage, IQuery
        {
        }

        public class GetAllQuery : IQuery
        {
        }

        public class ASearchQuery : IQuery
        {
            public string keywords;
        }

        public class ATokenQuery : IQuery
        {
            public string refreshToken;
        }

        public abstract class AMessage : IMessage
        {
            public Guid id;
            public Guid idReferent;
            public string name;
            public int nb;
            public double lat;
            public AStringObject stringObject;
            public CustomType customTypeValue;
            public string[] tab;

            public AMessage()
            {
            }
        }

        public class AStringObject : ValueObject<string>
        {
            protected AStringObject()
            {
            }

            public AStringObject(string value)
                : base(value)
            {
            }
        }

        public class CustomType
        {
            private string _value;
            public CustomType(string value)
            {
                _value = value;
            }

            public override string ToString()
            {
                return _value;
            }

            public static implicit operator CustomType(string value)
            {
                return new CustomType(value);
            }
        }

        public class InvalidQuery : IQuery
        {
            public ValueObjetNoParameterlessConstructor invalidValueObject;
        }

        public class ValueObjetNoParameterlessConstructor : ValueObject<string>
        {
            public ValueObjetNoParameterlessConstructor(string value)
                : base(value)
            {
            }
        }

        public class AnODataQuery : IQuery
        {
            public int Top;
            public int Skip;
            public ODataOrderBy OrderBy;
            public ODataFilter Filter;
        }
    }
}
