using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using FluentValidation;
using Xunit;

namespace CocoriCore.ExtendedValidation.Test
{
    public class HandlerWithValidatorTest
    {
        private ILifetimeScope _scope;
        private ContainerBuilder _builder;
        private UnitOfWorkOptionsBuilder _unitOfWorkBuilder;
        private MessageBusOptionsBuilder _optionBuilder;
        private IMessageBus _messageBus;

        public HandlerWithValidatorTest()
        {
            _builder = new ContainerBuilder();
            _optionBuilder = new MessageBusOptionsBuilder();
            _unitOfWorkBuilder = new UnitOfWorkOptionsBuilder();
            var assembly = this.GetType().Assembly;
            _optionBuilder.ForAll<IMessage>(assembly)
                .CallHandlerValidators(assembly)
                .CallMessageHandlers(assembly);

            _builder.RegisterInstance(_optionBuilder.Options).AsSelf();
            _builder.RegisterType<MessageBus>().As<IMessageBus>();

            _builder.Register<Func<ILifetimeScope>>(c => Scope.BeginLifetimeScope).AsSelf();
            _builder.Register(c => _unitOfWorkBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _builder.RegisterType<AutofacUnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();
        }

        public ILifetimeScope Scope
        {
            get
            {
                if (_scope == null)
                    _scope = _builder.Build();
                return _scope;
            }
        }

        public IMessageBus MessageBus
        {
            get
            {
                if (_messageBus == null)
                    _messageBus = Scope.Resolve<IMessageBus>();
                return _messageBus;
            }
        }

        [Fact]
        public void ValidationExceptionIfHandlerWithInnerValidatorAndInvalidCommand()
        {
            var handler = new AHandlerWithValidator();
            _builder.RegisterInstance(handler);

            Func<Task> action = () => MessageBus.ExecuteAsync(new ACommand() { Name = null });

            action.Should().Throw<ValidationException>();
            handler.Executed.Should().BeFalse();
        }

        [Fact]
        public void ExecuteHandlerWithInnerValidatorIfValidCommand()
        {
            var handler = new AHandlerWithValidator();
            _builder.RegisterInstance(handler);

            Func<Task> action = () => MessageBus.ExecuteAsync(new ACommand() { Name = "toto" });

            action.Should().NotThrow<ValidationException>();
            handler.Executed.Should().BeTrue();
        }

        class AHandlerWithValidator : VoidCommandHandler<ACommand>, IValidate<ACommand>
        {
            public bool Executed = false;

            public void SetupValidator(ExtendedValidator<ACommand> validator)
            {
                validator.RuleFor(x => x.Name).NotEmpty();
            }

            public override Task ExecuteAsync(ACommand command)
            {
                Executed = true;
                return Task.CompletedTask;
            }
        }

        class ACommand : ICommand
        {
            public string Name;
        }
    }
}