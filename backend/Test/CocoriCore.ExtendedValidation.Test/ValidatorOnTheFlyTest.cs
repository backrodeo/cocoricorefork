using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using FluentValidation;
using Xunit;

namespace CocoriCore.ExtendedValidation.Test
{
    public class ValidatorOnTheFlyTest
    {
        private ILifetimeScope _scope;
        public ValidatorOnTheFlyTest()
        {
            var assembly = this.GetType().Assembly;
            var messageBusBuilder = new MessageBusOptionsBuilder();
            messageBusBuilder
                .ForAll<IMessage>(assembly)
                .CallHandlerValidators(assembly)
                .CallMessageHandlers(assembly);
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Register(c => messageBusBuilder.Options).As<MessageBusOptions>().SingleInstance();
            containerBuilder.RegisterType<MessageBus>().As<IMessageBus>().SingleInstance();
            containerBuilder.RegisterAssemblyTypes(assembly)
                .AsClosedTypesOf(typeof(IValidate<>)).AsSelf()
                .SingleInstance();
            containerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            containerBuilder.RegisterType<RepositoryImplementingIExists>().As<IRepository, IExists>().SingleInstance();
            containerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            containerBuilder.Register(c => new UnitOfWorkOptionsBuilder().Options).As<UnitOfWorkOptions>().SingleInstance();
            containerBuilder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().SingleInstance();
            containerBuilder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
            _scope = containerBuilder.Build();
        }

        [Fact]
        public async Task TestHandlerWithValidatorOnTheFly()
        {
            var messageBus = _scope.Resolve<IMessageBus>();
            var repository = _scope.Resolve<IRepository>();
            var handler = _scope.Resolve<AHandler>();
            var entity = new AnEntity();
            await repository.InsertAsync(entity);
            var message = new AGuidMessage();
            message.id = entity.Id;

            Func<Task> action = async () => await messageBus.ExecuteAsync(message);

            action.Should().NotThrow<Exception>();
            handler.ValidatorCalled.Should().BeTrue();
        }

        [Fact]
        public void ValidationExceptionIfIdentifierDoesntMatchExistingEntityOnTheFly()
        {
            var messageBus = _scope.Resolve<IMessageBus>();
            var repository = _scope.Resolve<IRepository>();
            var handler = _scope.Resolve<AHandler>();
            var message = new AGuidMessage();
            message.id = Guid.NewGuid();

            Func<Task> action = async () => await messageBus.ExecuteAsync(message);

            action.Should().Throw<ValidationException>();
            handler.ValidatorCalled.Should().BeTrue();
        }
    }
}