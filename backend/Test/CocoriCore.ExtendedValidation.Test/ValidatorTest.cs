using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Autofac;
using FluentAssertions;
using FluentValidation;
using Xunit;

namespace CocoriCore.ExtendedValidation.Test
{
    public class ValidatorTest
    {
        private IFormats _formats;
        private ILifetimeScope _scope;

        public ValidatorTest()
        {
            _formats = new Formats();
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            containerBuilder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            containerBuilder.RegisterType<RepositoryImplementingIExists>().As<IRepository, IExists>().SingleInstance();
            _scope = containerBuilder.Build();
        }

        [Fact]
        public void ValidateStringMatchingDateTime()
        {
            var message = new AMessage();
            message.datetime = "31-12-1986";
            _formats.Set<DateTime>("dd-MM-yyyy");
            var validator = new AMessageValidator(_formats);
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public void UnvalidateStringMatchingDateTime()
        {
            var message = new AMessage();
            message.datetime = "31-12-1986";
            _formats.Set<DateTime>("MM-dd-yyyy");
            var validator = new AMessageValidator(_formats);
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be("datetime must be a string corresponding to DateTime format.");
            //TODO peut-on avoir le type du message et la profondeurs des champs ex : (AMessage.?)adresseLivraison.codePostal
        }

        [Fact]
        public void ValidateStringMatchingTimeSpan()
        {
            var message = new AMessage();
            message.timespan = "1.02:03:04.0050000";
            var validator = new AMessageValidator(_formats);
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public void UnvalidateStringMatchingTimeSpan()
        {
            var message = new AMessage();
            message.timespan = "truc";
            var validator = new AMessageValidator(_formats);
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be("timespan must be a string corresponding to TimeSpan format.");
        }

        [Fact]
        public void ThrowExceptionIfCultureInfoNotSetIntoValidator()
        {
            Action action = () => new NotProperlyConfiguredValidator(CultureInfo.InvariantCulture);

            action.Should().Throw<ConfigurationException>()
                .Which.Message.Should()
                .Contain($"A validation rule is missing an instance of {nameof(IFormats)}");
        }

        [Fact]
        public void ThrowExceptionIfValidatorDoesntInheritExtendedValidator()
        {
            Action action = () => new NotProperlyInheritedValidator();

            action.Should().Throw<ValidatorInheritanceException>()
            .Which.Message.Should().Contain("Your validator must inherit ExtendedValidator");
        }

        [Fact]
        public async Task ValidateEntityId()
        {
            var entity = new AnEntity();
            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var message = new AGuidMessage();
            message.id = entity.Id;

            var validator = new AGuidMessageValidator(_scope.Resolve<IExists>());
            var result = validator.Validate(message);

            result.Errors.Should().BeEmpty();
        }

        [Fact]
        public async Task UnvalidateEntityId()
        {
            var entity = new AnEntity();
            var repository = _scope.Resolve<IRepository>();
            await repository.InsertAsync(entity);

            var message = new AGuidMessage();
            message.id = Guid.NewGuid();

            var validator = new AGuidMessageValidator(_scope.Resolve<IExists>());
            var result = validator.Validate(message);

            result.Errors.Should().ContainSingle()
                .Which.ErrorMessage.Should().Be($"id with value {message.id} must correspond to an entity of type {typeof(AnEntity)}.");
        }
    }
}