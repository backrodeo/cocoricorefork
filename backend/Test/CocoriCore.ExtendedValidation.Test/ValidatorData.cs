using System;
using System.Globalization;
using System.Threading.Tasks;
using FluentValidation;

namespace CocoriCore.ExtendedValidation
{
    public class AHandler : VoidCommandHandler<AGuidMessage>, IValidate<AGuidMessage>
    {
        private IRepository _repository;
        public bool ValidatorCalled = false;

        public AHandler(IRepository repository)
        {
            _repository = repository;
        }

        public void SetupValidator(ExtendedValidator<AGuidMessage> validator)
        {
            validator.RuleFor(x => x.id).Identifier(typeof(AnEntity));
            ValidatorCalled = true;
        }

        public override Task ExecuteAsync(AGuidMessage message)
        {
            return Task.CompletedTask;
        }
    }

    public class AMessageValidator : ExtendedValidator<AMessage>
    {
        public AMessageValidator(IFormats formats)
        {
            Set(formats);
            RuleFor(x => x.datetime).MatchDateTime().When(x => x.datetime != null);
            RuleFor(x => x.timespan).MatchTimeSpan().When(x => x.timespan != null);
        }
    }

    public class NotProperlyConfiguredValidator : ExtendedValidator<AMessage>
    {

        public NotProperlyConfiguredValidator(CultureInfo cultureInfo)
        {
            RuleFor(x => x.datetime).MatchDateTime().When(x => x.datetime != null);
            RuleFor(x => x.timespan).MatchTimeSpan().When(x => x.timespan != null);
        }
    }

    public class NotProperlyInheritedValidator : AbstractValidator<AMessage>
    {
        public NotProperlyInheritedValidator()
        {
            RuleFor(x => x.datetime).MatchDateTime().When(x => x.datetime != null);
            RuleFor(x => x.timespan).MatchTimeSpan().When(x => x.timespan != null);
        }
    }

    public class AMessage
    {
        public string datetime;
        public string timespan;
    }

    public class AGuidMessageValidator : ExtendedValidator<AGuidMessage>
    {
        public AGuidMessageValidator(IExists exists)
        {
            Set(exists);
            RuleFor(x => x.id).Identifier(typeof(AnEntity));
        }
    }

    public class AnEntity : IEntity
    {
        public Guid Id { get; set; }
    }

    public class AGuidMessage : ICommand
    {
        public Guid id;
    }

    public class RepositoryImplementingIExists : MemoryRepository, IExists
    {
        public RepositoryImplementingIExists(IInMemoryEntityStore entityStore)
            : base(entityStore)
        {
        }
    }
}