namespace CocoriCore.TestUtils
{
    public interface IWithDataSets
    {
        IDataSets DataSets { get; set; }
        IEntity Entity { get; set; }
    }
}
