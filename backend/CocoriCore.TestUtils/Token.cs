﻿namespace CocoriCore.TestUtils
{
    public class Token : ValueObject<string>
    {
        public Token(string value, IClaims payload, string extraSalt = null)
            : base(value)
        {
            Payload = payload;
            ExtraSalt = extraSalt;
        }

        public IClaims Payload { get; }
        public string ExtraSalt { get; }

        public Token<T> CastPayload<T>() where T : IClaims
        {
            return new Token<T>(Value, (T)Payload, ExtraSalt);
        }
    }

    public class Token<T> : Token
        where T : IClaims
    {
        public Token(string value, T payload, string extraSalt = null)
            : base(value, payload, extraSalt)
        {
        }

        public new T Payload => (T)base.Payload;
    }
}
