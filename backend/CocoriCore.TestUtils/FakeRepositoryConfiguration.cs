using System;
using System.Collections.Generic;

namespace CocoriCore.Test
{
    public class FakeRepositoryConfiguration
    {
        public int DelayInMilliseconds { get; set; }
        public Func<CUDOperation, IEntity, bool> ErrorCondition { get; set; }
        public Func<Type, bool> QueryErrorCondition { get; set; }
        public int TotalNbQueryCall { get; set; }
        public int TotalNbUpdateCall { get; set; }
        public int TotalCommitCall { get; set; }
        public List<EntityUID> LoadedUIDs { get; set; }

        public FakeRepositoryConfiguration()
        {
            Reset();
        }

        public FakeRepositoryConfiguration WithDelay(uint delayInMilliseconds)
        {
            DelayInMilliseconds = (int)delayInMilliseconds;
            return this;
        }

        public FakeRepositoryConfiguration ErrorWhen(Func<CUDOperation, IEntity, bool> errorCondition)
        {
            ErrorCondition = errorCondition;
            return this;
        }

        public FakeRepositoryConfiguration ErrorWhenQuery(Func<Type, bool> errorCondition)
        {
            QueryErrorCondition = errorCondition;
            return this;
        }

        public FakeRepositoryConfiguration Reset()
        {
            DelayInMilliseconds = 0;
            ErrorCondition = (o, e) => false;
            QueryErrorCondition = t => false;
            TotalNbQueryCall = 0;
            TotalNbUpdateCall = 0;
            TotalCommitCall = 0;
            LoadedUIDs = new List<EntityUID>();
            return this;
        }
    }
}