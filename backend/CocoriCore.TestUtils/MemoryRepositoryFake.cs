using System;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.Test
{

    public class MemoryRepositoryFake : TransactionalMemoryRepository
    {
        private FakeRepositoryConfiguration _configuration;
        private IUIDProvider _uidProvider;

        public MemoryRepositoryFake(IInMemoryEntityStore entityStore, IUIDProvider uidProvider,
            IStateStore stateStore, FakeRepositoryConfiguration configuration)
            : base(entityStore, stateStore)
        {
            _configuration = configuration;
            _uidProvider = uidProvider;
        }

        public override async Task InsertAsync<TEntity>(TEntity entity)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.CREATE, entity))
            {
                throw new Exception("Fake insert error.");
            }
            else
            {
                await base.InsertAsync(entity);
            }
        }

        private bool HaveToThrowError(CUDOperation operation, IEntity entity)
        {
            return _configuration.ErrorCondition(operation, entity);
        }

        public override async Task UpdateAsync<TEntity>(TEntity entity)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.UPDATE, entity))
            {
                throw new Exception("Fake update error.");
            }
            else
            {
                _configuration.TotalNbUpdateCall++;
                await base.UpdateAsync(entity);
            }
        }

        public override async Task DeleteAsync<TEntity>(TEntity entity)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);

            if (HaveToThrowError(CUDOperation.DELETE, entity))
            {
                throw new Exception("Fake delete error.");
            }
            else
            {
                await base.DeleteAsync(entity);
            }
        }

        public override IQueryable<TEntity> Query<TEntity>()
        {
            if (_configuration.QueryErrorCondition(typeof(TEntity)))
            {
                throw new Exception("Fake query error.");
            }
            else
            {
                _configuration.TotalNbQueryCall++;
                return base.Query<TEntity>();
            }
        }

        public override async Task<TEntity> LoadAsync<TEntity>(Guid id)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);
            var entity = await base.LoadAsync<TEntity>(id);
            _configuration.LoadedUIDs.Add((EntityUID)_uidProvider.GetUID(entity));
            return entity;
        }

        public override async Task<object> LoadAsync(Type type, Guid id)
        {
            await Task.Delay(_configuration.DelayInMilliseconds);
            var entity = await base.LoadAsync(type, id);
            _configuration.LoadedUIDs.Add((EntityUID)_uidProvider.GetUID(entity));
            return entity;
        }

        public Task Commit()
        {
            _configuration.TotalCommitCall++;
            return Task.CompletedTask;
        }
    }
}