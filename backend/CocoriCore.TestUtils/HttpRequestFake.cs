using System;
using System.IO;
using CocoriCore.TestUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace CocoriCore.TestUtils
{
    public class HttpRequestFake : DefaultHttpRequest
    {
        public new string Body
        {
            get
            {
                return base.Body.ReadBodyAsString();
            }
            set
            {
                base.Body = new MemoryStream();
                base.Body.WriteBodyAsString(value);
            }
        }

        public HttpRequestFake()
            : this(new DefaultHttpContext())
        {
        }

        public HttpRequestFake(HttpContext httpContext)
            : base(httpContext)
        {
            base.Body = new MemoryStream();
        }

        public HttpRequestFake Default(params Action<HttpRequestFake>[] actions)
        {
            Scheme = "http";
            Host = new HostString("localhost:8080");
            Path = "/";
            Method = HttpMethods.Get;
            Body = "{}";
            Protocol = "HTTP/1.1";
            actions.ApplyOn(this);
            return this;
        }

        public HttpRequestFake CorsPreflight(params Action<HttpRequestFake>[] actions)
        {
            Scheme = "http";
            Host = new HostString("localhost:8080");
            Path = "/";
            Method = HttpMethods.Options;
            Body = "{}";
            Protocol = "HTTP/1.1";
            Headers["Access-Control-Request-Method"] = HttpMethods.Post;
            Headers["Access-Control-Request-Headers"] = "Content-Type";
            actions.ApplyOn(this);
            return this;
        }
    }
}