using Castle.DynamicProxy;

namespace CocoriCore.TestUtils
{
    public class DataSetInterceptor<TEntity> : IInterceptor
        where TEntity : IEntity
    {
        private IDataSets _dataSets;
        private TEntity _entity;

        public DataSetInterceptor(IDataSets dataSets, TEntity entity)
        {
            _dataSets = dataSets;
            _entity = entity;
        }

        public void Intercept(IInvocation invocation)
        {
            if (invocation.Method.Name == "get_DataSets")
            {
                invocation.ReturnValue = _dataSets;
            }
            else if (invocation.Method.Name == "get_Entity")
            {
                invocation.ReturnValue = _entity;
            }
            else if (invocation.Method.Name == "set_Entity")
            {
                _entity = (TEntity)invocation.Arguments[0];
            }
            else
            {
                invocation.Proceed();
            }
        }
    }
}
