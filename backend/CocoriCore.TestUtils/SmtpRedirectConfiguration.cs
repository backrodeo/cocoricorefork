﻿namespace CocoriCore.TestUtils
{
    public class SmtpRedirectConfiguration : SmtpConfiguration
    {
        public string RedirectTo { get; set; }
    }
}