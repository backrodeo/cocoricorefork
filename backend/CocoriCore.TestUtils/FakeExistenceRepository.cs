﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class FakeExistenceRepository : IRepository, IExists
    {
        private List<Tuple<Type, Guid>> _existingIds;
        private List<Tuple<Type, MemberInfo, object>> _existingUniqueMembers;

        public FakeExistenceRepository()
        {
            _existingIds = new List<Tuple<Type, Guid>>();
            _existingUniqueMembers = new List<Tuple<Type, MemberInfo, object>>();
        }

        public virtual FakeExistenceRepository SetExistingId<TEntity>(Guid id)
        {
            return SetExistingId(id, typeof(TEntity));
        }

        public virtual FakeExistenceRepository SetExistingUniqueMember<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
        {
            _existingUniqueMembers.Add(new Tuple<Type, MemberInfo, object>(typeof(TEntity), uniqueMember.GetMemberInfo(), value));
            return this;
        }

        public virtual FakeExistenceRepository SetExistingId(Guid id, Type entityType)
        {
            _existingIds.Add(new Tuple<Type, Guid>(entityType, id));
            return this;
        }

        public virtual Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return Task.FromResult(_existingIds.Any(x => x.Item1 == entityType && x.Item2 == id));
        }

        public virtual Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            var result = _existingIds
                .Where(x => x.Item1 == entityType)
                .Select(x => x.Item2)
                .Intersect(collection).Count() == collection.Count();
            return Task.FromResult(result);
        }

        public virtual Task DeleteAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> ExistsAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            return ExistsAsync(typeof(TEntity), id);
        }

        public virtual Task InsertAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }

        public virtual Task<TEntity> LoadAsync<TEntity>(Guid id)
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }

        public Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public virtual Task<object> LoadAsync(Type type, Guid id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }

        public virtual IQueryable<TEntity> Query<TEntity>()
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }

        public IQueryable<IEntity> Query(Type type)
        {
            throw new NotImplementedException();
        }

        public virtual Task UpdateAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            throw new NotImplementedException();
        }



        public virtual Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            return Task.FromResult(_existingUniqueMembers
                .Any(x =>
                    Equals(x.Item1, entityType) &&
                    Equals(x.Item2, member) &&
                    Equals(x.Item3, value)));
        }

        public void Dispose()
        {
        }
    }
}
