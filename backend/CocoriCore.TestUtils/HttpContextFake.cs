using System;
using System.IO;
using CocoriCore.TestUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;

namespace CocoriCore.TestUtils
{
    public class HttpContextFake : DefaultHttpContext
    {
        private HttpRequest _request;
        private HttpResponse _response;

        public HttpContextFake()
        {
            _request = new HttpRequestFake(this).Default();
            _response = new HttpResponseFake(this).Default();
        }

        public override HttpResponse Response => _response;
        public override HttpRequest Request => _request;

        public HttpContextFake Default(params Action<HttpContext>[] actions)
        {
            actions.ApplyOn(this);
            return this;
        }
    }
}