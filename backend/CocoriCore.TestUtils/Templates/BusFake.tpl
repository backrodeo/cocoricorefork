{{#usingList}}
using {{{namespace}}};
{{/usingList}}

//This code is generated, do not modify
namespace {{{namespace}}}
{
    public partial class {{{className}}}
    {
        public async Task<object> ProcessAsync(IVoidCommand command, IUser user = null)
        {
            return await ProcessAsync(command, _scope, user);
        }

        public async Task<Guid> ProcessAsync(ICreateCommand command, IUser user = null)
        {
            return (Guid)await ProcessAsync(command, _scope, user);
        }

        {{#queryList}}
        public async Task<{{{returnType}}}> ProcessAsync({{{type}}} query, IUser user = null)
        {
            return ({{{returnType}}})await ProcessAsync(query, _scope);
        }

        {{/queryList}}
    }
}
