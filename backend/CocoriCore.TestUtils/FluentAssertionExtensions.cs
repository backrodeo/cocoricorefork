﻿using FluentAssertions;
using FluentAssertions.Collections;
using FluentAssertions.Primitives;
using System.Text.RegularExpressions;

namespace CocoriCore.TestUtils
{
    public static class FluentAssertionExtensions
    {
        public static AndConstraint<TAssertions> NotContain<TAssertions, T>(this SelfReferencingCollectionAssertions<T, TAssertions> assertion,
            params T[] unexpectedElements)
            where TAssertions : SelfReferencingCollectionAssertions<T, TAssertions>
        {
            return assertion.NotContain(unexpectedElements);
        }

        public static AndConstraint<DateTimeAssertions> BeCloseToNow(this DateTimeAssertions assertion, IClock clock)
        {
            return assertion.BeCloseTo(clock.Now, 10000);
        }

        public static AndConstraint<StringAssertions> NotContainVariableDelimiters(this StringAssertions assertion)
        {
            return assertion.NotContainAny("{{", "}}");
        }

        public static AndConstraint<StringAssertions> UseTemplate(this StringAssertions assertion,
            object templateKey, IResourceProvider resourceProvider)
        {
            var template = resourceProvider.ReadAsTextAsync(templateKey).Result;
            var regex = new Regex("\\{\\{[^\\}]+\\}\\}");
            var segments = regex.Split(template);
            return assertion.ContainAll(segments);
        }

        public static AndConstraint<StringAssertions> BeResource(this StringAssertions assertion,
            object resourceKey, IResourceProvider resourceProvider)
        {
            return assertion.Be(resourceProvider.ReadAsTextAsync(resourceKey).Result);
        }
    }
}