using System;
using System.Threading.Tasks;
using AutoMapper;
using Castle.DynamicProxy;

namespace CocoriCore.TestUtils
{
    public class DataSetsBase : IDataSets, IDisposable
    {
        private static ProxyGenerator _generator = new ProxyGenerator();
        protected IUnitOfWorkFactory _unitOfWorkFactory;
        protected IUnitOfWork _defaultUnitOfWork;

        public DataSetsBase(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IRepository Repository => Resolve<DataSetsDecorator>();

        public IMapper Mapper => Resolve<IMapper>();

        public IFormats Formats => Resolve<IFormats>();

        public IFactory Factory => Resolve<IFactory>();

        public IFileSystem FileSystem => Resolve<IFileSystem>();

        public IClock Clock => Resolve<IClock>();

        public T Get<T>() => Resolve<T>();

        public Task<TEntity> ReloadAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            return Resolve<DataSetsDecorator>().ReloadAsync(entity);
        }

        public async Task<TEntity[]> ReloadAsync<TEntity>(params TEntity[] entities) where TEntity : class, IEntity
        {
            for (var i = 0; i < entities.Length; i++)
            {
                entities[i] = await ReloadAsync(entities[i]);
            }
            return entities;
        }

        public Task<TEntity> ReloadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return Resolve<DataSetsDecorator>().ReloadAsync<TEntity>(id);
        }

        protected T Resolve<T>()
        {
            return DefaultUnitOfWork.Resolve<T>();
        }

        private IUnitOfWork DefaultUnitOfWork
        {
            get
            {
                if (_defaultUnitOfWork == null)
                {
                    _defaultUnitOfWork = _unitOfWorkFactory.NewUnitOfWork();
                }
                return _defaultUnitOfWork;
            }
        }

        public virtual async Task<object> ExecuteAsync(IMessage message)
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                var messageBus = unitOfWork.Resolve<IMessageBus>();
                var response = await messageBus.ExecuteAsync(message);
                await unitOfWork.FinishAsync();
                return response;
            }
        }

        public virtual async Task ExecuteInUnitOfWorkAsync(Func<IUnitOfWork, Task> action)
        {
            using (var unitOfWork = _unitOfWorkFactory.NewUnitOfWork())
            {
                await action(unitOfWork);
                await unitOfWork.FinishAsync();
            }
        }

        public TEntity AddDataSetsProxy<TEntity>(TEntity entity)
        {
            if (entity is IWithDataSets)
                return entity;
            var entityType = entity.GetType();
            var interceptor = typeof(DataSetInterceptor<>).CreateGenericInstance<IInterceptor>(entityType, this, entity);
            var proxy = _generator.CreateClassProxyWithTarget(entityType, new Type[] { typeof(IWithDataSets) }, entity, interceptor);
            return (TEntity)proxy;
        }

        public void Dispose()
        {
        }
    }
}