using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;

namespace CocoriCore.TestUtils
{
    public static class RepositoryAssertionsExtensions
    {
        public static RepositoryAssertions Should(this IRepository repository)
        {
            return new RepositoryAssertions(repository);
        }

        public class RepositoryAssertions
        {
            private IRepository _repository;

            public RepositoryAssertions(IRepository repository)
            {
                _repository = repository;
            }

            public Task ContainAsync<TEntity>(Guid id)
                where TEntity : class, IEntity
            {
                return ContainAsync<TEntity>(new[] { id });
            }

            public Task ContainAsync<TEntity>(params Guid[] ids)
                where TEntity : class, IEntity
            {
                return ContainAsync<TEntity>((IEnumerable<Guid>)ids);
            }

            public async Task ContainAsync<TEntity>(IEnumerable<Guid> ids)
                where TEntity : class, IEntity
            {
                var result = await _repository.ExistsAsync(typeof(TEntity), ids);
                result.Should().BeTrue();
            }

            public Task NotContainAsync<TEntity>(Guid id)
                where TEntity : class, IEntity
            {
                return NotContainAsync<TEntity>(new[] { id });
            }

            public Task NotContainAsync<TEntity>(params Guid[] ids)
                where TEntity : class, IEntity
            {
                return NotContainAsync<TEntity>((IEnumerable<Guid>)ids);
            }

            public async Task NotContainAsync<TEntity>(IEnumerable<Guid> ids)
                where TEntity : class, IEntity
            {
                var result = await _repository.ExistsAsync(typeof(TEntity), ids);
                result.Should().BeFalse();
            }
        }
    }
}