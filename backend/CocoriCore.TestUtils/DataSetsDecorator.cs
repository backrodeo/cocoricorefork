using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class DataSetsDecorator : IRepository, IReloader
    {
        private IRepository _repository;
        private IReloader _reloader;
        private IDataSets _dataSets;
        private Dictionary<IEntity, IEntity> _cache;

        public DataSetsDecorator(IRepository repository, IReloader reloader, IDataSets dataSets)
        {
            _repository = repository;
            _reloader = reloader;
            _dataSets = dataSets;
            _cache = new Dictionary<IEntity, IEntity>();
        }

        public Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            entity = UnproxifyEntity(entity);
            return _repository.DeleteAsync(entity);
        }

        public Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return _repository.ExistsAsync<TEntity>(id);
        }

        public Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            return _repository.InsertAsync(entity);
        }

        public Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            entity = UnproxifyEntity(entity);
            return _repository.UpdateAsync(entity);
        }

        public async Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            var entity = await _repository.LoadAsync<TEntity>(id);
            return GetOrCreateDataSetProxy(entity);
        }

        private TEntity GetOrCreateDataSetProxy<TEntity>(TEntity entity) where TEntity : IEntity
        {
            //TODO TECH si on oublie un membre virtual alors on a des valeur par d�faut dans les proxy ce qui ets source d'erreurs.
            if (_cache.ContainsKey(entity))
            {
                var proxy = (IWithDataSets)_cache[entity];
                proxy.Entity = entity;
                return (TEntity)proxy;
            }
            else
            {
                var entityType = entity.GetType();
                var interceptor = typeof(DataSetInterceptor<>).CreateGenericInstance<IInterceptor>(entityType, _dataSets, entity);
                ProxyGenerator generator = new ProxyGenerator();
                var proxy = (TEntity)generator.CreateClassProxyWithTarget(entityType, new Type[] { typeof(IWithDataSets) }, entity, interceptor);
                _cache[entity] = proxy;
                return proxy;
            }
        }

        public async Task<object> LoadAsync(Type type, Guid id)
        {
            var entity = (IEntity)await _repository.LoadAsync(type, id);
            return GetOrCreateDataSetProxy(entity);
        }

        async Task<TEntity> IRepository.LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
        {
            var entity = await _repository.LoadAsync(uniqueMember, value);
            return GetOrCreateDataSetProxy(entity);
        }

        public async Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids)
        {
            var results = await _repository.LoadAsync(type, ids);
            return results.Select(x => GetOrCreateDataSetProxy(x)).ToArray();
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return new ExtendedEnumerable<TEntity>(_repository.Query<TEntity>(), e => GetOrCreateDataSetProxy(e));
        }

        public IQueryable<IEntity> Query(Type type)
        {
            return new ExtendedEnumerable<IEntity>(_repository.Query(type), e => GetOrCreateDataSetProxy(e));
        }

        public Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return _repository.ExistsAsync(entityType, id);
        }

        public Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> collection)
        {
            return _repository.ExistsAsync(entityType, collection);
        }

        public Task<bool> ExistsAsync(Type entityType, MemberInfo member, object value)
        {
            return _repository.ExistsAsync(entityType, member, value);
        }

        public async Task<TEntity> ReloadAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            entity = UnproxifyEntity(entity);
            entity = await _reloader.ReloadAsync(entity);
            return GetOrCreateDataSetProxy(entity);
        }

        private TEntity UnproxifyEntity<TEntity>(TEntity entity)
        {
            if (entity is IWithDataSets)
            {
                IWithDataSets proxy = (IWithDataSets)entity;
                return (TEntity)proxy.Entity;
            }
            else
            {
                return entity;
            }
        }

        public async Task<TEntity> ReloadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            var entity = await _reloader.ReloadAsync<TEntity>(id);
            return GetOrCreateDataSetProxy(entity);
        }
        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
