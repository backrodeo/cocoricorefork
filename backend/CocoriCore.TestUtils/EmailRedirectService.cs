﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class EmailRedirectService : EmailService
    {
        private SmtpRedirectConfiguration _configuration;

        public EmailRedirectService(SmtpRedirectConfiguration configuration) 
            : base(configuration)
        {
            _configuration = configuration;
        }

        public override Task SendAsync(MailMessage message)
        {
			if(!string.IsNullOrWhiteSpace(_configuration.RedirectTo))
			{
				message.Subject += $" (To : {message.To})";
				message.To.Clear();
				message.To.Add(_configuration.RedirectTo);
			}
            return base.SendAsync(message);
        }
    }
}
