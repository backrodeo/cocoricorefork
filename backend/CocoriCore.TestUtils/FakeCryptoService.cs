﻿using System.Text;

namespace CocoriCore.TestUtils
{
    public class FakeCryptoService : ICryptoService
    {
        public string Decrypt(byte[] encryptedBinaryText)
        {
            if(encryptedBinaryText == null)
            {
                return null;
            }
            return Encoding.UTF8.GetString(encryptedBinaryText);
        }

        public byte[] Encrypt(string text)
        {
            if (text == null)
            {
                return null;
            }
            return Encoding.UTF8.GetBytes(text);
        }

        public void InstallKey(byte[] binaryKey)
        {
        }

        public void InstallKey(string base64Key)
        {
        }

        public string InstallNewKey()
        {
            return "a fake key";
        }

        public void UninstallKey()
        {
        }
    }
}
