﻿using System;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class SettableClock : IClock
    {
        private TimeSpan _delta;
        private bool _timeStopped;
        private DateTime? _fixedNow;

        public SettableClock()
        {
            _delta = new TimeSpan(0);
        }

        public DateTime Now
        {
            get
            {
                return GetNow().Add(_delta);
            }
            set
            {
                _delta = value - GetNow();
            }
        }

        private DateTime GetNow()
        {
            if (_timeStopped)
            {
                return _fixedNow.Value;
            }
            else
            {
                return RoundMilliseconds(DateTime.UtcNow, 3);
            }
        }

        private DateTime RoundMilliseconds(DateTime dateTime, int nbDigit)
        {
            var roundedMilliseconds = (int)Math.Round((double)dateTime.Millisecond, nbDigit);
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, roundedMilliseconds, DateTimeKind.Utc);
        }

        public DateTime Today => Now.Date;

        public async Task<T> ExecuteAtAsync<T>(DateTime dateTime, Func<Task<T>> action)
        {
            _fixedNow = dateTime;
            _timeStopped = true;
            var result = await action();
            _timeStopped = false;
            return result;
        }

        public async Task ExecuteAtAsync(DateTime dateTime, Func<Task> action)
        {
            _fixedNow = dateTime;
            _timeStopped = true;
            await action();
            _timeStopped = false;
        }

        public SettableClock StopTimeAt(DateTime dateTime)
        {
            _fixedNow = dateTime;
            _timeStopped = true;
            return this;
        }

        public SettableClock StopTime()
        {
            _fixedNow = GetNow();
            _timeStopped = true;
            return this;
        }
    }
}
