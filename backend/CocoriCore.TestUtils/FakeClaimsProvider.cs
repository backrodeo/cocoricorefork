using CocoriCore;
using System;
using System.Collections.Generic;

namespace CocoriCore.TestUtils
{
    public class FakeClaimsProvider : IClaimsProvider
    {
        private Dictionary<Guid, IClaims> _userClaims;
        private IFactory _factory;

        public FakeClaimsProvider(IFactory factory)
        {
            _factory = factory;
            _userClaims = new Dictionary<Guid, IClaims>();
        }

        public IClaims Claims
        {
            get
            {
                if (LastUser == null)
                {
                    throw new InvalidOperationException($"No user authenticated, please provide one using {nameof(LastUser)}.");
                }
                if (_userClaims.ContainsKey(LastUser.Id))
                {
                    return _userClaims[LastUser.Id];
                }
                else
                {
                    throw new InvalidOperationException("No claims for this user.");
                }
            }
        }

        public IUser LastUser { get; set; }

        public bool HasClaims => LastUser != null && _userClaims.ContainsKey(LastUser.Id);

        public TClaims GetClaims<TClaims>()
        {
            return (TClaims)Claims;
        }

        public void SetUserClaims(IUser user, IClaims claims)
        {
            _userClaims[user.Id] = claims;
        }
    }
}
