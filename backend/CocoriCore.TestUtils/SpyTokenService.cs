﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class SpyTokenService : TokenService
    {
        private Dictionary<string, Token> _tokens;

        public SpyTokenService(IClock clock, JWTConfiguration configuration,
            JWTSerializer jsonSerializer, IFactory factory)
            : base(clock, configuration, jsonSerializer, factory)
        {
            _tokens = new Dictionary<string, Token>();
        }

        public Token LastToken => _tokens.Last().Value;

        public IEnumerable<Token> Tokens => _tokens.Values;

        public override async Task<string> GenerateTokenAsync<T>(T payload, string extraSalt = null)
        {
            var token = await base.GenerateTokenAsync(payload, extraSalt);
            _tokens[token] = new Token(token, payload, extraSalt);
            return token;
        }

        public Token<T> GetToken<T>(string token) where T : IClaims
        {
            return _tokens[token].CastPayload<T>();
        }

        public DateTime GetExpirationDateTime(string token)
        {
            return _tokens[token].Payload.ExpireAt;
        }
    }
}
