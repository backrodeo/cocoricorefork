﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class SpyEmailService : IEmailService
    {
        public MailMessage LastEmail { get; private set; }

        public Task SendAsync(MailMessage message)
        {
            LastEmail = message;
            return Task.CompletedTask;
        }
    }
}
