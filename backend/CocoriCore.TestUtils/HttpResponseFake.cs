using System;
using System.IO;
using CocoriCore.TestUtils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.Extensions.Primitives;

namespace CocoriCore.TestUtils
{
    public class HttpResponseFake : DefaultHttpResponse
    {
        public new string Body
        {
            get
            {
                return base.Body.ReadBodyAsString();
            }
            set
            {
                // base.Body = new MemoryStream();
                base.Body.WriteBodyAsString(value);
            }
        }

        public HttpResponseFake()
            : this(new DefaultHttpContext())
        {
        }

        public HttpResponseFake(HttpContext httpContext)
            : base(httpContext)
        {
            base.Body = new MemoryStream();
        }

        public HttpResponseFake Default(params Action<HttpResponseFake>[] actions)
        {
            StatusCode = 200;
            Body = "{}";
            ContentLength = Body.Length;
            ContentType = "application/json";
            Headers.Add("ContentType", new StringValues(ContentType));
            Headers.Add("ContentLength", new StringValues(string.Empty + ContentLength));
            Cookies.Append("myCookie", "1234");
            actions.ApplyOn(this);
            return this;
        }
    }
}