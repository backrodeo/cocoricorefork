using System.IO;
using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public static class StreamExtension
    {
        public static string ReadBodyAsString(this Stream body)
        {
            var oldPosition = body.Position;
            body.Position = 0;
            var sr = new StreamReader(body);
            string bodyStr = sr.ReadToEnd();
            body.Position = body.Position;
            return bodyStr;
        }

        public static void WriteBodyAsString(this Stream body, string value)
        {
            var sw = new StreamWriter(body);
            sw.Write(value);
            sw.Flush();
            body.Position = 0;
        }
    }
}