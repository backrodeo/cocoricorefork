using System.Threading.Tasks;

namespace CocoriCore.TestUtils
{
    public class SpyPasswordService : PasswordService
    {
        public SpyPasswordService(IHashService hashService) 
            : base(hashService)
        {
        }

        public string LastPasswordHash { get; private set; }

        public override async Task<string> HashPasswordAsync(string password)
        {
            var hash = await base.HashPasswordAsync(password);
            LastPasswordHash = hash;
            return hash;
        }
    }
}