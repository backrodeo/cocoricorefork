﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Ninject;
using Ninject.Extensions.ContextPreservation;
using Ninject.Extensions.NamedScope;
using Ninject.Modules;

namespace CocoriCore.Ninject.All
{
    public class CocoricoreNinjectRepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUIDProvider>().To<UIDProvider>().InSingletonScope();
            Bind<IInMemoryEntityStore>().To<InMemoryEntityStore>().InSingletonScope();
            Bind<IRepository>().To<MemoryRepository>().InNamedScope("unitofwork");
        }
    }
}
