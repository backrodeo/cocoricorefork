﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Ninject;
using Ninject.Extensions.ContextPreservation;
using Ninject.Extensions.NamedScope;
using Ninject.Modules;

namespace CocoriCore.Ninject.All
{
    public class CocoricoreNinjectApiModule : NinjectModule
    {
        public HttpErrorWriterOptionsBuilder HttpErrorWriterOptionsBuilder = new HttpErrorWriterOptionsBuilder();
        public HttpResponseWriterOptionsBuilder HttpResponseWriterOptionsBuilder = new HttpResponseWriterOptionsBuilder();
        public RouterOptionsBuilder RouterOptionsBuilder = new RouterOptionsBuilder();
        public ErrorBusOptionsBuilder ErrorBusOptionsBuilder = new ErrorBusOptionsBuilder();
        public Type[] JsonConverterTypes;

        public override void Load()
        {
            Bind<IErrorBus>().To<ErrorBus>().InNamedScope("unitofwork");
            Bind<ErrorBusOptions>().ToConstant(ErrorBusOptionsBuilder.Options);

            Bind<IHttpErrorWriter>().To<HttpErrorWriter>().InSingletonScope();
            Bind<HttpErrorWriterOptions>().ToConstant(HttpErrorWriterOptionsBuilder.Options);

            Bind<IHttpResponseWriter>().To<HttpResponseWriter>().InSingletonScope();
            Bind<HttpResponseWriterOptions>().ToConstant(HttpResponseWriterOptionsBuilder.Options);

            Bind<ITracer>().To<ConsoleTraceBus>();

            Bind<MessageDeserializer>().ToSelf();
            Bind<RouterOptions>().ToConstant(RouterOptionsBuilder.Options);
            Bind<IRouter>().To<CocoriCore.Router.Router>().InSingletonScope();

            // Autres services
            var settings = new JsonSerializerSettings();
            Bind<JsonSerializer>().ToMethod(ctx =>
            {
                var serializer = new JsonSerializer();
                foreach (var t in JsonConverterTypes)
                {
                    serializer.Converters.Add((JsonConverter)ctx.GetContextPreservingResolutionRoot().Get(t));
                }
                //serializer.Converters.Add(new StringEnumConverter());
                //serializer.Converters.Add(new IDConverter());
                return serializer;
            }).InSingletonScope();
        }
    }
}
