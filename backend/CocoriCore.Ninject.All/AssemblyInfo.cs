﻿using System.Reflection;

namespace CocoriCore.Ninject.All
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
