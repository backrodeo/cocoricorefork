﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Ninject;
using Ninject.Extensions.ContextPreservation;
using Ninject.Extensions.NamedScope;
using Ninject.Modules;

namespace CocoriCore.Ninject.All
{
    public class CocoricoreNinjectApplicationModule : NinjectModule
    {

        MessageBusOptionsBuilder MessageBusOptionsBuilder = new MessageBusOptionsBuilder();

        public override void Load()
        {
            /*
            Bind<IUnitOfWorkFactory>().ToMethod(ctx => new UnitOfWorkFactory(ctx.GetContextPreservingResolutionRoot()));
            Bind<IUnitOfWork>().To<UnitOfWork>().InNamedScope("unitofwork");
            Bind<IFactory>().ToMethod(ctx => new NinjectFactory(ctx.GetContextPreservingResolutionRoot()));
            */

            Bind<IHashService>().To<HashService>().InSingletonScope();
            Bind<IClock>().To<Clock>().InSingletonScope();

            Bind<IMessageBus>().To<MessageBus>().InNamedScope("unitofwork");
            Bind<MessageBusOptions>().ToConstant(MessageBusOptionsBuilder.Options);
            /*
                        builder.RegisterAssemblyTypesOrGenerics(cocoriCoreApplicationAssembly, commandAssembly)
                           .AssignableTo<ICommandHandler>()
                           .AsSelf();
                        builder.RegisterAssemblyTypesOrGenerics(cocoriCoreApplicationAssembly, fakeMailAssembly, queryAssembly)
                            .AssignableTo<IQueryHandler>()
                            //TODO optimiser en utilisant un repo qui ne conserve pas les états et faire apparaitre IReadRepository ? 
                            .AsSelf();
                        builder.RegisterAssemblyTypes(cocoriCoreApplicationAssembly, commonAssembly, commandAssembly, queryAssembly)
                            .AssignableTo<IValidator>()
                            .AsSelf()
                            .As<IValidator>();
            */
        }
    }
}
