using AutoMapper;

namespace CocoriCore.Application
{
    public class ApplicationProfile : Profile
    {
        public ApplicationProfile()
        {
            CreateMap<CreateFileCommand, File>();
            CreateMap<File, FileResponse>();
        }
    }
}
