﻿namespace CocoriCore.Application
{
    public class CreateFileCommand : ICommand
    {
        public MimeType MimeType;
        public string FileName;
        public string Base64Content;
        public Path FolderPath;
    }
}
