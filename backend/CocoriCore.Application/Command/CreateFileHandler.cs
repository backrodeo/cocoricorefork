﻿using AutoMapper;
using CocoriCore.ExtendedValidation;
using FluentValidation;
using System;
using System.Threading.Tasks;

namespace CocoriCore.Application
{
    public class CreateFileHandler : CreateCommandHandler<CreateFileCommand>, IValidate<CreateFileCommand>
    {
        private IFactory _factory;
        private IMapper _mapper;
        private IRepository _repository;
        private IFileSystem _fileSystem;

        public CreateFileHandler(IFactory factory, IMapper mapper, IRepository repository, IFileSystem fileSystem)
        {
            _factory = factory;
            _mapper = mapper;
            _repository = repository;
            _fileSystem = fileSystem;
        }
        public void SetupValidator(ExtendedValidator<CreateFileCommand> validator)
        {
            validator.RuleFor(x => x.Base64Content).NotEmpty();
            validator.RuleFor(x => x.FileName).NotEmpty();
            validator.RuleFor(x => x.FolderPath).NotEmpty();
            validator.RuleFor(x => x.MimeType).NotEmpty();
        }

        public override async Task<Guid> ExecuteAsync(CreateFileCommand command)
        {
            var file = _factory.Create<File>();
            _mapper.Map(command, file);
            await _repository.InsertAsync(file);
            await _fileSystem.CreateDirectoryAsync(file.GetPath().GetParent());
            while (await _fileSystem.FileExistsAsync(file.GetPath()))//TODO tester l'incrément de nom de fichier
            {
                file.FileName = file.FileName.Increment();
            }
            await _fileSystem.CreateBinaryFileAsync(file.GetPath(), command.Base64Content);
            return file.Id;
        }


    }
}
