﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Autofac;
using FluentValidation;

namespace CocoriCore.Autofac
{
    public class CocoricoreAutofacModule : Module
    {
        UnitOfWorkOptionsBuilder UnitOfWorkOptionsBuilder = new UnitOfWorkOptionsBuilder();

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => UnitOfWorkOptionsBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();
        }
    }
}
