using System.Threading.Tasks;
using Autofac;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public class HttpContextScopeMiddleware : IMiddleware
    {
        private ILifetimeScope _rootScope;

        public HttpContextScopeMiddleware(ILifetimeScope rootScope)
        {
            _rootScope = rootScope;
        }

        public virtual async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
        {
            using (var unitOfWork = _rootScope.BeginLifetimeScope().Resolve<IUnitOfWork>())
            {
                httpContext.SetUnitOfWork(unitOfWork);
                await next(httpContext);
                await unitOfWork.FinishAsync();
            }
        }
    }
}