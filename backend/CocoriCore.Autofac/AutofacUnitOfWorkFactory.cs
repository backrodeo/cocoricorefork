﻿using Autofac;
using System;

namespace CocoriCore
{
    public class AutofacUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private ILifetimeScope _rootScope;

        public AutofacUnitOfWorkFactory(ILifetimeScope rootScope)
        {
            if (rootScope.Tag as string != "root")
                throw new ArgumentException("The injected scope must be root scope.");
            _rootScope = rootScope;
        }

        public IUnitOfWork NewUnitOfWork()
        {
            return _rootScope.BeginLifetimeScope().Resolve<IUnitOfWork>();
        }
    }
}
