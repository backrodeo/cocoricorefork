using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Autofac.Builder;
using Autofac.Core;

namespace CocoriCore
{
    public static class AutofacExtensions
    {
        public static AutofacAssemblyFluent RegisterAssemblyTypesOrGenerics(
            this ContainerBuilder builder, params System.Reflection.Assembly[] assemblies)
        {
            var types = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsConcrete());
            return new AutofacAssemblyFluent(builder, types);
        }
    }

    public class AutofacAssemblyFluent
    {
        private IEnumerable<Type> _types;
        private ContainerBuilder _builder;
        private List<IRegistrationBuilder<object, ReflectionActivatorData, object>> _registrations;

        public AutofacAssemblyFluent(ContainerBuilder buidler, IEnumerable<Type> types)
        {
            _builder = buidler;
            _types = types;
            _registrations = new List<IRegistrationBuilder<object, ReflectionActivatorData, object>>();
        }

        public AutofacAssemblyFluent AssignableTo<T>()
        {
            _types = _types.Where(x => x.IsAssignableTo<T>());
            return this;
        }

        public AutofacAssemblyFluent AsSelf()
        {
            foreach (var type in _types)
            {
                if (type.IsGenericType)
                    _registrations.Add(_builder.RegisterGeneric(type).AsSelf());
                else
                    _registrations.Add(_builder.RegisterType(type).AsSelf());
            }
            return this;
        }

        public AutofacAssemblyFluent WithParameter(Parameter parameter)
        {
            foreach (var registration in _registrations)
            {
                registration.WithParameter(parameter);
            }
            return this;
        }
    }
}