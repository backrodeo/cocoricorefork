using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace CocoriCore
{
    public static class MiddlewareExtension 
    {
        public static void UseMiddleware<TMilddleware>(this IApplicationBuilder builder, ILifetimeScope scope) 
            where TMilddleware : IMiddleware
        {
            IMiddleware middleware = scope.Resolve<TMilddleware>();
            builder.Use(next => 
            {
                return context => middleware.InvokeAsync(context, next);
            });
        }
    }
}