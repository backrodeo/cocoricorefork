﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Autofac;

namespace CocoriCore.Autofac.All
{
    public class CocoriCoreAutofacApiModule : Module
    {
        public Action<HttpErrorWriterOptionsBuilder> HttpErrorWriterConfiguration = x => { };
        public Action<HttpResponseWriterOptionsBuilder> HttpResponseWriterConfiguration = x => { };
        public Action<RouterOptionsBuilder> RouterConfiguration = x => { };
        public Action<ErrorBusOptionsBuilder> ErrorBusConfiguration = x => { };
        public Type[] JsonConverterTypes;

        protected override void Load(ContainerBuilder builder)
        {
            var errorBusOptionsBuilder = new ErrorBusOptionsBuilder();
            ErrorBusConfiguration(errorBusOptionsBuilder);
            builder.Register(c => errorBusOptionsBuilder.Options).As<ErrorBusOptions>().SingleInstance();
            builder.RegisterType<ErrorBus>().As<IErrorBus>().InstancePerLifetimeScope();
            //builder.RegisterAssemblyTypes(apiAssembly).AssignableTo<IErrorHandler>().AsSelf();
            builder.RegisterType<ConsoleLogger>().AsSelf().SingleInstance();

            var httpErrorWriterOptionsBuilder = new HttpErrorWriterOptionsBuilder();
            HttpErrorWriterConfiguration(httpErrorWriterOptionsBuilder);
            builder.Register(c => httpErrorWriterOptionsBuilder.Options).As<HttpErrorWriterOptions>().SingleInstance();
            builder.RegisterType<HttpErrorWriter>().As<IHttpErrorWriter>().InstancePerLifetimeScope();
            //builder.RegisterAssemblyTypes(cocoriCoreAssembly, apiAssembly).AssignableTo<IHttpErrorWriterHandler>().AsSelf();

            var httpResponseWriterOptionsBuilder = new HttpResponseWriterOptionsBuilder();
            HttpResponseWriterConfiguration(httpResponseWriterOptionsBuilder);
            builder.Register(c => httpResponseWriterOptionsBuilder.Options).As<HttpResponseWriterOptions>().SingleInstance();
            builder.RegisterType<HttpResponseWriter>().As<IHttpResponseWriter>().InstancePerLifetimeScope();
            //TODO TECH vérifier que tous les les bus sont en instanceperlifetimescope
            //builder.RegisterAssemblyTypes(cocoriCoreAssembly, cocoriCoreODataAssembly, apiAssembly).AssignableTo<IHttpReponseWriterHandler>().AsSelf();

            builder.RegisterType<ConsoleTraceBus>().As<ITracer>();

            var routerOptionsBuilder = new RouterOptionsBuilder();
            RouterConfiguration(routerOptionsBuilder);
            builder.Register(c => routerOptionsBuilder.Options).As<RouterOptions>().SingleInstance();
            builder.RegisterType<CocoriCore.Router.Router>().As<IRouter>().SingleInstance();
            builder.RegisterType<MessageDeserializer>().AsSelf().SingleInstance();

            // Autres services
            /*
            var settings = new JsonSerializerSettings();
            Bind<JsonSerializer>().ToMethod(ctx =>
            {
                var serializer = new JsonSerializer();
                foreach (var t in JsonConverterTypes)
                {
                    serializer.Converters.Add((JsonConverter)ctx.GetContextPreservingResolutionRoot().Get(t));
                }
                //serializer.Converters.Add(new StringEnumConverter());
                //serializer.Converters.Add(new IDConverter());
                return serializer;
            }).InSingletonScope();
            */
        }
    }
}
