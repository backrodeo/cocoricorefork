using Autofac;
using Autofac.Features.ResolveAnything;

namespace CocoriCore.Page.Autofac
{

    public class CocoricorePageAutofacModule : Module
    {
        private readonly PageMapperOptions pageMapperOptions;
        private readonly EmailMapperOptions emailMapperOptions;

        public CocoricorePageAutofacModule(
            PageMapperOptions pageMapperOptions,
            EmailMapperOptions emailMapperOptions)
        {
            this.pageMapperOptions = pageMapperOptions;
            this.emailMapperOptions = emailMapperOptions;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PageMapper>().As<IPageMapper>().SingleInstance();
            builder.RegisterInstance(pageMapperOptions);
            builder.RegisterType<EmailMapper>().As<IEmailMapper>().SingleInstance();
            builder.RegisterInstance(emailMapperOptions);
            builder.RegisterType<GenericMessageConverter>().AsSelf();
            builder.RegisterType<PageQueryConverter>().AsSelf();
            builder.RegisterType<RouteToUrl>().AsSelf().SingleInstance();

            builder.RegisterType<MessageBus>().AsSelf();
            builder.RegisterType<PageMessageBus<MessageBus>>().As<IMessageBus>().InstancePerLifetimeScope();

            builder.RegisterType<UserFluentFactory>().AsSelf().SingleInstance();
            builder.RegisterSource(new AnyConcreteTypeNotAlreadyRegisteredSource());
            /*
            builder.RegisterType<UserFluent>().AsSelf();
            builder.RegisterType<UserFluent>().AsSelf();
            builder.RegisterSource<UserFluent>().AsSelf();
            */
        }
    }
}
