using Autofac;

namespace CocoriCore.Page.Autofac
{

    public enum CocoriCorePageEmailAutofacModuleMode
    {
        ForTest,
        ForLocalDevWithGenericRenderer,
        ForLocalDevWithTemplateRenderer,
        RealEmails
    }

    public class CocoriCorePageEmailAutofacModule : Module
    {


        public CocoriCorePageEmailAutofacModuleMode Mode;

        protected override void Load(ContainerBuilder builder)
        {
            /*
            Dans la commande, on peut écrire : this.emailSender.SendAsync(new TypedMailMessage() {...})
            Le message est transformé en un autre TypedMailMessage par le PageMapper, avec des IPageQuery
            Le message transformé est rendu en en html par un Renderer
            Le message est envoyé par IEmailService par smtp

            On veut pouvoir espionner l'envoi de mail tranformés pour les tests avec les pages
            On veut pouvoir espionner l'envoi de mail après rendu pour faire un faux serveur mail pendant le dev en local
            (Dans un premier temps on avec rendu généric, puis créer des templates)
            On veut vraiment envoyer le mail par smtp
             */

            /* 
                Implems de IEmailSender:
                - pour les tests : 
                    juste un spy après transformation par le PageMapper, pour continuer une cinématique en 
                    suivant les liens représentés par de IPageQuery

                - pendant le prototypage :
                    un spy sur la version transformée après rendu générique (Subject = type du body, <a href> pour les IPageQuery)
                    faire un point d'api qui montre les emails envoyés pour venir cliquer sur les liens.

                - une fois que le front met en page les emails
                    utilise un rendu d'email par template
                    il faut lancer une exception si un template n'est pas trouvé
                    Les tests seleniums sont sensés toujours fonctionner avec la fausse interface webmail
                    
                - verification du rendu front avec un vrai lecteur d'email :
                    Cas d'utilisation ? Est-ce que c'est juste pour des tests à la main ? 
                    Si on réutilise les scenrios de test, attention aux adresses emails utilisées ?
                    Ce qui pose aussi la question de : 
                        Est-ce que les scenarios de test peuvent-être utilisés sur un site en prod pour vérifier que le site fonctionne ?
                        Peut-être seulement sur un site de recette ?
                        Peut-être pas du tout, et seulement sur une version en local, 
                          mais avec le front web non-générique : Angular / React, 
                          et des vraies adresses email, et un vrai webmail (plus simple qu'un outlook)
            */

            builder.RegisterType<GenericEmailRenderer>().AsSelf();
            builder.RegisterType<TemplateEmailRenderer>().AsSelf();

            if (Mode == CocoriCorePageEmailAutofacModuleMode.ForTest)
                builder.RegisterType<PageEmailSenderAndReader>().As<IEmailSender, IEmailReader>().SingleInstance();

            if (Mode == CocoriCorePageEmailAutofacModuleMode.ForLocalDevWithGenericRenderer)
                builder.RegisterType<PageEmailSenderAndReaderWithRenderer<GenericEmailRenderer>>().As<IEmailSender, IMailMessageReader>().SingleInstance();

            if (Mode == CocoriCorePageEmailAutofacModuleMode.ForLocalDevWithTemplateRenderer)
                builder.RegisterType<PageEmailSenderAndReaderWithRenderer<TemplateEmailRenderer>>().As<IEmailSender, IMailMessageReader>().SingleInstance();

            if (Mode == CocoriCorePageEmailAutofacModuleMode.RealEmails)
            {
                builder.RegisterType<EmailService>().As<IEmailService>();
                builder.RegisterType<SmtpPageEmailSenderWithRenderer<TemplateEmailRenderer>>().As<IEmailSender>().SingleInstance();
            }
        }
    }
}
