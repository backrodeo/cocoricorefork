﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Autofac;
using FluentValidation;

namespace CocoriCore.Autofac.All
{
    public class CocoriCoreAutofacApplicationModule : Module
    {

        public Action<MessageBusOptionsBuilder> MessageBusConfiguration = x => { };
        public Action<UnitOfWorkOptionsBuilder> UnitOfWorkConfiguration = x => { };

        protected override void Load(ContainerBuilder builder)
        {
            var unitOfWorkOptionsBuilder = new UnitOfWorkOptionsBuilder();
            UnitOfWorkConfiguration(unitOfWorkOptionsBuilder);
            builder.Register(c => unitOfWorkOptionsBuilder.Options).As<UnitOfWorkOptions>().SingleInstance();
            builder.RegisterType<AutofacUnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<AutofacUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();

            builder.RegisterType<HashService>().As<IHashService>().SingleInstance();
            builder.RegisterType<Clock>().As<IClock>().SingleInstance();

            var messageBusOptionsBuilder = new MessageBusOptionsBuilder();
            MessageBusConfiguration(messageBusOptionsBuilder);
            builder.Register(c => messageBusOptionsBuilder.Options).As<MessageBusOptions>().SingleInstance();
            builder.RegisterType<MessageBus>().As<IMessageBus>().InstancePerLifetimeScope();
            /*
            builder.RegisterAssemblyTypesOrGenerics(cocoriCoreApplicationAssembly, commandAssembly)
               .AssignableTo<ICommandHandler>()
               .AsSelf();
            builder.RegisterAssemblyTypesOrGenerics(cocoriCoreApplicationAssembly, fakeMailAssembly, queryAssembly)
                .AssignableTo<IQueryHandler>()
                //TODO optimiser en utilisant un repo qui ne conserve pas les états et faire apparaitre IReadRepository ? 
                .AsSelf();
            builder.RegisterAssemblyTypes(cocoriCoreApplicationAssembly, commonAssembly, commandAssembly, queryAssembly)
                .AssignableTo<IValidator>()
                .AsSelf()
                .As<IValidator>();
            */
        }
    }
}
