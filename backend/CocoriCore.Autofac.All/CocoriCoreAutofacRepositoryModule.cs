﻿using System;
using System.Threading.Tasks;
using CocoriCore.Router;
using Newtonsoft.Json;
using Autofac;

namespace CocoriCore.Autofac.All
{
    public class CocoriCoreAutofacRepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();

            builder.RegisterType<UIDProvider>().As<IUIDProvider>().SingleInstance();
            builder.RegisterType<InMemoryEntityStore>().As<IInMemoryEntityStore>().SingleInstance();
            builder.RegisterType<MemoryRepository>().As<IRepository>().InstancePerLifetimeScope();
        }

        /*
        private void RegisterInMemoryRepository(this ContainerBuilder builder, IInMemoryEntityStore persistentStore = null)
        {
            if (persistentStore == null)
            {
                persistentStore = new InMemoryEntityStore(new UIDProvider());
            }
            builder.RegisterInstance(persistentStore)
                .As<IPersistentStore>()
                .As<IInMemoryEntityStore>();
            builder.RegisterType<MyMyMyRepository>()
                .AsImplementedInterfaces()
                .WithParameter(new TypedParameter<IRepository>(c => c.Resolve<TransactionalMemoryRepository>()))
                .InstancePerLifetimeScope();
            builder.RegisterType<RepositoryBusContextStore>().AsSelf().InstancePerLifetimeScope();//TODO simplifier, hériter d'une classe contenant ce comportement
            builder.RegisterType<TransactionalMemoryRepository>()
                .AsSelf()
                .As<IReloader>()
                .As<ITransactionHolder>()
                .InstancePerLifetimeScope();
        }
        */
    }
}
