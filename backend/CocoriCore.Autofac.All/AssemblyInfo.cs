﻿using System.Reflection;

namespace CocoriCore.Autofac.All
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
