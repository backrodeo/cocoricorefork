using System.Reflection;
using Autofac;
using AutoMapper;

namespace CocoriCore.Autofac.All
{
    public class CocoriCoreAutoMapperModule : global::Autofac.Module
    {
        private readonly Assembly[] assemblies;

        public CocoriCoreAutoMapperModule(params Assembly[] assemblies)
        {
            this.assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                // Add all profiles in current assembly
                cfg.AddMaps(this.assemblies);
            });
            builder.Register(c => configuration).As<MapperConfiguration>().SingleInstance();
            builder.Register(c => c.Resolve<MapperConfiguration>().CreateMapper(c.Resolve<IComponentContext>().Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();
        }
    }
}
