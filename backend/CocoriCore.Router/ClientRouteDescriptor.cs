namespace CocoriCore.Router
{
    public class ClientRouteDescriptor
    {
        public string RoutePath;
        public string ParameterizedUrl;
        public ClientType MessageType;
        public bool MessageTypeIsArray;
        public ClientType ResponseType;
        public bool ResponseTypeIsArray;
        public ClientType[] InnerResponseTypes;
    }
}