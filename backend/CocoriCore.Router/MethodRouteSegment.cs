using System.Net.Http;

namespace CocoriCore.Router
{
    public class MethodRouteSegment : IRouteSegment
    {
        public HttpMethod Segment { get; }
        public string Value { get; }

        public MethodRouteSegment(string value)
        {
            Segment = new HttpMethod(value);
            Value = value;
        }

        public MethodRouteSegment(HttpMethod method)
        {
            Segment = method;
            Value = method.Method;
        }

        public bool Match(string value)
        {
            return Segment == new HttpMethod(value);
        }

        public override string ToString()
        {
            return Value;
        }

        public override int GetHashCode()
        {
            return Segment.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj is MethodRouteSegment methodSegment && methodSegment.Value == Value;
        }
    }
}