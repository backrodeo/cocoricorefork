namespace CocoriCore.Router
{
    public class EnumValue
    {
        public string Name;
        public int Value;

        public EnumValue()
        {
        }

        public EnumValue(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public override bool Equals(object obj)
        {
            return obj is EnumValue @enum &&
            @enum.Name == Name &&
            @enum.Value == Value;
        }

        public override int GetHashCode()
        {
            return 5 ^
                Name.GetHashCode() ^
                Value.GetHashCode();
        }
    }
}