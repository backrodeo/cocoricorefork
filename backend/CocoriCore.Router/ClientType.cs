namespace CocoriCore.Router
{
    public class ClientType
    {
        public string Name;
        public string Method;
        public string Url;
        public Import[] Imports;
        public Field[] Fields;
        public string[] QueryStringFields;
        public string[] BodyFields;
        public EnumValue[] Values;
        public bool IsEnum;
        public ClientType[] InnerTypes;

        public ClientType()
        {
        }
    }
}