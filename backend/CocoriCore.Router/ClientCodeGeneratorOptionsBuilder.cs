using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public class ClientCodeGeneratorOptionsBuilder
    {
        public ClientCodeGeneratorOptions Options { get; }

        public ClientCodeGeneratorOptionsBuilder()
        {
            Options = new ClientCodeGeneratorOptions();
        }

        public ClientCodeGeneratorOptionsBuilder TransformFieldNames(Func<string, string> transformation)
        {
            Options.TransformFieldName = transformation;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder TransformFieldTypes(Func<string, string> transformation)
        {
            Options.TransformFieldType = transformation;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder AddTypeCorrespondence(Type type, string clientType)
        {
            Options.TypeCorrespondences.Add(type, clientType);
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder AddConditionalImport(string clientType, string from)
        {
            Options.Imports.Add(clientType, from);
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder ExcludeRouteWhen(Func<RouteDescriptor, bool> condition)
        {
            Options.ExcludeRoute = condition;
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder SetFinalTypes(IEnumerable<Type> finalTypes)
        {
            Options.FinalTypes = finalTypes.ToList();
            return this;
        }

        public ClientCodeGeneratorOptionsBuilder RenderPathParameter(Func<string, string> render)
        {
            Options.RenderPathParameter = render;
            return this;
        }
    }
}