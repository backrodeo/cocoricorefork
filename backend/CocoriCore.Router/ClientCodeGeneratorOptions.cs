using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Router
{
    public class ClientCodeGeneratorOptions
    {
        public static Type[] FINAL_TYPES = new[]
        {
            typeof(object),
            typeof(bool),
            typeof(byte),
            typeof(sbyte),
            typeof(char),
            typeof(decimal),
            typeof(double),
            typeof(float),
            typeof(int),
            typeof(uint),
            typeof(long),
            typeof(ulong),
            typeof(short),
            typeof(ushort),
            typeof(string),
            typeof(Guid),
            typeof(DateTime),
            typeof(TimeSpan),
        };

        public Dictionary<Type, string> TypeCorrespondences { get; }
        public Dictionary<string, string> Imports { get; }
        public string Extension { get; set; }
        public Func<string, string> TransformFieldName { get; set; }
        public Func<string, string> TransformFieldType { get; set; }
        public Func<RouteDescriptor, bool> ExcludeRoute { get; set; }
        public List<Type> FinalTypes { get; set; }
        public Func<string, string> RenderPathParameter { get; set; }

        public ClientCodeGeneratorOptions()
        {
            Extension = ".ts";
            TypeCorrespondences = new Dictionary<Type, string>();
            Imports = new Dictionary<string, string>();
            TransformFieldName = f => f;
            TransformFieldType = t => t;
            ExcludeRoute = d => false;
            FinalTypes = FINAL_TYPES.ToList();
            RenderPathParameter = n => n;
        }
    }
}