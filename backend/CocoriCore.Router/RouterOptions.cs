using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Router
{
    public class RouterOptions
    {
        private List<Route> _routes;

        public RouterOptions()
        {
            //TODO a tester
            ParametersPattern = new Dictionary<Type, string>()
            {
                { typeof(Guid),      "[0-9a-f]{8}-([0-9a-f]{4}-){3}[0-9a-f]{12}"},
                { typeof(string),    "[^/\\?]+"},
                { typeof(int),       "\\d+"},
            };
            _routes = new List<Route>();
        }

        public Dictionary<Type, string> ParametersPattern { get; }

        public RouterOptions AddRoute(Route route)
        {
            _routes.Add(route);
            return this;
        }

        public IEnumerable<Route> AllRoutes => _routes;
    }
}