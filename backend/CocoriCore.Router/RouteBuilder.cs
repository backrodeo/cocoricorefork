using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace CocoriCore.Router
{
    public class RouteBuilder<TMessage>
    {
        private Route _route;

        public RouteBuilder(Route route)
        {
            _route = route;
        }

        public RouteBuilder<TMessage> Get()
        {
            _route.SetMethod(HttpMethod.Get).SetMessageType(typeof(TMessage));
            return this;
        }

        public RouteBuilder<TMessage> Post()
        {
            _route.SetMethod(HttpMethod.Post).SetMessageType(typeof(TMessage));
            return this;
        }

        public RouteBuilder<TMessage> Put()
        {
            _route.SetMethod(HttpMethod.Put).SetMessageType(typeof(TMessage));
            return this;
        }

        public RouteBuilder<TMessage> Delete()
        {
            _route.SetMethod(HttpMethod.Delete).SetMessageType(typeof(TMessage));
            return this;
        }

        public RouteBuilder<TMessage> AddPathParameter(Expression<Func<TMessage, object>> expression)
        {
            var memberType = expression.GetMemberType();
            if (memberType.IsAssignableTo<IValueObject>())
            {
                AddParameterPatternForValueObject(memberType);
            }
            _route.AddPathParameter<TMessage>(expression);
            return this;
        }

        //TODO il ne faut pas modifier la configuration quand on créé les routes et la rendre immuable
        //configurer les patterns des objets valeurs à l'avance
        private void AddParameterPatternForValueObject(Type valueObjectType)
        {
            try
            {
                var valueObject = (IValueObject)Activator.CreateInstance(valueObjectType, true);
                _route.SetParameterPattern(valueObjectType, valueObject.ValidationRegex);
            }
            catch (MissingMethodException e)
            {
                throw new InvalidOperationException($"Can't instanciate type {valueObjectType}, see inner exception.", e);
            }
        }

        public RouteBuilder<TMessage> AddPath(string path)
        {
            _route.AddPath(path);
            return this;
        }

        public RouteBuilder<TMessage> SetPath(string path)
        {
            var pathArray = path.Split("/");
            foreach (var p in pathArray)
                _route.AddPath(p);
            return this;
        }

        public RouteBuilder<TMessage> SetPath(Expression<Func<TMessage, string>> expression)
        {
            if (expression.Body is ConstantExpression)
                return SetPath((ConstantExpression)expression.Body, new Expression[0]);

            if (!expression.Body.IsMethodCall(typeof(String), "Format"))
                throw new InvalidOperationException("SetPath can only be used with interpolated string : SetPath(x => $\"users/{x.Id})\" or SetPath(\"users\")");

            var methodCall = (MethodCallExpression)expression.Body;
            var pathExpression = (ConstantExpression)methodCall.Arguments[0];
            var pathParameters = methodCall.Arguments.Skip(1).ToArray();
            return SetPath(pathExpression, pathParameters);
        }

        private RouteBuilder<TMessage> SetPath(ConstantExpression pathExpression, Expression[] pathParameters)
        {
            var pathArray = ((string)pathExpression.Value).Split("/");
            foreach (var path in pathArray)
            {
                if (!path.StartsWith("{"))
                    _route.AddPath(path);
                else
                {
                    var index = int.Parse(path.Substring(1, path.Length - 2));
                    _route.AddPathParameter(pathParameters[index].GetMemberInfo());
                }
            }
            return this;
        }

        public RouteBuilder<TMessage> AddHeader(string headerName, Expression<Func<TMessage, object>> expression)
        {
            _route.AddHeader<TMessage>(headerName, expression);
            return this;
        }

        public RouteBuilder<TMessage> UseQuery()
        {
            _route.UseQuery();
            return this;
        }

        public RouteBuilder<TMessage> UseBody()
        {
            _route.UseBody();
            return this;
        }

        public RouteBuilder<TMessage> UseCookies()
        {
            _route.UseCookies();
            return this;
        }

        public RouteBuilder<TMessage> SetDescritpion(string description)
        {
            _route.SetDescription(description);
            return this;
        }

        public Route Configuration => _route;
    }
}