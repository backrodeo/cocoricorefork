using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Router
{
    public class ResponseTypeProvider
    {
        private List<Type> _handlerTypes;
        private Dictionary<Type, Type> _messageAndResponseTypes;

        public ResponseTypeProvider(params Assembly[] assemblies)
        {
            _handlerTypes = assemblies
                .SelectMany(a => a.GetTypes())
                .Where(t => t.IsAssignableToGeneric(typeof(IHandler<>)) && t.IsConcrete())
                .ToList();
            _messageAndResponseTypes = _handlerTypes
                .Select(x => GetMessageAndResponseTypes(x))
                .ToDictionary(x => x.messageType, x => x.responseType);
        }

        public Type GetResponseType(Type messageType)
        {
            if (messageType.IsGenericType)
            {
                messageType = messageType.GetGenericTypeDefinition();
            }
            if (!_messageAndResponseTypes.ContainsKey(messageType))
            {
                throw new InvalidOperationException($"Could not find corresponding handler for {messageType}, among provided assemblies.");
            }
            return _messageAndResponseTypes[messageType];
        }

        private (Type messageType, Type responseType) GetMessageAndResponseTypes(Type handlerType)
        {
            if (handlerType.IsAssignableToGeneric(typeof(IHandler<,>)))
            {
                var genericArguments = handlerType.GetGenericArguments(typeof(IHandler<,>));
                var messageType = GetMessageType(genericArguments[0]);
                var responseType = genericArguments[1];
                return (messageType, responseType);
            }
            else
            {
                var genericArguments = handlerType.GetGenericArguments(typeof(IHandler<>));
                var messageType = GetMessageType(genericArguments[0]);
                return (messageType, null);
            }
        }

        private Type GetMessageType(Type messageType)
        {
            if (messageType.IsGenericParameter)
            {
                messageType = messageType.BaseType;
            }
            if (messageType.IsGenericType)
            {
                messageType = messageType.GetGenericTypeDefinition();
            }
            return messageType;
        }
    }
}