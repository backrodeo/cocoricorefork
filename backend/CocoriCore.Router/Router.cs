using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace CocoriCore.Router
{
    public class Router : IRouter
    {
        private RouterOptions _options;
        private MessageDeserializer _messageDeserializer;
        private IEnumerable<MethodRoutes> _routesSets;

        public Router(RouterOptions options, MessageDeserializer messageDeserializer)
        {
            _options = options;
            _messageDeserializer = messageDeserializer;
            _routesSets = _options
                .AllRoutes
                .GroupBy(x => x.Method)
                .Select(x => new MethodRoutes(x.Key).AddRoutes(x));
        }

        public IRoute TryFindRoute(HttpRequest request)
        {
            var routes = FindRoutes(request);
            if (routes.Count() > 1)
            {
                Route route;
                if (request.QueryString == QueryString.Empty &&
                    routes.HasSingle(x => !x.HasQueryParameters, out route))
                {
                    return route;
                }
                else if (request.QueryString != QueryString.Empty &&
                    routes.HasSingle(x => x.HasQueryParameters, out route))
                {
                    return route;
                }
                else
                {
                    var errorMessage = $"Several routes match url {request.GetDisplayUrl()} : \n"
                        + $"{string.Join("\n", routes.Select(x => "\t" + x.ToString()))}\n"
                        + "Please review your configuration to avoid duplicate routes.\n"
                        + $"Also method router.{nameof(CheckDuplicatesRoutes)}() can help you detect duplicate routes "
                        + "when called at application startup or in automated test.";
                    throw new DuplicateRoutesException(errorMessage);
                }
            }
            return routes.FirstOrDefault();
        }

        public IEnumerable<Route> FindRoutes(HttpRequest request)
        {
            return _routesSets
                .Where(x => x.Segment.Match(request.Method))
                .SelectMany(x => x.FindRoutes(request));
        }

        public IEnumerable<Route> AllRoutes => _options.AllRoutes;

        public Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request, IRoute route)
        {
            return _messageDeserializer.DeserializeMessageAsync<TMessage>(request, (Route)route);
        }

        public void CheckDuplicatesRoutes()
        {
            Dictionary<int, Route> routeDictionary = new Dictionary<int, Route>();
            foreach (var route in AllRoutes)
            {
                var key = route.HashCode;
                if (routeDictionary.ContainsKey(key))
                {
                    throw new DuplicateRoutesException("At least two routes produce same pattern :\n"
                        + "\troute 1 : " + route + "\n"
                        + "\troute 2 : " + routeDictionary[key]);
                }
                else
                {
                    routeDictionary.Add(key, route);
                }
            }
        }
    }
}