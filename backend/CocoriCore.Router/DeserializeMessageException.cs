using System;

namespace CocoriCore.Router
{
    public class DeserializeMessageException : Exception
    {
        public DeserializeMessageException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}