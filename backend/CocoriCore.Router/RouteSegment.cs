namespace CocoriCore.Router
{
    public class UrlSegment
    {
        public string Value;
        public bool IsParameter;

        public UrlSegment(string value, bool isParameter)
        {
            Value = value;
            IsParameter = isParameter;
        }
    }
}