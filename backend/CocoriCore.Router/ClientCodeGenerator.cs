using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Router
{
    public class ClientCodeGenerator
    {
        private ClientCodeGeneratorOptions _options;
        private ResponseTypeProvider _responseTypeProvider;

        public ClientCodeGenerator(ClientCodeGeneratorOptions options, ResponseTypeProvider responseTypeprovider)
        {
            _options = options;
            _responseTypeProvider = responseTypeprovider;
        }

        public virtual IEnumerable<ClientRouteDescriptor> GenerateClientDescriptors(IEnumerable<RouteDescriptor> routeDescriptors)
        {
            var clientRouteDescriptors = new List<ClientRouteDescriptor>();
            foreach (var descriptor in routeDescriptors)
            {
                var routeDescriptor = CreateCompleteRouteDescriptor(descriptor);
                if (_options.ExcludeRoute(routeDescriptor))
                {
                    continue;
                }
                var clientDescriptor = ConstructClientRouteDescriptor(routeDescriptor);
                clientRouteDescriptors.Add(clientDescriptor);
            }
            return clientRouteDescriptors;
        }

        protected virtual RouteDescriptor CreateCompleteRouteDescriptor(RouteDescriptor descriptor)
        {
            var routeDescriptor = new RouteDescriptor();
            routeDescriptor.UrlSegments = descriptor.UrlSegments.ToArray();
            routeDescriptor.ParameterizedUrl = descriptor.ParameterizedUrl;
            routeDescriptor.UrlParameters = descriptor.UrlParameters;
            routeDescriptor.Method = descriptor.Method;
            routeDescriptor.UseBody = descriptor.UseBody;
            routeDescriptor.UseQuery = descriptor.UseQuery;

            var messageType = UnboxAndGetIsArray(descriptor.MessageType);
            routeDescriptor.MessageType = messageType.type;
            routeDescriptor.MessageTypeIsArray = messageType.isArray;
            routeDescriptor.InnerMessageTypes = ExtractSubTypesRecursively(routeDescriptor.MessageType).ToArray();

            var responseType = UnboxAndGetIsArray(_responseTypeProvider.GetResponseType(descriptor.MessageType));
            routeDescriptor.ResponseType = responseType.type;
            routeDescriptor.ResponseTypeIsArray = responseType.isArray;
            routeDescriptor.InnerResponseTypes = ExtractSubTypesRecursively(routeDescriptor.ResponseType).ToArray();
            return routeDescriptor;
        }

        protected virtual (Type type, bool isArray) UnboxAndGetIsArray(Type type)
        {
            var unboxedType = UnboxType(type);
            if (_options.FinalTypes.Contains(unboxedType))
            {
                unboxedType = null;
            }
            var isArray = type.IsAssignableTo<IEnumerable>() && type != typeof(string);
            return (unboxedType, isArray);
        }

        protected virtual IEnumerable<Type> ExtractSubTypesRecursively(Type type)
        {
            HashSet<Type> extractedTypes = new HashSet<Type>();
            ExtractTypesRecursively(type, extractedTypes);
            extractedTypes.Remove(type);
            return extractedTypes;
        }

        protected virtual void ExtractTypesRecursively(Type type, HashSet<Type> extractedTypes)
        {
            var serverType = UnboxType(type);
            if (serverType == null || _options.FinalTypes.Contains(serverType))
            {
                return;
            }
            else if (extractedTypes.Add(serverType))
            {
                var subTypes = serverType
                    .GetPropertiesAndFields()
                    .Select(t => t.GetMemberType());
                foreach (var subType in subTypes)
                {
                    ExtractTypesRecursively(subType, extractedTypes);
                }
            }
        }

        protected virtual Type UnboxType(Type type)
        {
            if (type == null)
            {
                return null;
            }
            if (type.IsAssignableTo<IEnumerable>() && type.IsGenericType)
            {
                return type.GetGenericArguments()[0];
            }
            if (type.IsArray)
            {
                return type.GetElementType();
            }
            if (type.IsNullable(out var nonNullType))
            {
                return nonNullType;
            }
            else
            {
                return type;
            }
        }

        protected virtual ClientRouteDescriptor ConstructClientRouteDescriptor(RouteDescriptor descriptor)
        {
            var clientDescriptor = new ClientRouteDescriptor();
            clientDescriptor.RoutePath = ConstructRoutePath(descriptor.UrlSegments);
            clientDescriptor.ParameterizedUrl = descriptor.ParameterizedUrl;
            clientDescriptor.MessageType = ConstructClientType(descriptor.MessageType, descriptor.InnerMessageTypes);
            clientDescriptor.MessageType.Method = descriptor.Method.ToString();
            clientDescriptor.MessageType.Url = ConstructUrl(descriptor.UrlSegments);
            if (descriptor.UseQuery)
            {
                clientDescriptor.MessageType.QueryStringFields = ConstructFieldsExceptParameters(descriptor.MessageType, descriptor.UrlParameters.Keys);
            }
            if (descriptor.UseBody)
            {
                clientDescriptor.MessageType.BodyFields = ConstructFieldsExceptParameters(descriptor.MessageType, descriptor.UrlParameters.Keys);
            }
            clientDescriptor.MessageTypeIsArray = descriptor.MessageTypeIsArray;
            clientDescriptor.ResponseType = ConstructClientType(descriptor.ResponseType, descriptor.InnerResponseTypes);
            clientDescriptor.ResponseTypeIsArray = descriptor.ResponseTypeIsArray;
            return clientDescriptor;
        }

        protected virtual Path ConstructRoutePath(UrlSegment[] segments)
        {
            return new Path(segments.Where(x => !x.IsParameter).Select(s => s.Value));
        }

        protected virtual string ConstructUrl(UrlSegment[] segments)
        {
            var textSegments = segments.Select(s => ConstructParameterSegment(s));
            return "/" + string.Join("/", textSegments);
        }

        protected virtual string ConstructParameterSegment(UrlSegment segment)
        {
            if (segment.IsParameter)
            {
                var clientFieldName = GetClientFieldName(segment.Value);
                return _options.RenderPathParameter(clientFieldName);
            }
            else
            {
                return segment.Value;
            }
        }

        protected virtual string[] ConstructFieldsExceptParameters(Type type, IEnumerable<string> parameterNames)
        {
            return type
                .GetPropertiesAndFields()
                .Select(x => x.Name)
                .Except(parameterNames)
                .Select(x => GetClientFieldName(x))
                .ToArray();
        }

        protected virtual ClientType ConstructClientType(Type type, IEnumerable<Type> innerTypes)
        {
            if (type == null)
            {
                return null;
            }
            var clientType = new ClientType();
            clientType.IsEnum = type.IsEnum;
            clientType.Name = GetTypeName(type);//TODO pouvoir redefinir un nom de type (ex ArchiveEntityCommand<Toto> devient ArchiveTotoCommand)
            clientType.Fields = ConstructFields(type);
            clientType.Values = ConstructValues(type);
            clientType.InnerTypes = innerTypes
                .Select(x => ConstructClientType(x, new Type[0]))
                .Where(x => x != null)
                .ToArray();
            var allFields = clientType
                .InnerTypes
                .SelectMany(t => t.Fields)
                .Concat(clientType.Fields);
            clientType.Imports = ConstructImports(allFields);
            return clientType;
        }

        protected virtual string GetTypeName(Type type)
        {
            var typeName = type.Name;
            if (type.IsGenericType)
            {
                typeName = type.Name.Substring(0, type.Name.IndexOf("`"));
                var genericParamaters = type
                    .GetGenericArguments()
                    .Select(x => GetClientFieldType(x))
                    .ToArray();
                typeName += $"<{string.Join(", ", genericParamaters)}>";
            }
            return typeName;
        }

        protected virtual Field[] ConstructFields(Type type)
        {
            if (type.IsEnum)
            {
                return new Field[0];
            }
            return type
                .GetPropertiesAndFields()
                .Select(x => ConstructField(x))
                .ToArray();
        }

        protected virtual Field ConstructField(MemberInfo memberInfo)
        {
            var memberType = memberInfo.GetMemberType();
            var isNullable = memberType.IsNullable();
            var isArray = memberType.IsAssignableTo<IEnumerable>() && memberType != typeof(string);
            return new Field
            {
                Name = GetClientFieldName(memberInfo.Name),
                Type = GetClientFieldType(memberType),
                IsNullable = isNullable,
                IsArray = isArray
            };
        }

        protected virtual string GetClientFieldName(string memberName)
        {
            return _options.TransformFieldName(memberName);
        }

        protected virtual string GetClientFieldType(Type memberType)
        {
            var unboxedType = UnboxType(memberType);
            var clientType = _options.TypeCorrespondences.TryGetValue(unboxedType, unboxedType.Name);
            return _options.TransformFieldType(clientType);
        }

        protected virtual EnumValue[] ConstructValues(Type type)
        {
            if (!type.IsEnum)
            {
                return new EnumValue[0];
            }
            return Enum.GetValues(type)
                .Cast<object>()
                .Select(x => new EnumValue(Enum.GetName(type, x), (int)x))
                .ToArray();
        }

        protected virtual Import[] ConstructImports(IEnumerable<Field> fields)
        {
            var imports = new HashSet<Import>();
            foreach (var field in fields)
            {
                if (_options.Imports.ContainsKey(field.Type))
                {
                    imports.Add(new Import(field.Type, _options.Imports[field.Type]));
                }
            }
            return imports.ToArray();
        }
    }
}