using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json.Linq;

namespace CocoriCore.Router
{
    public class MessageDeserializer
    {
        private Newtonsoft.Json.JsonSerializer _jsonSerializer;

        public MessageDeserializer(Newtonsoft.Json.JsonSerializer jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }

        public async Task<object> DeserializeMessageAsync(HttpRequest request, Route route)
        {
            try
            {
                JContainer jsonContainer = await InitJsonObjectAsync(request, route);
                if (jsonContainer is JObject jsonObject)
                {
                    AddValuesFromQuery(jsonObject, request, route);
                    AddValuesFromHeader(jsonObject, request, route);
                    AddValuesFromCookies(jsonObject, request, route);
                    AddValuesFromUrl(jsonObject, request, route);
                }
                return jsonContainer.ToObject(route.MessageType, _jsonSerializer);
            }
            catch (Exception exception)
            {
                throw new DeserializeMessageException("Cant' deserialize message.", exception);
            }
        }

        public async Task<TMessage> DeserializeMessageAsync<TMessage>(HttpRequest request, Route route)
        {
            var message = await DeserializeMessageAsync(request, route);
            return (TMessage)message;
        }

        private void AddValuesFromHeader(JObject jsonObject, HttpRequest request, Route route)
        {
            foreach (var header in route.Headers)
            {
                StringValues headerValue;
                if (request.Headers.TryGetValue(header.Key, out headerValue))
                {
                    AddJsonValue(jsonObject, header.Value, headerValue);
                }
            }
        }

        private void AddValuesFromCookies(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasCookieParameters)
            {
                foreach (var cookie in request.Cookies)
                {
                    string value = cookie.Value;
                    if (value.StartsWith('[') && value.EndsWith(']'))
                    {
                        AddJsonArrayFromValue(jsonObject, cookie.Key, value);
                    }
                    else
                    {
                        AddJsonValue(jsonObject, cookie.Key, value);
                    }
                }
            }
        }

        private async Task<JContainer> InitJsonObjectAsync(HttpRequest request, Route route)
        {
            if (route.HasBodyParameters)
            {
                string bodyText = await request.Body.ReadBodyAsStringAsync();
                if (!string.IsNullOrWhiteSpace(bodyText))
                {
                    //TODO ajouter des tests pour v�rifier que l'on utilise bien la conf du s�rialiseur pour parser l'url et le body etc...
                    return _jsonSerializer.Parse<JContainer>(bodyText.Trim());
                }
            }
            return new JObject();
        }

        private void AddValuesFromQuery(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasQueryParameters)
            {
                foreach (string key in request.Query.Keys)
                {
                    string value = request.Query[key];
                    var normalizedKey = key.StartsWith("$") ? key.Substring(1) : key;
                    if (key.EndsWith("[]"))
                    {
                        AddJsonArrayFromKey(jsonObject, normalizedKey, value);
                    }
                    else if (value.StartsWith('[') && value.EndsWith(']'))
                    {
                        AddJsonArrayFromValue(jsonObject, normalizedKey, value);
                    }
                    else
                    {
                        AddJsonValue(jsonObject, normalizedKey, value);
                    }
                }
            }
        }

        private void AddJsonArrayFromKey(JObject jsonObject, string key, string value)
        {
            var jarray = new JArray();
            var array = value.Split(",");
            foreach (var element in array)
            {
                var jtoken = _jsonSerializer.Parse<JToken>(element);
                jarray.Add(jtoken);
            }
            jsonObject[key.Substring(0, key.Length - 2)] = jarray;
        }

        private void AddJsonArrayFromValue(JObject jsonObject, string key, string value)
        {
            var jarray = _jsonSerializer.Parse<JArray>(value);
            jsonObject[key] = jarray;
        }

        private void AddJsonValue(JObject jsonObject, string key, string value)
        {
            var jtoken = _jsonSerializer.Parse<JToken>(value);
            jsonObject[key] = jtoken;
        }

        private void AddValuesFromUrl(JObject jsonObject, HttpRequest request, Route route)
        {
            if (route.HasPathParameters)
            {
                Match match = route.GetMatch(request);
                foreach (Group group in match.Groups)
                {
                    if (route.HasUrlParameter(group.Name))
                    {
                        var jtoken = _jsonSerializer.Parse<JToken>(group.Value);
                        jsonObject[group.Name] = jtoken;
                    }
                }
            }
        }
    }
}