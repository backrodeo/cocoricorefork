﻿using CocoriCore.FluentValidation;
using FluentValidation;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public class ExtendedValidatorExecutor
    {
        public IExtendedValidator CreateAndSetupValidator(object handler, Type messageType)
        {
            var validator = InstantiateValidator(handler, messageType);
            validator = RegisterHandlerDependencies(validator, handler);
            validator = SetupValidator(validator, handler);
            return validator;
        }

        private IExtendedValidator InstantiateValidator(object handler, Type messageType)
        {
            var validatorType = typeof(ExtendedValidator<>).MakeGenericType(messageType);
            return (IExtendedValidator)Activator.CreateInstance(validatorType);
        }

        private IExtendedValidator RegisterHandlerDependencies(IExtendedValidator validator, object handler)
        {
            var fieldsInfo = handler.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (var fieldInfo in fieldsInfo)
            {
                var value = fieldInfo.GetValue(handler);
                if (value != null)
                    validator.Set(value, fieldInfo.FieldType);
            }
            return validator;
        }

        private IExtendedValidator SetupValidator(IExtendedValidator validator, object handler)
        {
            var handlerType = handler.GetType();
            var setupValidatorMethod = handlerType.GetMethod(nameof(IValidate<dynamic>.SetupValidator));
            setupValidatorMethod.Invoke(handler, new object[] { validator });
            return validator;
        }

        public Task ValidateMessageAsync(IExtendedValidator validator, object message)
        {
            return validator.ValidateAndThrowAsync(message);
        }
    }
}
