namespace CocoriCore.ExtendedValidation
{
    public interface IRuleBuilderData
    {
        T Get<T>();
    }
}
