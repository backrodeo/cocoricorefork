using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.ExtendedValidation
{
    public class RuleBuilderCollectionDecorator<T, TProperty> : IRuleBuilderInitialCollection<T, TProperty>, IRuleBuilderOptions<T, TProperty>, IRuleBuilderData
    {
        private IRuleBuilderInitialCollection<T, TProperty> _rule;
        public Dictionary<Type, object> _data;

        public RuleBuilderCollectionDecorator(IRuleBuilderInitialCollection<T, TProperty> rule, Dictionary<Type, object> data)
        {
            _rule = rule;
            _data = data;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IPropertyValidator validator)
        {
            _rule.SetValidator(validator);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IValidator<TProperty> validator)
        {
            _rule.SetValidator(validator);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TValidator> validatorProvider) where TValidator : IValidator<TProperty>
        {
            _rule.SetValidator(validatorProvider);
            return this;
        }

        IRuleBuilderOptions<T, TProperty> IConfigurable<PropertyRule, IRuleBuilderOptions<T, TProperty>>.Configure(Action<PropertyRule> configurator)
        {
            _rule.Configure(configurator);
            return this;
        }

        public TData Get<TData>()
        {
            return _data.GetService<TData>();
            //var dataType = typeof(TData);
            //var registeredDataType = _data.Keys.FirstOrDefault(x => dataType.IsAssignableFrom(x));
            //if (registeredDataType == null)
            //    throw new ConfigurationException($"Missing data : A validation rule is missing an instance of {dataType.Name} which is required to execute validation.\n" +
            //        $"If you validator is integrated into an command/query handler ensure that the service implementing {dataType.Name} " + 
            //        $"is stored into a protected or public field/property within you handler.\n It then will be registred automatically." +
            //        "\n\n" +
            //        $"You can provide an instance of {dataType.Name} manually (for exemple if you defined a specific validator class) " +
            //        $"by calling method {nameof(IExtendedValidator.Set)} to set this instance from you validator constructor class.\n"); 
            //return (TData)_data[registeredDataType];
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TValidator> validatorProvider, params string[] ruleSets) 
            where TValidator : IValidator<TProperty>
        {
            _rule.SetValidator(validatorProvider, ruleSets);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IValidator<TProperty> validator, params string[] ruleSets)
        {
            _rule.SetValidator(validator, ruleSets);
            return this;
        }

        public IRuleBuilderInitialCollection<T, TProperty> Configure(Action<CollectionPropertyRule<TProperty>> configurator)
        {
            _rule.Configure(configurator);
            return this;
        }
    }
}
