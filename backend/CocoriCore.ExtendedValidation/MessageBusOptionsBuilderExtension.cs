﻿using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.ExtendedValidation
{
    public static class MessageBusOptionsBuilderExtension
    {
        public static IEnumerable<MessageBusRule> CallHandlerValidators(this IEnumerable<MessageBusRule> rules,
            params System.Reflection.Assembly[] assemblies)
        {
            if (assemblies.Count() == 0)
            {
                throw new ConfigurationException("You must pass at least one assembly.");
            }
            var validators = assemblies.IndexTypesByGenericParameter(typeof(IValidate<>));
            var executor = new ExtendedValidatorExecutor();
            foreach (var rule in rules)
            {
                var messageType = rule.TargetType;
                if (validators.ContainsKey(messageType))
                {
                    rule.AddHandler(validators[messageType], async (h, c, u) =>
                    {
                        var validator = executor.CreateAndSetupValidator(h, messageType);
                        await executor.ValidateMessageAsync(validator, c.Message);
                    });
                }
            }
            return rules;
        }
    }
}