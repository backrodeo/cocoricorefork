﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public static class FluentValidationExtension
    {
        public static IRuleBuilderOptions<T, dynamic> MatchTimeSpan<T>(this IRuleBuilder<T, dynamic> ruleBuilder)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var format = ruleWithData.Get<IFormats>().Get<TimeSpan>();
                return ruleBuilder
                    .Must(x =>
                    {
                        TimeSpan timeSpan;
                        return x is string && TimeSpan.TryParseExact(x, format, CultureInfo.InvariantCulture, out timeSpan);
                    })
                    .WithMessage("{PropertyName} must be a string corresponding to TimeSpan format.")
                    .WithErrorCode("NOT_TIME_FORMAT");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, dynamic> MatchDateTime<T>(this IRuleBuilder<T, dynamic> ruleBuilder)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var format = ruleWithData.Get<IFormats>().Get<DateTime>();
                return ruleBuilder
                    .Must(x =>
                    {
                        DateTime dateTime;
                        return x is string && DateTime.TryParseExact(x, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime);//TODO DateTimeStyles.None à creuser
                    })
                    .WithMessage("{PropertyName} must be a string corresponding to DateTime format.")
                    .WithErrorCode("NOT_DATETIME_FORMAT");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, dynamic> Identifier<T>(this IRuleBuilder<T, dynamic> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync(async (x, c) => x is Guid id && await EntityWithIdExists(id, entityType, exists))
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
            }
            else
                throw new ValidatorInheritanceException();
        }


        //TODO test si guid empty
        private static async Task<bool> EntityWithIdExists(Guid id, Type entityType, IExists exists)
        {
            return id != Guid.Empty && await exists.ExistsAsync(entityType, id);
        }

        public static IRuleBuilderOptions<T, Guid> Identifier<T>(this IRuleBuilder<T, Guid> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync((id, c) => EntityWithIdExists(id, entityType, exists))
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, Guid?> NotUsedIdentifier<T>(this IRuleBuilder<T, Guid?> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync(async (id, c) =>
                    {
                        if (id.HasValue)
                        {
                            if (await EntityWithIdExists(id.Value, entityType, exists))
                            {
                                return false;
                            }
                        }
                        return true;
                    })
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} is already used for an entity of type {entityType}.")
                    .WithErrorCode("ALREADY_USED_ID");
            }
            else
                throw new ValidatorInheritanceException();
        }

        //TODO à tester
        public static IRuleBuilderOptions<T, Guid?> Identifier<T>(this IRuleBuilder<T, Guid?> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync((id, c) =>
                    {
                        if (id.HasValue)
                            return EntityWithIdExists(id.Value, entityType, exists);
                        else
                            return Task.FromResult(true);
                    })
                    .WithMessage($"{{PropertyName}} with value {{PropertyValue}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, IEnumerable<Guid>> Identifier<T>(this IRuleBuilder<T, IEnumerable<Guid>> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                ruleBuilder.NotNull();
                return ruleBuilder
                    .MustAsync((c, t) =>
                    {
                        if (c != null && c.Count() > 0)
                            return exists.ExistsAsync(entityType, c);
                        else
                            return Task.FromResult(true);
                    })
                    .WithMessage($"Each value in collection {{PropertyName}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_ID");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, object> Identify<T>(this IRuleBuilder<T, object> ruleBuilder, Type entityType)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync((o, value, property, c) => CheckExistingUniqueValueAsync(value, entityType, property.PropertyName, exists))
                    .WithMessage($"Value {{PropertyValue}} must correspond to property {{PropertyName}} for an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_UNIQUE_VALUE");
            }
            else
                throw new ValidatorInheritanceException();

        }

        public static IRuleBuilderOptions<T, object> Identify<T>(this IRuleBuilder<T, IEnumerable<object>> ruleBuilder, Type entityType, string memberName)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync((value, c) => CheckExistingUniqueValueAsync(value, entityType, memberName, exists))
                    .WithMessage($"Each value {memberName} in collection {{PropertyName}} must correspond to an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_UNIQUE_VALUE");
            }
            else
                throw new ValidatorInheritanceException();
        }

        public static IRuleBuilderOptions<T, object> Identify<T>(this IRuleBuilder<T, object> ruleBuilder, Type entityType, string memberName)
        {
            if (ruleBuilder is IRuleBuilderData ruleWithData)
            {
                var exists = ruleWithData.Get<IExists>();
                return ruleBuilder
                    .MustAsync((value, c) => CheckExistingUniqueValueAsync(value, entityType, memberName, exists))
                    .WithMessage($"Value {{PropertyValue}} must correspond to property {{PropertyName}} for an entity of type {entityType}.")
                    .WithErrorCode("NOT_EXISTING_UNIQUE_VALUE");
            }
            else
                throw new ValidatorInheritanceException();
        }

        private static async Task<bool> CheckExistingUniqueValueAsync(object value, Type entityType, string memberName, IExists exists)
        {
            if (value == null)
            {
                return false;
            }
            var memberInfo = entityType.GetMember(memberName).SingleOrDefault();
            return await exists.ExistsAsync(entityType, memberInfo, value);
        }
    }
}
