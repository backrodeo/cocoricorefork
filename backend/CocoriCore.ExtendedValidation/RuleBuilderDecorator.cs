using FluentValidation;
using FluentValidation.Internal;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.ExtendedValidation
{
    public class RuleBuilderDecorator<T, TProperty> : IRuleBuilderInitial<T, TProperty>, IRuleBuilderOptions<T, TProperty>, IRuleBuilderData
    {
        private IRuleBuilderInitial<T, TProperty> _rule;
        public Dictionary<Type, object> _data;

        public RuleBuilderDecorator(IRuleBuilderInitial<T, TProperty> rule, Dictionary<Type, object> data)
        {
            _rule = rule;
            _data = data;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IPropertyValidator validator)
        {
            _rule.SetValidator(validator);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IValidator<TProperty> validator)
        {
            _rule.SetValidator(validator);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TValidator> validatorProvider) where TValidator : IValidator<TProperty>
        {
            _rule.SetValidator(validatorProvider);
            return this;
        }

        public IRuleBuilderInitial<T, TProperty> Configure(Action<PropertyRule> configurator)
        {
            _rule.Configure(configurator);
            return this;
        }

        IRuleBuilderOptions<T, TProperty> IConfigurable<PropertyRule, IRuleBuilderOptions<T, TProperty>>.Configure(Action<PropertyRule> configurator)
        {
            _rule.Configure(configurator);
            return this;
        }

        public TData Get<TData>()
        {
            return _data.GetService<TData>();
            //var dataType = typeof(TData);
            //var registeredDataType = _data.Keys.FirstOrDefault(x => dataType.IsAssignableFrom(x));
            //if(registeredDataType == null)
            //    throw new ConfigurationException($"Missing data : use `Set<an instance implementing {dataType.Name}>(data)` "
            //        + "in your validator and ensure data is not null.");
            //return (TData)_data[registeredDataType];
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator<TValidator>(Func<T, TValidator> validatorProvider, params string[] ruleSets) 
            where TValidator : IValidator<TProperty>
        {
            _rule.SetValidator(validatorProvider, ruleSets);
            return this;
        }

        public IRuleBuilderOptions<T, TProperty> SetValidator(IValidator<TProperty> validator, params string[] ruleSets)
        {
            _rule.SetValidator(validator, ruleSets);
            return this;
        }
    }
}
