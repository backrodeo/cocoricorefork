﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.ExtendedValidation
{
    public interface IExtendedValidator : IValidator
    {
        void Set<TData>(TData data);
        void Set(object data, Type dataType);
        Task ValidateWithExceptionAsync(object message);
    }

    public class ExtendedValidator<T> : AbstractValidator<T>, IExtendedValidator
    {
        public ExtendedValidator()
        {
            Data = new Dictionary<Type, object>();
        }

        public Dictionary<Type, object> Data { get; }

        public new IRuleBuilderInitial<T, TProperty> RuleFor<TProperty>(Expression<Func<T, TProperty>> expression)
        {
            return new RuleBuilderDecorator<T, TProperty>(base.RuleFor(expression), Data);
        }

        public new IRuleBuilderInitialCollection<T, TProperty> RuleForEach<TProperty>(Expression<Func<T, IEnumerable<TProperty>>> expression)
        {
            return new RuleBuilderCollectionDecorator<T, TProperty>(base.RuleForEach(expression), Data);
        }

        public void Set<TData>(TData data)
        {
            Set(data, typeof(TData));
        }

        public void Set(object data, Type dataType)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));
            Data[dataType] = data;
            foreach (var type in data.GetType().GetInterfaces())//TODO a tester
            {
                Data[type] = data;
            }
        }

        public async Task ValidateWithExceptionAsync(object message)
        {
            var result = await ValidateAsync((T)message);
            if (!result.IsValid)
                throw new ValidationException(result.Errors);
        }
    }

    public class ValidatorInheritanceException : ConfigurationException
    {
        public ValidatorInheritanceException() : base($"Your validator must inherit {typeof(ExtendedValidator<>).Name}.")
        {
        }
    }
}
