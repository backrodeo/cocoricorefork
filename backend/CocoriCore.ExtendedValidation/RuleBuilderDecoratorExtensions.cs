﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.ExtendedValidation
{
    public static class RuleBuilderDecoratorExtensions
    {
        public static TData GetService<TData>(this Dictionary<Type, object> data)
        {
            var dataType = typeof(TData);
            var registeredDataType = data.Keys.FirstOrDefault(x => dataType.IsAssignableFrom(x));
            if (registeredDataType == null)
                throw new ConfigurationException($"A validation rule is missing an instance of {dataType.Name} which is required to execute validation.\n" +
                    $"If you validator is integrated into an command/query handler ensure that the instance implementing {dataType.Name} " +
                    $"is stored into a protected or public field/property within you handler.\nIt then will be registred automatically." +
                    "\n" +
                    $"You can provide an instance of {dataType.Name} manually (for exemple if you defined a specific validator class) " +
                    $"by calling method {nameof(IExtendedValidator.Set)}(instance) from you validator constructor class.\n");
            return (TData)data[registeredDataType];
        }
    }
}
