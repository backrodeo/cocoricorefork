﻿
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CocoriCore
{
    public class GuidResponseWriter : HttpResponseWriterBase, IHttpReponseWriterHandler
    {
        public GuidResponseWriter(JsonSerializer jsonSerializer) : base(jsonSerializer)
        {
        }

        public async Task WriteResponseAsync(HttpResponseWriterContext context)
        {
            var id = (Guid)context.Response;
            await WriteResponseAsync(context.HttpResponse, new { id });
        }
    }
}