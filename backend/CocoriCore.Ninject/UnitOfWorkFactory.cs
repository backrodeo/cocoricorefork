﻿using System;
using CocoriCore;
using Ninject;
using Ninject.Extensions.NamedScope;
using Ninject.Syntax;

namespace CocoriCore.Ninject
{
    public class UnitOfWorkFactory : IUnitOfWorkFactory
    {
        private IResolutionRoot kernel;

        public UnitOfWorkFactory(IResolutionRoot kernel)
        {
            this.kernel = kernel;
        }

        public IUnitOfWork NewUnitOfWork()
        {
            var scope = kernel.CreateNamedScope("unitofwork");
            var unitOfWork = scope.Get<IUnitOfWork>();
            ((UnitOfWork)unitOfWork).SetScope(scope);
            return unitOfWork;
        }
    }
}
