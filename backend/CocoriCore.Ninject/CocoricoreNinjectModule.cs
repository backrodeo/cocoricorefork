﻿using Ninject.Extensions.ContextPreservation;
using Ninject.Extensions.NamedScope;
using Ninject.Modules;

namespace CocoriCore.Ninject
{
    public class CocoricoreNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWorkFactory>().ToMethod(ctx => new UnitOfWorkFactory(ctx.GetContextPreservingResolutionRoot()));
            Bind<IUnitOfWork>().To<UnitOfWork>().InNamedScope("unitofwork");
            Bind<IFactory>().ToMethod(ctx => new NinjectFactory(ctx.GetContextPreservingResolutionRoot()));
        }
    }
}
