using System;
using System.Threading.Tasks;
using Ninject;
using Ninject.Extensions.NamedScope;
using Ninject.Syntax;

namespace CocoriCore.Ninject
{
    public class UnitOfWork : IUnitOfWork
    {
        private NamedScope scope;

        public UnitOfWork()
        {
        }
        public void SetScope(NamedScope scope)
        {
            this.scope = scope;
        }

        public void Dispose()
        {
            this.scope.Dispose();
        }

        public Task FinishAsync()
        {
            return Task.CompletedTask;
        }

        public T Resolve<T>()
        {
            return this.scope.Get<T>();
        }

        public object Resolve(Type type)
        {
            return this.scope.Get(type);
        }
    }
}