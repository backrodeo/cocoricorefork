﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;

namespace CocoriCore.FluentValidation
{
    public static class FluentValidationMessageBusOptionsBuilderExtension
    {
        public static IEnumerable<MessageBusRule> CallValidators(this IEnumerable<MessageBusRule> rules,
            params System.Reflection.Assembly[] assemblies)
        {
            if (assemblies.Count() == 0)
            {
                throw new ConfigurationException("You must pass at least one assembly.");
            }
            var validators = assemblies.IndexTypesByGenericParameter(typeof(IValidator<>));
            RegisterValidatorsForRules(rules, validators);
            return rules;
        }

        private static void RegisterValidatorsForRules(IEnumerable<MessageBusRule> rules, Dictionary<Type, Type> validators)
        {
            foreach (var rule in rules)
            {
                var messageType = rule.TargetType;
                GetValidatorTypesForMessageType(validators, messageType)
                    .ForEach(t => rule.AddHandler(t, (v, c, u) => ((IValidator)v).ValidateAndThrowAsync(c.Message)));
            }
        }

        private static List<Type> GetValidatorTypesForMessageType(Dictionary<Type, Type> validators, Type messageType)
        {
            var validatorTypes = new List<Type>();
            foreach (var type in messageType.GetTypeHierarchy())
            {
                if (validators.ContainsKey(type))
                {
                    validatorTypes.Add(validators[type]);
                }
            }
            return validatorTypes;
        }
    }
}