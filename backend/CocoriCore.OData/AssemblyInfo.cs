﻿using System.Reflection;

namespace CocoriCore.OData
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
