using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.OData.Linq
{
    public static class ODataQueryableExtension
    {
        private static BindingFlags _oDataBindingFlags = BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public;
        private static MethodInfo _orderByMethod;
        private static MethodInfo _orderByDescendingMethod;

        static ODataQueryableExtension()
        {
            var queryableMethods = typeof(Queryable).GetMethods();
            _orderByMethod = queryableMethods
                    .Where(x => x.Name == nameof(Queryable.OrderBy) && x.GetParameters().Length == 2)
                    .Single();
            _orderByDescendingMethod = queryableMethods
                   .Where(x => x.Name == nameof(Queryable.OrderByDescending) && x.GetParameters().Length == 2)
                   .Single();
        }

        public static IQueryable<T> OrderBy<T>(this IQueryable<T> queryable, ODataOrderBy order)
        {
            if (order == null)
                return queryable;
            var parameterExpression = Expression.Parameter(typeof(T), "x");
            var memberInfo = typeof(T).GetPropertyOrField(order.Member, _oDataBindingFlags);
            var memberAccessExpression = Expression.MakeMemberAccess(parameterExpression, memberInfo);
            var orderByLambdaType = typeof(Func<,>).MakeGenericType(typeof(T), memberInfo.GetMemberType());
            var orderByLambda = Expression.Lambda(orderByLambdaType, memberAccessExpression, parameterExpression);
            var orderMethod = GetOrderMethod(order.Direction);
            var typedOrderByMethod = orderMethod.MakeGenericMethod(typeof(T), memberInfo.GetMemberType());
            queryable = (IQueryable<T>)typedOrderByMethod.Invoke(queryable, new object[] { queryable, orderByLambda });
            return queryable;
        }

        private static MethodInfo GetOrderMethod(OrderDirection direction)
        {
            if (direction == OrderDirection.Asc)
            {
                return _orderByMethod;
            }
            else if (direction == OrderDirection.Desc)
            {
                return _orderByDescendingMethod;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static IQueryable<T> Where<T>(this IQueryable<T> queryable, ODataFilter filter)
        {
            if (filter == null)
                return queryable;
            var parameterExpression = Expression.Parameter(typeof(T), "x");
            Expression andExpression = Expression.Constant(true);
            andExpression = ConstructAndEqExpression<T>(filter, andExpression, parameterExpression);
            andExpression = ConstructAndContainsExpression<T>(filter, andExpression, parameterExpression);
            var filterExpression = Expression.Lambda<Func<T, bool>>(andExpression, parameterExpression);
            return queryable.Where(filterExpression);
        }

        private static Expression ConstructAndEqExpression<T>(ODataFilter filter,
            Expression andExpression, ParameterExpression parameterExpression)
        {
            foreach (var propertyOrField in filter.Eq.Keys)
            {
                var memberInfo = typeof(T).GetPropertyOrField(propertyOrField, _oDataBindingFlags);
                var memberType = memberInfo.GetMemberType();
                var memberAccessExpression = Expression.MakeMemberAccess(parameterExpression, memberInfo);
                var orExpression = ConstructOrEqExpression(filter.Eq[propertyOrField], memberType, memberAccessExpression);
                andExpression = Expression.AndAlso(andExpression, orExpression);
            }
            return andExpression;
        }

        private static Expression ConstructOrEqExpression(IEnumerable<string> values, Type valuesType,
            MemberExpression memberAccessExpression)
        {
            Expression orExpression = Expression.Constant(false);
            foreach (var value in values)
            {
                var memberType = memberAccessExpression.Member.GetMemberType();
                var parsedValue = TypeDescriptor.GetConverter(memberType).ConvertFromString(value);
                var valueExpression = Expression.Constant(parsedValue, memberType);
                var equalsExpression = Expression.Equal(memberAccessExpression, valueExpression);
                orExpression = Expression.OrElse(orExpression, equalsExpression);
            }
            return orExpression;
        }

        private static Expression ConstructAndContainsExpression<T>(ODataFilter filter,
            Expression andExpression, ParameterExpression parameterExpression)
        {

            foreach (var propertyOrField in filter.Contains.Keys)
            {
                var memberInfo = typeof(T).GetPropertyOrField(propertyOrField, _oDataBindingFlags);
                var memberType = memberInfo.GetMemberType();
                var memberAccessExpression = Expression.MakeMemberAccess(parameterExpression, memberInfo);
                var orExpression = ConstructOrContainsExpression(filter.Contains[propertyOrField], memberType, memberAccessExpression);
                andExpression = Expression.AndAlso(andExpression, orExpression);
            }
            return andExpression;
        }

        private static Expression ConstructOrContainsExpression(IEnumerable<string> values, Type valuesType,
            MemberExpression memberAccessExpression)
        {
            Expression orExpression = Expression.Constant(false);
            foreach (var value in values)
            {
                var memberType = memberAccessExpression.Member.GetMemberType();
                var parsedValue = TypeDescriptor.GetConverter(memberType).ConvertFromString(value);
                var valueExpression = Expression.Constant(parsedValue, memberType);
                var containsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
                var containsExpression = Expression.Call(memberAccessExpression, containsMethod, valueExpression);
                orExpression = Expression.OrElse(orExpression, containsExpression);
            }
            return orExpression;
        }

        public static IQueryable<TEntity> Select<TEntity>(this IQueryable<TEntity> queryable, ODataSelect select = null)
        {
            return queryable.Select<TEntity, TEntity>(select);
        }

        public static IQueryable<TResponse> Select<TEntity, TResponse>(this IQueryable<TEntity> queryable, ODataSelect select = null)
        {
            var entityType = typeof(TEntity);
            var responseType = typeof(TResponse);
            IEnumerable<MemberInfo> entityMembersToSelect = GetMembersToSelect<TEntity>(select, entityType);

            var paramaterExpression = Expression.Parameter(entityType, "x");
            var newResponseExpression = Expression.New(responseType);
            var assignments = ConstructAllAssignmentExpressions(entityMembersToSelect, responseType, paramaterExpression);
            var initExpression = Expression.MemberInit(newResponseExpression, assignments);
            var selectExpression = Expression.Lambda<Func<TEntity, TResponse>>(initExpression, paramaterExpression);

            return queryable.Select(selectExpression);
        }

        private static IEnumerable<MemberInfo> GetMembersToSelect<TEntity>(ODataSelect select, Type entityType)
        {
            if (select != null && select.Members.Length > 0)
            {
                return select
                    .Members
                    .Select(x => entityType.GetPropertyOrField(x, _oDataBindingFlags));
            }
            else
            {
                return entityType.GetPropertiesAndFields();
            }
        }

        private static IEnumerable<MemberAssignment> ConstructAllAssignmentExpressions(IEnumerable<MemberInfo> entityMembersToSelect,
            Type responseType, ParameterExpression paramaterExpression)
        {
            var assignments = new List<MemberAssignment>();
            foreach (var entityMemberInfo in entityMembersToSelect)
            {
                var responseMemberInfo = responseType.GetPropertyOrFieldOrDefault(entityMemberInfo.Name, _oDataBindingFlags);
                if (entityMemberInfo != null && responseMemberInfo != null)
                {
                    assignments.Add(ConstructAssignmentExpression(entityMemberInfo, responseMemberInfo, paramaterExpression));
                }
            }
            return assignments;
        }

        private static MemberAssignment ConstructAssignmentExpression(MemberInfo entityMemberInfo, MemberInfo responseMemberInfo,
            ParameterExpression paramaterExpression)
        {
            var propertyExpression = (Expression)Expression.MakeMemberAccess(paramaterExpression, entityMemberInfo);
            if (entityMemberInfo.GetMemberType() != responseMemberInfo.GetMemberType())
                propertyExpression = Expression.Convert(propertyExpression, responseMemberInfo.GetMemberType());
            return Expression.Bind(responseMemberInfo, propertyExpression);
        }

        public static IQueryable<T> Distinct<T>(this IQueryable<T> queryable, bool distinct)
        {
            if (distinct)
            {
                return queryable.Distinct();
            }
            else
            {
                return queryable;
            }
        }

        public static IQueryable<T> Top<T>(this IQueryable<T> queryable, int count)
        {
            return queryable.Take(count > 0 ? count : 0);
        }
    }
}