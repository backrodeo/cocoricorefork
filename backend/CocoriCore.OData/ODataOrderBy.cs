namespace CocoriCore
{
    public class ODataOrderBy
    {
        public OrderDirection Direction;
        public string Member;

        public ODataOrderBy()
        {
        }

        public ODataOrderBy(OrderDirection direction, string member)
        {
            Direction = direction;
            Member = member;
        }
    }

    public enum OrderDirection
    {
        Asc,
        Desc
    }
}