using System;
using System.Text.RegularExpressions;
using CocoriCore.OData.Parser;
using Newtonsoft.Json;
using Soltys.ChangeCase;

namespace CocoriCore.OData.Converter
{
    public class ODataFilterConverter : JsonConverter
    {
        private ODataFilterParser _parser;

        public ODataFilterConverter(ODataFilterParser parser)
        {
            _parser = parser;
        }
        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ODataFilter);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            var value = reader.Value;
            try
            {
                return _parser.Parse((string)value);
            }
            catch (Exception e)
            {
                var lineInfo = reader as IJsonLineInfo;
                throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                    + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
        }
    }

}