using System;
using Newtonsoft.Json;

namespace CocoriCore.OData.Converter
{
    public class ODataSelectConverter : JsonConverter
    {
        public ODataSelectConverter()
        {
        }

        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ODataSelect);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            var value = reader.Value;
            try
            {
                var fields = ((string)value).Split(",");
                var result = new ODataSelect(fields);
                return result;
            }
            catch (Exception e)
            {
                var lineInfo = reader as IJsonLineInfo;
                throw new JsonSerializationException($"Error converting value \"{value}\" to type '{objectType}'."
                    + $" Path '{reader.Path}', line {lineInfo.LineNumber}, position {lineInfo.LinePosition}.", e);
            }
        }

        public override void WriteJson(JsonWriter writer, object value, Newtonsoft.Json.JsonSerializer serializer)
        {
        }
    }

}