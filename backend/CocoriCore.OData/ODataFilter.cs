using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore
{
    public class ODataFilter
    {
        public IndexedLists<string, string> Eq { get; }
        public IndexedLists<string, string> Contains { get; }

        public ODataFilter()
        {
            Eq = new IndexedLists<string, string>();
            Contains = new IndexedLists<string, string>();
        }

        public ODataFilter ReplaceKey(string key, string replacementKey)
        {
            ReplaceKey(Eq, key, replacementKey);
            ReplaceKey(Contains, key, replacementKey);
            return this;
        }

        private void ReplaceKey(IndexedLists<string, string> indexedLists, string key, string replacementKey)
        {
            if (indexedLists.ContainsKey(key))
            {
                indexedLists.Add(replacementKey, indexedLists[key]);
                indexedLists.RemoveKey(key);
            }
        }

        public ODataFilter TransformValue(string key, Func<string, string> valueTransformation)
        {
            TransformValue(Eq, key, valueTransformation);
            TransformValue(Contains, key, valueTransformation);
            return this;
        }

        private void TransformValue(IndexedLists<string, string> indexedLists,
            string key, Func<string, string> valueTransformation)
        {
            if (indexedLists.ContainsKey(key))
            {
                var transformedValues = indexedLists[key].Select(x => valueTransformation(x));
                indexedLists.RemoveKey(key);
                indexedLists.Add(key, transformedValues);
            }
        }
    }
}