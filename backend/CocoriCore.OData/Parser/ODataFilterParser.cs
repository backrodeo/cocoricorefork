using System;
using System.Text.RegularExpressions;
using Soltys.ChangeCase;

namespace CocoriCore.OData.Parser
{
    public class ODataFilterParser
    {
        private Regex _regexEq;
        private Regex _regexContains;
        private Func<string, string> _keyNameTransformation;

        public ODataFilterParser(Func<string, string> keyNameTransformation = null)
        {
            _regexEq = new Regex(@"\(?(?<key>[A-Za-z0-9_]+) eq (?<value>[^\)]+)\)?");
            _regexContains = new Regex(@"contains\((?<key>[A-Za-z0-9_]+), ?(?<value>[^\)]+)\)");
            _keyNameTransformation = keyNameTransformation ?? (k => k.PascalCase());
        }

        public ODataFilter Parse(string input)
        {
            var result = new ODataFilter();
            result = ParseEq(result, input);
            result = ParseContains(result, input);
            return result;
        }

        public ODataFilter ParseEq(ODataFilter filter, string input)
        {
            var matches = _regexEq.Matches(input);
            foreach (Match match in matches)
            {
                var filterValue = match.Groups["value"].Value;
                filterValue = RemoveSurroundingQuotes(filterValue);
                filterValue = UnescapeQuotes(filterValue);
                var keyName = _keyNameTransformation(match.Groups["key"].Value);
                filter.Eq.Add(keyName, filterValue);
            }
            return filter;
        }

        public ODataFilter ParseContains(ODataFilter filter, string input)
        {
            var matches = _regexContains.Matches(input);
            foreach (Match match in matches)
            {
                var filterValue = match.Groups["value"].Value;
                filterValue = RemoveSurroundingQuotes(filterValue);
                filterValue = UnescapeQuotes(filterValue);
                var keyName = _keyNameTransformation(match.Groups["key"].Value);
                filter.Contains.Add(keyName, filterValue);
            }
            return filter;
        }

        private string RemoveSurroundingQuotes(string filterValue)
        {
            if (filterValue.StartsWith("'"))
                filterValue = filterValue.Substring(1);
            if (filterValue.EndsWith("'"))
                filterValue = filterValue.Substring(0, filterValue.Length - 1);
            return filterValue;
        }

        private string UnescapeQuotes(string filterValue)
        {
            return filterValue.Replace("''", "'");
        }
    }
}