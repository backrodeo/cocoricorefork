namespace CocoriCore
{
    public class ODataSelect
    {
        public string[] Members { get; }

        public ODataSelect(params string[] members)
        {
            Members = members;
        }
    }
}