using System.Collections;
using System.Collections.Generic;

namespace CocoriCore
{
    public interface IODataResponse
    {
        IEnumerable Results { get; }
        int Count { get; }
    }

    public class ODataResponse<T> : IODataResponse
    {
        public T[] Results;
        public int Count;

        IEnumerable IODataResponse.Results => Results;

        int IODataResponse.Count => Count;
    }
}