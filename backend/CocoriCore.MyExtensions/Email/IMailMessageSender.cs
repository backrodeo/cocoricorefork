using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IMailMessageSender
    {
        Task SendAsync(MailMessage message);
    }
}