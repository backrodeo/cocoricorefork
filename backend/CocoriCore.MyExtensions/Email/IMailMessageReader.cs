using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IMailMessageReader
    {
        Task<ReceivedMailMessage[]> ReadMailMessages();
    }
}