using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IEmailSender
    {
        Task SendAsync(TypedMailMessage mailMessage);
    }
}