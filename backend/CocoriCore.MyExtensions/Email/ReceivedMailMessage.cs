using System;
using System.Net.Mail;

namespace CocoriCore
{
    public class ReceivedMailMessage
    {
        public MailMessage MailMessage;
        public DateTime DateTime;
        public int Index;
    }
}