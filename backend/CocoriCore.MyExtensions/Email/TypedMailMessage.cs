using System;
using System.Net.Mail;

namespace CocoriCore
{
    public class TypedMailMessage
    {
        public TypedMailMessage()
        {
            To = new MailAddressCollection();
        }

        public MailAddress From { get; set; }
        public MailAddressCollection To { get; set; }
        public object Body { get; set; }
    }
}