﻿using System;
using System.Collections.Generic;

namespace CocoriCore
{

    public class TemplateEmailRendererOptions
    {
        public Dictionary<Type, string> TemplateKeys = new Dictionary<Type, string>();
    }
}