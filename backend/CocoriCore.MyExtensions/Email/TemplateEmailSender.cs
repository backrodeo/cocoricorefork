using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class TemplateEmailSender : IEmailSender
    {
        private readonly IEmailService emailService;
        private readonly IRenderer renderer;
        private readonly TemplateEmailRendererOptions emailRendererOptions;

        public TemplateEmailSender(
            IEmailService emailService,
            IRenderer renderer,
            TemplateEmailRendererOptions emailRendererOptions)
        {
            this.emailService = emailService;
            this.renderer = renderer;
            this.emailRendererOptions = emailRendererOptions;
        }

        public async Task SendAsync(TypedMailMessage TypedMailMessage)
        {
            var templateKey = emailRendererOptions.TemplateKeys[TypedMailMessage.Body.GetType()];
            var mailMessage = new MailMessage();
            mailMessage.From = TypedMailMessage.From;
            foreach (var to in TypedMailMessage.To)
                mailMessage.To.Add(to);
            mailMessage.Body = await renderer.RenderAsync(templateKey, TypedMailMessage.Body);
            await emailService.SendAsync(mailMessage);
        }
    }


}