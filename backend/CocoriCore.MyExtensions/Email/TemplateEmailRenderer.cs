using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public class TemplateEmailRenderer : IEmailRenderer
    {
        private readonly IRenderer renderer;
        private readonly TemplateEmailRendererOptions emailRendererOptions;

        public TemplateEmailRenderer(
            IRenderer renderer,
            TemplateEmailRendererOptions emailRendererOptions)
        {
            this.renderer = renderer;
            this.emailRendererOptions = emailRendererOptions;
        }

        public async Task<MailMessage> RenderAsync(object body)
        {
            var mailMessage = new MailMessage();
            var templateKey = emailRendererOptions.TemplateKeys[body.GetType()];
            var renderedTemplate = await renderer.RenderAsync(templateKey, body);
            var newLineIndex = mailMessage.Body.IndexOf("\n");
            mailMessage.Subject = renderedTemplate.Substring(0, newLineIndex);
            mailMessage.Body = renderedTemplate.Substring(newLineIndex + 1);
            mailMessage.IsBodyHtml = true;
            return mailMessage;
        }
    }
}