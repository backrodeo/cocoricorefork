﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IEmailRenderer
    {
        Task<MailMessage> RenderAsync(object body);
        //string RenderSubject(object body);
    }
}