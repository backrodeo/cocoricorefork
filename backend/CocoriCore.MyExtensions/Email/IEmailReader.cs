﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore
{

    public interface IEmailReader
    {
        Task<int> LogInAsync(string emailAddress);
        Task<int> DisplayAsync<TEmail>();
        //Task<TMember> DeserializeAsync<TMember>(string memberExpr);
        Task<TEmail> DeserializeAsync<TEmail>();
    }
}