using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore
{
    public static class ExpressionExtension
    {
        public static string ExprToString(this Expression expr)
        {
            var expression = expr;
            var memberInfos = new List<MemberInfo>();

            if (expression.NodeType == ExpressionType.Convert)
                expression = ((UnaryExpression)expression).Operand;

            var str = expression.ToString();
            return str.Substring(str.IndexOf(".") + 1);
        }
    }
}