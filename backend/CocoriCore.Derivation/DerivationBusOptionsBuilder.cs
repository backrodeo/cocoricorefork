using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationBusOptionsBuilder : BusOptionsBuilder<IDerivationBusContext, DerivationBusRule, DerivationBusOptions>
    {
        public DerivationBusOptionsBuilder()
        {
            _options.ExceptionIfNoRuleDefined = false;
        }

        public new DerivationBusOptionsBuilder SetFireAndForgetExceptionHandler<THandler>(Func<THandler, Exception, Task> unexpectedExceptionHandler)
        {
            base.SetFireAndForgetExceptionHandler(unexpectedExceptionHandler);
            return this;
        }

        protected override DerivationBusRule AddRule(Type targetType)
        {
            VirtualPropertyChecker.CheckAllVirtualSetter(targetType);
            return base.AddRule(targetType);
        }

        public InsertDerivationRuleBuilder<TEntity> WhenInsert<TEntity>()
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new InsertDerivationRuleBuilder<TEntity>(_options, rule);
        }

        public UpdateDerivationRuleBuilder<TEntity> WhenUpdate<TEntity>(
            params Expression<Func<TEntity, object>>[] propertiesExpressions)
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new UpdateDerivationRuleBuilder<TEntity>(_options, rule, propertiesExpressions);
        }

        public DeleteDerivationRuleBuilder<TEntity> WhenDelete<TEntity>()
            where TEntity : class, IEntity
        {
            var rule = AddRule(typeof(TEntity));
            return new DeleteDerivationRuleBuilder<TEntity>(_options, rule);
        }

        public ProjectOptionBuilder<TEntity> Project<TEntity>(string discriminator = null)
            where TEntity : class, IEntity
        {
            var entityType = typeof(TEntity);
            var insertRule = base.AddRule(entityType);
            var updateRule = base.AddRule(entityType);
            var deleteRule = base.AddRule(entityType);
            insertRule.SetDiscriminator(discriminator);
            updateRule.SetDiscriminator(discriminator);
            deleteRule.SetDiscriminator(discriminator);
            VirtualPropertyChecker.CheckAllVirtualSetter(entityType);
            return new ProjectOptionBuilder<TEntity>(_options, insertRule, updateRule, deleteRule);
        }

        public ProjectManyOptionBuilder<TOneEntity, TManyEntity> Project<TOneEntity, TManyEntity>(Func<TOneEntity, IEnumerable<Guid>> manyIdsFunc)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
        {
            var entityType = typeof(TOneEntity);
            var insertRule = base.AddRule(entityType);
            var updateRule = base.AddRule(entityType);//TODO on peut peut-etre optimiser en n'inserant qu'un seule rule mais qui apelle un synchronizer qui gère les trois opération ?
            var deleteRule = base.AddRule(entityType);
            insertRule.SetManyIdsFunc(e => manyIdsFunc((TOneEntity)e));
            updateRule.SetManyIdsFunc(e => manyIdsFunc((TOneEntity)e));
            deleteRule.SetManyIdsFunc(e => manyIdsFunc((TOneEntity)e));
            VirtualPropertyChecker.CheckAllVirtualSetter(entityType);
            return new ProjectManyOptionBuilder<TOneEntity, TManyEntity>(_options, insertRule, updateRule, deleteRule);
        }

        public DependencyOptionBuilder<TEntity> ForDependency<TEntity>(string discriminator,
            params Expression<Func<TEntity, object>>[] propertiesExpressions)
            where TEntity : class, IEntity
        {
            var entityType = typeof(TEntity);
            VirtualPropertyChecker.CheckAllVirtualSetter(entityType);
            return new DependencyOptionBuilder<TEntity>(_options, discriminator, propertiesExpressions);
        }

        public DependencyOptionBuilder<TEntity> ForDependency<TEntity>(
            params Expression<Func<TEntity, object>>[] propertiesExpressions)
            where TEntity : class, IEntity
        {
            var entityType = typeof(TEntity);
            VirtualPropertyChecker.CheckAllVirtualSetter(entityType);
            return new DependencyOptionBuilder<TEntity>(_options, null, propertiesExpressions);
        }

        public DerivationBusOptionsBuilder SetErrorLimit(int limit)
        {
            _options.ErrorLimit = limit;
            return this;
        }
    }
}
