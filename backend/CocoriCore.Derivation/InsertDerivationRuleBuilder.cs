namespace CocoriCore.Derivation
{
    public class InsertDerivationRuleBuilder<TEntity>
        where TEntity : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }
        public InsertDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Options = options;
            Rule = rule;
        }

        public InsertDerivationRuleBuilder<TEntity, TProjection> For<TProjection>()
            where TProjection : class, IEntity
        {
            return new InsertDerivationRuleBuilder<TEntity, TProjection>(Options, Rule);
        }
    }

    public class InsertDerivationRuleBuilder<TEntity, TProjection>
      where TEntity : class, IEntity
      where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }

        public InsertDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Options = options;
            Rule = rule;
        }

        public InsertDerivationRuleBuilder<TEntity, TProjection> Call<TSynchronizer>()
            where TSynchronizer : class, IInsertSynchronizer<TEntity, TProjection>
            //TODO ici ne pas garder le synchronizer ligne a ligne mais le synchronizer multi-lignes avec en entrée operation, entité, state 
        {
            Rule.SynchronizerType = typeof(TSynchronizer);
            Rule.ProjectionType = typeof(TProjection);
            Rule.AddHandler<InsertHandler>((h, c, u) => //TODO est-ce que l'insertion ne devrait pas être gérée par le synchronizer ici plutôt que par le InsertHandler ?
            //Ca pourrait faciliterait de découplage entre le synchronizer et IRepository
            //on ne garderai ici que la gestion d'erreur
                h.ProcessInsert<TSynchronizer, TEntity, TProjection>(Rule, (DerivationBusContext)c, u));
            Rule.SetCondition(c => c is DerivationBusContext ctx && ctx.Operation == CUDOperation.CREATE);
            return this;
        }

        public InsertDerivationRuleBuilder<TEntity, TProjection> FireAndForget()
        {
            Rule.FireAndForget = true;
            return this;
        }
    }
}
