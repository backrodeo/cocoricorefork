using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using CocoriCore;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation
{
    public class MappingSynchronizer : ISynchronizer, IMappingSynchronizer
    {
        private static ProxyGenerator _generator = new ProxyGenerator();

        private IndexedLists<Type, DependencyInfo> _dependenciesConfiguration;
        private Dictionary<(Type, Type, string), MappingRule> _mappingRules;
        private IDerivationRepository _repository;
        private bool _handleGuidToNullableGuid;

        public MappingSynchronizer(DerivationBusOptions options, IDerivationRepository repository)
        {
            _dependenciesConfiguration = options.ProjectionDependencies;
            _mappingRules = options
                .MappingRules
                .SelectMany(x => x.Value.SelectMany(v => v.Value))
                .ToDictionary(x => (x.ProjectionType, x.EntityType, x.Discriminator), x => x);//TODO optimiser ceci en le faisant une seule fois et lever exception explicite si doublons de clés

            _repository = repository;
            _handleGuidToNullableGuid = options.HandleGuidToNullableGuid;
        }

        public virtual async Task UpdateProjectionAsync<TProjection, TEntity>(TProjection projection, TEntity entity, string discriminator = null)
            where TProjection : IEntity
            where TEntity : IEntity
        {
            var dependencyTrackers = ConstructDependencyTrackers(projection);
            ApplyMappingOrReset(projection, entity, discriminator);
            dependencyTrackers.ForEach(x => x.SaveValues());
            await ApplyMappingsForDependenciesAsync(dependencyTrackers);
            if (_handleGuidToNullableGuid)
            {
                ConvertEmptyGuidToNullable(projection);
            }
        }

        protected virtual List<DependencyTracker> ConstructDependencyTrackers<TProjection>(TProjection projection)
            where TProjection : IEntity
        {
            return _dependenciesConfiguration
                .Get(typeof(TProjection))
                .Select(x => new DependencyTracker(x, projection))
                .ToList();
        }

        protected virtual void ApplyMappingOrReset<TProjection, TEntity>(TProjection projection, TEntity entity, string discriminator)
            where TProjection : IEntity
            where TEntity : IEntity
        {
            var mappingRule = GetMappingRule(typeof(TEntity), typeof(TProjection), discriminator);
            if (entity == null)
            {
                ResetMapping(mappingRule, projection);
            }
            else
            {
                ApplyMapping(mappingRule, entity, projection);
            }
        }

        protected virtual MappingRule GetMappingRule(Type entityType, Type projectionType, string discriminator)
        {
            try
            {
                return _mappingRules[(projectionType, entityType, discriminator)];
            }
            catch (KeyNotFoundException)
            {
                throw new ConfigurationException("No mapping defined for entity "
                    + $"{entityType}, projection {projectionType} and discriminator {discriminator}.");
            }
        }

        protected virtual void ResetMapping(MappingRule mappingRule, IEntity projection)
        {
            IEntity entity = null;
            try
            {
                if (mappingRule.ResetAction != null)
                {
                    mappingRule.ResetAction(projection);
                    return;
                }
                else
                {
                    entity = GetEmpty(mappingRule.EntityType);
                    mappingRule.MapAction(entity, projection);
                }
            }
            catch (Exception e)//TODO a tester
            {
                throw ConstructMappingException(mappingRule, projection, entity, e);
            }
        }

        protected virtual IEntity GetEmpty(Type entityType)
        {
            if (entityType.IsInterface)
            {
                return (IEntity)_generator.CreateInterfaceProxyWithoutTarget(entityType, new DefaultValuePropertyInterceptor());
            }
            else if (entityType.IsAbstract)
            {
                return (IEntity)_generator.CreateClassProxy(entityType, new DefaultValuePropertyInterceptor());
            }
            else
            {
                return (IEntity)Activator.CreateInstance(entityType);
            }
        }

        protected virtual Exception ConstructMappingException(MappingRule mappingRule, IEntity projection, IEntity entity, Exception inner)
        {
            var discriminatorLabel = string.IsNullOrEmpty(mappingRule.Discriminator) ? "empty" : $"'{mappingRule.Discriminator}'";
            var exception = new DerivationException($"Derivation mapping "
                + $"with {discriminatorLabel} discriminator from {mappingRule.EntityType} to projection {mappingRule.ProjectionType} "
                + "throw an exception, see inner for details.", inner);
            exception.Data[GetType().Name] = new
            {
                EntityType = mappingRule?.EntityType,
                Entity = entity,
                ProjectionType = mappingRule?.ProjectionType?.FullName,
                Projection = projection
            };
            return exception;
        }

        protected virtual void ApplyMapping(MappingRule mappingRule, IEntity entity, IEntity projection)
        {
            try
            {
                mappingRule.MapAction(entity, projection);
            }
            catch (Exception e)//TODO a tester
            {
                throw ConstructMappingException(mappingRule, projection, entity, e);
            }
        }

        protected virtual async Task ApplyMappingsForDependenciesAsync(List<DependencyTracker> dependencyTrackers)
        {
            while (dependencyTrackers.Any(x => x.HasChanged))
            {
                foreach (var tracker in dependencyTrackers)
                {
                    if (tracker.HasChanged)
                    {
                        var item = tracker.Info;
                        var mappingRule = GetMappingRule(item.DependencyType, item.ProjectionType, item.Discriminator);
                        ResetMapping(mappingRule, tracker.Projection);
                    }
                }
                foreach (var tracker in dependencyTrackers)
                {
                    if (tracker.HasChanged)
                    {
                        var item = tracker.Info;
                        var mappingRule = GetMappingRule(item.DependencyType, item.ProjectionType, item.Discriminator);
                        tracker.RestoreTrackedValues();
                        var dependency = await GetDependencyOrDefaultAsync(item, tracker.Projection);
                        if (dependency != null)
                        {
                            ApplyMapping(mappingRule, dependency, tracker.Projection);
                        }
                    }
                }
                dependencyTrackers.ForEach(x => x.SaveValues());
            }
        }

        protected virtual async Task<IEntity> GetDependencyOrDefaultAsync(DependencyInfo dependencyInfos, IEntity projection)
        {
            Type dependencyType = dependencyInfos.DependencyType;
            LambdaExpression joinExpression = dependencyInfos.JoinExpression;
            Type projectionType = dependencyInfos.ProjectionType;
            IEntity dependency = null;
            if (dependencyInfos.DependencyIsDefinedWithForeignKey())
            {
                var dependencyId = dependencyInfos.GetForeignKey(projection);
                dependency = await GetDependencyByIdAsync(dependencyType, dependencyId);
            }
            else
            {
                dependency = await GetDependencyUsingJoinExpressionAsync(dependencyType, joinExpression, projectionType, projection);
            }
            return dependency;
        }

        protected virtual async Task<IEntity> GetDependencyByIdAsync(Type dependencyType, Guid? dependencyId)
        {
            IEntity dependency = null;
            if (dependencyId.HasValue && dependencyId != Guid.Empty)
            {
                if (await _repository.ExistsAsync(dependencyType, dependencyId.Value))
                {
                    dependency = (IEntity)await _repository.LoadAsync(dependencyType, dependencyId.Value);
                }
            }
            return dependency;
        }

        protected virtual async Task<IEntity> GetDependencyUsingJoinExpressionAsync(Type dependencyType,
            LambdaExpression joinExpression, Type projectionType, IEntity projection)
        {
            var dependencyParameter = joinExpression.Parameters[0];
            var projectionParameter = joinExpression.Parameters[1];
            var visitor = new EntityToConstantVisitor(projectionParameter, projection);
            Expression entityJoinExpression = visitor.Modify(joinExpression.Body);
            var lambdaType = typeof(Func<,>).MakeGenericType(dependencyType, typeof(bool));
            var predicate = Expression.Lambda(lambdaType, entityJoinExpression, dependencyParameter);

            var singleOrDefaultAsyncMethod = typeof(LinqMethods)
                .GetMethod(nameof(LinqMethods.SingleOrDefaultAsync), BindingFlags.NonPublic | BindingFlags.Static)
                .MakeGenericMethod(dependencyType);

            var task = (Task)singleOrDefaultAsyncMethod.Invoke(_repository, new object[] { _repository, predicate });
            return await task.GetResultAsync<IEntity>();
        }

        protected virtual void ConvertEmptyGuidToNullable<TProjection>(TProjection projection) where TProjection : IEntity
        {
            foreach (var property in typeof(TProjection).GetProperties())
            {
                if (property.GetMemberType() == typeof(Guid?) && Equals(property.InvokeGetter(projection), default(Guid)))
                {
                    property.InvokeSetter(projection, null);
                }
            }
        }
    }

    internal static class LinqMethods
    {
        internal static Task<TEntity> SingleOrDefaultAsync<TEntity>(IDerivationRepository repository, Expression<Func<TEntity, bool>> predicate)
            where TEntity : class, IEntity
        {
            return repository.Query<TEntity>().Where(predicate).SingleOrDefaultAsync();
        }
    }

    class DefaultValuePropertyInterceptor : IInterceptor
    {
        public DefaultValuePropertyInterceptor()
        {
        }

        public void Intercept(IInvocation invocation)
        {
            invocation.ReturnValue = invocation.Method.ReturnType.GetDefault();
        }
    }
}
