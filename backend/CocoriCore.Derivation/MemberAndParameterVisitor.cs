using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class MemberAndParameterVisitor : ExpressionVisitor
    {
        public MemberExpression Member;
        public ParameterExpression Parameter;
        public MemberAndParameterVisitor()
        {
        }

        public Expression Modify(Expression expression)
        {
            return Visit(expression);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            Member = node;
            return node;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            Parameter = node;
            return node;
        }
    }
}