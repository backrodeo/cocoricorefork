using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DependencyTracker
    {
        private TrackedMember[] _trackedMembers;
        public IEntity Projection { get; }
        public DependencyInfo Info { get; }
        public bool HasChanged => _trackedMembers.Any(x => x.HasChanged);

        public DependencyTracker(DependencyInfo info, IEntity projection)
        {
            Projection = projection;
            Info = info;
            _trackedMembers = info
                .ProjectionMembers
                .Select(x => new TrackedMember(x))
                .ToArray();
            SaveValues();
        }

        public void SaveValues()
        {
            foreach (var trackedValue in _trackedMembers)
            {
                trackedValue.SaveCurrent(Projection);
            }
        }

        public void RestoreTrackedValues()
        {
            foreach (var trackedValue in _trackedMembers)
            {
                trackedValue.RestoreCurrent(Projection);
            }
        }

        public override string ToString()
        {
            return $"{Info}\n{string.Join("\n", _trackedMembers.ToString())}";
        }

        public IEnumerable<object> PreviousValues => _trackedMembers.Select(x => x.PreviousValue);
        public IEnumerable<object> CurrentValues => _trackedMembers.Select(x => x.CurrentValue);

        public class TrackedMember
        {
            public TrackedMember(MemberInfo memberInfo)
            {
                Member = memberInfo;
            }

            public bool HasChanged => !Equals(CurrentValue, PreviousValue);

            public void SaveCurrent(object entity)
            {
                PreviousValue = CurrentValue;
                CurrentValue = Member.InvokeGetter(entity);
            }

            public void RestoreCurrent(object entity)
            {
                Member.InvokeSetter(entity, CurrentValue);
            }

            public MemberInfo Member { get; private set; }

            public object PreviousValue { get; private set; }

            public object CurrentValue { get; private set; }

            public override string ToString()
            {
                return $"{Member.Name} : {PreviousValue}/{CurrentValue}";
            }
        }
    }
}
