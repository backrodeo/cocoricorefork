using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class DerivationBusContext : IDerivationBusContext
    {
        public CUDOperation Operation { get; }
        public IEntity Entity { get; }
        public IState State { get; }

        public DerivationBusContext(CUDOperation operation, IEntity entity, IState state)
        {
            Operation = operation;
            Entity = entity;
            State = state;
        }

        public State<TEntity> GetState<TEntity>()
            where TEntity : class, IEntity
        {
            return State?.Cast<TEntity>();
        }

        public Type GetTartgetType()
        {
            return Entity.GetType();
        }

        public bool AtleastOnePropertyHasChanged<TDependency>(Expression<Func<TDependency, object>>[] propertiesExpressions)
            where TDependency : class, IEntity
        {

            if (propertiesExpressions.Length == 0)
            {
                return true;
            }
            var entity = (TDependency)Entity;
            var state = GetState<TDependency>();
            foreach (var propertyExpression in propertiesExpressions)
            {
                var previousValue = state.PreviousValue(propertyExpression);
                var currentValue = propertyExpression.Compile()(entity);
                if (!Equals(previousValue, currentValue))
                {
                    return true;
                }
            }
            return false;
        }
    }
}