using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class ProjectionHandler : IDerivationHandler, IProjectionHandler
    {
        private IDerivationRepository _repository;
        private IMappingSynchronizer _synchronizer;
        private IFactory _factory;


        public ProjectionHandler(IDerivationRepository repository, IMappingSynchronizer synchronizer, IFactory factory)
        {
            _repository = repository;
            _synchronizer = synchronizer;
            _factory = factory;
        }

        public virtual async Task InsertProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var projection = _factory.Create<TProjection>();
            var entity = (TEntity)context.Entity;
            await _synchronizer.UpdateProjectionAsync(projection, entity, rule.GetDiscriminator());
            projection.Id = context.Entity.Id;
            await _repository.InsertAsync(projection);
        }

        public virtual async Task UpdateProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var projection = await _repository.LoadAsync<TProjection>(context.Entity.Id);
            var entity = (TEntity)context.Entity;
            await _synchronizer.UpdateProjectionAsync(projection, entity, rule.GetDiscriminator());
            projection.Id = context.Entity.Id;
            await _repository.UpdateAsync(projection);
        }

        public virtual async Task DeleteProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var projection = await _repository.LoadAsync<TProjection>(context.Entity.Id);
            await _repository.DeleteAsync(projection);//TODO ici on peut optimiser en ne faisant qu'un seul appel
        }
    }
}
