using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DerivationBusOptions : BusOptions<IDerivationBusContext, DerivationBusRule>
    {
        public IndexedLists<Type, DependencyInfo> ProjectionDependencies { get; }
        public IndexedLists<Type, Type> ProjectionOrigins { get; }
        public int ErrorLimit { get; set; }
        public bool HandleGuidToNullableGuid { get; set; }
        public int MaxDependencyRoundtrip { get; set; }
        public Dictionary<Type, IndexedLists<Type, MappingRule>> MappingRules { get; set; }

        public DerivationBusOptions()
        {
            ProjectionDependencies = new IndexedLists<Type, DependencyInfo>();
            ProjectionOrigins = new IndexedLists<Type, Type>();
            MappingRules = new Dictionary<Type, IndexedLists<Type, MappingRule>>();
            HandleGuidToNullableGuid = true;
            MaxDependencyRoundtrip = 10;
        }

        public override Type GetTargetType(IDerivationBusContext context)
        {
            return context.GetTartgetType();
        }

        public DerivationBusOptions AddMappingRule(MappingRule rule)
        {
            if (!MappingRules.ContainsKey(rule.ProjectionType))
            {
                MappingRules[rule.ProjectionType] = new IndexedLists<Type, MappingRule>();
            }
            MappingRules[rule.ProjectionType].Add(rule.EntityType, rule);
            if (!MappingRules[rule.ProjectionType][rule.EntityType].IsUnique(r => r.Discriminator))
            {
                var discriminatorMessage = rule.Discriminator == null ? "an empty discriminator" : $"discriminator '{rule.Discriminator}'";
                throw new ConfigurationException($"Duplicate mapping defined for entity {rule.EntityType}, "
                    + $"projection {rule.ProjectionType} and {discriminatorMessage}.");
            }
            return this;
        }

        public void CheckDependencyWithSameDiscriminatorDuplicate(Type dependencyType, string discriminator, Type projectionType)
        {
            if (DependencyWithSameDiscriminatorExistsForProjection(dependencyType, discriminator, projectionType))
            {
                var discriminatorMessage = discriminator == null ? "an empty discriminator" : $"discriminator '{discriminator}'";
                throw new ConfigurationException($"Projection {projectionType} already have a "
                    + $"dependency {dependencyType} with {discriminatorMessage}. "
                    + $"Set discriminator using builder.{nameof(DerivationBusOptionsBuilder.ForDependency)}(\"discriminator\").");
            }
        }

        private bool DependencyWithSameDiscriminatorExistsForProjection(Type dependencyType, string discriminator, Type projectionType)
        {
            var dependencyIsAProjectionOrigin = discriminator == null &&
                ProjectionOrigins[projectionType].Contains(dependencyType);
            var dependencyIsDefinedWithSameDiscrinator = ProjectionDependencies[projectionType]
                .Any(x => x.DependencyType == dependencyType && x.Discriminator == discriminator);
            return dependencyIsAProjectionOrigin || dependencyIsDefinedWithSameDiscrinator;
        }

        public override void ValidateConfiguration()
        {
            base.ValidateConfiguration();
            CheckMappingDefinition();
        }

        private void CheckMappingDefinition()
        {
            var rulesWithMapping = _typeRuleEntries
                .SelectMany(x => x.Value)
                .Where(x => x.GetUseMapping());
            foreach (var rule in rulesWithMapping)
            {
                if (!MappingRules.ContainsKey(rule.ProjectionType) ||
                    !MappingRules[rule.ProjectionType].ContainsKey(rule.TargetType) ||
                    !MappingRules[rule.ProjectionType][rule.TargetType].Any(r => r.Discriminator == rule.GetDiscriminator()))
                {
                    var errorMessage = $"No mapping defined from entity {rule.TargetType} to projection {rule.ProjectionType} "
                        + $"with discriminator '{rule.GetDiscriminator()}'. Ensure you have configured mappings using "
                        + $"{nameof(DerivationBusMappingExtension.ConfigureMappings)}() and defined a mapping action for "
                        + $"this entity, projection and discriminator within an implementation of {typeof(IMappingConfiguration)}.";
                    throw new ConfigurationException(errorMessage);
                }
            }
        }


    }
}