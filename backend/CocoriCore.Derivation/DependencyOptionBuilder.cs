using System;
using System.Linq;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class DependencyOptionBuilder<TDependency>
         where TDependency : class, IEntity
    {
        private Expression<Func<TDependency, object>>[] _propertiesExpressions;
        public string Discriminator;
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; set; }
        public DerivationBusRule UpdateRule { get; set; }
        public DerivationBusRule DeleteRule { get; set; }

        public DependencyOptionBuilder(DerivationBusOptions options, string discriminator, Expression<Func<TDependency, object>>[] propertiesExpressions)
        {
            _propertiesExpressions = propertiesExpressions;
            Discriminator = discriminator;
            Options = options;
        }

        public SingleDependencyOptionBuilder<TDependency, TProjection> Update<TProjection>(
            Expression<Func<TDependency, TProjection, bool>> joinExpression)
            where TProjection : class, IEntity
        {
            //TODO déplacer les vérifications dans une phase dédiée
            Options.CheckDependencyWithSameDiscriminatorDuplicate(typeof(TDependency), Discriminator, typeof(TProjection));
            var dependencyInfos = new DependencyInfo(joinExpression, Discriminator);
            Options.ProjectionDependencies.Add(typeof(TProjection), dependencyInfos);

            InsertRule = CreateAndInitRule(dependencyInfos);
            InsertRule.SetCondition(c =>
                c is DerivationBusContext ctx &&
                ctx.Operation == CUDOperation.CREATE);

            UpdateRule = CreateAndInitRule(dependencyInfos);
            UpdateRule.SetCondition(c =>
                c is DerivationBusContext ctx &&
                ctx.Operation == CUDOperation.UPDATE &&
                ctx.AtleastOnePropertyHasChanged(_propertiesExpressions));

            DeleteRule = CreateAndInitRule(dependencyInfos);
            DeleteRule.SetCondition(c =>
                c is DerivationBusContext ctx &&
                ctx.Operation == CUDOperation.DELETE);

            return new SingleDependencyOptionBuilder<TDependency, TProjection>(Options, InsertRule, UpdateRule, DeleteRule);
        }

        private DerivationBusRule CreateAndInitRule(DependencyInfo dependencyInfos)
        {
            var rule = Options.AddRule(typeof(TDependency));
            rule.SetDependencyInfo(dependencyInfos);
            rule.SetDiscriminator(dependencyInfos.Discriminator);
            return rule;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> GroupUpdate<TProjection>(
            Expression<Func<TDependency, TProjection, bool>> joinExpression)
            where TProjection : class, IEntity
        {
            var rule = Options.AddRule(typeof(TDependency));
            var info = new DependencyInfo(joinExpression, Discriminator);
            rule.SetDependencyInfo(info);
            rule.SetCondition(c => c is DerivationBusContextGroup ctx);
            return new GroupDependencyOptionBuilder<TDependency, TProjection>(Options, rule);
        }
    }
}
