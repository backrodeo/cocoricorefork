using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class ProjectionMemberVisitor : ExpressionVisitor
    {
        public List<MemberInfo> ProjectionMembers;
        public List<MemberInfo> DependencyMembers;
        private Type _projectionType;
        private Type _dependencyType;

        public ProjectionMemberVisitor(Type projectionType, Type dependencyType)
        {
            _projectionType = projectionType;
            _dependencyType = dependencyType;
            ProjectionMembers = new List<MemberInfo>();
            DependencyMembers = new List<MemberInfo>();
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            if (node.Expression.Type == _projectionType)
                ProjectionMembers.Add(node.Member);
            else if (node.Expression.Type == _dependencyType)
                DependencyMembers.Add(node.Member);
            return node;
        }
    }
}