using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation
{
    public class DependencyHandler : IDerivationHandler, IDependencyHandler
    {
        private IUnitOfWorkFactory _unitOfWorkFactory;
        private IDerivationRepository _repository;
        private IMappingSynchronizer _synchronizer;

        public DependencyHandler(IUnitOfWorkFactory unitOfWorkFactory, IDerivationRepository repository,
            IMappingSynchronizer synchronizer)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _repository = repository;
            _synchronizer = synchronizer;
        }

        public virtual async Task UpdateProjectionAsync<TDependency, TProjection>(
            DerivationBusOptions options, DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            var dependencyInfos = rule.GetDependencyInfo();
            await UpdateProjectionsAsync<TDependency, TProjection>(dependencyInfos, context, unitOfWork);
        }

        //TODO est-ce que le batch ne devrait pas etre géré (le plus souvent) par des update natifs plutôt ?
        //Les cas ou on a eu besoin de batch sont la maintenance et les évolutions
        //on garderait des outils spécifique batch pour la maintenance ca éviterait les pb de doublon de batch, du code complexe, de derivation en cascade en mode btach, d'optimisation.
        //Les outils de batch permettrait de lancer un bout de code tant qu'une condition n'est pas vraie et on pourrait plus facilement gérer le cache
        //les transaction, la gestion d'erreur (continuer jusqu'a un certain nb d'erreurs ou pas), la maintenance utiliserait 
        //d'abord un update natif pour flagguer les lignes à recalculer.
        //Ceci ne serait à utiliser que dans de rares cas.

        protected virtual async Task UpdateProjectionsAsync<TDependency, TProjection>(
            DependencyInfo dependencyInfos, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            await UpdateProjectionsForPreviousDependencyState<TDependency, TProjection>(dependencyInfos, context);
            await UpdateProjectionForCurrentDependencyState<TDependency, TProjection>(dependencyInfos, context);
        }


        protected virtual async Task UpdateProjectionsForPreviousDependencyState<TDependency, TProjection>(
            DependencyInfo dependencyInfos, DerivationBusContext context)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            var updatedDependency = (TDependency)context.Entity;
            if (context.Operation == CUDOperation.UPDATE)
            {
                var previousDependency = (TDependency)context.State.PreviousEntity;
                if (ADependencyMemberHasChanged(dependencyInfos, previousDependency, updatedDependency))
                {
                    var previousProjections = await LoadProjectionsAsync<TDependency, TProjection>(dependencyInfos, previousDependency);
                    await UpdateProjectionsAsync(previousProjections, (TDependency)null, dependencyInfos, context);
                }
            }
            //TODO si on delete une dépendence ca doit aussi provoquer des recalculs
        }

        protected virtual bool ADependencyMemberHasChanged<TDependency>(
            DependencyInfo dependencyInfos, TDependency previousDependency, TDependency currentDependency)
            where TDependency : class, IEntity
        {
            foreach (var memberInfo in dependencyInfos.DependencyMembers)
            {
                var previousValue = memberInfo.InvokeGetter(previousDependency);
                var currentValue = memberInfo.InvokeGetter(currentDependency);
                if (!Equals(previousValue, currentValue))
                {
                    return true;
                }
            }
            return false;
        }

        protected virtual async Task<IEnumerable<TProjection>> LoadProjectionsAsync<TDependency, TProjection>(
            DependencyInfo dependencyInfos, TDependency dependency)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            var queryable = _repository.Query<TProjection>();
            Expression<Func<TProjection, bool>> predicate = p => false;
            var joinExpression = (Expression<Func<TDependency, TProjection, bool>>)dependencyInfos.JoinExpression;
            predicate = CreateWhereExpression(joinExpression, dependency);
            return await queryable.Where(predicate).ToListAsync();//TODO ici on peut faire appel au cache si utilise seulement Id
        }

        protected virtual Expression<Func<T2, TResult>> CreateWhereExpression<T1, T2, TResult>(
           Expression<Func<T1, T2, TResult>> joinExpression, T1 entity)
        {
            var dependencyParameter = joinExpression.Parameters[0];
            var projectionParameter = joinExpression.Parameters[1];
            Expression entityJoinExpression =
                new EntityToConstantVisitor(dependencyParameter, entity).Modify(joinExpression.Body);
            return Expression.Lambda<Func<T2, TResult>>(entityJoinExpression, projectionParameter);
        }

        protected virtual async Task UpdateProjectionsAsync<TDependency, TProjection>(IEnumerable<TProjection> projections,
            TDependency dependency, DependencyInfo dependencyInfos, DerivationBusContext context)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            foreach (var projection in projections)
            {
                var savedId = projection.Id;
                await _synchronizer.UpdateProjectionAsync(projection, dependency, dependencyInfos.Discriminator);
                projection.Id = savedId;
                await _repository.UpdateAsync(projection);
            }
        }

        protected virtual async Task UpdateProjectionForCurrentDependencyState<TDependency, TProjection>(
            DependencyInfo dependencyInfos, DerivationBusContext context)
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            var dependency = (TDependency)context.Entity;
            var projections = await LoadProjectionsAsync<TDependency, TProjection>(dependencyInfos, dependency);
            var currentDependency = context.Operation == CUDOperation.DELETE ? null : dependency;
            await UpdateProjectionsAsync(projections, currentDependency, dependencyInfos, context);
        }
    }
}