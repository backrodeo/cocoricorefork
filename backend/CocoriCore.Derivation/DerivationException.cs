using System;

namespace CocoriCore.Derivation
{
    public class DerivationException : Exception
    {
        public DerivationException(string message)
            : base(message)
        {
        }

        public DerivationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public Type EntityType { get; set; }
        public Guid? EntityId { get; set; }
        public CUDOperation Operation { get; set; }
        public Type ProjectionType { get; set; }
        public Guid? ProjectionId { get; set; }
        public Type SynchronizerType { get; set; }
    }
}