using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IManyProjectionHandler
    {
        Task DeleteProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusOptions options, DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity;
        Task InsertProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity;
        Task UpdateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusOptions options, DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity;
    }
}
