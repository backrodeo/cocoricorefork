﻿using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDependencyHandler
    {
        Task UpdateProjectionAsync<TDependency, TProjection>(DerivationBusOptions options, DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TDependency : class, IEntity
            where TProjection : class, IEntity;
    }
}