using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public static class DerivationBusRuleExtension
    {
        private const string UseMapping = "UseMapping";
        private const string Discriminator = "Discriminator";
        private const string ManyIdsFunc = "ChildIdsFunc";
        private const string DependencyInfo = "DependencyInfos";

        public static void SetUseMapping(this DerivationBusRule rule, bool useMapping)
        {
            rule[UseMapping] = useMapping;
        }

        public static bool GetUseMapping(this DerivationBusRule rule)
        {
            if (!rule.ContainsKey(UseMapping))
                return false;
            else
                return (bool)rule[UseMapping];
        }

        public static Func<object, IEnumerable<Guid>> GetManyIdsFunc(this DerivationBusRule rule)
        {
            if (!rule.ContainsKey(ManyIdsFunc))
                return null;
            else
                return (Func<object, IEnumerable<Guid>>)rule[ManyIdsFunc];
        }

        public static void SetManyIdsFunc(this DerivationBusRule rule, Func<object, IEnumerable<Guid>> manyIdsFunc)
        {
            rule[ManyIdsFunc] = manyIdsFunc;
        }

        public static void SetDiscriminator(this DerivationBusRule rule, string discriminator)
        {
            rule[Discriminator] = discriminator;
        }

        public static string GetDiscriminator(this DerivationBusRule rule)
        {
            if (!rule.ContainsKey(Discriminator))
                return null;
            else
                return (string)rule[Discriminator];
        }

        public static void SetDependencyInfo(this DerivationBusRule rule, DependencyInfo dependencyInfos)
        {
            rule[DependencyInfo] = dependencyInfos;
        }

        public static DependencyInfo GetDependencyInfo(this DerivationBusRule rule)
        {
            if (!rule.ContainsKey(DependencyInfo))
                return null;
            else
                return (DependencyInfo)rule[DependencyInfo];
        }
    }
}
