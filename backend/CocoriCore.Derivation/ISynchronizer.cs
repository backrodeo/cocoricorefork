using System.Threading.Tasks;

namespace CocoriCore.Derivation
{

    public interface ISynchronizer
    {
    }

    public interface IInsertSynchronizer<TEntity, TProjection> : ISynchronizer
       where TEntity : IEntity
       where TProjection : IEntity
    {
        Task OnInsertAsync(TEntity entity);
    }

    public interface IUpdateSynchronizer<TEntity, TProjection> : ISynchronizer
        where TEntity : IEntity
        where TProjection : IEntity
    {
        Task OnUpdateAsync(TEntity entity, State<TEntity> state);
    }

    public interface IDeleteSynchronizer<TEntity, TProjection> : ISynchronizer
        where TEntity : IEntity
        where TProjection : IEntity
    {
        Task OnDeleteAsync(TEntity entity);
    }
}