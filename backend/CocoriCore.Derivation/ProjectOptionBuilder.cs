using System.Reflection;

namespace CocoriCore.Derivation
{
    public class ProjectOptionBuilder<TEntity>
         where TEntity : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; }
        public DerivationBusRule UpdateRule { get; }
        public DerivationBusRule DeleteRule { get; }

        public ProjectOptionBuilder(DerivationBusOptions options,
            DerivationBusRule insertRule, DerivationBusRule updateRule, DerivationBusRule deleteRule)
        {
            Options = options;
            InsertRule = insertRule;
            UpdateRule = updateRule;
            DeleteRule = deleteRule;
        }

        public ProjectOptionBuilder<TEntity, TProjection> Into<TProjection>()
            where TProjection : class, IEntity
        {
            return new ProjectOptionBuilder<TEntity, TProjection>(Options, InsertRule, UpdateRule, DeleteRule);
        }
    }


    public class ProjectOptionBuilder<TEntity, TProjection>
        where TEntity : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; }
        public DerivationBusRule UpdateRule { get; }
        public DerivationBusRule DeleteRule { get; }

        public ProjectOptionBuilder(DerivationBusOptions options,
            DerivationBusRule insertRule, DerivationBusRule updateRule, DerivationBusRule deleteRule)
        {
            Options = options;
            InsertRule = insertRule;
            UpdateRule = updateRule;
            DeleteRule = deleteRule;
        }

        public ProjectOptionBuilder<TEntity, TProjection> UsingMapping()
        {
            SetRuleProperties(InsertRule, CUDOperation.CREATE);
            SetRuleProperties(UpdateRule, CUDOperation.UPDATE);
            SetRuleProperties(DeleteRule, CUDOperation.DELETE);
            InsertRule.AddHandler<IProjectionHandler>((h, c, u) =>
                h.InsertProjectionAsync<TEntity, TProjection>(InsertRule, (DerivationBusContext)c, u));
            UpdateRule.AddHandler<IProjectionHandler>((h, c, u) =>
                h.UpdateProjectionAsync<TEntity, TProjection>(UpdateRule, (DerivationBusContext)c, u));
            DeleteRule.AddHandler<IProjectionHandler>((h, c, u) =>
                h.DeleteProjectionAsync<TEntity, TProjection>(DeleteRule, (DerivationBusContext)c, u));
            Options.ProjectionOrigins.Add(typeof(TProjection), typeof(TEntity));
            return this;
        }

        protected void SetRuleProperties(DerivationBusRule rule, CUDOperation operation)
        {
            rule.SetUseMapping(true);
            rule.SynchronizerType = typeof(IMappingSynchronizer);
            rule.ProjectionType = typeof(TProjection);
            rule.SetCondition(c => c is DerivationBusContext ctx && ctx.Operation == operation);
        }

        public ProjectOptionBuilder<TEntity, TProjection> FireAndForget()
        {
            InsertRule.FireAndForget = true;
            UpdateRule.FireAndForget = true;
            DeleteRule.FireAndForget = true;
            return this;
        }
    }
}
