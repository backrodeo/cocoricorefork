using System;

namespace CocoriCore.Derivation
{
    public class DerivationError : IEntity
    {
        public virtual Guid Id { get; set; }
        public virtual Guid? EntityId { get; set; }
        public virtual Type EntityType { get; set; }
        public virtual Guid? ExceptionId { get; set; }
        public virtual CUDOperation Operation { get; set; }
        public virtual Guid? ProjectionId { get; set; }
        public virtual Type ProjectionType { get; set; }
    }
}