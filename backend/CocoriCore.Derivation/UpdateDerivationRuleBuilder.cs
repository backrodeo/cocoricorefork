using System;
using System.Linq.Expressions;

namespace CocoriCore.Derivation
{
    public class UpdateDerivationRuleBuilder<TEntity>
        where TEntity : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }
        private Func<IDerivationBusContext, bool> _handlerCondition;


        public UpdateDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule,
            params Expression<Func<TEntity, object>>[] propertiesExpressions)
        {
            Options = options;
            Rule = rule;
            _handlerCondition = c =>
                c is DerivationBusContext ctx &&
                ctx.Operation == CUDOperation.UPDATE &&
                ctx.AtleastOnePropertyHasChanged(propertiesExpressions);
        }

        public UpdateDerivationRuleBuilder<TEntity, TProjection> For<TProjection>()
            where TProjection : class, IEntity
        {
            return new UpdateDerivationRuleBuilder<TEntity, TProjection>(Options, Rule, _handlerCondition);
        }
    }

    public class UpdateDerivationRuleBuilder<TEntity, TProjection>
        where TEntity : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }
        private Func<IDerivationBusContext, bool> _handlerCondition;

        public UpdateDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Options = options;
            _handlerCondition = c => c is DerivationBusContext ctx && ctx.Operation == CUDOperation.UPDATE;
            Rule = rule;
        }

        public UpdateDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule,
            Func<IDerivationBusContext, bool> handlerCondition)
        {
            Options = options;
            _handlerCondition = handlerCondition;
            Rule = rule;
        }

        public UpdateDerivationRuleBuilder<TEntity, TProjection> Call<TSynchronizer>()
            where TSynchronizer : class, IUpdateSynchronizer<TEntity, TProjection>
        {
            Rule.SynchronizerType = typeof(TSynchronizer);
            Rule.ProjectionType = typeof(TProjection);
            Rule.AddHandler<UpdateHandler>((h, c, u) =>
                h.ProcessUpdate<TSynchronizer, TEntity, TProjection>(Rule, (DerivationBusContext)c, u));
            Rule.SetCondition(_handlerCondition);
            return this;
        }


        public UpdateDerivationRuleBuilder<TEntity, TProjection> FireAndForget()
        {
            Rule.FireAndForget = true;
            return this;
        }
    }
}
