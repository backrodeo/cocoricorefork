namespace CocoriCore.Derivation
{
    public class ProjectManyOptionBuilder<TOneEntity, TManyEntity>
         where TOneEntity : class, IEntity
         where TManyEntity : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; }
        public DerivationBusRule UpdateRule { get; }
        public DerivationBusRule DeleteRule { get; }

        public ProjectManyOptionBuilder(DerivationBusOptions options,
            DerivationBusRule insertRule, DerivationBusRule updateRule, DerivationBusRule deleteRule)
        {
            Options = options;
            InsertRule = insertRule;
            UpdateRule = updateRule;
            DeleteRule = deleteRule;
        }

        public ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection> IntoMany<TProjection>()
            where TProjection : class, IEntity
        {
            return new ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection>(Options, InsertRule, UpdateRule, DeleteRule);
        }
    }


    public class ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection>
        where TOneEntity : class, IEntity
        where TManyEntity : class, IEntity
        where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; }
        public DerivationBusRule UpdateRule { get; }
        public DerivationBusRule DeleteRule { get; }

        public ProjectManyOptionBuilder(DerivationBusOptions options,
            DerivationBusRule insertRule, DerivationBusRule updateRule, DerivationBusRule deleteRule)
        {
            Options = options;
            InsertRule = insertRule;
            UpdateRule = updateRule;
            DeleteRule = deleteRule;
        }

        public ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection> UsingMapping()
        {
            SetRuleProperties(InsertRule, CUDOperation.CREATE);
            SetRuleProperties(UpdateRule, CUDOperation.UPDATE);
            SetRuleProperties(DeleteRule, CUDOperation.DELETE);
            InsertRule.AddHandler<IManyProjectionHandler>((h, c, u) =>
                h.InsertProjectionsAsync<TOneEntity, TManyEntity, TProjection>(InsertRule, (DerivationBusContext)c, u));
            UpdateRule.AddHandler<IManyProjectionHandler>((h, c, u) =>
                h.UpdateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(Options, UpdateRule, (DerivationBusContext)c, u));
            DeleteRule.AddHandler<IManyProjectionHandler>((h, c, u) =>
                h.DeleteProjectionsAsync<TOneEntity, TManyEntity, TProjection>(Options, DeleteRule, (DerivationBusContext)c, u));
            return this;
        }

        protected void SetRuleProperties(DerivationBusRule rule, CUDOperation operation)
        {
            rule.SetUseMapping(true);
            rule.SynchronizerType = typeof(IMappingSynchronizer);
            rule.ProjectionType = typeof(TProjection);
            rule.SetCondition(c => c is DerivationBusContext ctx && ctx.Operation == operation);
        }

        public ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection> FireAndForget()
        {
            InsertRule.FireAndForget = true;
            UpdateRule.FireAndForget = true;
            DeleteRule.FireAndForget = true;
            return this;
        }
    }
}
