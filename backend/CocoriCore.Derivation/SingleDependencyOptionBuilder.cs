namespace CocoriCore.Derivation
{
    public class SingleDependencyOptionBuilder<TEntity, TProjection>
       where TEntity : class, IEntity
       where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule InsertRule { get; }
        public DerivationBusRule UpdateRule { get; }
        public DerivationBusRule DeleteRule { get; }
        public SingleDependencyOptionBuilder(DerivationBusOptions options,
            DerivationBusRule insertRule, DerivationBusRule updateRule, DerivationBusRule deleteRule)
        {
            Options = options;
            InsertRule = insertRule;
            UpdateRule = updateRule;
            DeleteRule = deleteRule;
        }

        public SingleDependencyOptionBuilder<TEntity, TProjection> UsingMapping()
        {
            SetRuleProperties(InsertRule);
            InsertRule.AddHandler<IDependencyHandler>((h, c, u) =>
                h.UpdateProjectionAsync<TEntity, TProjection>(Options, InsertRule, (DerivationBusContext)c, u));
            SetRuleProperties(UpdateRule);
            UpdateRule.AddHandler<IDependencyHandler>((h, c, u) =>
                h.UpdateProjectionAsync<TEntity, TProjection>(Options, UpdateRule, (DerivationBusContext)c, u));
            SetRuleProperties(DeleteRule);
            DeleteRule.AddHandler<IDependencyHandler>((h, c, u) =>
                h.UpdateProjectionAsync<TEntity, TProjection>(Options, DeleteRule, (DerivationBusContext)c, u));
            return this;
        }

        protected void SetRuleProperties(DerivationBusRule rule)
        {
            rule.SetUseMapping(true);
            rule.SynchronizerType = typeof(IMappingSynchronizer);
            rule.ProjectionType = typeof(TProjection);
        }

        public SingleDependencyOptionBuilder<TEntity, TProjection> FireAndForget()
        {
            InsertRule.FireAndForget = true;
            UpdateRule.FireAndForget = true;
            DeleteRule.FireAndForget = true;
            return this;
        }
    }
}
