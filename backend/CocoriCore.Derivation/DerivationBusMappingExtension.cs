using System;
using System.Linq;

namespace CocoriCore.Derivation
{
    public static class DerivationBusMappingExtension
    {
        public static DerivationBusOptionsBuilder ConfigureMappings(this DerivationBusOptionsBuilder builder, System.Reflection.Assembly assembly,
            params System.Reflection.Assembly[] assemblies)
        {
            assemblies
                .Append(assembly)
                .SelectMany(a => a.GetTypes())
                .Where(t => t.GetInterfaces().Any(i => i == typeof(IMappingConfiguration)) && t.IsConcrete())
                .SelectMany(t => ((IMappingConfiguration)Activator.CreateInstance(t)).GetMapRules())
                .ToList()
                .ForEach(r => builder.Options.AddMappingRule(r));
            return builder;
        }
    }
}