using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationBus : BusBase<IDerivationBusContext, DerivationBusRule, DerivationBusOptions>, IDerivationBus
    {
        private IRepositoryBusContextStore _contextStore;

        public DerivationBus(IUnitOfWorkFactory unitOfWorkFactory, IUnitOfWork unitOfWork,
            DerivationBusOptions options, IRepositoryBusContextStore contextStore)
            : base(unitOfWorkFactory, unitOfWork, options)
        {
            _contextStore = contextStore;
        }

        public async Task ApplyRulesAsync()
        {
            var dependencyRoundtrips = new List<IEnumerable<Type>>();
            var dependencyRountripCount = 0;
            while (true)
            {
                var contexts = GetPendingContexts();
                dependencyRoundtrips.Add(contexts.Select(x => x.GetTartgetType()).Distinct());

                if (contexts.Count() == 0)
                    break;
                await ProcessPendingContextsAsync(contexts);
                dependencyRountripCount++;
                if (dependencyRountripCount == Options.MaxDependencyRoundtrip)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine("Max dependency roundtrip reached.");
                    sb.AppendLine("This mean there is a cycle in your dependencies and that the configuration can't prevent infinite treatments.");
                    for (var i = 0; i < dependencyRoundtrips.Count; i++)
                    {
                        sb.AppendLine($"\tRountrip {i + 1} trace :");
                        foreach (var type in dependencyRoundtrips[i])
                        {
                            sb.AppendLine($"\t\t{type.GetPrettyName()}");
                        }
                    }
                    sb.Append("You may also increase the max dependency rountrip allowed using ");
                    sb.AppendLine($"{nameof(DerivationBusOptions)}.{nameof(DerivationBusOptions.MaxDependencyRoundtrip)}.");
                    throw new DerivationException(sb.ToString());
                }
            }
            //TODO si on groupe les pendingContext alors les rule peuvent traiter tout le lot sans qu'on ait à parcourir toutes les rules pour toutes les entités
        }

        protected virtual async Task ProcessPendingContextsAsync(IEnumerable<DerivationBusContext> contexts)
        {
            _contextStore.CreateNewContainer();
            await ExecuteSingleContextRulesAsync(contexts);
            await ExecuteGroupContextRulesAsync(contexts);
        }

        protected virtual IEnumerable<DerivationBusContext> GetPendingContexts()
        {
            return _contextStore
                .PendingContexts
                .Select(x => new DerivationBusContext(x.Operation, x.Entity, x.State))
                .ToList();
        }

        protected virtual async Task ExecuteSingleContextRulesAsync(IEnumerable<DerivationBusContext> contexts)
        {
            foreach (var context in contexts)
            {
                await base.HandleAsync(context);
            }
        }

        protected virtual async Task ExecuteGroupContextRulesAsync(IEnumerable<DerivationBusContext> contexts)
        {
            var groupedContexts = contexts
                .GroupBy(x => x.Entity.GetType(), (k, g) => new DerivationBusContextGroup(k, g));
            foreach (var context in groupedContexts)
            {
                await base.HandleAsync(context);
            }
        }
    }
}