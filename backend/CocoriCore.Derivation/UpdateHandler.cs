using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    //TODO : UpdateHandler, InsertHandler et DeleteHandler sont tout petits, factoriser dans une classe pour simplifier la conf ?
    //ou Appeler le synchronizer depuis la conf fluent plutôt
    public class UpdateHandler : IDerivationHandler
    {
        public async Task ProcessUpdate<TSynchronizer, TEntity, TProjection>(
            DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TSynchronizer : IUpdateSynchronizer<TEntity, TProjection>
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            //TODO ajouter gestion d'erreur ici
            var synchronizer = unitOfWork.Resolve<TSynchronizer>();
            await synchronizer.OnUpdateAsync((TEntity)context.Entity, context.GetState<TEntity>());
        }
    }
}
