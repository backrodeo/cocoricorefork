using System;

namespace CocoriCore.Derivation
{
    public class MappingRule
    {
        public Type EntityType;
        public Type ProjectionType;
        public string Discriminator;
        public Action<object, object> MapAction;
        public Action<object> ResetAction;

        public MappingRule(Type entityType)
        {
            EntityType = entityType;
        }
    }
}