using System;
using System.Collections.Generic;

namespace CocoriCore.Derivation
{
    public class DerivationBusContextGroup : IDerivationBusContext
    {
        public IEnumerable<DerivationBusContext> Values { get; }
        public Type EntityType { get; }

        public DerivationBusContextGroup(Type entityType, IEnumerable<DerivationBusContext> contexts)
        {
            Values = contexts;
            EntityType = entityType;
        }

        public Type GetTartgetType()
        {
            return EntityType;
        }
    }
}