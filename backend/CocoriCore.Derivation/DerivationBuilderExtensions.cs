﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocoriCore.Derivation
{
    public static class DerivationBuilderExtensions
    {
        public static ProjectOptionBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this ProjectOptionBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.InsertRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.UpdateRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.DeleteRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection> FlushAfter<TOneEntity, TManyEntity, TProjection>(this ProjectManyOptionBuilder<TOneEntity, TManyEntity, TProjection> builder)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.InsertRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.UpdateRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.DeleteRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static SingleDependencyOptionBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this SingleDependencyOptionBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.InsertRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.UpdateRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            builder.DeleteRule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static GroupDependencyOptionBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this GroupDependencyOptionBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.Rule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static InsertDerivationRuleBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this InsertDerivationRuleBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.Rule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static UpdateDerivationRuleBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this UpdateDerivationRuleBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.Rule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }

        public static DeleteDerivationRuleBuilder<TEntity, TProjection> FlushAfter<TEntity, TProjection>(this DeleteDerivationRuleBuilder<TEntity, TProjection> builder)
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            builder.Rule.AfterProcessRuleAction = u => u.Resolve<IBufferedRepository>().FlushAsync();
            return builder;
        }
    }
}
