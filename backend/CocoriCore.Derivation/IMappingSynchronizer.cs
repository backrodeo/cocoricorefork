using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IMappingSynchronizer
    {
        Task UpdateProjectionAsync<TProjection, TEntity>(TProjection projection, TEntity entity, string discriminator = null)
            where TProjection : IEntity
            where TEntity : IEntity;
    }
}
