using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IGroupSynchronizer<TDependency, TProjection> : ISynchronizer
        where TDependency : IEntity
        where TProjection : IEntity
    {
        Task GroupUpdateAsync(Guid groupKey, TProjection projection);
    }
}