﻿using System.Reflection;

namespace CocoriCore.Derivation
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
