﻿using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IProjectionHandler
    {
        Task DeleteProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity;
        Task InsertProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity;
        Task UpdateProjectionAsync<TEntity, TProjection>(DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TEntity : class, IEntity
            where TProjection : class, IEntity;
    }
}