namespace CocoriCore.Derivation
{
    public class DeleteDerivationRuleBuilder<TEntity>
        where TEntity : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }
        public DeleteDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Rule = rule;
            Options = options;
        }

        public DeleteDerivationRuleBuilder<TEntity, TProjection> For<TProjection>()
            where TProjection : class, IEntity
        {
            return new DeleteDerivationRuleBuilder<TEntity, TProjection>(Options, Rule);
        }
    }

    public class DeleteDerivationRuleBuilder<TEntity, TProjection>
      where TEntity : class, IEntity
      where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }
        public DeleteDerivationRuleBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Rule = rule;
            Options = options;
        }

        public DeleteDerivationRuleBuilder<TEntity, TProjection> Call<TSynchronizer>()
           where TSynchronizer : class, IDeleteSynchronizer<TEntity, TProjection>
            //TODO ici ne pas garder le synchronizer ligne a ligne mais le synchronizer multi-lignes avec en entr�e operation, entit�, state 
        {
            Rule.SynchronizerType = typeof(TSynchronizer);
            Rule.ProjectionType = typeof(TProjection);
            Rule.AddHandler<DeleteHandler>((h, c, u) => //TODO est-ce que l'insertion ne devrait pas �tre g�r�e par le synchronizer ici plut�t que par le InsertHandler ?
            //Ca pourrait faciliterait de d�couplage entre le synchronizer et IRepository
            //on ne garderai ici que la gestion d'erreur
                h.ProcessDelete<TSynchronizer, TEntity, TProjection>(Rule, (DerivationBusContext)c, u));
            Rule.SetCondition(c => c is DerivationBusContext ctx && ctx.Operation == CUDOperation.DELETE);
            return this;
        }

        public DeleteDerivationRuleBuilder<TEntity, TProjection> FireAndForget()
        {
            Rule.FireAndForget = true;
            return this;
        }
    }
}
