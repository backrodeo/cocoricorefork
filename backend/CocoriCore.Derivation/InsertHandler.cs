using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class InsertHandler : IDerivationHandler
    {
        public async Task ProcessInsert<TSynchronizer, TEntity, TProjection>(
            DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TSynchronizer : IInsertSynchronizer<TEntity, TProjection>
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            //TODO ajouter gestion d'erreur ici
            var synchronizer = unitOfWork.Resolve<TSynchronizer>();
            await synchronizer.OnInsertAsync((TEntity)context.Entity);
        }
    }
}
