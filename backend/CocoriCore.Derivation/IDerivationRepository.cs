using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CocoriCore
{
    public interface IDerivationRepository
    {
        Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity;

        Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<bool> ExistsAsync(Type entityType, Guid id);

        Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity;

        Task<object> LoadAsync(Type type, Guid id);

        Task<IEntity[]> LoadAsync(Type type, IEnumerable<Guid> ids);

        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;

        IQueryable<IEntity> Query(Type type);
    }
}