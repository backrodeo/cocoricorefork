using System;
using System.Collections.Generic;
using System.Linq;

namespace CocoriCore.Derivation
{
    public interface IMappingConfiguration
    {
        IEnumerable<MappingRule> GetMapRules();
    }

    public class MappingConfigurationBase : IMappingConfiguration
    {
        //TODO lever erreur si on ajoute deux paires entity-projection similaires de mapping (erreur copier-coller)
        private List<MappingRule> _mapRules;
        public MappingConfigurationBase()
        {
            _mapRules = new List<MappingRule>();
        }

        public IEnumerable<MappingRule> GetMapRules()
        {
            return _mapRules;
        }

        protected void AutoMap<TEntity, TProjection>(TEntity entity, TProjection projection)
        {
            var entityType = typeof(TEntity);
            var projectionType = typeof(TProjection);
            foreach (var entityProperty in entityType.GetProperties())
            {
                var projectionProperty = projectionType.GetProperty(entityProperty.Name);
                if (projectionProperty != null)
                {
                    var entityPropertyValue = entityProperty.InvokeGetter(entity);
                    projectionProperty.InvokeSetter(projection, entityPropertyValue);
                }
            }
            //TODO si on un objet valeur d'un c�t� et pas de l'autre alors erreur
        }

        protected void CallMap<TEntity, TProjection>(TEntity entity, TProjection projection, string discriminator = null)
        {
            _mapRules
                .Single(x =>
                    x.EntityType == typeof(TEntity) &&
                    x.ProjectionType == typeof(TProjection) &&
                    x.Discriminator == discriminator)
                .MapAction(entity, projection);
        }

        protected Guid GetIdOrDefault<TEntity>(TEntity entity, Func<TEntity, Guid> memberAccessor)
        {
            if (entity == null)
            {
                return default(Guid);
            }
            else
            {
                return memberAccessor.Invoke(entity);
            }
        }

        protected Guid GetIdOrDefault<TEntity>(TEntity entity, Func<TEntity, Guid?> memberAccessor)
        {
            Guid? id = null;
            if (entity != null)
            {
                id = memberAccessor.Invoke(entity);
            }
            if (id == null)
            {
                id = default(Guid);
            }
            return id.Value;
        }

        protected Guid? GetIdOrNull<TEntity>(TEntity entity, Func<TEntity, Guid> memberAccessor)
        {
            Guid? id = null;
            if (entity != null)
                id = (Guid?)memberAccessor.Invoke(entity);
            if (id == Guid.Empty)
                id = null;
            return id;
        }

        protected Guid? GetIdOrNull<TEntity>(TEntity entity, Func<TEntity, Guid?> memberAccessor)
        {
            if (entity != null)
            {
                return memberAccessor.Invoke(entity);
            }
            else
            {
                return null;
            }
        }

        public MapRuleBuilder<TEntity> Map<TEntity>(string discriminator = null)
        {
            var rule = new MappingRule(typeof(TEntity));
            rule.Discriminator = discriminator;
            _mapRules.Add(rule);
            return new MapRuleBuilder<TEntity>(rule);
        }
    }

    public class MapRuleBuilder<TEntity>
    {
        private MappingRule _rule;

        public MapRuleBuilder(MappingRule rule)
        {
            _rule = rule;
        }

        public MapRuleBuilder<TEntity, TProjection> To<TProjection>()
        {
            _rule.ProjectionType = typeof(TProjection);
            return new MapRuleBuilder<TEntity, TProjection>(_rule);
        }
    }

    public class MapRuleBuilder<TEntity, TProjection>
    {
        private MappingRule _rule;

        public MapRuleBuilder(MappingRule rule)
        {
            _rule = rule;
        }

        public MapRuleBuilder<TEntity, TProjection> Using(Action<TEntity, TProjection> mapAction)
        {
            _rule.MapAction = (e, p) => mapAction((TEntity)e, (TProjection)p);
            return this;
        }

        public void ResetUsing(Action<TProjection> resetAction)
        {
            _rule.ResetAction = p => resetAction((TProjection)p);
        }
    }

}