using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public interface IDerivationBus
    {
        Task ApplyRulesAsync();
    }
}