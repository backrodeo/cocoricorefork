namespace CocoriCore.Derivation
{
    public class GroupDependencyOptionBuilder<TDependency, TProjection>
       where TDependency : class, IEntity
       where TProjection : class, IEntity
    {
        public DerivationBusOptions Options { get; }
        public DerivationBusRule Rule { get; }

        public GroupDependencyOptionBuilder(DerivationBusOptions options, DerivationBusRule rule)
        {
            Options = options;
            Rule = rule;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> Using<TSynchronizer>()
            where TSynchronizer : IGroupSynchronizer<TDependency, TProjection>
        {
            var info = Rule.GetDependencyInfo();
            if (info.DependencyMembers.Length != 1)
            {
                //TODO tester et améliorer le message d'erreur
                throw new ConfigurationException($"The provided lambda for {nameof(DependencyOptionBuilder<IEntity>.GroupUpdate)} must use one and only one member of {typeof(TDependency)}");
            }
            if (info.ProjectionMembers.Length != 1)
            {
                //TODO tester et améliorer le message d'erreur
                throw new ConfigurationException($"The provided lambda for {nameof(DependencyOptionBuilder<IEntity>.GroupUpdate)} must use one and only one member of {typeof(TProjection)}");
            }
            //TODO erreur si pas de dependencyMember
            Rule.SynchronizerType = typeof(TSynchronizer);
            Rule.ProjectionType = typeof(TProjection);
            Rule.AddHandler<GroupDependencyHandler>((h, c, u) =>
                h.UpdateProjectionAsync<TDependency, TProjection, TSynchronizer>(Options, Rule, (DerivationBusContextGroup)c, u));
            return this;
        }

        public GroupDependencyOptionBuilder<TDependency, TProjection> FireAndForget()
        {
            Rule.FireAndForget = true;
            return this;
        }
    }
}
