using System;

namespace CocoriCore.Derivation
{
    public interface IDerivationBusContext
    {
        Type GetTartgetType();
    }
}