using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Castle.DynamicProxy;
using CocoriCore.Linq.Async;

namespace CocoriCore.Derivation
{
    public class ManyProjectionHandler : IDerivationHandler, IManyProjectionHandler
    {
        protected IDerivationRepository _repository;
        protected IMappingSynchronizer _synchronizer;
        protected IFactory _factory;

        public ManyProjectionHandler(IDerivationRepository repository, IMappingSynchronizer synchronizer, IFactory factory)
        {
            _repository = repository;
            _synchronizer = synchronizer;
            _factory = factory;
        }

        public virtual async Task InsertProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusRule rule,
            DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            try
            {
                var manyIdsFunc = rule.GetManyIdsFunc();
                var manyEntityIds = manyIdsFunc(context.Entity);
                var manyEntities = await _repository
                    .Query<TManyEntity>()
                    .Where(x => manyEntityIds.Contains(x.Id))
                    .ToListAsync();
                await CreateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(
                    (TOneEntity)context.Entity, manyEntities.Cast<TManyEntity>(), rule, unitOfWork);
            }
            catch (Exception exception)
            {
                exception.AddDerivationDatas(rule, context);
                throw;
            }
        }

        protected virtual async Task CreateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(TOneEntity oneEntity,
            IEnumerable<TManyEntity> manyEntities, DerivationBusRule rule, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            foreach (var manyEntity in manyEntities)
            {
                var projection = _factory.Create<TProjection>();
                var savedId = projection.Id;
                await _synchronizer.UpdateProjectionAsync(projection, oneEntity, rule.GetDiscriminator());
                await _synchronizer.UpdateProjectionAsync(projection, manyEntity, rule.GetDiscriminator());
                projection.Id = savedId;
                await _repository.InsertAsync(projection);
            }
        }

        public virtual async Task UpdateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusOptions options,
            DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            try
            {
                var oneEntity = (TOneEntity)context.Entity;
                var existingProjections = await LoadExistingProjectionsDictionaryAsync<TOneEntity, TManyEntity, TProjection>(oneEntity, options);
                var currentIds = GetCurrentIds(context.Entity, rule);
                var removedIds = existingProjections.Keys.Except(currentIds);
                await DeleteProjectionsForRemovedIdsAsync(existingProjections, removedIds);
                var addedIds = currentIds.Except(existingProjections.Keys);
                await CreateProjectionsForAddedIdsAsync<TOneEntity, TManyEntity, TProjection>(context, rule, unitOfWork, addedIds);
            }
            catch (Exception exception)//TODO tester gestion d'erreur
            {
                exception.AddDerivationDatas(rule, context);
                throw;
            }
        }

        protected virtual async Task<Dictionary<Guid, TProjection>> LoadExistingProjectionsDictionaryAsync<TOneEntity, TManyEntity, TProjection>(
            TOneEntity oneEntity, DerivationBusOptions options)
            where TOneEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var existingProjections = await LoadExistingProjectionsAsync<TOneEntity, TManyEntity, TProjection>(oneEntity, options);
            var manyProjectionMember = GetManyProjectionMember<TManyEntity, TProjection>(options);
            return existingProjections.ToDictionary(x => manyProjectionMember.InvokeGetter<Guid>(x), x => x);
        }

        protected virtual async Task<IEnumerable<TProjection>> LoadExistingProjectionsAsync<TOneEntity, TManyEntity, TProjection>(
            TOneEntity oneEntity, DerivationBusOptions options)
            where TOneEntity : class, IEntity
            where TProjection : class, IEntity
        {
            Expression<Func<TProjection, bool>> predicate =
                ConstructPredicate<TOneEntity, TManyEntity, TProjection>(oneEntity, options);
            return await _repository
                .Query<TProjection>()
                .Where(predicate)
                .ToArrayAsync();
        }

        protected virtual Expression<Func<TProjection, bool>> ConstructPredicate<TOneEntity, TManyEntity, TProjection>(
            TOneEntity oneEntity, DerivationBusOptions options)
            where TOneEntity : IEntity
            where TProjection : class, IEntity
        {
            var joinExpression = options
                .ProjectionDependencies
                .Get(typeof(TProjection))
                .Single(x => x.DependencyType == typeof(TOneEntity))
                .JoinExpression;
            var dependencyParameter = joinExpression.Parameters[0];
            var projectionParameter = joinExpression.Parameters[1];
            var visitor = new EntityToConstantVisitor(dependencyParameter, oneEntity);
            var projectionJoinExpression = visitor.Modify(joinExpression.Body);

            var manyProjectionMember = GetManyProjectionMember<TManyEntity, TProjection>(options);
            var memberAccessExpression = Expression.MakeMemberAccess(projectionParameter, manyProjectionMember);
            var defaultExpression = Expression.Default(manyProjectionMember.GetMemberType());
            var notNullExpression = Expression.NotEqual(memberAccessExpression, defaultExpression);

            projectionJoinExpression = Expression.AndAlso(projectionJoinExpression, notNullExpression);

            return Expression.Lambda<Func<TProjection, bool>>(projectionJoinExpression, projectionParameter);
        }

        protected virtual MemberInfo GetManyProjectionMember<TManyEntity, TProjection>(DerivationBusOptions options)
            where TProjection : class, IEntity
        {
            var manyEntityMembers = options
                .ProjectionDependencies
                .Get(typeof(TProjection))
                .Single(x => x.DependencyType == typeof(TManyEntity))
                .ProjectionMembers;
            if (manyEntityMembers.Count() != 1)
            {
                throw new ConfigurationException("One and only one projection member is allowed in lambda "
                    + $"between dependency {typeof(TManyEntity)} and projection {typeof(TProjection)}");
            }
            return manyEntityMembers.First();
        }

        protected virtual IEnumerable<Guid> GetCurrentIds(IEntity entity, DerivationBusRule rule)
        {
            var manyIdsFunc = rule.GetManyIdsFunc();
            return manyIdsFunc(entity);
        }

        protected virtual async Task DeleteProjectionsForRemovedIdsAsync<TProjection>(Dictionary<Guid, TProjection> existingProjections,
            IEnumerable<Guid> removedIds) where TProjection : class, IEntity
        {
            foreach (var id in removedIds)
            {
                await _repository.DeleteAsync(existingProjections[id]);
            }
        }

        protected virtual async Task CreateProjectionsForAddedIdsAsync<TOneEntity, TManyEntity, TProjection>(
            DerivationBusContext context, DerivationBusRule rule, IUnitOfWork unitOfWork, IEnumerable<Guid> addedIds)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var addedManyEntities = await _repository
                .Query<TManyEntity>()
                .Where(x => addedIds.Contains(x.Id))
                .ToListAsync();
            await CreateProjectionsAsync<TOneEntity, TManyEntity, TProjection>(
                (TOneEntity)context.Entity, addedManyEntities.Cast<TManyEntity>(), rule, unitOfWork);
        }

        public virtual async Task DeleteProjectionsAsync<TOneEntity, TManyEntity, TProjection>(DerivationBusOptions options,
            DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TOneEntity : class, IEntity
            where TManyEntity : class, IEntity
            where TProjection : class, IEntity
        {
            var oneEntity = (TOneEntity)context.Entity;
            var existingProjections = await LoadExistingProjectionsAsync<TOneEntity, TManyEntity, TProjection>(oneEntity, options);
            foreach (var projection in existingProjections)
            {
                await _repository.DeleteAsync(projection);
            }
        }
    }
}
