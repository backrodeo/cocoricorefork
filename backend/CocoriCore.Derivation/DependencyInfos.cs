using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Derivation
{
    public class DependencyInfo
    {
        public LambdaExpression JoinExpression { get; }
        public string Discriminator { get; }
        private Type _projectionType;
        private Type _dependencyType;
        private MemberInfo[] _dependencyMembers;
        private MemberInfo[] _projectionMembers;

        public DependencyInfo(LambdaExpression joinExpression, string discriminator)
        {
            JoinExpression = joinExpression;
            Discriminator = discriminator;
            InitDependencyAndProjectionMembers();
        }

        public MemberInfo[] ProjectionMembers => _projectionMembers;
        public MemberInfo[] DependencyMembers => _dependencyMembers;
        public Type ProjectionType => _projectionType;
        public Type DependencyType => _dependencyType;

        private void InitDependencyAndProjectionMembers()
        {
            _dependencyType = JoinExpression.Parameters.ElementAt(0).Type;
            _projectionType = JoinExpression.Parameters.ElementAt(1).Type;
            var visitor = new ProjectionMemberVisitor(_projectionType, _dependencyType);
            visitor.Visit(JoinExpression.Body);
            _dependencyMembers = visitor.DependencyMembers.ToArray();
            _projectionMembers = visitor.ProjectionMembers.ToArray();
        }

        public bool DependencyIsDefinedWithForeignKey()
        {
            return DependencyMembers.Length == 1 &&
                DependencyMembers.First().Name == nameof(IEntity.Id) &&
                ProjectionMembers.Length == 1;
        }

        public Guid? GetForeignKey(IEntity projection)
        {
            return ProjectionMembers.First().InvokeGetter<Guid?>(projection);
        }

        public override string ToString()
        {
            return $"({DependencyType.GetPrettyName()}, {ProjectionType.GetPrettyName()}, "
                + $"'{Discriminator}') {JoinExpression.ToString()} ";
        }

        public override bool Equals(object obj)
        {
            return obj is DependencyInfo infos &&
                infos.DependencyType == DependencyType &&
                infos.ProjectionType == ProjectionType &&
                infos.Discriminator == Discriminator;
        }

        public override int GetHashCode()
        {
            return 23 ^
                DependencyType.GetHashCode() ^
                ProjectionType.GetHashCode() ^
                (Discriminator != null ? Discriminator.GetHashCode() : string.Empty.GetHashCode());
        }
    }
}