﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocoriCore.Derivation
{
    public static class DerivationExceptionExtensions
    {
        public static void AddDerivationDatas(this Exception exception, DerivationBusRule rule, DerivationBusContext context)
        {
            if (exception is DerivationException derivationException)
            {
                derivationException.Operation = context.Operation;
                derivationException.EntityType = rule?.TargetType;
                derivationException.EntityId = context?.Entity?.Id;
                //TODO toujours connaitre projectionId
                //derivationException.ProjectionId = projectionId;
                derivationException.ProjectionType = rule.ProjectionType;
                derivationException.SynchronizerType = rule.SynchronizerType;
            }
            exception.Data["Derivation"] = new
            {
                Operation = context.Operation,
                EntityType = rule?.TargetType?.FullName,
                Entity = context?.Entity,
                PreviousEntity = context.State?.PreviousEntity,
                //TODO toujours connaitre projectionId
                //ProjectionId = projectionId,
                ProjectionType = rule.ProjectionType?.FullName,
                SynchronizerType = rule.SynchronizerType?.FullName
            };
        }

        //TODO brancher cette fonction et avoir un type dédié d'exception 
        public static void AddDerivationDatas(this Exception exception, DerivationBusRule rule, DerivationBusContextGroup context)
        {
            exception.Data["Derivation"] = new
            {
                EntityType = rule?.TargetType?.FullName,
                EntityIds = context?.Values != null ? context.Values.Select(x => x?.Entity?.Id).ToArray() : null,
                ProjectionType = rule?.ProjectionType?.FullName,
                SynchronizerType = rule?.SynchronizerType?.FullName
            };
        }
    }
}
