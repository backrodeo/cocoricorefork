using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationErrorHandler
    {
        private IDerivationRepository _repository;

        public DerivationErrorHandler(IDerivationRepository repository)
        {
            _repository = repository;
        }

        public async Task HandleErrorAsync(DerivationException exception)
        {
            var derivationError = new DerivationError
            {
                EntityId = exception.EntityId,
                EntityType = exception.EntityType,
                ExceptionId = exception.GetId(),
                Operation = exception.Operation,
                ProjectionType = exception.ProjectionType,
                ProjectionId = exception.ProjectionId
            };
            await _repository.InsertAsync(derivationError);
        }
    }
}
