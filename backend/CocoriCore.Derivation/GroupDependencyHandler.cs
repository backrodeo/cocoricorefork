using CocoriCore.Linq.Async;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class GroupDependencyHandler : IDerivationHandler
    {
        private IDerivationRepository _repository;

        public GroupDependencyHandler(IDerivationRepository repository)
        {
            _repository = repository;
        }

        public async Task UpdateProjectionAsync<TDependency, TProjection, TSynchronizer>(DerivationBusOptions options,
            DerivationBusRule rule, DerivationBusContextGroup context, IUnitOfWork unitOfWork)
            where TSynchronizer : IGroupSynchronizer<TDependency, TProjection>
            where TDependency : class, IEntity
            where TProjection : class, IEntity
        {
            var synchronizer = unitOfWork.Resolve<TSynchronizer>();
            var dependencyInfos = rule.GetDependencyInfo();
            var dependencyGroupMember = dependencyInfos.DependencyMembers.First();
            var projectionGroupMember = dependencyInfos.ProjectionMembers.First();
            var groupValues = GetGroupValues<TDependency>(context, dependencyGroupMember);

            foreach (var value in groupValues)
            {
                var projectionParameter = Expression.Parameter(typeof(TProjection), "x");
                var memberAccessExpression = Expression.MakeMemberAccess(projectionParameter, projectionGroupMember);
                var constantExpression = Expression.Constant(value);
                var equalsExpression = Expression.Equal(memberAccessExpression, constantExpression);
                var predicate = Expression.Lambda<Func<TProjection, bool>>(equalsExpression, projectionParameter);

                var projections = await _repository.Query<TProjection>().Where(predicate).ToArrayAsync();

                foreach (var projection in projections)
                {
                    await synchronizer.GroupUpdateAsync(value, projection);
                    await _repository.UpdateAsync(projection);
                }
            }
        }

        private static IEnumerable<Guid> GetGroupValues<TDependency>(DerivationBusContextGroup context, MemberInfo groupDependencyMember)
            where TDependency : class, IEntity
        {
            var currentGroupValues = context
                .Values
                .Select(x => groupDependencyMember.InvokeGetter<Guid?>(x.Entity));
            var previousGroupValues = context
                .Values
                .Where(x => x.Operation == CUDOperation.UPDATE || x.Operation == CUDOperation.DELETE)
                .Select(x => x.GetState<TDependency>().PreviousValue<Guid?>(groupDependencyMember));
            var groupValues = currentGroupValues
                .Concat(previousGroupValues)
                .Where(x => x != null && x != Guid.Empty)
                .Distinct()
                .Cast<Guid>();
            return groupValues;
        }
    }
}