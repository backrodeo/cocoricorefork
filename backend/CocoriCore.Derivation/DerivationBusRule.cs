using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DerivationBusRule : BusRule<IDerivationBusContext>
    {
        public Type ProjectionType { get; set; }
        public Type SynchronizerType { get; set; }
        public Func<IUnitOfWork, Task> BeforeProcessRuleAction { get; set; }
        public Func<IUnitOfWork, Task> AfterProcessRuleAction { get; set; }

        public DerivationBusRule()
        {
            AllowMultipleHandler = true;
            BeforeProcessRuleAction = u => Task.CompletedTask;
            AfterProcessRuleAction = u => Task.CompletedTask;
        }

        public override async Task ProcessAsync(IDerivationBusContext context, IUnitOfWork unitOfWork)
        {
            try
            {
                await BeforeProcessRuleAction(unitOfWork);
                await base.ProcessAsync(context, unitOfWork);
                await AfterProcessRuleAction(unitOfWork);
            }
            catch (Exception e)
            {
                var discriminator = this.GetDiscriminator();
                discriminator = string.IsNullOrEmpty(discriminator) ? "empty" : $"'{discriminator}'";
                var errorMessage = $"Error while executing derivation rule from {TargetType?.FullName} "
                    + $"to {ProjectionType?.FullName} with {discriminator} discriminator "
                    + $"using {SynchronizerType?.FullName}, see inner for details.";
                var exception = new DerivationException(errorMessage, e);
                if (context is DerivationBusContext derivationContext)
                {
                    exception.AddDerivationDatas(this, derivationContext);
                }
                else if (context is DerivationBusContextGroup derivationContextGroup)
                {
                    exception.AddDerivationDatas(this, derivationContextGroup);
                }
                throw exception;
            }
        }
    }
}