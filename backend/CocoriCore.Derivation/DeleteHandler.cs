using System;
using System.Threading.Tasks;

namespace CocoriCore.Derivation
{
    public class DeleteHandler : IDerivationHandler
    {
        public async Task ProcessDelete<TSynchronizer, TEntity, TProjection>(
            DerivationBusRule rule, DerivationBusContext context, IUnitOfWork unitOfWork)
            where TSynchronizer : IDeleteSynchronizer<TEntity, TProjection>
            where TEntity : class, IEntity
            where TProjection : class, IEntity
        {
            //TODO ajouter gestion d'erreur ici
            var synchronizer = unitOfWork.Resolve<TSynchronizer>();
            await synchronizer.OnDeleteAsync((TEntity)context.Entity);
        }
    }
}
