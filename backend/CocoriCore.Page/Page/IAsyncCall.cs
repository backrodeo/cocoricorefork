using System;

namespace CocoriCore.Page
{
    public interface IAsyncCall
    {
        void SetResult(object o);
        void SetMemberName(string name);
        string GetMemberName();
        IMessage GetMessage();
        Type GetModelType();
        AsyncCallType GetCallType();
    }
}