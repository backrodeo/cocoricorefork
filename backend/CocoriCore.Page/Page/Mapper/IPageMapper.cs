using System;

namespace CocoriCore.Page
{
    public interface IPageMapper
    {
        object BuildModel(object message, object response, Type modelType);
        object ExecutePageQuery(object message);
    }
}