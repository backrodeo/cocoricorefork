﻿using System;

namespace CocoriCore.Page
{
    public class PageQueryHandling
    {
        public Type PageQueryType;
        public Func<object, object> Func { get; set; }
    }
}