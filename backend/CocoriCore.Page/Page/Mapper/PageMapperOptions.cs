﻿using System.Reflection;

namespace CocoriCore.Page
{
    public class PageMapperOptions
    {
        public PageMapperOptions(params Assembly[] assemblies)
        {
            Assemblies = assemblies;
        }

        public Assembly[] Assemblies { get; }
    }
}