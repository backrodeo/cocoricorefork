using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Page
{

    public class PageMapper : IPageMapper
    {
        Dictionary<Tuple<Type, Type, Type>, CallBuildModelMapping> callMappings;
        Dictionary<Type, PageQueryHandling> pageQueryHandlings;

        public PageMapper(PageMapperOptions configuration)
        {
            var assemblies = configuration.Assemblies;
            var moduleTypes = assemblies.SelectMany(a => a.GetTypes().Where(t => t.IsAssignableTo(typeof(PageModule)))).ToArray();
            var modules = moduleTypes.Select(t => Activator.CreateInstance(t)).Cast<PageModule>().ToList();

            callMappings = modules.SelectMany(m => m.CallMappings).ToDictionary(x => x.Key, x => x);
            pageQueryHandlings = modules.SelectMany(m => m.PageQueryHandlings).ToDictionary(x => x.PageQueryType, x => x);
        }


        public object BuildModel(object o, object p, Type modelType)
        {
            if (p == null)
                p = new object();
            if (p.GetType() == modelType)
                return p;
            CallBuildModelMapping found;
            if (!callMappings.TryGetValue(Tuple.Create(o.GetType(), p.GetType(), modelType), out found))
                throw new Exception(
                      "A Call Mapping has not been found : did you forget to add "
                    + $"ForMessage<{o.GetType().Name}>().WithResponse<{p.GetType().Name}.BuildModel<{modelType.Name}((c, r, m) => {{}});"
                );
            return found.Func(o, p);
        }

        public object ExecutePageQuery(object message)
        {
            PageQueryHandling found;
            if (!this.pageQueryHandlings.TryGetValue(message.GetType(), out found))
                throw new Exception(
                       "A PageQuery Mapping has not been found : did you forget to add "
                    + $"HandlePage<{message.GetType().Name}, {message.GetType().Name.Replace("Query", "")}>();"
                );
            return found.Func(message);
        }
    }
}