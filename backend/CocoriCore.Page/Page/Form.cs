using System.Linq;
using System.Collections.Generic;

namespace CocoriCore.Page
{

    public interface IForm : IAsyncCall
    {
    }

    public class Form<TCommand, TResponse> : AsyncCall<TCommand, TResponse>, IForm
        where TCommand : IMessage, new()
    {
        public Dictionary<string, string> MessageTypeInfo
        {
            get
            {
                return typeof(TCommand).GetPropertiesAndFields()
                      .ToDictionary(x => x.Name, x => x.GetMemberType().GetFriendlyName());
            }
        }



        public Form() : base()
        {
            this.CallType = AsyncCallType.Form;
        }
    }
}