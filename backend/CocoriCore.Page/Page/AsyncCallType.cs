namespace CocoriCore.Page
{
    public enum AsyncCallType
    {
        OnInit,
        Action,
        Server,
        Form
    }
}