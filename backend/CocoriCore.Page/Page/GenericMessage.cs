using System;

namespace CocoriCore.Page
{
    public class GenericMessage : IMessage
    {
        public Type _Type;
        public GenericMessage()
        {
            _Type = this.GetType();
        }
    }
}