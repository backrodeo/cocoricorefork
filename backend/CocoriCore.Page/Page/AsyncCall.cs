using System;

namespace CocoriCore.Page
{


    public class AsyncCall<TMessage, TModel> : GenericMessage, IMessage<TModel>, IAsyncCall where TMessage : IMessage
    {
        public bool IsAsyncCall = true;
        public AsyncCallType CallType;
        public TMessage Message = Activator.CreateInstance<TMessage>();
        public TModel Result;
        public string MemberName;

        public AsyncCall()
        {
            _Type = this.GetType();
        }

        public void SetResult(object o)
        {
            Result = (TModel)o;
        }

        public void SetMemberName(string name)
        {
            MemberName = name;
        }

        public IMessage GetMessage()
        {
            return Message;
        }

        public string GetMemberName()
        {
            return MemberName;
        }

        public AsyncCallType GetCallType()
        {
            return this.CallType;
        }

        public Type GetModelType()
        {
            return typeof(TModel);
        }
    }
}