namespace CocoriCore.Page
{

    public interface IPageQuery : IMessage
    {

    }

    public interface IPageQuery<T> : IMessage<T>, IPageQuery
    {

    }

    public class PageQuery<T> : IPageQuery<T>
    {
        public string HRef;
    }
}