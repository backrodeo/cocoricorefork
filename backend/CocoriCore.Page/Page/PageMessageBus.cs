using System;
using System.Linq;
using System.Threading.Tasks;
using CocoriCore;

namespace CocoriCore.Page
{
    public class PageMessageBus<T> : IMessageBus where T : IMessageBus
    {
        private readonly T innerMessageBus;
        private readonly IPageMapper pageMapper;
        private readonly IAuthenticationController authenticationController;
        private readonly IClaimsProvider claimsProvider;

        public PageMessageBus(
            T innerMessageBus,
            IPageMapper pageMapper,
            IAuthenticationController authenticationController,
            IClaimsProvider claimsProvider)
        {
            this.innerMessageBus = innerMessageBus;
            this.pageMapper = pageMapper;
            this.authenticationController = authenticationController;
            this.claimsProvider = claimsProvider;
        }

        public async Task<object> ExecuteAsync(IMessage message)
        {
            if (message.GetType().IsAssignableTo<IPageQuery>())
            {
                return pageMapper.ExecutePageQuery(message);
            }
            else if (message.GetType().IsAssignableTo<IAsyncCall>())
            {
                var asyncCall = (IAsyncCall)message;
                var innerMessage = asyncCall.GetMessage();

                if (authenticationController.RequireAuthentication(innerMessage.GetType()))
                {
                    //claimsProvider.GetClaims()
                    //var authenticator = unitOfWork.Resolve<IHttpAuthenticator>();
                    //await authenticator.AuthenticateAsync(context);
                }

                var response = await innerMessageBus.ExecuteAsync(asyncCall.GetMessage());
                return pageMapper.BuildModel(
                        asyncCall.GetMessage(),
                        response,
                        asyncCall.GetModelType()
                       );
            }
            else
            {
                return await innerMessageBus.ExecuteAsync(message);
            }
        }
    }
}