﻿using System.Reflection;

namespace CocoriCore.Page
{
    public class EmailMapperOptions
    {
        public EmailMapperOptions(params Assembly[] assemblies)
        {
            Assemblies = assemblies;
        }

        public Assembly[] Assemblies { get; }
    }
}