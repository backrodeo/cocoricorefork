﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CocoriCore.Page
{

    public class EmailMapper : IEmailMapper
    {
        Dictionary<Type, EmailBuildModelMapping> emailMappings;

        public EmailMapper(EmailMapperOptions configuration)
        {
            var assemblies = configuration.Assemblies;
            var moduleTypes = assemblies.SelectMany(a => a.GetTypes().Where(t => t.IsAssignableTo(typeof(EmailModule)))).ToArray();
            var modules = moduleTypes.Select(t => Activator.CreateInstance(t)).Cast<EmailModule>().ToList();

            emailMappings = modules.SelectMany(m => m.EmailMappings).ToDictionary(x => x.EmailType, x => x);
        }

        public object BuildEmailModel(object body)
        {
            var mapping = emailMappings[body.GetType()];
            return mapping.Func(body);
        }
    }
}