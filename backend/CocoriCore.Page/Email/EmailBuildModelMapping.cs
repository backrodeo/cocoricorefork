﻿using System;

namespace CocoriCore.Page
{
    public class EmailBuildModelMapping
    {
        public Type EmailType;
        public Func<object, object> Func { get; set; }
    }
}