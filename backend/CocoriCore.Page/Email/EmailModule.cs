﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace CocoriCore.Page
{
    public class EmailModule
    {
        public List<EmailBuildModelMapping> EmailMappings = new List<EmailBuildModelMapping>();

        public EmailModuleForEmail<T> ForEmail<T>()
        {
            return new EmailModuleForEmail<T>(this);
        }
    }


    public class EmailModuleForEmail<T>
    {
        private readonly EmailModule emailModule;

        public EmailModuleForEmail(EmailModule emailModule)
        {
            this.emailModule = emailModule;
        }

        public EmailModule BuildModel<TModel>(Action<T, TModel> action) where TModel : new()
        {
            var emailMapping = new EmailBuildModelMapping();
            emailMapping.EmailType = typeof(T);
            emailMapping.Func = body =>
            {
                var model = new TModel();
                action((T)body, model);
                return model;
            };
            emailModule.EmailMappings.Add(emailMapping);
            return emailModule;
        }
    }
}