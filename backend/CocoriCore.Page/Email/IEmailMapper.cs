using System;

namespace CocoriCore.Page
{
    public interface IEmailMapper
    {
        object BuildEmailModel(object email);
    }
}