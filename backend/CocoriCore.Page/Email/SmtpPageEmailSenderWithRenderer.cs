﻿using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.Page
{
    public class SmtpPageEmailSenderWithRenderer<TRenderer> : IEmailSender where TRenderer : IEmailRenderer
    {
        private readonly IEmailService emailService;
        private readonly TRenderer renderer;
        private readonly IEmailMapper emailMapper;

        public SmtpPageEmailSenderWithRenderer(
            IEmailService emailService,
            TRenderer renderer,
            IEmailMapper emailMapper)
        {
            this.emailService = emailService;
            this.renderer = renderer;
            this.emailMapper = emailMapper;
        }

        public async Task SendAsync(TypedMailMessage TypedMailMessage)
        {
            var body = emailMapper.BuildEmailModel(TypedMailMessage.Body);
            var renderedMailMessage = await renderer.RenderAsync(body);

            var mailMessage = new MailMessage();
            mailMessage.From = TypedMailMessage.From;
            foreach (var to in TypedMailMessage.To)
                mailMessage.To.Add(to);
            mailMessage.Body = renderedMailMessage.Body;
            mailMessage.Subject = renderedMailMessage.Subject;
            await emailService.SendAsync(mailMessage);
        }
    }

}