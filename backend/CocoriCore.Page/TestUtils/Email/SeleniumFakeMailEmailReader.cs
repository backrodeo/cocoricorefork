﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using CocoriCore;
using CocoriCore.Router;
using CocoriCore.TestUtils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace CocoriCore.Page
{

    public class SeleniumFakeMailEmailReader // : IEmailReader
    {
        public IWebDriver driver;
        private readonly IRouter router;
        private readonly RouterOptions routerOptions;

        public SeleniumFakeMailEmailReader(
            IRouter router,
            RouterOptions routerOptions)
        {
            driver = new FirefoxDriver();
            Thread.Sleep(3000);
            this.router = router;
            this.routerOptions = routerOptions;
        }

        public async Task LogIn(string emailAddress)
        {
            await Task.CompletedTask;
            var url = "https://localhost:5000/fakemail/to/" + emailAddress;
            driver.Navigate().GoToUrl(url);
        }
        public async Task DisplayAsync<TEmail>()
        {
            await Task.CompletedTask;
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var _page = (object)js.ExecuteScript("document.querySelectorAll()");
        }

        public async Task<TMember> DeserializeAsync<TMember>(string memberExpr)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            var id = "Email.Result." + memberExpr;
            var href = (string)js.ExecuteScript($"document.getElementById({id}).getAttribute('href')");
            var httpRequest = new HttpRequestFake();
            var indexQuery = href.IndexOf("?");
            httpRequest.Path = indexQuery == -1 ? href : href.Substring(0, indexQuery);
            httpRequest.QueryString = new Microsoft.AspNetCore.Http.QueryString(indexQuery == -1 ? "" : href.Substring(indexQuery));
            var route = router.TryFindRoute(httpRequest);
            return await router.DeserializeMessageAsync<TMember>(httpRequest, route);
        }

        /*
        public async Task<TEmail> DeserializeAsync<TEmail>()
        {
            var mis = typeof(Email).GetPropertiesAndFields();
            foreach (var mi in mis)
            {
                IJavaScriptExecutor js = driver as IJavaScriptExecutor;
                var id = "Email.Result." + mi.Name;
                if (mi.GetMemberType().IsAssignableTo<IPageQuery>())
                {
                    var href = (string)js.ExecuteScript($"document.getElementById({id}).getAttribute('href')");
                    var httpRequest = new HttpRequestFake();
                    var indexQuery = href.IndexOf("?");
                    httpRequest.Path = indexQuery == -1 ? href : href.Substring(0, indexQuery);
                    httpRequest.QueryString = new Microsoft.AspNetCore.Http.QueryString(indexQuery == -1 ? "" : href.Substring(indexQuery));
                    var route = router.TryFindRoute(httpRequest);
                    router.DeserializeMessageAsync(httpRequest, route, mi.GetMemberType());

                }
                else if (mi.GetMemberType() == typeof(string))
                {
                    var href = (string)js.ExecuteScript($"document.getElementById({id}).innerHtml");
                }

            }
        }
        */
    }
}