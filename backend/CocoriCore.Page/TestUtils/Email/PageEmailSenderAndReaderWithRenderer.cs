﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.Page
{
    public class PageEmailSenderAndReaderWithRenderer<TRenderer> : IEmailSender, IMailMessageReader where TRenderer : IEmailRenderer
    {
        public List<ReceivedMailMessage> MailMessages = new List<ReceivedMailMessage>();
        private readonly TRenderer renderer;
        private readonly IEmailMapper emailMapper;
        private int IndexMessage = 0;

        public PageEmailSenderAndReaderWithRenderer(
            TRenderer renderer,
            IEmailMapper emailMapper)
        {
            this.renderer = renderer;
            this.emailMapper = emailMapper;
        }

        public async Task<ReceivedMailMessage[]> ReadMailMessages()
        {
            await Task.CompletedTask;
            return MailMessages.ToArray();
        }

        public async Task SendAsync(TypedMailMessage TypedMailMessage)
        {
            var body = emailMapper.BuildEmailModel(TypedMailMessage.Body);
            var renderedMailMessage = await renderer.RenderAsync(body);

            var mailMessage = new MailMessage();
            if (TypedMailMessage.From != null)
                mailMessage.From = TypedMailMessage.From;
            foreach (var to in TypedMailMessage.To)
                mailMessage.To.Add(to);
            mailMessage.Body = renderedMailMessage.Body;
            mailMessage.Subject = renderedMailMessage.Subject;
            MailMessages.Add(new ReceivedMailMessage()
            {
                Index = IndexMessage++,
                DateTime = DateTime.Now,
                MailMessage = mailMessage
            });
        }
    }

}