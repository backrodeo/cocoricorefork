using System.Net.Mail;
using System.Threading.Tasks;

namespace CocoriCore.Page
{
    public class GenericEmailRenderer : IEmailRenderer
    {
        private readonly RouteToUrl routeToUrl;

        public GenericEmailRenderer(RouteToUrl routeToUrl)
        {
            this.routeToUrl = routeToUrl;
        }

        public async Task<MailMessage> RenderAsync(object body)
        {
            await Task.CompletedTask;
            var mailMessage = new MailMessage();
            var bodyHtml = "";
            foreach (var member in body.GetType().GetPropertiesAndFields())
            {
                var variableKey = member.Name;
                var variableValue = member.InvokeGetter(body);

                if (variableValue.GetType().IsAssignableTo<IPageQuery>())
                {
                    var href = routeToUrl.ToUrl((IPageQuery)variableValue);
                    href = href.Replace("/pages/", "/");
                    bodyHtml += $"<a href={href}> {variableKey} </a><br/>";
                }
                else
                    bodyHtml += $"{variableKey} : {variableValue.ToString()}<br/>";
            }
            mailMessage.Subject = body.GetType().Name;
            mailMessage.Body = bodyHtml;
            return mailMessage;
        }
    }
}