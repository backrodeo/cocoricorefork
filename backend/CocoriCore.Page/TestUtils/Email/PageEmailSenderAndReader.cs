using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Page
{

    public class PageEmailSenderAndReader : IEmailReader, IEmailSender
    {
        private readonly ICurrentUserLogger logger;
        private readonly IEmailMapper emailMapper;
        public List<TypedMailMessage> ReadMessages = new List<TypedMailMessage>();
        public List<TypedMailMessage> NewMessages = new List<TypedMailMessage>();

        private string emailLogin;
        private object emailDisplayed;

        public PageEmailSenderAndReader(ICurrentUserLogger logger, IEmailMapper emailMapper)
        {
            this.logger = logger;
            this.emailMapper = emailMapper;
        }

        public async Task<int> DisplayAsync<TEmail>()
        {
            await Task.CompletedTask;
            this.emailDisplayed = NewMessages
                                        .Where(x => x.Body.GetType().IsAssignableTo<TEmail>()
                                                 && x.To.Any(y => y.Address == this.emailLogin))
                                        .Last();
            NewMessages = NewMessages.Where(x => x != emailDisplayed).ToList();
            return 0;
        }

        //public async Task<TMember> Get<TEmail, TMember>(Expression<Func<TEmail, TMember>> memberExpression)
        public async Task<TEmail> DeserializeAsync<TEmail>()//, TMember>(Expression<Func<TEmail, TMember>> memberExpression)
        {
            await Task.CompletedTask;
            //return memberExpression.Compile().Invoke((TEmail)this.emailDisplayed);
            return (TEmail)this.emailDisplayed;
        }

        public async Task<int> LogInAsync(string email)
        {
            await Task.CompletedTask;
            this.emailLogin = email;
            return 0;
        }

        public async Task SendAsync(TypedMailMessage mailMessage)
        {
            emailMapper.BuildEmailModel(mailMessage.Body);
            logger.Log(new LogEmailSent() { MailMessage = mailMessage });
            await Task.CompletedTask;
            NewMessages.Add(mailMessage);
        }
    }
}