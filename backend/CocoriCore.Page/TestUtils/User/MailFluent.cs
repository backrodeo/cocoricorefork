using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CocoriCore.Page
{
    public class MailFluent
    {
        private readonly Lazy<UserFluent> userFluent;
        private readonly IEmailReader emailReader;
        private readonly IFactory factory;

        public MailFluent(
            Lazy<UserFluent> userFluent,
            IEmailReader emailReader,
            IFactory factory)
        {
            this.userFluent = userFluent;
            this.emailReader = emailReader;
            this.factory = factory;
        }

        public MailFluentMessage<T> Read<T>(string emailAddress)
        {
            //var mailMessage = (await this.emailReader.ReadAsync<T>(emailAddress)).First();
            return factory.Create<MailFluentMessage<T>>().SetEmailAddress(emailAddress);
        }
    }


    public class MailFluentMessage<TMail>
    {
        private readonly Lazy<UserFluent> userFluent;
        private readonly IEmailReader emailReader;
        private string emailAddress;
        public TypedMailMessage MailMessage;
        public MailFluentMessage(
            Lazy<UserFluent> userFluent,
            IEmailReader emailReader)
        {
            this.userFluent = userFluent;
            this.emailReader = emailReader;
        }

        /*
        public MailFluentMessage<TMail> SetMessage(TypedMailMessage mailMessage)
        {
            this.MailMessage = mailMessage;
            return this;
        }*/

        public MailFluentMessage<TMail> SetEmailAddress(string emailAddress)
        {
            this.emailAddress = emailAddress;
            return this;
        }

        public BrowserFluent<TMessage> Follow<TMessage>(Expression<Func<TMail, IMessage<TMessage>>> expressionPageQuery)
            where TMessage : IPageBase
        {
            int i = emailReader.LogInAsync(this.emailAddress).Result;
            i = emailReader.DisplayAsync<TMessage>().Result;
            var mailBody = emailReader.DeserializeAsync<TMail>().Result;
            var pageQuery = (IMessage<TMessage>)expressionPageQuery.Compile().Invoke((TMail)mailBody);
            return userFluent.Value.Display(pageQuery);

            //var message = (IMessage<TMessage>)expressionLink.Compile().Invoke((TMail)this.MailMessage.Body);
            //return userFluent.Value.Display(message);
        }
    }
}
