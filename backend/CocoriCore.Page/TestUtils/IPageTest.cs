using CocoriCore.Router;

namespace CocoriCore.Page
{
    public interface IPageTest
    {
        void WithSeleniumBrowser(RouterOptions routerOptions);
        object[] GetLogs();
        string GetFilePath();
        int GetLineNumber();
    }

}