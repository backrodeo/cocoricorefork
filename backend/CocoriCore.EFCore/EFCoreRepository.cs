using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.EntityFrameworkCore.Internal;

namespace CocoriCore.EFCore
{
    public class EFCoreRepository : DbContext, IRepository, IReloader, ITransactionHolder, IBufferedRepository
    {
        private static MethodInfo _setMethod = typeof(DbContext).GetMethod(nameof(Set));

        protected IMemoryCache _cache;
        protected IDbContextTransaction _transaction;
        protected Action<ModelBuilder> _defineMappings;
        protected Dictionary<EntityUID, IEntity> _entityCache;
        protected List<Func<IEntity, Task>> EntityInserted;
        protected List<Func<IEntity, Task>> EntityUpdated;
        protected List<Func<IEntity, Task>> EntityDeleted;
        protected List<Func<IEntity, IEntity>> EntityLoaded;
        protected List<Func<Task>> ContextFlushed;
        protected Func<Type, Guid, Task<IEntity>> FindEntityAsync;

        public EFCoreRepository(DbContextOptions options, IMemoryCache cache,
            Action<ModelBuilder> defineMappings = null, bool buffered = false, bool useCocoriCoreCache = false)//TODO utiliser les extensions de DbContextOptions pour ajouter de la conf
            : base(options)
        {
            EntityInserted = new List<Func<IEntity, Task>>();
            EntityUpdated = new List<Func<IEntity, Task>>();
            EntityDeleted = new List<Func<IEntity, Task>>();
            EntityLoaded = new List<Func<IEntity, IEntity>>();
            ContextFlushed = new List<Func<Task>>();
            _cache = cache;
            _defineMappings = defineMappings;
            ConfigureBuffer(buffered);
            ConfigureCache(useCocoriCoreCache);//TODO séparer les fonction de cache des actions pour rendre le context (presque) stateless ? 
            //ca permettrait de bracher le cache (un décorateur) sur n'importe quel impl de IRepository 
        }

        protected virtual void ConfigureBuffer(bool buffered)
        {
            if (buffered)
            {
                ContextFlushed.Add(() => base.SaveChangesAsync());
            }
            else
            {
                EntityInserted.Add(e => base.SaveChangesAsync());
                EntityUpdated.Add(e => base.SaveChangesAsync());
                EntityDeleted.Add(e => base.SaveChangesAsync());
                ContextFlushed.Add(() => base.SaveChangesAsync());
            }
        }

        protected virtual void ConfigureCache(bool useCocoriCoreCache)
        {
            if (useCocoriCoreCache)
            {
                _entityCache = new Dictionary<EntityUID, IEntity>();
                ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                EntityInserted.Add(e => CacheEntityAsync(e));
                EntityUpdated.Add(e => CacheEntityAsync(e));
                EntityLoaded.Add(e => CacheEntityAndDetach(e));
                FindEntityAsync = FindUsingCacheAsync;
                ContextFlushed.Add(() => DetachAllEntitiesAsync());
            }
            else
            {
                FindEntityAsync = DefaultFindAsync;
            }
        }

        public EFCoreRepository()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //décommenter et lancer en débug pour voir les requêtes SQL générées
            //mais ne pas laisser tel quel sinon fuites de mémoire
            //optionsBuilder.UseLoggerFactory(LoggerFactory
            //    .Create(b =>
            //    {
            //        b.AddFilter((c, l) =>
            //            c == DbLoggerCategory.Database.Command.Name &&
            //            l == LogLevel.Debug)
            //        .AddDebug();
            //    }));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            _defineMappings?.Invoke(modelBuilder);
        }

        public virtual async Task<bool> ExistsAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            return await ExistsAsync(typeof(TEntity), id);
        }

        public virtual async Task<bool> ExistsAsync(Type entityType, Guid id)
        {
            return await FindAsync(entityType, id) != null;
        }

        protected virtual async Task<object> FindAsync(Type entityType, Guid id)
        {
            var entity = await FindEntityAsync(entityType, id);
            if (entity != null)
            {
                foreach (var action in EntityLoaded)
                {
                    entity = action(entity);
                }
            }
            return entity;
        }

        protected virtual async Task<IEntity> DefaultFindAsync(Type entityType, Guid id)
        {
            IEntity entity = null;
            if (!entityType.IsConcrete())
            {
                foreach (var type in GetConcreteTypes(entityType))
                {
                    entity = (IEntity)await base.FindAsync(type, id);
                    if (entity != null)
                    {
                        break;
                    }
                }
            }
            else
            {
                entity = (IEntity)await base.FindAsync(entityType, id);
            }
            return entity;
        }

        protected virtual async Task<IEntity> FindUsingCacheAsync(Type entityType, Guid id)
        {
            var uid = new EntityUID(entityType, id);
            var entity = _entityCache.TryGetValue(uid, null);
            if (entity == null)
            {
                entity = await DefaultFindAsync(entityType, id);
            }
            return entity;
        }

        protected virtual IEnumerable<Type> GetConcreteTypes(Type entityType)
        {
            Type[] concreteTypes = null;
            if (!_cache.TryGetValue(entityType, out concreteTypes))
            {
                if (entityType.IsInterface)
                {
                    concreteTypes = Model
                        .GetEntityTypes()
                        .Select(x => x.ClrType)
                        .Where(x => x.IsConcrete() && x.IsAssignableTo(entityType))
                        .ToArray();
                }
                else
                {
                    concreteTypes = new[] { entityType };
                }
                _cache.Set(entityType, concreteTypes);
                return concreteTypes;
            }
            else
            {
                return concreteTypes;
            }
        }

        public virtual async Task<bool> ExistsAsync(Type entityType, MemberInfo memberInfo, object value)
        {
            var predicate = ConstructEqualsExpression<IEntity>(entityType, memberInfo, value);
            var queryable = Query<IEntity>(entityType);
            queryable = queryable.Where(predicate);
            var result = await CocoriCore.Linq.Async.AsyncQueryableExtensions.ToArrayAsync(queryable);
            return result.Any();
        }

        protected virtual Expression<Func<T, bool>> ConstructEqualsExpression<T>(Type objectType, MemberInfo objectMember, object valueToCompare)
        {
            var memberType = objectMember.GetMemberType();
            var parameter = Expression.Parameter(typeof(T), "x");
            Expression entityParameter = parameter;
            if (typeof(T) != objectType)
            {
                entityParameter = Expression.Convert(parameter, objectType);
            }
            Expression memberAccess = Expression.MakeMemberAccess(entityParameter, objectMember);
            Expression constant = Expression.Constant(valueToCompare);
            if (memberType != constant.Type)
            {
                constant = Expression.Convert(constant, memberType);
            }
            var equals = Expression.Equal(memberAccess, constant);
            return Expression.Lambda<Func<T, bool>>(equals, parameter);
        }

        public virtual async Task<bool> ExistsAsync(Type entityType, IEnumerable<Guid> idCollection)
        {
            idCollection = idCollection.Distinct();
            var queryable = Query<IEntity>(entityType).Where(x => idCollection.Contains(x.Id));
            var result = await CocoriCore.Linq.Async.AsyncQueryableExtensions.ToArrayAsync(queryable);
            return idCollection.Count() == result.Length;
        }

        public virtual async Task<TEntity> LoadAsync<TEntity>(Guid id) where TEntity : class, IEntity
        {
            var result = (TEntity)await FindAsync(typeof(TEntity), id);
            if (result == null)
            {
                throw new InvalidOperationException($"There is no entity of type {typeof(TEntity)} with id {id}.");
            }
            else
            {
                return result;
            }
        }

        public virtual async Task<object> LoadAsync(Type type, Guid id)
        {
            var result = await FindAsync(type, id);
            if (result == null)
            {
                throw new InvalidOperationException($"There is no entity of type {type} with id {id}.");
            }
            return result;
        }

        public virtual async Task<TEntity> LoadAsync<TEntity>(Expression<Func<TEntity, object>> uniqueMember, object value)
            where TEntity : class, IEntity
        {
            //TODO mettre en cache les entités que l'on charge par membre unique ou dont on vérifier l'existence de la même manière
            //il faut que le validateur et le chargement ne fassent qu'un seul appel BDD
            var predicate = ConstructEqualsExpression<TEntity>(typeof(TEntity), uniqueMember.GetMemberInfo(), value);
            var query = Query<TEntity>().Where(predicate);
            var results = await CocoriCore.Linq.Async.AsyncQueryableExtensions.ToArrayAsync(query);//TODO mettre en cache ici
            if (results.Length == 0)
                throw new InvalidOperationException($"There is no entity of type {typeof(TEntity)} with value '{value}' "
                    + $"for unique field {uniqueMember.GetMemberInfo().Name}.");
            else if (results.Length > 1)
                throw new InvalidOperationException($"There are several entities of type {typeof(TEntity)} with value '{value}' "
                    + $"for unique field {uniqueMember.GetMemberInfo().Name}, "
                    + $"use {nameof(Query)}() method instead.");
            else
                return results.First(); ;
        }

        //TODO erreur si nb chargé différent du nombre d'id après distinct
        public virtual async Task<IEntity[]> LoadAsync(Type entityType, IEnumerable<Guid> idCollection)
        {
            idCollection = idCollection.Distinct();
            var queryable = Query<IEntity>(entityType).Where(x => idCollection.Contains(x.Id));
            return await CocoriCore.Linq.Async.AsyncQueryableExtensions.ToArrayAsync(queryable);
        }

        public virtual async Task<TEntity> ReloadAsync<TEntity>(Guid id)
        where TEntity : class, IEntity
        {
            var entity = (TEntity)await FindAsync(typeof(TEntity), id);
            return await ReloadAsync(entity);
        }

        public virtual async Task<TEntity> ReloadAsync<TEntity>(TEntity entity)
            where TEntity : class, IEntity
        {
            var entry = ChangeTracker
                .Entries()
                .SingleOrDefault(e =>
                    e.Entity is TEntity trackedEntity &&
                    trackedEntity.Id == entity.Id);
            if (entry == null)
            {
                entry = base.Entry(entity);
            }
            await entry.ReloadAsync();
            return (TEntity)entry.Entity;
        }

        public virtual new IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return Query<TEntity>(typeof(TEntity));
        }

        public virtual IQueryable<IEntity> Query(Type type)
        {
            return Query<IEntity>(type);
        }

        public virtual IQueryable<TEntity> Query<TEntity>(Type entityType)
            where TEntity : class, IEntity
        {
            var queryable = DefaultQueryAsync<TEntity>(entityType);
            return new ExtendedEnumerable<TEntity>(queryable, e =>
            {
                foreach (var action in EntityLoaded)
                {
                    e = (TEntity)action(e);
                }
                return e;
            }, "entity_loaded");
        }

        protected virtual IQueryable<TEntity> DefaultQueryAsync<TEntity>(Type entityType)
        {
            IQueryable<TEntity> queryable = null;
            if (!entityType.IsConcrete())
            {
                var sets = GetConcreteTypes(entityType)
                    .Select(x => Set<TEntity>(x))
                    .ToArray();
                queryable = new MultiSourceQueryProvider<TEntity>(sets);
            }
            else
            {
                queryable = Set<TEntity>(entityType);
            }
            return queryable;
        }

        protected virtual IQueryable<TEntity> Set<TEntity>(Type entityType)
        {
            return (IQueryable<TEntity>)_setMethod
                .MakeGenericMethod(entityType)
                .Invoke(this, new object[0]);
        }

        public virtual async Task InsertAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _transaction = _transaction ?? await Database.BeginTransactionAsync();
            Add(entity);
            foreach (var action in EntityInserted)
            {
                await action(entity);
            }
        }

        public virtual async Task UpdateAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _transaction = _transaction ?? await Database.BeginTransactionAsync();
            Update(entity);
            foreach (var action in EntityUpdated)
            {
                await action(entity);
            }
        }

        public virtual async Task DeleteAsync<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            _transaction = _transaction ?? await Database.BeginTransactionAsync();
            Remove(entity);
            foreach (var action in EntityDeleted)
            {
                await action(entity);
            }
        }

        public virtual async Task CommitAsync()
        {
            await FlushAsync();
            if (_transaction != null)
            {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        public virtual Task RollbackAsync()
        {
            DetachAllEntitiesAsync();
            if (_transaction != null)
            {
                _transaction.Rollback();
                _transaction.Dispose();
                _transaction = null;
            }
            return Task.CompletedTask;
        }

        public virtual async Task FlushAsync()
        {
            foreach (var action in ContextFlushed)
            {
                await action();
            }
        }

        protected virtual Task CacheEntityAsync(IEntity entity)
        {
            var uid = new EntityUID(entity.GetType(), entity.Id);
            _entityCache[uid] = entity;
            return Task.CompletedTask;
        }

        protected virtual IEntity CacheEntityAndDetach(IEntity entity)
        {
            var uid = new EntityUID(entity.GetType(), entity.Id);
            _entityCache[uid] = entity;
            base.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        protected virtual Task DetachAllEntitiesAsync()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                if (entry.Entity != null)
                {
                    entry.State = EntityState.Detached;
                }
            }
            return Task.CompletedTask;
        }
        public override void Dispose()
        {
            _transaction?.Dispose();
            base.Dispose();
        }
    }
}