﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public interface IDatabaseContext : IDisposable
    {
        void Delete();
        Task DeleteAsync();
        void Migrate();
        Task MigrateAsync();
    }
}
