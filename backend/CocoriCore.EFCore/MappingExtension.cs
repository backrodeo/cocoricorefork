using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CocoriCore.EFCore
{
    public static class MappingExtension
    {
        public static PropertyBuilder<TProperty> ToJson<TProperty>(this PropertyBuilder<TProperty> propertyBuilder)
        {
            var settings = new JsonSerializerSettings();
            return propertyBuilder.HasConversion(
                v => JsonConvert.SerializeObject(v, settings),
                v => (TProperty)JsonConvert.DeserializeObject(v, typeof(TProperty), settings));
        }
    }
}