﻿using System.Reflection;

namespace CocoriCore.EFCore
{
    public static class AssemblyInfo
    {
        public static Assembly Assembly => typeof(AssemblyInfo).Assembly;
    }
}
