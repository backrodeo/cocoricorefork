﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CocoriCore.EFCore
{
    public abstract class DatabaseContext : EFCoreRepository, IDatabaseContext
    {
        protected string _connectionString;
        protected bool _allowDropDatabase;

        public DatabaseContext()
        {
        }

        public DatabaseContext(string connectionString = "dummy_for_migration", bool allowDropDatabase = false)
        {
            _connectionString = connectionString;
            _allowDropDatabase = allowDropDatabase;
        }

        public virtual void Delete()
        {
            if (_allowDropDatabase)
            {
                Database.EnsureDeleted();
            }
        }

        public virtual async Task DeleteAsync()
        {
            if (_allowDropDatabase)
            {
                await Database.EnsureDeletedAsync();
            }
        }

        public virtual void Migrate()
        {
            Database.Migrate();
        }

        public virtual Task MigrateAsync()
        {
            return Database.MigrateAsync();
        }
    }
}
