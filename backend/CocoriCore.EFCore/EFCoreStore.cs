using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace CocoriCore.EFCore
{
    public class EFCoreStore : IPersistentStore
    {
        public DbContextOptions Options { get; }
        private Func<EFCoreRepository> _repositoryFactory;
        private DatabaseFacade _database;
        private Action<DatabaseFacade> _clearAction;

        public EFCoreStore(DbContextOptions options, Func<EFCoreRepository> repositoryFactory, Action<DatabaseFacade> clearAction)
        {
            Options = options;
            _repositoryFactory = repositoryFactory;
            _database = _repositoryFactory().Database;
            _database.EnsureCreated();
            _clearAction = clearAction;
        }

        public virtual EFCoreRepository CreateRepository()
        {
            return _repositoryFactory();
        }

        public virtual void Clear()
        {
            _clearAction(_database);
        }
    }
}