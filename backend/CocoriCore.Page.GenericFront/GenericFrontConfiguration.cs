﻿
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;

namespace CocoriCore.Page.GenericFront
{

    public static class GenericFrontConfiguration
    {

        public static void UseGenericFront(this IApplicationBuilder builder, GenericFrontOptions options)
        {
            if (options.UseManifest)
            {
                builder.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = GetManifestEmbeddedFileProvider()
                });
            }
            else
            {
                builder.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = GetPhysicalFileProvider()
                });
            }

            builder.Use(async (ctx, next) =>
            {
                if (ctx.Request.Path.Value.StartsWith("/pages"))
                {
                    await next();
                    return;
                }

                ctx.Response.ContentType = "text/html";

                string content;
                if (options.UseManifest)
                {
                    var assembly = typeof(GenericFrontConfiguration).Assembly;
                    content = Encoding.Default.GetString(
                            assembly
                            .GetManifestResourceStream("CocoriCore.Page.GenericFront.wwwroot.page.html")
                            .ReadAllBytes()
                        );
                }
                else
                {
                    var filename = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..", "CocoriCore.Page.GenericFront", "wwwroot", "page.html");
                    content = File.ReadAllText(filename);
                }

                await ctx.Response.WriteAsync(content);
            });
        }

        public static ManifestEmbeddedFileProvider GetManifestEmbeddedFileProvider()
        {
            return new ManifestEmbeddedFileProvider(typeof(GenericFrontConfiguration).Assembly, "wwwroot");
        }

        public static PhysicalFileProvider GetPhysicalFileProvider()
        {
            return new PhysicalFileProvider(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "..", "CocoriCore.Page.GenericFront", "wwwroot"));
        }

    }
}