# Ajouter le middleware GenericFront dans Startup.cs

``` csharp
app.UseGenericFront(new GenericFrontOptions() { UseManifest = false });
```

Par exemple à coté de la ligne :
``` csharp
_appBuilderAction.ForEach(action => action(app, _rootScope));
```