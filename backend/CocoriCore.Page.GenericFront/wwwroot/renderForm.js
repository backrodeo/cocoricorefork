function renderForm2(form, id, r) {

    r.afterRender(() => document.getElementById(id).addEventListener('submit', (evt) => {
        evt.preventDefault();

        r.applyInputsToModel();
        r.applyOnSubmits();
        console.log('submit ' + id);
        formCall(form);
        return false;
    }));

    let messageTypeInfo = getValue(form, 'MessageTypeInfo')
    let inputFields = getInputsToBeRendered(r.page, messageTypeInfo, id + '.Message');
    console.log('InputsToBeRendered : \n  ' + inputFields.join('\n  '));

    let html = '';
    for (let idField of inputFields) {
        let fieldValue = getValue(r.page, idField);
        let input2 = createInput(form, id, fieldValue, idField, r);
        html += input2.render(fieldValue, idField, r);
        r.afterRender(() => {
            input2.updateFromModel()
        });
    }

    return `<form id="${id}">
                ${html}
                <button type="submit" class="btn btn-primary">${field(id)}</button>
            </form>`;
}

function getInputsToBeRendered(page, messageTypeInfo, idCommand) {

    let idInputs = [];
    for (let k of Object.keys(messageTypeInfo)) {

        let commandField = idCommand + '.' + k;

        let foundOnSubmit = false;
        for (let onSubmit of getValue(page, 'OnSubmits')) {
            let from = getValue(onSubmit, 'From');
            let to = getValue(onSubmit, 'To');
            let onSubmitCommandField = to;
            if (onSubmitCommandField == commandField) {
                idInputs.push(from.split('.')[0]);
                foundOnSubmit = true;
            }
        }
        if (!foundOnSubmit) {
            idInputs.push(commandField);
        }
    }
    return idInputs;
}

function createInput(form, idForm, value, idField, r) {

    let commandFieldTypeNames = getValue(form, 'MessageTypeInfo')
    let typeName = null;
    let idMessage = idForm + '.Message';
    if (idField.startsWith(idMessage)) {
        let fieldName = field(idField);//.split('.')[idField.split('.').length - 1];
        typeName = getValue(commandFieldTypeNames, fieldName);
    }

    var input = selectInput(value, typeName);

    r.inputs.push(input);
    return input;
}

function selectInput(x, typeName) {
    if (typeName != null) {
        if (typeName == 'string')
            return new GenericInput('');
        if (typeName == 'double')
            return new GenericInput('number');
        if (typeName == 'DateTime')
            return new GenericInput('date');
        //if (typeName.startsWith('ID<'))
        //    return new GenericInput();
    }
    if (x != undefined && valueExists(x, "IsSelect"))
        return new SelectInput();

    return new GenericInput();
}