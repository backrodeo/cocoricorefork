
class GenericInput {
    constructor(inputType) {
        this.inputType = inputType;
        this.id = '';
        this.page = null;
    }
    render(x, id, r) {
        let f = field(id);
        this.id = id;
        this.page = r.page;

        //r.afterRender(() => {
        //    document.getElementById(id).addEventListener('change', () => this.updateModel());
        //});

        return `<div class="form-group">
                    <label for="${id}">${f}</label>
                    <input class="form-control" type="${this.inputType}" id="${id}"></input>
                </div>`;
    }

    updateFromModel() {
        if (!valueExists(this.page, this.id))
            return;
        let value = getValue(this.page, this.id);
        if (this.inputType == 'date')
            value = value.substring(0, value.indexOf('T'));
        document.getElementById(this.id).value = value;
    }

    updateModel() {
        let value = document.getElementById(this.id).value;
        setValue(this.page, this.id, value);
    }

    onPageUpdate(id) {
        if (id == this.id) {
            this.updateFromModel();
        }
    }
}

class SelectInput {
    constructor() {
        this.id = '';
        this.page = null;
    }

    render(x, id, r) {
        let f = field(id);
        this.page = r.page;
        this.id = id;

        r.afterRender(async () => {

            //document.getElementById(id).addEventListener('change', () => this.updateModel());

            let response = await call(getValue(x, 'Source'));
            x.Source.Result = response;
            this.updateFromModel();
        });
        return `<div class="form-group">
            <label for="${id}">${f}</label>
            <select class="form-control" id="${id}">
            </select>
        </div>`;
    }

    updateFromModel() {
        let select = getValue(this.page, this.id);
        let selectElt = document.getElementById(this.id);
        let sourceResult = getValue(select, 'Source.Result');
        if (sourceResult != null) {
            let v = getValue(x, 'Value');
            let l = getValue(x, 'Label');
            selectElt.innerHTML = sourceResult.map(x => `<option value="${v}">${l}</option>`).join('');
        }


        let selected = getValue(select, 'Selected');
        if (selected != null) {
            let selectedValue = getValue(selected, 'Value');
            for (let i = 0; i < selectElt.options.length; i++) {
                let option = selectElt.options[i];
                if (option.value == selectedValue) {
                    selectElt.selectedIndex = i;
                }
            }
        }
    }

    updateModel() {
        let selectElt = document.getElementById(this.id);
        //let selectedIndex = selectElt.selectedIndex;
        let selectedValue = selectElt.selectedOptions[0].value;

        let selectModel = getValue(this.page, this.id);
        let selected = {};
        setValue(selected, 'Value', selectedValue);
        setValue(selectModel, 'Selected', selected);
    }

    onPageUpdate(id) {
        if (id.startsWith(this.id))
            this.updateFromModel();
    }
}