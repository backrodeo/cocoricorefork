var _isCamelCase = false;
function detectCamelCase(page) {
    if (page["PageTypeName"] == undefined)
        isCamelCase = true;
}

function toPascalCase(x) {
    if (x == null)
        return null;
    if (Array.isArray(x))
        return x.map(elt => toPascalCase(elt))
    if (x === Object(x)) {
        let y = {};
        for (let k of Object.keys(x)) {
            let K = k.substring(0, 1).toUpperCase() + k.substring(1);
            y[K] = toPascalCase(x[k]);
        }
        return y;
    }
    return x;
}

function toCamelCase(s) {
    let result = s.substring(0, 1).toLowerCase() + s.substring(1);
    return result;
}

function field(id) {
    let split = id.split('.');
    let f = split[split.length - 1];
    return f;
}

function pathWithoutPage(id) {
    let split = id.split('.');
    return split.slice(1).join('.');
}

/*
function getPageName(id) {
    let split = id.split('.');
    return split[0];
}
*/

function valueExists(x, expr) {
    let members = expr.split('.');
    let y = x;
    for (let i = 0; i < members.length; ++i) {
        y = y[members[i]];// || y[toCamelCase(members[i])];
        if (y == null)
            return false;
    }
    return y != null;
}

function getValue(x, expr) {
    let members = expr.split('.');
    //console.log(' get ' + members.join('.'));
    let y = x;
    for (let i = 0; i < members.length; ++i)
        y = y[members[i]];// || y[toCamelCase(members[i])];
    return y;
}

function setValue(x, expr, value) {
    let members = expr.split('.');
    //console.log(' set ' + members.join('.') + ' = ' + value);
    let y = x;
    for (let i = 0; i < members.length - 1; ++i)
        y = y[members[i]];// || y[toCamelCase(members[i])];
    let lastMember = members[members.length - 1];
    //if (_isCamelCase)
    //    lastMember = toCamelCase(lastMember);
    y[lastMember] = value;
}


function documentGetElementById() {

}