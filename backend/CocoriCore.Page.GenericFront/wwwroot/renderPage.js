


function renderPage(page, id, r) {

    let pageTypeName = getValue(page, "PageTypeName");
    pageTypeName = pageTypeName.substring(pageTypeName.lastIndexOf('.') + 1);


    let htmlOnInits = getValue(page, 'OnInits').map(b => " - " + getValue(b, 'From') + ' =&gt; ' + getValue(b, 'To')).join('<br/>');
    let htmlOnSubmits = getValue(page, 'OnSubmits').map(b => " - " + getValue(b, 'From') + ' =&gt; ' + getValue(b, 'To')).join('<br/>');


    if (htmlOnInits.length > 0)
        htmlOnInits = "OnInits :<br/>" + htmlOnInits + "<br/>";
    if (htmlOnSubmits.length > 0)
        htmlOnSubmits = "OnSubmits :<br/>" + htmlOnSubmits + "<br/>";

    let page2 = constructPageToRender(page);

    let menu = getValue(page, 'Menu');
    let menuFieldName = 'Menu';
    return `${renderMenu(page['Menu'], menuFieldName, r)}
            <div class="container" style="margin-top: 90px">
                <b>${pageTypeName}</b>
                <ul>
                    ${renderObjectAsList(page2, '', r)}
                    
                </ul>
                <div style="font-size: 12px">${htmlOnInits + htmlOnSubmits}</div>
            </div>`;
}
//${Object.keys(page2).map(k => `<li>${k} : ${r.render(page2[k], k)}</li>`).join('')}

function constructPageToRender(page) {
    console.log('fields to render :');
    let inputs = getValue(page, 'OnSubmits').map(x => getValue(x, 'From')[0]);
    let page2 = {};
    for (let k of Object.keys(page)) {
        if (k == 'PageQuery' || k == 'OnSubmits' || k == 'OnInits' || k == 'PageTypeName' || k == 'RenderInfos'
            || k == 'pageQuery' || k == 'onSubmits' || k == 'onInits' || k == 'pageTypeName' || k == 'renderInfos')
            continue;
        if (k == 'Menu'
            || k == 'menu')
            continue;
        if (-1 != inputs.indexOf(k)) {
            continue;
        }
        console.log('  ' + k);
        page2[k] = page[k];
    }
    return page2;
}
